package APMS::View::Drill::Invoice;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Panel;
use APMS::View::Browser;
use APMS::View::Buttons;

use base qw/ APMS::View::Drill /;

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	# Create the panel
	$self->{panel} = APMS::View::Panel->radio_choose(
		{
			name     => 'Filter By',
			options  => [ 'Unapproved', 'Approved', 'All' ]
		},
	);

	$self->{browser} = APMS::View::Browser->browse_window(
			fields => [
			{
				name   => 'S',
				column => 'invoicestatus',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
			}, {
				name   => 'Invoice',
				column => 'invoiceno',
				type   => 'Glib::Uint',
				widget => 'Gtk2::CellRendererText',
				dosort => 1
			}, {
				name   => 'Date',
				column => 'companycode',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				dosort => 1
			}, {
				name   => 'Entity',
				column => 'entitycode',
				type   => 'Glib::Uint',
				widget => 'Gtk2::CellRendererText',
				dosort => 1
			}, {
				name   => 'Entity Name',
				column => 'entitytype',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				dosort => 1
			}, {
				name   => 'Total',
				column => 'topay',
				type   => 'Glib::Uint',
				widget => 'Gtk2::CellRendererText'
			}, {
				name   => 'To',
				column => 'todetail',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				expand => 1,
				minw   => 100
			}
		],
		data => $data{invoicelist}
	);

	$self->{buttons} = APMS::View::Buttons->button_pack( 'H', $data{buttons} );

	$self->drill_window(
		title     => $data{title},
		panel     => $self->{panel},
		browser   => $self->{browser},
		buttons   => $self->{buttons}
	);

	return $self;
}

=item selected_invoice

Callback for when the user double clicks a row

=cut

sub selected_invoiceseq {
	my $self = shift;

	my $iter = 0;
	my $selection = $self->{browser}->get_selection();
	my ( $model, $iter ) = $selection->get_selected();
	return 0 unless $model;
	return $model->get_value( $iter, 1 );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

