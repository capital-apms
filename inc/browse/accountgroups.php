<?php

include_once("classEditor.php");

$editrow = new Editor("Account Group", "accountgroup" );
$editrow->SetSubmitName( 'saverow' );
if ( $editrow->IsSubmit() ) {
  $editrow->SetWhere( "accountgroupcode=".qpg($_POST['editgroup']));
  $editrow->Write();
  if ( $_POST['accountgroupcode'] != $_POST['editgroup'] ) {
    $qry = new PgQuery("UPDATE chartofaccount SET accountgroupcode=? WHERE accountgroupcode = ?", $_POST['accountgroupcode'], $_POST['editgroup'] );
    $qry->Exec('accountgroups');
    $editrow->GetRecord( "accountgroupcode=".qpg($_POST['accountgroupcode']));
  }
  unset($_GET['editgroup']);
}


function edit_row( $row_data ) {
  global $editrow;

  $rowsubmittype = ($row_data->accountgroupcode == '' ? 'Add' : 'Save');
  $form_url = preg_replace( '#&(edit|del)seq=\d+#', '', $_SERVER['REQUEST_URI'] );

  $template = <<<EOTEMPLATE
  <td class="left">##accountgroupcode.input.8##<input type="hidden" name="id" value="##accountgroupcode.value##"><input type="hidden" name="editgroup" value="##accountgroupcode.value##"></td>
  <td class="left">##name.input.30##</td>
  <td class="left">##sequencecode.input.4##</td>
  <td class="left">##creditgroup.checkbox##</td>
  <td class="left">##grouptype.input.1##</td>
  <td class="left">##$rowsubmittype.submit##</td>

EOTEMPLATE;

  $editrow->Layout( $template );
  $editrow->Title("");
  if ( $row_data->accountgroupcode == '' ) {
    $editrow->Initialise( (array) $row_data );
  }
  else
    $editrow->SetRecord( $row_data );

  return $editrow->Render();
}


$browser = new Browser("Accounts");
$browser->AddColumn( 'accountgroupcode', 'A/C Group' );
$browser->AddColumn( 'name', 'Name' );
$browser->AddColumn( 'sequencecode', 'Seq' );
$browser->AddColumn( 'creditgroup', 'Cr?' );
$browser->AddColumn( 'grouptype', 'Type' );
$browser->SetJoins( 'accountgroup' );
if ( isset($_GET['editgroup']) ) {
  $browser->RowFormat( "<tr class=\"r%d\">\n", "</tr>\n", '#even' );
}
else {
  $rowurl = '/browse.php?t=chartofaccounts&accountgroup=%s';
  $browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'accountgroupcode', '#even' );
}

$edit_link = "<a href=\"/browse.php?t=accountgroups&editgroup=##accountgroupcode##\" class=\"submit\">Edit</a>";
$del_link  = "<a href=\"/browse.php?t=accountgroups&delgroup=##accountgroupcode##\" class=\"submit\">Del</a>";
$browser->AddColumn( 'action', 'Action', 'center', '', "'$edit_link&nbsp;$del_link'" );

$browser->SetOrdering( 'sequencecode' );

@include_once("menus_accounts.php");

if ( isset($_GET['delgroup']) ) {
  $qry = new PgQuery("DELETE FROM accountgroup WHERE accountgroupcode=? AND NOT EXISTS(SELECT 1 FROM chartofaccount WHERE chartofaccount.accountgroupcode = accountgroup.accountgroupcode)", $_GET['delgroup'] );
  $qry->Exec('accountgroups');
}
if ( isset($_GET['editgroup']) ) {
  $browser->MatchedRow('accountgroupcode', $_GET['editgroup'], 'edit_row');
}
else {
  $extra_row = array( 'accountgroupcode' => '' );
  $browser->MatchedRow('accountgroupcode', '', 'edit_row');
  $extra_row = (object) $extra_row;
  $browser->AddRow($extra_row);
}

$page_elements[] = $browser;
