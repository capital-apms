package APMS::Control::ButtonsWindow;

=item APMS::Control::ButtonsWindow

Generic controller functions for windows with DB buttons.

=cut

use strict;

=item button_callbacks

The controller fetches the buttons from the DB and passes that data
to the view, but the view needs to have each button call a controller
callback. That callback must have access to the APMS->run method, which
is only available to the controller. This ensures that view callbacks
go via the controller, and the views remain solely focussed on the
presentation of the data, and do not become part of the controllers.

The $self object is in the drill controller class, but crucially contains
the APMS->run object+method.

=cut

sub button_callbacks {
	my ( $self, $view ) = @_;

	foreach my $button ( $view->buttons ) {
		# Map the Progress procedure name to a Perl class name
		my $class = $self->{apms}->model->mapper( $button->{viewer} );

		# Turn the "function" database column into a hash
		my $args = $self->function_to_hash( $button->{function} );

		# When the callback is done, pass a reference to a place where we can
		# get the selected record from
		$args->{recordid} = sub { return $self->recordid };

		if ( $class ) {
			if ( $button->{linktype} =~ /DRL/i ) {
				$view->attach_button_callback( $button,
					sub {
						$self->{apms}->run(
							class      => $class,
							parentview => $view,
							nodecode   => $button->{target},
							args       => $args
						);
					}
				);
			}
			else {
				$view->attach_button_callback( $button,
					sub {
						$self->{apms}->run(
							class      => $class,
							parentview => $view,
							args       => $args
						);
					}
				);
			}
		}
		else {
			if ( $button->{linktype} =~ /MNU/i ) {
				$view->attach_button_callback( $button,
					sub {
						$self->{apms}->runmenu(
							parentview => $view,
							nodecode   => $button->{target}
						);
					}
				);
			}
			else {
				$view->set_bg_dark( $button );
				#$view->disable_button( $button );
			}
		}

		# Create a tooltip for the button with the viewer name (debugging)
		$view->set_tooltip( $button, $button->{viewer} );
		#$view->set_tooltip( $button, );
	}
}

sub function_to_hash {
	my ( $self, $string ) = @_;

	my $hash;
	my @options = split /\s*\,\s*/, $string;
	foreach my $option ( @options ) {
		my ( $key, $value ) = split /\s*\=\s*/, $option;
		$hash->{lc($key)} = lc($value);
	}

	return $hash;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

