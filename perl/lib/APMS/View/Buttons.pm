package APMS::View::Buttons;

=head1 APMS::UI::Buttons

Manage the button boxes in APMS

=cut

use strict;
use APMS::Class qw/ new_object /;

=item button_pack

Given a list of buttons, pack them into either a vertical or horizontal box and
return the box widget.

  use base "APMS::UI::Buttons";
  ..
  $self->button_pack( "[V|H]", @button_list );

Where $self must contain the root apms object in $self->{apms} (for callbacks),
and @button_list is a list of hashrefs, taken from the DB query
APMS::DB::Buttons->buttons.

=cut

sub button_pack {
	my ( $proto, $orientation, $buttons ) = @_;
	my $self = new_object( $proto );

	# If this is to be a horizontally stacked list of buttons
	$self->{box} = Gtk2::VBox->new( 0, 0 );
	my $boxes = [];

	if ( $orientation eq 'H' ) {
		# Create the containers, The HBox is a list
		push @{ $boxes }, Gtk2::HBox->new( 0, 0 );
	}

	$self->{buttons} = [];

	# Generate the GTK buttons
	my $count = 0;
	foreach my $buttondef ( @{ $buttons } ) {

		# Create the button
		my $button = Gtk2::Button->new( $buttondef->{buttonlabel} );
		my $tooltip = Gtk2::Tooltips->new();

		# This info is used later to assign the button callbacks
		push @{ $self->{buttons} }, {
			widget    => $button,
			tooltip   => $tooltip,
			viewer    => $buttondef->{viewer},
			target    => $buttondef->{target},
			function  => $buttondef->{function},
			linktype  => $buttondef->{linktype}
		};

		if ( $orientation eq 'H' ) {
			# Add a new HBox every 6 buttons
			unless ( $count % 7 ) {
				push @{ $boxes }, Gtk2::HBox->new( 0, 0 );
			}
		
			# Pack the button into the last HBox. We start_pack them so
			# the remainder buttons are at the bottom, not at the top.
			$boxes->[ -1 ]->pack_start( $button, 0, 0, 0 );
			$count++;
		}
		else {
			$self->{box}->pack_start( $button, 0, 0, 0 );
		}
	}

	if ( $orientation eq 'H' ) {
		# Pack each of the HBoxes into the vbox
		foreach my $hbox ( @{ $boxes } ) {
			$self->{box}->pack_start( $hbox, 0, 0, 0 );
		}
	}

	return $self;
}

=item box

Accessor for getting the root level packing object for inserting into
a menu window, or a drill window.

=cut

sub box {
	my $self = shift;
	return $self->{box};
}

=item buttons

Accessor for getting the button widgets as a list of Gtk2::Button's

=cut

sub buttonlist {
	my $self = shift;
	return @{ $self->{buttons} };
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

