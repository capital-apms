<?php

// Viewer component for creditor records
$viewer = new Viewer("Creditor", "creditor_maintenance");
$viewer->AddField( 'balance', "(SELECT TO_CHAR( COALESCE(sum(balance),0.0),'999G999G990D00PR') FROM accountsummary WHERE entitytype = 'C' AND entitycode = creditorcode)" );
$viewer->SetWhere( 'creditorcode=' . intval($id) );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Creditor:</th>
  <td class="center">##creditorcode.enc##</td>
  <td class="left">##creditor_name.input.50##</td>
 </tr>
 <tr>
  <th class="right">Active:</th>
  <td class="center">##creditor_active.checkbox##</td>
  <td>
   <table class="form_inner">
    <tr>
     <th class="right" style="width:10em;">Payment style:</th>
     <td class="left">##paymentstyle.select##</td>
     <th class="right" style="width:6em;">Cheques/mth:</th>
     <td class="left">##chequespermonth.input.2##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right" colspan="2">Payee Name:</th>
  <td class="left">##payeename.input.50##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">Bank A/C Details:</th>
  <td class="left">##bankdetails.input.50##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">DC Statement Text:</th>
  <td class="left">##dcstatementtext.input.36##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">DC Remittance Email:</th>
  <td class="left">##dcremittanceemail.input.50##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">Voucher Coding:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left">##vchrentitytype.select##</td>
     <td class="left">##vchrentitycode.input.4##</td>
     <td class="left">##vchraccountcode.input.7## ##accountname.value##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th colspan="1" rowspan="3">Payment Details</th>
  <th class="right">Address</th>
  <td class="left">##pymt_address.textarea.40x2##</td>
 </tr>
 <tr>
  <th class="right">City:</th>
  <td class="left">##pymt_city.input.16## ##pymt_state.input.16##</td>
 </tr>
 <tr>
  <th class="right">Country:</th>
  <td class="left">##pymt_country.input.20## ##pymt_zip.input.11##</td>
 </tr>
 <tr>
  <th colspan="1" rowspan="3">Contact</th>
  <th class="right">Person:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left">##pymtperson_title.select## ##pymtperson_first.input.15## ##pymtperson_last.input.20##</td>
     <th class="right" style="width:6em;">Job Title:</th>
     <td class="left">##pymtperson_job.input.41##</td>
     <th class="right" style="width:6em;">Phone:</th>
     <td class="left">+##busphone_isd.input.5## ( ##busphone_std.input.6## ) ##busphone_no.input.33##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Balance:</th>
  <td class="left" colspan="2">##balance.value##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Creditor: ".$viewer->Value('creditor_name'));
$page_elements[] = $viewer;

include_once("menus_entityaccount.php");

// And let's show a transaction browser below the creditor details...
require_once('classBrowser.php');
$browser = new Browser("Transactions Outstanding");
$browser->AddHidden( 'document', "batchcode || '.' || documentcode" );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'reference', 'Reference', 'left', '', "CASE WHEN accttran.reference IS NULL THEN document.reference ELSE accttran.reference END" );
$browser->AddColumn( 'description', 'Description', 'left', '', "CASE WHEN accttran.description IS NULL THEN document.description ELSE accttran.description END" );
$browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$browser->AddTotal( 'amount' );
$browser->SetJoins( "accttran JOIN document USING (batchcode, documentcode)" );
$browser->SetWhere( "entitytype='C' AND entitycode=".intval($id)." AND closedstate != 'F'" );
$browser->AddOrder( 'date', 'ASC' );
$rowurl = '/view.php?t=document&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'document', '#even' );
$browser->DoQuery();
$page_elements[] = $browser;

// Now for an Voucher browser
$browser = new Browser("Vouchers Outstanding or in last 65 days");
$browser->AddColumn( 'voucherseq', '#', 'right' );
$browser->AddHidden( 'vchrst', "'vchrst'||lower(voucherstatus)" );
$browser->AddColumn( 'voucherstatus', 'S', 'center' );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'invoicereference', 'Invoice#', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'ourorderno', 'Our Order', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'description', 'Description' );
$browser->AddColumn( 'goodsvalue', 'Goods', 'right', '%0.2lf' );
$browser->AddColumn( 'taxvalue', 'Tax', 'right', '%0.2lf' );
$browser->AddColumn( 'total', 'Total', 'right', '%0.2lf', '(goodsvalue + taxvalue)' );

$copy_voucher_link = '<a href="/edit.php?t=voucher&copy=##voucherseq##" class="submit">Copy Voucher</a>';
$browser->AddColumn( 'actions', 'Actions', 'center', '&nbsp;%s&nbsp;', "'$copy_voucher_link'");

$browser->AddTotal( 'goodsvalue' );
$browser->AddTotal( 'taxvalue' );
$browser->AddTotal( 'total' );
$browser->SetJoins( "voucher " );
$where = "voucher.creditorcode=".intval($id);
$where .= " AND ( voucherstatus NOT IN ( 'P', 'C' ) OR date > (current_timestamp - '65 days'::interval))";
$browser->SetWhere( $where );
$browser->AddOrder( 'voucherseq', 'ASC', 1 );
$rowurl = '/view.php?t=voucher&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d%s\">\n", "</tr>\n",
                         'voucherseq', '#even', 'vchrst' );
$browser->DoQuery();
$page_elements[] = $browser;
