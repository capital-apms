<?php

// Viewer component for document (of batch)
$viewer = new Viewer("Document");
$viewer->AddField( 'batchcode' );
$viewer->AddField( 'batchdesc', 'b.description' );
$viewer->AddField( 'documentcode' );
$viewer->AddField( 'documenttype' );
$viewer->AddField( 'doctypedesc', 'dt.description' );
$viewer->AddField( 'reference' );
$viewer->AddField( 'reflink', "get_docref_link(reference,documenttype)");
$viewer->AddField( 'description', 'd.description' );
$viewer->AddField( 'transactioncount' );
$joins = "document d JOIN batch b USING ( batchcode ) ";
$joins .= "LEFT JOIN documenttype dt USING ( documenttype ) ";
$viewer->SetJoins( $joins );

unset($id); if ( isset($_REQUEST['id']) ) $id = $_REQUEST['id'];
if ( isset($id) ) {
  list( $b, $d ) = explode( '.', $id, 2);
  $b = intval($b);
  $d = intval($d);
}
$viewer->SetWhere( "b.batchcode=$b AND d.batchcode=$b AND d.documentcode=$d");

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Batch:</th>
  <td class="center"><a href="/view.php?t=batch&id=##batchcode.value##">##batchcode.value##</a></td>
  <td class="left">##batchdesc.value##</td>
 </tr>
 <tr>
  <th class="right">Document:</th>
  <td class="center">##documentcode.value##</td>
  <td class="left">##doctypedesc.value## with ##transactioncount.value## transactions</td>
 </tr>
 <tr>
  <th class="right">Details:</th>
  <td class="left">##reflink.value##</td>
  <td class="left">##description.value##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Document $id / ".$viewer->Record->{'reference'});
$page_elements[] = $viewer;

include_once("menus_document.php");

// And let's show a transaction browser below the tenant details...
require_once('classBrowser.php');
$browser = new Browser("Transactions of Document");
$browser->AddHidden( 'linkviewer',
  "CASE WHEN entitytype='T' THEN 'debtor' WHEN entitytype='C' THEN 'creditor' WHEN entitytype='L' THEN 'company' WHEN entitytype='P' THEN 'property' END" );
$browser->AddHidden( 'entitycode' );
$browser->AddHidden( 'entitycode' );
$browser->AddColumn( 'transactioncode', 'Tx#', 'center' );
$browser->AddColumn( 'account', "Account", 'left', '', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')" );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'reference', 'Reference', 'left', '', "COALESCE(accttran.reference, document.reference)" );
$browser->AddColumn( 'description', 'Description', 'left', '', "COALESCE(accttran.description, document.description)" );
$browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$browser->AddTotal( 'amount' );
$browser->SetJoins( "accttran JOIN batch USING ( batchcode ) JOIN document USING ( batchcode, documentcode )" );

$browser->AndWhere( "accttran.batchcode=$b" );
$browser->AndWhere( "accttran.documentcode=$d" );
$browser->AddOrder( 'transactioncode', 'ASC' );
$rowurl = '/view.php?t=account&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'account', '#even' );
$browser->DoQuery();
$page_elements[] = $browser;

