<?php

// Clean the input value
param_to_global('id', '/^([^,]+,)?[0-9]{1,7}$/' );
if ( !preg_match('/^[^,]+,[0-9]{1,7}$/', $id) && intval($id) == "$id" ) {
  $bankaccountcode = "";
  $chequeno = intval($id);
}
else {
  list($bankaccountcode,$chequeno) = split( ",", $id );
}

/**
* Viewer component for Cheque
*/
$viewer = new Viewer("Cheque", "cheque");
$viewer->AddField( 'bankname', 'bankaccount.bankname' );
$viewer->AddField( 'creditorname', 'cr.name' );
$viewer->AddField( 'payeename', 'c.payeename' );
$joins = "cheque c JOIN creditor cr USING (creditorcode) LEFT JOIN bankaccount USING (bankaccountcode) ";
$viewer->SetJoins( $joins );

if ( !isset($bankaccountcode) || $bankaccountcode == "" ) {
  $viewer->SetWhere( "chequeno=$chequeno");
}
else {
  $viewer->SetWhere( "chequeno=$chequeno AND bankaccountcode = ".qpg($bankaccountcode) );
}

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Bank:</th>
  <td class="left">##bankaccountcode.value## - ##bankname.value##</td>
 </tr>
 <tr>
  <th class="right">Cheque #:</th>
  <td class="left">##chequeno.value##</td>
 </tr>
 <tr>
  <th class="right">Date:</th>
  <td class="left">##date.value##</td>
 </tr>
 <tr>
  <th class="right">Creditor:</th>
  <td class="left"><a href="/view.php?t=creditor&id=##creditorcode.value##">##creditorname.value## (C##creditorcode.value##)</a></td>
 </tr>
 <tr>
  <th class="right">Payee:</th>
  <td class="left">##payeename.value##</td>
 </tr>
 <tr>
  <th class="right">Amount:</th>
  <td class="left">##amount.value##</td>
 </tr>
 <tr>
  <th class="right">Presented:</th>
  <td class="left">##datepresented.value## $##presentedamount.value##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Cheque ".$viewer->Record->{'bankaccountcode'}."/".$viewer->Record->{'chequeno'});
$page_elements[] = $viewer;

$creditorcode = $viewer->Record->{'creditorcode'};
include_once("menus_entityaccount.php");

// And the invoice line details
require_once('classBrowser.php');
$browser = new Browser("Vouchers");
$browser->AddColumn( 'voucherseq', 'Vchr#', 'right' );
$browser->AddHidden( 'vchrst', "'vchrst'||lower(voucherstatus)" );
$browser->AddColumn( 'voucherstatus', 'S', 'center' );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'invoicereference', 'Invoice#', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'ourorderno', 'Order', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'description', 'Description' );
$browser->AddColumn( 'goodsvalue', 'Goods', 'right', '%0.2lf' );
$browser->AddColumn( 'taxvalue', 'Tax', 'right', '%0.2lf' );
$browser->AddColumn( 'total', 'Total', 'right', '%0.2lf', '(goodsvalue + taxvalue)' );
$browser->AddTotal( 'goodsvalue' );
$browser->AddTotal( 'taxvalue' );
$browser->AddTotal( 'total' );
$browser->SetJoins( "voucher " );
$qry = new PgQuery( "SELECT name FROM creditor WHERE creditorcode = ?;", $creditorcode );
if ( $qry->Exec("view-cheque") && $row = $qry->Fetch() ) {
  $browser->Title("Vouchers for $row->name (C$creditorcode)");
}
$where = "voucher.creditorcode=$creditorcode AND voucher.chequeno = $chequeno";

$browser->SetWhere( $where );
$browser->AddOrder( 'voucherseq', 'DESC', 1 );
$rowurl = '/view.php?t=voucher&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n",
                         'voucherseq', '#even' );
$page_elements[] = $browser;

