#!/usr/bin/perl 

open RXTEST, "<", "regextest.txt";
while( <RXTEST> ) {
  next if ( /\s*#/ );
  my $before = $_;

  # This is the RegEx we are testing.  I think that this could
  # be flawed in some situations that do not occur at present
  # in the Catalyst APMS data.

  s/((?<="")|(?<!"))"(?=( |$))/|/;


  my $after = $_;
  $_ = <RXTEST>;
  if ( $after ne $_ ) {
    print "! ! ! ! ! Mismatch ! ! ! ! ! \n";
    print "Before: $before";
    print " After: $after";
    print "Answer: $_";
  }
}
