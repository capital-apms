#
# == Apms::Window
# A base class which all APMS windows inherit from which handles
# stuff like saving it's last position on close, and adding itself
# to the system wide navigator menu and such.
#
# == Bugs
# Perish the thought!  Those are _features_!
#
# == Author
# Andrew McMillan <andrew@mcmillan.net.nz>
#
# == License
# Copyright (c) Andrew McMillan, 2007
# GNU General Public License version 2
#

class ApmsButton < Gtk::Button
  #
  # Button.new( row )
  #
  # Creates a button, based on the DB row passed in
  #
  def row=(dbrow)
    set_row(dbrow)
  end

  attr_reader :linktype, :source, :target, :row

  def set_row( row )
    set_label(row["buttonlabel"])
    @row = row
    @source = row["source"]
    @target = row["target"]
    @linktype = row["linktype"]
    puts row["buttonlabel"] + " added: targetted at >>#{@target}<<,  type >>#{@linktype}<<"

    signal_connect("clicked") do |button|
      puts button.label + " clicked"
      if button.linktype == "MNU" then
        puts "Clicked Menu button >>#{button.label}<< targetted at >>#{button.target}<<"
        new_window = ApmsMenuWindow.new( button.label )
        new_window.set_target(button.target)
      elsif button.linktype == "DRL" then
        puts "Clicked Drill button >>#{button.label}<< targetted at >>#{button.target}<<"
        new_window = ApmsDrillWindow.new( button.label )
        new_window.set_target(button.target)
        new_window.set_source(button.source)
      else
        new_window = ApmsWindow.new( button.label )
      end
      new_window.show_all
    end

  end
end
