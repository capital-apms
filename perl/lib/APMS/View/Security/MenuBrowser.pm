package APMS::View::Security::MenuBrowser;

use strict;
use PMS::Class qw/ new_object /;
use APMS::View::Buttons;
use APMS::View::Browser::MenuBrowser;

use base qw/ APMS::View /;

=item APMS::View::Security::MenuBrowser

This allows browsing of the button links structure

=cut

sub run {
	my ( $proto, $apms, $nodecode ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;
	
	# Create the window
	push @{ $apms->{windows} }, Gtk2::Window->new( 'toplevel' );
	my $window = $apms->{windows}->[ -1 ];
	$window->set_default_size( 400, 600 );

	# Create a single vertical box for all widgets
	my $vbox = Gtk2::VBox->new( 0, 0 );

	# Get the main menu code if we have not been passed a code
	unless ( $nodecode ) {
		$nodecode = $self->{apms}->model->mainmenucode();
	}

	# Create the browse window
	my $browser = $self->menu_view( $nodecode );

	# Put the browse widget into a scrolled window
	my $scroll = Gtk2::ScrolledWindow->new();
	$scroll->add( $browser );

	my $button = Gtk2::Button->new( 'Menu Items' );
	$button->signal_connect( clicked => sub { $self->into_sub_menu() } );
	my $buttonbox = Gtk2::HBox->new( 0, 0 );
	$buttonbox->pack_start( $button, 0, 0, 0 );

	# Pack the objects in to the Vbox
	$vbox->pack_start( $scroll, 1, 1, 0 );
	$vbox->pack_start( $buttonbox, 0, 0, 0 );

	# Add it all to the window
	$window->add( $vbox );
	$window->show_all;
}

=item set_selected

When the user selects one of the menu options, this is called to store that
nodecode value in self. Then later when they push the Menu Items button, that
code is used to call this class again with the correct nodecode.

=cut

sub set_selected {
	my $row = shift;
}

sub into_sub_menu {
	my $self = shift;
	$self->{apms}->run( __PACKAGE__, 8 );
}

1;
