<?php
require_once('../inc/always.php');

require_once('classWidgets.php');
require_once('classBrowser.php');
require_once('classLinks.php');

param_to_global('component', '#^[a-z0-9_+-]+$#', 't');
param_to_global('id', '#^[0-9]+$#');

require_once('apms_menus.php');

$widget = new Widget();
$page_elements = array();

if ( isset($_POST['submit']) || $widget->IsSubmit() ) {
  if ( ! @include( "action/process/$component.php" ) ) {
    $c->messages[] = "action/process/$component not found";
  }
  dbg_error_log("LOG","Been there, done that!");
}
if ( ! @require( "action/screen/$component.php" ) ) {
  $c->messages[] = "action/screen/$component not found";
}

/**
* From here we start rendering the page to the user...
*/
include('page-header.php');

// Page elements could be an array of viewers, browsers or something else
// that supports the Render() method...
$heading_level = null;
foreach( $page_elements AS $k => $page_element ) {
  if ( is_object($page_element) ) {
    echo $page_element->Render($heading_level);
    $heading_level = 'h2';
  }
  else {
    echo $page_element;
  }
}

if (function_exists("post_render_function")) {
  post_render_function();
}

if ( isset($footers) ) {
  foreach( $footers AS $k => $footer ) {
    echo $footer;
  }
}

include('page-footer.php');
