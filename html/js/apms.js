// Javascript for Capital APMS

// Needs to be set externally somehow...  In-page javascript, perhaps.
//apms.gst_rate = 0.125;


// Simple function to send the browser to a given URL
function Go( url ) {
  window.location=url;
  return true;
}

// Make this tag into a Link to a given URL
function LinkTo( tag, url ) {
  tag.style.cursor = "pointer";
  tag.setAttribute('onClick', "Go('" + url + "')");
  tag.setAttribute('onMouseOut', "window.status='';return true;");
  window.status = window.location.protocol + '//' + document.domain + url;
  tag.setAttribute('onMouseover', "window.status = window.location.protocol + '//' + document.domain + '" + url + "';return true;");
  tag.setAttribute('href', url);
  return true;
}

/**
* Make this tag and all of it's contents into a clickable link, using the link target from an
* existing link target somewhere within the tag.  Setting 'which1' to '1' will make the target
* match the 1st href target within the HTML of the tag.
* @param objectref tag A reference to the object which will become clickable.
* @param int which1 A one-based index to select which internal href attribute will become the target.
*/
function LinkHref( tag, which1 ) {
  var urls = tag.innerHTML.match( / href="([^"]*)"/ig );
//  alert(show_props(urls,'urls', 1));
  try {
    var url = urls[which1 - 1];
    urls = url.match( /="([^"]*)"/ );
  }
  catch (e) {
    //alert("Here are the URLs found:\nYou appear to need to choose a different index for your LinkHref call (the second parameter).  Add 1 to the index below for the correct URL shown and use that.\n\n" + show_props(urls,'urls', 0));
    return false;
  }
//  alert(show_props(urls,'urls', 1));
  url = urls[1];
//  alert("Linking to >>>" + url + "<<<");
  LinkTo(tag,url);
  return true;
}


// Debugging function for showing (depth) levels of object information for an object
function show_props(obj, objName, depth) {
  var result = "";
  var dispval = "";
  try {
    for (var i in obj) {
      try {
        dispval = ( typeof(obj[i]) == "function" ? "function" : obj[i] );
      }
      catch (e) {
        dispval = "something unknown";
      }
      result += objName + "." + i + " = " + dispval + "\n";
      if ( i == "parentNode" || i == "previousSibling" || i == "nextSibling" || i == "ownerDocument" ) {
        continue;
      }
      if ( depth > 0 && typeof(obj[i]) == 'object' ) {
        result += show_props(obj[i], objName + "." + i, depth - 1);
      }
    }
  }
  catch (e) {
  }
  return result;
}



/**
* Field validation functions used in forms
*/

// Validating a positive, non-zero dollars field
// If the field is required, use not_empty first.
function positive_dollars (field)
{
   if(!field.value) return true;
   var number = field.value;
   var pattern = /^\$?[0-9]*\.?[0-9]?[0-9]?$/;
   if( pattern.test(number) ) {
     number = number.replace("$","");
     number = number.replace(".","");
     if( parseInt(number) > 0 ) return true;
   }
   return false;
}

// Validating a positive integer field
// If the field is required, use not_empty first.
function positive_integer (field)
{
   if(!field.value) return true;
   var number = field.value;
   var pattern = /^[0-9]*$/;
   return (pattern.test(number));
}

function validate_positive_integer (field) {
  if ( ! positive_integer(field) ) {
    alert("Please enter a positive integer");
    return false;
  }
  return true;
}

// Validating that a field is not empty
function not_empty (field)
{
   return ( field.value != "" );
}

// Validating that an option as been selected in a select box
function selected (field)
{
    return ( field.options[field.selectedIndex].value != "" && field.options[field.selectedIndex].value != 0 );
}

// Validating that a field contains a valid email address
function valid_email_format(field)
{
  if(!field.value) return true;
  var pattern = /^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/;
  return (pattern.test(field.value));
}

// Find the parent form containing this tag
function find_parent_form( tag ) {
  if ( !tag || !tag.parentNode ) {
    alert( "Node is null or parent is null" );
    return false;
  }
  if ( tag.parentNode.tagName == "FORM" ) {
    alert( "Found form parent: " + tag.parentNode.name );
    return tag.parentNode;
  }
  return find_parent_form( tag.parentNode );
}

// Find the parent table row containing this tag
function find_parent_row( tag ) {
  if ( !tag || !tag.parentNode ) {
    alert( "Node is null or parent is null" );
    return false;
  }
  if ( tag.parentNode.tagName == "TR" ) {
//    alert( "Table row parent: " + tag.parentNode.tagName );
    return tag.parentNode;
  }
  return find_parent_row( tag.parentNode );
}

// Find the field in a form with the given name
function find_form_field( f, fname ) {
  for ( var i=0; i < f.elements.length ; i++ ) {
    if ( f.elements[i].name == fname ) {
      return f.elements[i];
    }
  }
  alert("Field " + fname + " was not found in the form!");
  return false;
}


// Toggle the input checkbox field
function Toggle(fname)
{
  for each ( var f in document.forms ) {
    for ( var i=0; i < f.elements.length ; i++ ) {
      if ( f.elements[i].name == fname ) {
        if ( f.elements[i].checked )
          f.elements[i].checked = false;
        else
          f.elements[i].checked = true;

        var tr = find_parent_row( f.elements[i] );
        if ( tr ) {
          if ( f.elements[i].checked )
            tr.className = tr.className + "-checked";
          else
            tr.className = tr.className.replace("-checked", "");
        }

        return true;
      }
    }
  }
  alert("Field " + fname + " was not found in the document!");
  return false;
}

// Format number with decimals
function FormatDecimal(num) {
  sign = num < 0 ? "-":"";
  rval = Math.abs(Math.round(num*100));
  str = rval.toString();
  if(rval < 10) str = "0.0"+str;
  else if(rval < 100) str = "0."+str;
  else str = str.substring(0, str.length-2)
     + "." + str.substring(str.length-2, str.length);
  return sign+str;
}

//
function calculate_tax_total( goodsfield, taxfield, totfield, taxapplies ) {

  if ( taxapplies ) {
    taxfield.value = FormatDecimal((Number(goodsfield.value) * (apms.gst_rate)).toFixed(2));
  }
  else {
    taxfield.value = 0;
  }
  totfield.value = FormatDecimal(Number(goodsfield.value) + Number(taxfield.value));
  goodsfield.value = FormatDecimal(Number(goodsfield.value));

  return true;
}

function calculate_total( taxfield, goodsfield, totfield ) {

  totfield.value = FormatDecimal(Number(goodsfield.value) + Number(taxfield.value));
  taxfield.value = FormatDecimal(Number(taxfield.value));
  return true;
}

//
function calculate_tax_split( totfield, goodsfield, taxfield, taxapplies ) {
  // If the current sum is OK, then don't correct it, even if it seems odd.
  if ( Number(totfield.value) == (Number(goodsfield.value) + Number(taxfield.value)) ) {
    return true;
  }

  if ( taxapplies ) {
	inverse_rate = ( Number(apms.gst_rate) / (Number(1) + Number(apms.gst_rate)));
    taxfield.value = FormatDecimal( (Number(totfield.value) * inverse_rate).toFixed(2) );
  }
  else {
    taxfield.value = 0;
  }
  goodsfield.value = FormatDecimal(Number(totfield.value) - Number(taxfield.value));
  totfield.value = FormatDecimal(Number(totfield.value));
  return true;
}

//
function calculate_percent_share( amountfield, percentfield, sharefield ) {

  sharefield.value = FormatDecimal(Number(amountfield.value) * Number(percentfield.value) * 0.01);
  amountfield.value = FormatDecimal(Number(amountfield.value));

  return true;
}


function EnterToNextField( e, gothere ) {
  if ( e.keyCode == 13 ) {
    // e.keyCode = 9;
    gothere.focus();
    return false;
  }
  return true;
}


function nextFieldOnEnter(e){
  	f = e.target;
  	if ( f.name == "submit" || f.type == "submit" ) return;
    if ( e.keyCode != 13 ) return;
    j = 0
    do {
      next = f.firstChild;
      if ( !next ) next = f.nextSibling;
      if ( !next ) next = f.parentNode.nextSibling;
      if ( !next ) next = f.parentNode.parentNode.nextSibling;
      if ( !next ) next = f.parentNode.parentNode.parentNode.nextSibling;
  	  f = next;
      j++;
    } while( j < 50 && f && (f.tagName != 'INPUT' || f.type == "hidden" || f.readOnly)
    					 && f.name != "submit" && f.type != "submit" );

//    if ( f ) alert("Now at "+f.tagName+" - "+f.name+" - "+f.type);

    if ( f && f.tagName == 'INPUT' ) f.focus();
    return false;
}

apms.allowedTypes = new RegExp(/[CTLPJF]/i);
function onEntityTypeChange(e) {
	id = e.id.replace('entitytype','accountcode');
	e.setAttribute("class","entry");
	accountcode = document.getElementById(id);
	accountcode.setAttribute("readonly","");
	e.value = e.value.toUpperCase();
	if ( apms.allowedTypes.test(e.value) ) {
		if ( e.value == "C" )  {
			id = e.id.replace('entitytype','accountcode');
			accountcode.setAttribute("type","hidden");
			accountcode.value = apms.creditors.accountcode;
			return true;
		}
		else if ( e.value == "T" ) {
			accountcode.setAttribute("type","hidden");
			accountcode.value = apms.debtors.accountcode;
			return true;
		}
		else {
			switch( e.value ) {
				case "L":
				case "P":
				case "J":
				case "F":
					id = e.id.replace('entitytype','accountcode');
					accountcode.setAttribute("type","text");
					return true;
			}
		}
		return true;
	}
	alert("'"+e.value+"' is not a valid type");
	e.setAttribute("class","entry error");
	return false;
}