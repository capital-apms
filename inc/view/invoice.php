<?php

param_to_global('id', '/^(INV[, ]?)?([0-9]{1,7})$/i' );
if ( isset($id) && preg_match('/^(INV[, ]?)?([0-9]{1,7})$/', $id, $matches)  ) {
  $id = intval($matches[2]);
}

// Viewer component for invoice
$viewer = new Viewer("Invoice", 'invoice');
$viewer->AddField( 'etname', 'get_entity_type_name(entitytype)' );
$viewer->AddField( 'loweretname', 'lower(get_entity_type_name(entitytype))' );
$viewer->AddField( 'entityname', 'get_entity_name(entitytype,entitycode)' );
$viewer->AddField( 'display_status', "CASE WHEN invoicestatus = 'U' THEN 'Unapproved' WHEN invoicestatus = 'A' THEN 'Approved' ELSE 'Confused' END " );
$viewer->AddField( 'invoice_amount', '(total + taxamount)' );
$viewer->SetWhere( "invoiceno=$id");

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Invoice#:</th>
  <td class="left">##invoiceno.value##</td>
 </tr>
 <tr>
  <th class="right">Date:</th>
  <td class="left">##invoicedate.value##</td>
 </tr>
 <tr>
  <th class="right">##etname.value##:</th>
  <td class="left"><a href="/view.php?t=##loweretname.value##&id=##entitycode.value##">##entityname.value##</a></td>
 </tr>
 <tr>
  <th class="right">Re:</th>
  <td class="left">##todetail.value##</td>
 </tr>
 <tr>
  <th class="right">Description:</th>
  <td class="left">##blurb.value##</td>
 </tr>
 <tr>
  <th class="right">Amount:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left" style="width:4em;">##total.money.12##</td>
     <th class="right" style="width:4em;">+ Tax:</th>
     <td class="left" style="width:4em;">##taxamount.money.12##</td>
     <th class="right" style="width:4em;">Total:</th>
     <td class="left" style="width:4em;">##invoice_amount.money.12##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Status:</th>
  <td class="left">##display_status.value##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Invoice $id / ".$viewer->Record->{'todetail'});
$page_elements[] = $viewer;

$et = $viewer->Record->{'entitytype'};
$ec = $viewer->Record->{'entitycode'};
include_once("menus_entityaccount.php");

$related_menu->AddOption("Print Invoice","/print.php?t=invoice&id=$id","Print this invoice", false, 200);
if ( $viewer->Record->{'invoicestatus'} == 'U' ) {
  $related_menu->AddOption("Edit Invoice","/edit.php?t=invoice&id=$id","Edit this invoice", false, 100);
}

// And the invoice line details
require_once('classBrowser.php');
$browser = new Browser("Invoice Detail");
$browser->AddHidden( 'etname', 'lower(get_entity_type_name(entitytype))' );
$browser->AddColumn( 'lineseq', '#', 'right' );
$browser->AddHidden( 'entitycode' );
$browser->AddHidden( 'account', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')" );
$browser->AddColumn( 'accountname', 'Account', 'left', '', 'chartofaccount.name');
$browser->AddColumn( 'accounttext', 'Description', 'left' );
$browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$browser->AddColumn( 'percent', 'Percent', 'right', '', "TO_CHAR(percent,'FM990.00')" );
$browser->AddColumn( 'yourshare', 'Share', 'right', '%0.2lf' );
$browser->AddTotal( 'amount' );
$browser->AddTotal( 'yourshare' );
$browser->SetOrdering( 'lineseq' );
$browser->SetJoins( "invoiceline JOIN chartofaccount USING ( accountcode )" );

$browser->AndWhere( "invoiceno=$id" );
$rowurl = '/view.php?t=account&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Display %s Account '%s'\" class=\"r%d\">\n", "</tr>\n", 'account', 'account', 'accountname', '#even' );
$page_elements[] = $browser;

