#
# == Apms::MenuLinks
# Installs a set of menu links into the widget provided by
# constructing the set by querying the database for links
# for this node_id
#
# == Bugs
# * Don't be ridiculous!
#
# == Author
# Andrew McMillan <andrew@mcmillan.net.nz>
#
# == License
# Copyright (c) Andrew McMillan, 2007
# GNU General Public License version 2
#

class ApmsMenuLinks
  # Provides access to the buttons so they can be added to a container
  def buttons
    @buttons
  end

  #
  # ApmsMenuLinks.new( dbh, &tooltips, source_id, fill='H', size=500 )
  #
  # Builds a Box containing a series of Boxes containing the
  # buttons for this source node.  If width is set then a width first
  # fill will be used, if height is set then a height-first fill will
  # be used (the default).
  #
  def initialize(dbh, tips, node_id, fill='H', max_size=-1)
    # max_size = size.dup
    if max_size < 100 then
      max_size = 500
    end

    if fill == 'H' then
      @buttons = Gtk::VBox.new( false, 0 )
    else
      @buttons = Gtk::HBox.new( false, 0 )
    end
    sql = "SELECT * FROM button_links WHERE menucode = #{node_id} ORDER BY sequencecode;"
    puts sql
    buttons_sql = $dbh.prepare(sql)
    buttons_sql.execute()
    buttons_size = 0
    if fill == 'H' then
      box = Gtk::HBox.new( false, 0 )
    else
      box = Gtk::VBox.new( false, 0 )
    end

    while btn_row = buttons_sql.fetch do
      btn = ApmsButton.new
      btn.set_row(btn_row)
      tips.set_tip( btn, btn_row["description"], '' )
      (width,height) = btn.size_request
      buttons_size += ( fill == 'H' ? width : height )
      box.pack_start(btn, false, false, 0)
      if buttons_size >= max_size then
        puts "Button row added - width #{buttons_size}"
        @buttons.add(box)
        if fill == 'H' then
          box = Gtk::HBox.new( false, 0 )
        else
          box = Gtk::VBox.new( false, 0 )
        end
        buttons_size = 0
      end
    end

    if buttons_size > 0 then
      @buttons.add(box)
    end

  end
end
