<?php

if ( isset($batch) && $batch->Successful() ) return true;

param_to_global('batchcode', 'int');

$widget->AddField( 'batchcode', 'int1', "SELECT batchcode, batchcode::text || ' - ' || description FROM newbatch WHERE batchtype != 'ACCR' ORDER BY batchcode;" );
$widget->Defaults( array( 'batchcode' => $batchcode ));
// $widget->ReadWrite();  // Since the batchcode will not be present next time we are here this is not useful!
$widget->Title("Batch Update");
$widget->Layout( '<table>
 <tr>
  <th class="right">Batch:</th>
  <td class="left">##batchcode.select##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left">##Update Batch.submit##</td>
 </tr>
</table>' );
$page_elements[] = $widget;
