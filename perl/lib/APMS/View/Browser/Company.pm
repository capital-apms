package APMS::View::Browser::Company;

use strict;
use APMS::View::Browser;

sub company_view {
	my ( $self, $type ) = @_;

	# Populated with a SQL query
	my @data;

	# Call appropriate model method
	if ( $type eq 'all' ) {
		@data = $self->{apms}->model->all_companies();
	}
	else {
		@data = $self->{apms}->model->active_companies();
	}

	my $browser = APMS::View::Browser->browse_window(
		fields => [
			{
				name   => 'Short Name',
				column => 'shortname',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				dosort => 1
			}, {
				name   => 'Active',
				column => 'active',
				type   => 'Glib::String',	
				widget => 'Gtk2::CellRendererText'
			}, {
				name   => 'Code',
				column => 'companycode',
				type   => 'Glib::Uint',
				widget => 'Gtk2::CellRendererText',
				dosort => 1
			}, {
				name   => 'Legal Name',
				column => 'legalname',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				expand => 1,
				minw   => 100,
				dosort => 1
			}, {
				name   => 'Tax No',
				column => 'taxno',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => \@data,
		row_clicked => sub { $self->set_selected( @_ ) },
	);

	return $browser;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

