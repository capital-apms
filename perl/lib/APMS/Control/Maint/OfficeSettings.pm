package APMS::Control::Maint::OfficeSettings;

=head1 APMS::Control::Maint::OfficeSettings

Controller for the office settings screen

=cut

use strict;
use APMS::Class qw/ new_object /;
use APMS::Constants qw/ APMS_VIEW APMS_MAINT APMS_ADD /;
use APMS::View::Maint::OfficeSettings;

use base qw/ APMS::Control::Window /;

sub run {
	my ( $proto, $apms, $thisnode, %args ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;

	$self->{config} = $apms->model->all_settings();

	# Create the office settings window
	$self->{settingswindow} = APMS::View::Maint::OfficeSettings->new( config => $self->{config} );

	# Return the view object
	return $self->{settingswindow};
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

