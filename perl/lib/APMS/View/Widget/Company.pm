package APMS::View::Widget::Company;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Widget::Button;

sub company_select {
	my ( $proto ) = @_;
	my $self = new_object( $proto );

	$self->{companybox} = Gtk2::HBox->new( 0, 5 );

	$self->{companycode} = Gtk2::Entry->new();
	$self->{companycode}->set_size_request( 60, 25 );
	$self->{companybox}->pack_start( $self->{companycode}, 0, 1, 0 );

	$self->{choosecompany} = APMS::View::Widget::Button->jump_to_button();
	$self->{companybox}->pack_start( $self->{choosecompany}, 0, 1, 0 );

	$self->{companyname} = Gtk2::Label->new( '(select company)' );
	$self->{companybox}->pack_start( $self->{companyname}, 0, 1, 0 );

	return $self;
}

sub box {
	my $self = shift;
	return $self->{companybox};
}

sub set_company_details {
	my ( $self, $code, $name ) = @_;

	$self->{companycode}->set_text( $code );
	$self->{companyname}->set_text( $name );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

