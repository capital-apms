<?php
switch ( $viewer->Record->{'documenttype'} ) {
  case 'VCHR':
    $vchr = substr($viewer->Record->{'reference'},4);
    $related_menu->AddOption("Voucher","/view.php?t=voucher&id=$vchr","View the creditor voucher that created this document");
    break;
  case 'INVC':
    $invc = substr($viewer->Record->{'reference'},4);
    $related_menu->AddOption("Invoice","/view.php?t=invoice&id=$invc","View the debtor invoice that created this document");
    break;
  case 'CHEQ':
    list($bankaccountcode,$chequeno) = split( ",", $viewer->Record->{'reference'},2);
    $related_menu->AddOption("Cheque","/view.php?t=cheque&id=".$viewer->Record->{'reference'},"View the cheque payment that created this document");
    break;
}
$related_menu->AddOption("Batch","/view.php?t=batch&id=$b","View the entire batch");
