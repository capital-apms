<?php
include_once("classWidgets.php");

param_to_global( 'id', '#^[ACTLPJ]-\d+-\d+(\.\d+)?$#i' );
if ( isset($id) ) {
  list( $et, $ec, $ac ) = explode('-',$id,3);
  $et = strtoupper(substr($et,0,1));
  $ec = intval($ec);
  $ac = floatval($ac);
}
param_to_global('month', '#[0-9:/-]+#');

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'year', 'int1', "SELECT 0, '--- All Years ---' UNION SELECT financialyearcode, description FROM financialyear ORDER BY 1;" );
$widget->AddField( 'status', 'char1', "SELECT '.', '--- Any Status ---' UNION SELECT invoicestatus, description FROM invoicestatus ORDER BY 1;" );
$widget->ReadWrite();
$widget->Defaults( array('year' => $year ) );
$widget->Layout( '<table> <tr>
<th>Status:</th> <td>##status.select##</td>
<th>For year:</th> <td>##year.select##</td>
<td>##Show.submit##</td> </tr></table>' );
$page_elements[] = $widget;


$browser = new Browser("Browse Invoices");

if ( isset($et) && isset($ec) ) {
  $qry = new PgQuery( "SELECT get_entity_type_name(?) AS entitytypename, get_entity_name(?,?) AS entityname;", $et, $et, $ec);
  if ( $qry->Exec("browse-$component") && $row = $qry->Fetch() ) {
    $browser->Title("$row->entitytypename Invoices for $row->entityname ($et$ec)");
  }
}
$related_menu->AddOption("New Invoice","/edit.php?t=invoice","Create a new invoice", false, 3100);
include_once("menus_entityaccount.php");

$browser->AddColumn( 'invoiceno', 'Invoice#', 'center' );
$browser->AddColumn( 'invoicestatus', '?', 'center' );
$browser->AddColumn( 'invoicedate', 'Date', 'center' );
$browser->AddColumn( 'todetail', 'Brief', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'blurb', 'Invoice Details' );
$browser->AddColumn( 'total', 'Amount', 'right', '%0.2lf' );
$browser->AddTotal( 'total' );
$browser->SetJoins( "invoice" );
// $browser->SetJoins( "invoice JOIN invoiceline USING (invoiceno)" );
if ( isset($et) && isset($ec) )
  $browser->AndWhere( "invoice.entitytype='$et' AND invoice.entitycode=$ec" );
if ( $widget->Record->{'year'} > 0 ) {
  $browser->SetJoins("invoice JOIN month ON month.startdate <= invoice.invoicedate AND month.enddate >= invoice.invoicedate JOIN financialyear USING (financialyearcode)");
  $browser->AndWhere( "financialyearcode =".qpg($widget->Record->{'year'}) );
}
if ( isset($widget->Record->{'status'}) ) {
  $browser->AndWhere( "invoicestatus ~* ".qpg($widget->Record->{'status'}) );
}
$browser->AddOrder( 'invoiceno', 'DESC', 1 );
$rowurl = '/view.php?t=invoice&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Display Invoice Detail\" class=\"r-inv-%s\">\n", "</tr>\n", 'invoiceno', 'invoicestatus' );

$c->page_title = $browser->Title();
$page_elements[] = $browser;
