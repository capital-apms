<?php

param_to_global( 'ac', '#^\d+(\.\d{0,2})?$#', 'oldaccountcode', 'ac', 'id' );

// Viewer component for account
$editor = new Editor("Account", 'chartofaccount');
$editor->SetLookup("accountgroupcode", "SELECT accountgroupcode, accountgroupcode || ' - ' || name AS description FROM accountgroup ORDER BY sequencecode");
$editor->SetWhere( "accountcode=$ac");

if ( $editor->IsSubmit() ) {
  $ac = floatval($_POST['accountcode']);
  $editor->WhereNewRecord( "accountcode=".floatval($_POST['accountcode']) );
  $editor->Write();
  $editor->SetWhere( "accountcode=$ac");
  $editor->GetRecord();
}
else {
  $editor->GetRecord();
}

$template = <<<EOTEMPLATE
##form##
<table>
 <tr>
  <th class="right">Account:</th>
  <td class="left"><input type="hidden" name="oldaccountcode" value="##accountcode.enc##">##accountcode.input.7##</td>
 </tr>
 <tr>
  <th class="right">Short name:</th>
  <td class="left">##shortname.input.12##</td>
 </tr>
 <tr>
  <th class="right">Name:</th>
  <td class="left">##name.input.50##</td>
 </tr>
 <tr>
  <th class="right">Group:</th>
  <td class="left">##accountgroupcode.select##</td>
 </tr>
 <tr>
  <th class="right">High volume:</th>
  <td class="left">##highvolume.checkbox##</td>
 </tr>
 <tr>
  <th class="right">Update to:</th>
  <td class="left">##updateto.input.10##</td>
 </tr>
 <tr>
  <td>&nbsp;</td>
  <td class="left">##submit##</td>
 </tr>
</table>
</form>
EOTEMPLATE;

$editor->SetTemplate( $template );
$c->page_title = $editor->Title("Account $ac - " . $editor->Record->{'name'});
$page_elements[] = $editor;

include_once("menus_entityaccount.php");
