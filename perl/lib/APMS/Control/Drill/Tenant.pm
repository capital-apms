package APMS::Control::Drill::Tenant;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Drill::Tenant;

# Base class for drill windows
use base qw/
	APMS::Control::Window
	APMS::Control::Drill
/;

sub run {
	my ( $proto, $apms, $nodecode ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;

	my $type = 'active';

	# Call appropriate model method
	if ( $type eq 'all' ) {
		$self->{tenantlist} = $apms->model->all_tenants();
	}
	elsif ( $type eq 'active' ) {
		$self->{tenantlist} = $apms->model->active_tenants();
	}
	else {
		$self->{tenantlist} = $apms->model->inactive_tenants();
	}

	# Fetch the buttons for this window from the DB
	$self->{buttons} = $apms->model->buttons( $apms->config->{UserName}, $nodecode );

	# Create the view
	$self->{drillwindow} = APMS::View::Drill::Tenant->new(
		buttons    => $self->{buttons},
		tenantlist => $self->{tenantlist}
	);

	# Attach the destroy event
	$self->{drillwindow}->attach_destroy( sub { $self->destroy_window( @_ ) } );

	# Connect the generic button signal to every button in the drill view
	$self->button_callbacks( $self->{drillwindow} );

	return $self->{drillwindow};
}

=item recordid

Every drill window must have this callback defined. It describes how
this particular drill window passes the selected record id / ids on to
child windows.

=cut

sub recordid {
	my $self = shift;
	my $tenantcode = $self->{drillwindow}->selected_tenantcode();
	return $tenantcode;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

