<?php
require_once('../inc/always.php');

require_once('classPdfReport.php');

$component = clean_string($_GET['t']);
$id = clean_string($_GET['id']);

/**
* Note that for this one we don't render any HTML output
*/
if ( ! @include_once( "print/$component.php" ) ) {
  $c->messages[] = "print/$component not found";
  include('page-header.php');
  include('page-footer.php');
}
else {
  if ( isset($pdf) )  echo $pdf->Render();
}