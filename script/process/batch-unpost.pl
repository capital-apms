#!/usr/bin/perl -w
#
# This process unposts a batch, converting it back to an unposted
# one, which we could then re-post.
#

use strict;

use Sys::Syslog;
use DBI;
use POSIX qw(floor);
use Getopt::Long qw(:config permute);  # allow mixed args.

# Options variables
my $batchcode;
my $dbname ="apms";
my $dbuser ="";
my $debug = 0;
my $oneline = 0;
my $helpmeplease = 0;
my $syslog = 1;
my $log_open = 0;

GetOptions ('debug!'    => \$debug,
            'batch=s'   => \$batchcode,
            'dbname=s'  => \$dbname,
            'user=s'    => \$dbuser,
            'oneline!'  => \$oneline,
            'help'      => \$helpmeplease  );

show_usage() if ( $helpmeplease );

############################################################
# Open database connection and set up our queries
############################################################
my $dbh = DBI->connect("dbi:Pg:dbname=$dbname", $dbuser, "", { AutoCommit => 0 } ) or die "Can't connect to database $dbname";


############################################################
# Fancy SQL.  This process is pretty much just SQL...
############################################################

my $sql = <<EOQ ;
BEGIN;
INSERT INTO newdocument (batchcode, documentcode, description, reference, documenttype)
     SELECT batchcode, documentcode, description, reference, documenttype
       FROM document WHERE batchcode=$batchcode AND documenttype != 'ICOA';

INSERT INTO newaccttrans (batchcode, documentcode, transactioncode, entitytype, entitycode, accountcode, date, description, amount, reference)
     SELECT accttran.batchcode, accttran.documentcode, transactioncode, entitytype, entitycode, accountcode, date, accttran.description, amount, accttran.reference
       FROM accttran JOIN document USING (batchcode,documentcode)
      WHERE document.batchcode=$batchcode AND document.documenttype != 'ICOA' AND accttran.consequenceof=0;

INSERT INTO newbatch (batchcode, personcode, batchtype, documentcount, description )
     SELECT batchcode, operatorcode, 'NORM', (SELECT count(*) FROM newdocument WHERE batchcode=$batchcode), description
       FROM batch WHERE batchcode=$batchcode;

DELETE FROM accttran WHERE batchcode=$batchcode;
DELETE FROM document WHERE batchcode=$batchcode;
DELETE FROM batch WHERE batchcode=$batchcode;
COMMIT;
EOQ

if ( $oneline ) {
  $sql =~ s/\s+/ /gm ;
}

print $sql, "\n";

############################################################
# Tell the nice user how we do things.  Short and sweet.
############################################################
sub show_usage {
    print <<OPTHELP;

batch-update.pl [options] --batch <batchno>

Options are:
    --debug       Turn on debugging
    --dbname      The database to dig into
    --user        Connect to the database as this user.

The program will post the specified batch of transactions, with
a database commit after each document is processed.

OPTHELP
    exit 0;
}

############################################################
# Log to syslog or stdout, as selected
############################################################
sub logf {
my $severity = shift;

  if ( $syslog && ! $log_open ) {
    openlog( "logacct", "pid", "local0" );
    $log_open = 1;
  }
  if ( $syslog ) {
    syslog( $severity, @_ );
  }
  else {
    print scalar localtime, "[", $$, "]: ";
    printf( @_ );
    print "\n";
  }
}
