package APMS::Model::Office;

use strict;

=item this_office

Returns the Office record for this office

  my $officerec = $self->model->this_office();

=cut

sub this_office {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT accountingcontact, gst, gstno, name, officecode, streetaddress FROM office
		WHERE thisoffice = TRUE;
	) );

	$sth->execute();
	my $office = $sth->fetchrow_hashref();
	$sth->finish;

	return $office;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

