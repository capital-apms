<?php

param_to_global('id', '#^(VCHR)?[ ,]?\d+$#i', 'voucherseq');

if ( isset($id) && strtoupper(substr($_REQUEST['id'],0,4)) == 'VCHR' ) {
  $id = intval(substr($_REQUEST['id'],4));
}

// Viewer component for Voucher
$viewer = new Viewer("Voucher", 'voucher');
$viewer->AddField( 'voucherseq' );
$viewer->AddField( 'creditorcode' );
$viewer->AddField( 'entitytype' );
$viewer->AddField( 'entitycode' );
$viewer->AddField( 'accountcode' );
$viewer->AddField( 'etname', 'get_entity_type_name(entitytype)' );
$viewer->AddField( 'creditorname', 'cr.name' );
$viewer->AddField( 'date' );
$viewer->AddField( 'statusname', 'voucherstatus.description' );
$viewer->AddField( 'description', 'voucher.description' );
$viewer->AddField( 'taxvalue' );
$viewer->AddField( 'goodsvalue' );
$viewer->AddField( 'amount', '(goodsvalue + taxvalue)' );
$viewer->AddField( 'bankaccountcode' );
$viewer->AddField( 'chequeno' );
$joins = "voucher JOIN creditor cr USING (creditorcode) ";
$joins .= "LEFT JOIN voucherstatus USING (voucherstatus) ";
$joins .= "LEFT JOIN approver ap1 USING (approvercode) ";
$joins .= "LEFT JOIN person ap1p ON ap1.personcode = ap1p.personcode ";
$viewer->SetJoins( $joins );

$viewer->SetWhere( "voucherseq=$id");

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Vchr#:</th>
  <td class="left">##voucherseq.value##</td>
 </tr>
 <tr>
  <th class="right">Date:</th>
  <td class="left">##date.value##</td>
 </tr>
 <tr>
  <th class="right">Creditor:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left"><a href="/view.php?t=creditor&id=##creditorcode.value##">##creditorname.value## (C##creditorcode.value##)</a></td>
     <th class="right">Invoice Date:</th>
     <td class="left">##date.date##</td>
     <th class="right">Due:</th>
     <td class="left">##datedue.date##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Approvers:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left">##approvercode##</td>##secondapprover##</td>
     <th class="right">Invoice No:</th>
     <td class="left">##invoicereference##</td>
     <th class="right">Order ref:</th>
     <td class="left" style="width:5em;">##ourorderno##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Description:</th>
  <td class="left">##description.value##</td>
 </tr>
 <tr>
  <th class="right">Status:</th>
  <td class="left">##voucherstatus.value## ##statusname.value##</td>
 </tr>
 <tr>
  <th class="right">Cheque:</th>
  <td class="left"><a href="/view.php?t=cheque&id=##bankaccountcode.value##,##chequeno.value##">##bankaccountcode.value## ##chequeno.value##</a></td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Voucher $id / ".$viewer->Record->{'description'});
$page_elements[] = $viewer;


if ( $viewer->Record->{'voucherstatus'} == 'U' || $viewer->Record->{'voucherstatus'} == 'H' ) {
  $related_menu->AddOption("Edit Voucher","/edit.php?t=voucher&id=$id","Edit this voucher");
}
$related_menu->AddOption("Print Voucher","/print.php?t=voucher&id=$id","Print this voucher");

$et = $viewer->Record->{'entitytype'};
$ec = $viewer->Record->{'entitycode'};
$ac = $viewer->Record->{'accountcode'};
$creditorcode = $viewer->Record->{'creditorcode'};
include_once("menus_entityaccount.php");


// And the invoice line details
require_once('classBrowser.php');
$browser = new Browser("Voucher Detail");
$browser->AddHidden( 'etname', 'lower(get_entity_type_name(entitytype))' );
$browser->AddColumn( 'lineseq', '#', 'right' );
$browser->AddHidden( 'entitytype' );
$browser->AddHidden( 'entitycode' );
$browser->AddHidden( 'accountcode' );
$browser->AddHidden( 'account', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')" );
$browser->AddColumn( 'accountname', 'Account', 'left', '', 'chartofaccount.name');
$browser->AddColumn( 'description', 'Description', 'left' );
$browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$browser->AddColumn( 'taxamount', 'GST', 'right', '%0.2lf' );
$browser->AddColumn( 'total', 'Total', 'right', '%0.2lf', 'amount + taxamount' );
$browser->AddTotal( 'amount' );
$browser->AddTotal( 'taxamount' );
$browser->AddTotal( 'total' );
$browser->AddOrder( 'lineseq', 'ASC' );
$browser->SetJoins( "voucherline JOIN chartofaccount USING ( accountcode )" );

$browser->AndWhere( "voucherseq=$id" );
$rowurl = '/browse.php?t=transactions&et=%s&ec=%s&ac=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'entitytype', 'entitycode', 'accountcode', '#even' );
$page_elements[] = $browser;
