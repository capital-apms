package APMS::Model::Invoices;

use strict;

=item all_invoices

Returns all the invoices from the DB

  my $invoices = $self->model->all_invoices();

=cut

sub all_invoices {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT
			invoicestatus,
			invoiceno,
			entitycode,
			entitytype,
			topay,
			todetail
		FROM invoice;
	) );

	$sth->execute();
	my $invoices = $sth->fetchall_arrayref({});
	$sth->finish;

	return $invoices;
}

=item invoices_for_tenant

Returns the invoices_for_tenant for a given tenant

  my $invoices = $self->model->invoices_for_tenant( $tenantcode );

=cut

sub invoices_for_tenant {
	my ( $self, $tenantcode ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT
			invoicestatus,
			invoiceno,
			entitycode,
			entitytype,
			topay,
			todetail
		FROM invoice WHERE
			invoice.entitycode = ?;
	) );

	$sth->execute( $tenantcode );
	my $invoices = $sth->fetchall_arrayref({});
	$sth->finish();

	return $invoices;
}

sub invoice_record {
	my ( $self, $invoiceno ) = @_;

# invoice.attnto,
	my $sth = $self->{dbh}->prepare( qq(
		SELECT
			invoice.blurb,
			invoice.duedate,
			invoice.invoicedate,
			invoice.invoicetype,
			invoice.taxamount,
			invoice.taxapplies,
			invoice.termscode,
			invoice.total,
			invoice.invoicestatus,
			invoice.invoiceno,
			tenant.tenantcode,
			tenant.name AS tenantname,
			invoice.entitycode AS ientitycode,
			invoice.entitytype AS ientitytype,
			invoice.topay,
			invoice.todetail,
			office.gst,
			invoiceline.accountcode,
			invoiceline.accounttext,
			invoiceline.entitycode AS lentitycode,
			invoiceline.entitytype AS lentitytype,
			invoiceline.lineseq,
			invoiceline.percent,
			invoiceline.quantity,
			invoiceline.yourshare
		FROM invoice
		LEFT JOIN invoiceline ON ( invoiceline.invoiceno = invoice.invoiceno )
		LEFT JOIN tenant      ON ( tenant.tenantcode = invoice.entitycode )
		LEFT JOIN office      ON ( office.thisoffice = TRUE )
		WHERE invoice.invoiceno = ? ORDER BY invoiceline.lineseq;
	) );

	$sth->execute( $invoiceno );
	my $invoice = $sth->fetchall_arrayref({});
	$sth->finish();

	return $invoice;
}

sub available_invoice {
	my $self = shift;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT invoiceno FROM invoice ORDER BY invoiceno;
	) );

	$sth->execute();
	my $invoicenos = $sth->fetchall_arrayref({});
	$sth->finish();

	my $last = pop @{ $invoicenos };

	print $last . "\n";
}

sub invoice_terms {
	my $self = shift;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT termscode, description FROM invoiceterms;
	) );

	$sth->execute();
	my $terms = $sth->fetchall_arrayref({});
	$sth->finish;

	return $terms;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

