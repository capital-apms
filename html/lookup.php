<?php
require_once('../inc/always.php');

function logRequestHeaders() {
  global $c;

  /** Log the request headers */
  $lines = apache_request_headers();
  dbg_error_log( "LOG ", "***************** Request Header ****************" );
  dbg_error_log( "LOG ", "%s %s", $_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI'] );
  foreach( $lines AS $k => $v ) {
    if ( $k != 'Authorization' || (isset($c->dbg['password']) && $c->dbg['password'] ) )
    dbg_error_log( "LOG headers", "-->%s: %s", $k, $v );
    else
    dbg_error_log( "LOG headers", "-->%s: %s", $k, 'Delicious tasty password eaten by debugging monster!' );
  }
  dbg_error_log( "LOG ", "******************** Request ********************" );

  // Log the request in all it's gory detail.
  $lines = preg_split( '#[\r\n]+#', $c->raw_post );
  foreach( $lines AS $v ) {
    dbg_error_log( "LOG request", "-->%s", $v );
  }
  unset($lines);
}

if ( !isset($c->raw_post) ) $c->raw_post = file_get_contents( 'php://input');
// if ( (isset($c->dbg['ALL']) && $c->dbg['ALL']) || (isset($c->dbg['request']) && $c->dbg['request']) )
logRequestHeaders();

function Respond( $message="", $content_type="application/json; charset=\"utf-8\"" ) {
  global $session, $c;
  @header( "Content-type: ".$content_type );

  $lines = headers_list();
  dbg_error_log( "LOG ", "***************** Response Header ****************" );
  foreach( $lines AS $v ) {
    dbg_error_log( "LOG headers", "-->%s", $v );
  }
  dbg_error_log( "LOG ", "******************** Response ********************" );
  // Log the request in all it's gory detail.
  $lines = preg_split( '#[\r\n]+#', $message);
  foreach( $lines AS $v ) {
    dbg_error_log( "LOG response", "-->%s", $v );
  }

  header( "Content-Length: ".strlen($message) );
  echo $message;
  
  @ob_flush(); exit(0);
}

function getCreditor( $creditorcode ) {
  $qry = new AwlQuery('SELECT * FROM creditor WHERE creditorcode=?',$creditorcode);
  if ( $qry->Exec() && $qry->rows() > 0 ) {
    $result = $qry->Fetch();
  }
  else {
    $result['name'] = sprintf('Creditor %d not found', $creditorcode);
  }
  return $result;
}

function getDebtor( $code ) {
  $qry = new AwlQuery('SELECT * FROM tenant WHERE tenantcode=?',$code);
  if ( $qry->Exec() && $qry->rows() > 0 ) {
    $result = $qry->Fetch();
  }
  else {
    $result['name'] = sprintf('Tenant %d not found', $code);
  }
  return $result;
}

function getProperty( $code ) {
  $qry = new AwlQuery('SELECT * FROM property WHERE propertycode=?',$code);
  if ( $qry->Exec() && $qry->rows() > 0 ) {
    $result = $qry->Fetch();
  }
  else {
    $result['name'] = sprintf('Property %d not found', $code);
  }
  return $result;
}

function getCompany( $code ) {
  $qry = new AwlQuery('SELECT * FROM company WHERE companycode=?',$code);
  if ( $qry->Exec() && $qry->rows() > 0 ) {
    $result = $qry->Fetch();
    $result['name'] = $result['legalname'];
  }
  else {
    $result['name'] = sprintf('Company %d not found', $code);
  }
  return $result;
}

function getProject( $code ) {
  $qry = new AwlQuery('SELECT * FROM project WHERE projectcode=?',$code);
  if ( $qry->Exec() && $qry->rows() > 0 ) {
    $result = $qry->Fetch();
  }
  else {
    $result['name'] = sprintf('Project %d not found', $code);
  }
  return $result;
}

function getAsset( $code ) {
  $qry = new AwlQuery('SELECT * FROM fixedasset WHERE assetcode=?',$code);
  if ( $qry->Exec() && $qry->rows() > 0 ) {
    $result = $qry->Fetch();
    $result['name'] = substr( $result['description'], 0, 50);
  }
  else {
    $result['name'] = sprintf('Asset %d not found', $code);
  }
  return $result;
}

function getEntity( $code ) {
  $entitytype = strtoupper(substr($code,0,1));
  $entitycode = intval(substr($code,1));
  switch( $entitytype ) {
    case 'C':      return getCreditor($entitycode);
    case 'T':      return getDebtor($entitycode);
    case 'P':      return getProperty($entitycode);
    case 'J':      return getProject($entitycode);
    case 'L':      return getCompany($entitycode);
    case 'F':      return getAsset($entitycode);
    default:
      $result['name'] = sprintf('Entity %s not found', $code);
  }
  return $result;
}

function getAccount( $code ) {
  $qry = new AwlQuery('SELECT * FROM chartofaccount WHERE accountcode=?',$code);
  if ( $qry->Exec() && $qry->rows() > 0 ) {
    $result = $qry->Fetch();
  }
  else {
    $result['name'] = sprintf('Account %05.2lf not found', $code);
  }
  return $result;
}

param_to_global('request','#([a-z0-9_-]*)#i');
param_to_global('code','#([a-z0-9_.-]*)#i');
$result = array();
if ( isset($request) ) {
  switch( $request ) {
    case 'creditor':      $result = getCreditor($code);      break;
    case 'tenant':        $result = getDebtor($code);        break;
    case 'property':      $result = getProperty($code);      break;
    case 'project':       $result = getProject($code);       break;
    case 'company':       $result = getCompany($code);       break;
    case 'asset':         $result = getAsset($code);         break;
    case 'entity':        $result = getEntity($code);        break;
    case 'account':       $result = getAccount($code);       break;
    
    default:
      $result = sprintf("Request '%s' was not understood", $request);
  }
}
else {
  $result = "Request was not understood";
}
Respond(json_encode($result));
