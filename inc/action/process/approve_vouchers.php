<?php
/**
* Component for approving specified vouchers
* @package   apms
* @subpackage   approve_vouchers
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/

// In this case we only want to process when $_POST['submit'] is set.
if ( $widget->IsSubmit() ) return;

param_to_global('approve', '#^\d+#' );

get_office_accounts( 'GST-IN', 'CREDITORS' );

$gst_ec = $office_accounts['GST-IN']->entitycode;
$gst_out_ac = $office_accounts['GST-IN']->accountcode;
$sundry_creditors = $office_accounts['CREDITORS']->accountcode;

require_once( "transaction_helpers.php" );

$in_list = implode( ',', array_keys($approve) );
$sql = <<<EOQ
SELECT voucherseq, creditorcode, date, v.description, taxvalue, goodsvalue,
       vl.entitytype AS et, vl.entitycode AS ec, vl.accountcode AS ac, vl.description AS linedescription, vl.amount AS lineamount
  FROM voucher v JOIN voucherline vl USING (voucherseq)
 WHERE voucherseq IN ( $in_list ) AND voucherstatus = 'U'
 ORDER BY voucherseq, lineseq
EOQ;

$qry = new AwlQuery( $sql );
if ( $qry->Exec("approve_vouchers") && $qry->rows() > 0 ) {
  $batchcode = create_newbatch( 'AUTO', 0, "Creditor vouchers" );
  $documentcode = null;
  $voucherseq = null;
  while( $vl = $qry->Fetch() ) {
    if ( $voucherseq != $vl->voucherseq ) {
      $voucherseq = $vl->voucherseq;
      create_newdocument( $vl->description, sprintf('VCHR %05d', $voucherseq), 'VCHR' );
      $transactioncode = 1;
      create_newtransaction( 'C', $vl->creditorcode, $sundry_creditors, $vl->date, 0 - ($vl->taxvalue + $vl->goodsvalue), $vl->description );
      create_newtransaction( 'L', $gst_ec, $gst_out_ac, $vl->date, $vl->taxvalue, 'GST, '.$vl->description );
    }
    create_newtransaction( $vl->et, $vl->ec, $vl->ac, $vl->date, $vl->lineamount,
                            ($vl->linedescription == $vl->description ? null : $vl->linedescription) );
  }
  $qry->QDo("UPDATE voucher SET voucherstatus = 'A' WHERE voucherseq IN ( $in_list )");
  $sql = "UPDATE newbatch SET documentcount = (SELECT count(1) FROM newdocument d WHERE d.batchcode = newbatch.batchcode), total = (SELECT sum(amount) FROM newaccttrans t WHERE t.batchcode = newbatch.batchcode)  WHERE batchcode = :batchcode";
  $qry = new AwlQuery( $sql, $batchcode );
  if ( !$qry->Exec("approve_vouchers") ) {
    $c->messages[] = "Database transaction failure - please correct problem and retry.";
    $qry->Rollback();
  }
  else {
    $qry->Commit();
  }
}
else {
  $c->messages[] = "No vouchers processed.";
}