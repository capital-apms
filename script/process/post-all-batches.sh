#!/bin/sh
#
# Unpost all APMS batches
#

for B in `psql -qAt -c "SELECT batchcode FROM newbatch where batchtype != 'ACCR' order by batchcode;" apms`; do
  echo $B >>update-errors.log
  ./batch-update.pl --updater 3 --batch $B 2>>update-errors.log | psql -qAt apms;
  psql -qAt -c "vacuum newbatch; vacuum newdocument; vacuum newaccttrans; vacuum accountbalance; vacuum accountsummary; vacuum batch;" apms
  echo "."
done
