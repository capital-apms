package APMS::View::Browser::MenuBrowser;

use strict;
use base qw/
	APMS::View::Browser
/;

sub menu_view {
	my ( $self, $linkcode ) = @_;

	# Populated with a SQL query
	my @data;

	# Fetch the links
	@data = $self->{apms}->model->menu_links( $linkcode );

	my $browser = $self->browse_window(
		fields => [
			{
				name   => 'Seq',
				column => 'Sequence',
				type   => 'Glib::Uint',
				widget => 'Gtk2::CellRendererText'
			}, {
				name   => 'Button Label',
				column => 'ButtonLabel',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}, {
				name   => 'Group',
				column => 'GroupName',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => \@data
	);

	return $browser;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

