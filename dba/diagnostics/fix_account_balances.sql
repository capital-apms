-- This will force all accountbalance and accountsummary rows to have the same
-- balances as the sum of the transactions


UPDATE accountbalance ax
   SET balance = (SELECT sum(a.amount) FROM accttran a
                   WHERE a.entitytype=ax.entitytype AND a.entitycode=ax.entitycode
                     AND a.accountcode=ax.accountcode )
 WHERE balance != (SELECT sum(a.amount) FROM accttran a
                   WHERE a.entitytype=ax.entitytype AND a.entitycode=ax.entitycode
                     AND a.accountcode=ax.accountcode AND a.monthcode=ax.monthcode );

INSERT INTO accountbalance( entitytype, entitycode, accountcode, monthcode, balance )
SELECT entitytype, entitycode, accountcode, monthcode, sum(amount) AS balance 
  FROM accttran a
 WHERE NOT EXISTS(SELECT 1 FROM accountbalance ax
                   WHERE a.entitytype=ax.entitytype AND a.entitycode=ax.entitycode
                     AND a.accountcode=ax.accountcode AND a.monthcode=ax.monthcode )
 GROUP BY entitytype, entitycode, accountcode, monthcode
 HAVING sum(amount) != 0;

UPDATE accountsummary ax
   SET balance = (SELECT sum(a.amount) FROM accttran a
                   WHERE a.entitytype=ax.entitytype AND a.entitycode=ax.entitycode
                     AND a.accountcode=ax.accountcode )
 WHERE balance != (SELECT sum(a.amount) FROM accttran a
                   WHERE a.entitytype=ax.entitytype AND a.entitycode=ax.entitycode
                     AND a.accountcode=ax.accountcode );

INSERT INTO accountsummary( entitytype, entitycode, accountcode, balance )
SELECT entitytype, entitycode, accountcode, sum(amount) AS balance 
  FROM accttran a
 WHERE NOT EXISTS(SELECT 1 FROM accountsummary ax
                   WHERE a.entitytype=ax.entitytype AND a.entitycode=ax.entitycode
                     AND a.accountcode=ax.accountcode )
 GROUP BY entitytype, entitycode, accountcode
 HAVING sum(amount) != 0;
