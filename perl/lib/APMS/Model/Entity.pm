package APMS::Model::Entity;

use strict;

=item get_entity_name

Returns the entity name given a type and ID

  my $entityname = $self->model->get_entity_name( $type, $id );

=cut

sub get_entity_name {
	my ( $self, $type, $code ) = @_;

	my $sth;

	if ( $type eq 'P' ) {
		$sth = $self->{dbh}->prepare( qq(
			SELECT name FROM property WHERE propertycode = ?;
		) );
	}
	elsif ( $type eq 'L' ) {
		$sth = $self->{dbh}->prepare( qq(
			SELECT legalname FROM company WHERE companycode = ?;
		) );
	}

	$sth->execute( $code );
	my $row = $sth->fetchrow_hashref();
	my $name = $row->{legalname};
	$sth->finish;

	return $name;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

