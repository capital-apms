<?php
require_once('../inc/always.php');

require_once('classViewer.php');
require_once('classLinks.php');

$component = (isset($_GET['t']) ? clean_string($_GET['t']) : null);
$id = (isset($_GET['id']) ? clean_string($_GET['id']) : null);;

require_once('apms_menus.php');

$c->stylesheets[] = "css/view.css";
$page_elements = array();

if ( ! @include_once( "view/$component.php" ) ) {
  $c->messages[] = "view/$component not found";
}

///////////////////////////////////////////////////////
// From here we start rendering the page to the user...
///////////////////////////////////////////////////////
include('page-header.php');

// Page elements could be an array of viewers, browsers or something else
// that supports the Render() method...
$heading_level = null;
foreach( $page_elements AS $k => $page_element ) {
  if ( is_object($page_element) ) {
    echo $page_element->Render($heading_level);
    $heading_level = 'h2';
  }
  else {
    echo $page_element;
  }
}

if (function_exists("post_render_function")) {
  post_render_function();
}

include('page-footer.php');
