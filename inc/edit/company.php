<?php

param_to_global('id', 'int', 'companycode' );

// Editor component for company records
$editor = new Editor("Company");
$editor->AddField( 'companycode' );
$editor->AddField( 'active' );
$editor->AddField( 'legalname' );
$editor->AddField( 'parentcode' );
$editor->AddField( 'clientcode' );
$editor->AddField( 'shortname' );
$editor->AddField( 'registeredaddress' );
$editor->AddField( 'registeredno' );
$editor->AddField( 'incorporationdate' );
$editor->AddField( 'taxno' );
$editor->SetBaseTable( "company" );

$editor->SetWhere( "companycode=$id" );

if ( isset($_POST['submit']) ) {
  $editor->WhereNewRecord( "companycode=".intval($_POST['companycode']) );
  $editor->Write();
  if ( $editor->Action == "update" ) {
    $editor->SetWhere( 'companycode='.intval($_POST['companycode']) );
    $editor->GetRecord();
  }
}
else {
  $editor->GetRecord();
}
$id = $editor->Record->{'companycode'};
$submittype = (isset($editor->Record->{'legalname'}) ? 'Update' : 'Create' );

include_once("menus_entityaccount.php");

$template = <<<EOTEMPLATE
<form method="POST" enctype="multipart/form-data" id="entry">
<table>
 <tr>
  <th class="right">Company:</th>
  <td class="left">##companycode.value##<input type="hidden" name="companycode" value="##companycode.enc##"> ##shortname.input.12##</td>
 </tr>
 <tr>
  <th class="right">Legal Name:</th>
  <td class="left">##legalname.input.50##</td>
 </tr>
 <tr>
  <th class="right">Parent:</th>
  <td class="left">##parentcode.input.5##</td>
 </tr>
 <tr>
  <th class="right">Registered Address:</th>
  <td class="left">##registeredaddress.textarea.49x4##</td>
 </tr>
 <tr>
  <th class="right">Company No:</th>
  <td style="padding: 0; margin: 0;">
   <table>
    <tr>
     <td class="left">##registeredno.input.15##</td>
     <th class="right">Incorporation:</th>
     <td class="left">##incorporationdate.input.12##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">IRD no:</th>
  <td style="padding: 0; margin: 0;">
   <table>
    <tr>
     <td class="left">##taxno.input.15##</td>
     <th class="right">Active:</th>
     <td class="left">##active.checkbox##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left"><input type="submit" name="submit" value="$submittype"></td>
 </tr>
</table>
<form>

EOTEMPLATE;

$editor->SetTemplate( $template );

$c->page_title = $editor->Title("Company: ".$editor->Record->{'legalname'});
