/**
 * 
 */

// Specifically for skipping importable transactions, we have an "X" type here.
apms.allowedTypes = new RegExp(/[CTLPJFX]/i);

function getDocumentTotal(doc) {
	docTotal = new Number(0);
	i = 1; 
	do {
		moneyField = document.getElementById("txn["+doc+"]["+i+"][amount]");
		if ( moneyField ) {
			amount = new Number(moneyField.value);
			if ( amount ) docTotal += amount;
			i++;
		}
	} while( moneyField );
	return docTotal;
}


function checkDocumentTotal(doc) {
	matching = false;
	try {
		totalField = $("#wanttotal-"+doc).get(0);
		totalField.setAttribute("class","money");
		wantTotal = new Number(totalField.value);
		gotTotalField = $("#gottotal-"+doc).get(0);
		gotTotalField.setAttribute("class","money");
		docTotal = getDocumentTotal(doc);
		matching = (wantTotal == docTotal);
		$(totalField).css("background-color",(matching ? "#40f040" : "#ffc040" ));
		gotTotalField.value = docTotal;
//		alert("Total is now "+docTotal+" vs. "+wantTotal+ " they are " + (wantTotal == docTotal ? "" : "not " )+"equal.");

		visibility = ( !matching || (+i > 2) ? "text" : "hidden");
		totalField.setAttribute("type",visibility);
		gotTotalField.setAttribute("type",visibility);
		if ( matching ) $("#txn-"+doc).removeClass("error");
	} catch(e) {
		alert( e );
		return false;
	};
	return matching;
}

function splitDocument(doc){
	rows = new Array();
	txn = 1;
	do {
		idSelector = "#row-"+doc+"-"+txn; 
		row = $(idSelector).html();
		if ( row ) rows[txn++] = row;
	} while ( row );
	row = document.getElementById("row-"+doc+"-1");

	new_row = row.innerHTML;
	replaceTxn = new RegExp("txn\\["+doc+"\\]\\[1\\]","g")
	new_row = new_row.replace(replaceTxn,"txn["+doc+"]["+txn+"]");
//  alert("New Row "+txn+" is " +new_row);
	new_row = '<tr class="'+row.getAttribute("class")+'" id="row-'+doc+'-'+txn+'">'+new_row+'</tr>';
	new_row_id = new_row.id;
	rows[txn] = new_row;
	$("#txn-"+doc).append(new_row);

	rowButton = document.getElementById("split-txn["+doc+"]["+txn+"]");
	rowButton.innerHTML = "Join";
	rowButton.setAttribute("onClick","joinDocument("+doc+","+txn+")");
	$("#split-txn["+doc+"]["+txn+"]").text("Join");
	moneyField = document.getElementById("txn["+doc+"]["+txn+"][amount]");
	moneyField.setAttribute("onChange","checkDocumentTotal("+doc+")");

	$("#row-"+doc+"-"+txn+" "+"input").each(function(){
		$(this).on("focusin",function(e){e.target.select();});
		$(this).on("keypress",nextFieldOnEnter);
	});

	newMoney = document.getElementById("txn["+doc+"]["+txn+"][amount]");
	if ( newMoney ) {
		totalField = $("#wanttotal-"+doc).get(0);
		wantTotal = new Number(totalField.value);
		newMoney.value = 0;
		gotTotal = getDocumentTotal(doc);
//		alert("Want: "+wantTotal+",  Got: "+gotTotal+", Diff: "+(wantTotal - gotTotal));
		newMoney.value = (wantTotal - gotTotal).toFixed(2);
	}
	checkDocumentTotal(doc);
}


function joinDocument(doc,deleteTxn) {
	rows = new Array();
	txn = 1;
	do {
		idSelector = "#row-"+doc+"-"+txn; 
		row = $(idSelector).html();
		if ( row ) rows[txn++] = row;
	} while ( row );
	row = document.getElementById("row-"+doc+"-"+deleteTxn);
	row.parentNode.removeChild(row);

	for( i=deleteTxn+1; i< txn; i++ ) {
		idSelector = "#row-"+doc+"-"+i; 
		row = $(idSelector).get(0);
		if ( !row ) break;
		j = i-1;
		row.id="row-"+doc+"-"+j;
		inputs = $("#row-"+doc+"-"+j+" :input").get();
		replaceTxn = new RegExp("txn\\["+doc+"\\]\\[\\d+\\]","g");
		for( k=0; k<inputs.length; k++ ) {
			replacement = "txn["+doc+"]["+j+"]";
			inputs[k].name = inputs[k].name.replace(replaceTxn,replacement); 
			if ( inputs[k].id ) inputs[k].id = inputs[k].id.replace(replaceTxn,replacement); 
		}
		rowButtonId = "split-txn["+doc+"]["+i+"]";
		rowButton = document.getElementById(rowButtonId);
		if ( !rowButton ) {
			rowButton = document.getElementById("split-txn["+doc+"]["+j+"]");
		}
		else {
			rowButton.id = "split-txn["+doc+"]["+j+"]";
		}
		if ( !rowButton ) {
			alert("Row button not found for "+rowButtonId);
			continue;
		}
		rowButton.innerHTML = "Join";
		rowButton.setAttribute("onClick","joinDocument("+doc+","+j+")");
	}
	checkDocumentTotal(doc);
}


function validateAll() {
	doc = 1;
	firstBad = 0;
	do {
		et = document.getElementById("txn["+doc+"][1][entitytype]");
		if ( !et ) break;
		txn = 1;
		do {
			entityTypeField = document.getElementById("txn["+doc+"]["+txn+"][entitytype]");
			if ( entityTypeField && !onEntityTypeChange(entityTypeField) && firstBad == 0 ) firstBad = doc;
			txn++;
		} while( entityTypeField );
		if ( !checkDocumentTotal(doc) ) {
			if ( firstBad == 0 ) firstBad = doc;
			$("#txn-"+doc).addClass("error");
		}
		doc++;
	} while( et );

	if ( firstBad == 0 ) return true;

	alert("Errors in the form have been highlighted.");
	
	// Otherwise try and move the focus there.
	entityTypeField = document.getElementById("txn["+firstBad+"][1][entitytype]");
	entityTypeField.focus();
	
	return false;
}