<?php
require_once('../inc/always.php');
require_once('apms_menus.php');

$c->stylesheets[] = "css/home_menus.css";

include('page-header.php');

/** @todo Might have to to some str_replace() magic on this to really make it work properly. */
$home_menus = $main_menu->RenderAsCSS();

echo <<<EOPAGE
<div id="content">
<h1>$c->system_name</h1>
<p>Choose from the menus at the top of the page, or from the expanded list below.</p>
<div id="home">
$home_menus
</div>
</div>
EOPAGE;

include('page-footer.php');
