-- A bunch of badly coded PAYE transactions
update accttran set accountcode = 7560 where entitytype = 'L' and entitycode = 10 and accountcode = 7590 and monthcode in ( 1091, 1092) and batchcode not in ( 7385, 7387 );

-- Transaction coded incorrectly to insurance rather than travel

update accttran set accountcode = 2600 where batchcode = 6166 and documentcode = 9 and txncode = 3 and accountcode = 2380;
