package APMS::Model::Tenants;

use strict;

=item tenant_record

Return the full tenant record for a tenant, including address details

=cut

sub tenant_record {
	my ( $self, $tenantcode ) = @_;

	my $sth = $self->{dbh}->prepare( qq/
		SELECT
			tenant.tenantcode,
			tenant.active,
			tenant.legalname,
			tenant.name,
			tenant.entitytype,
			tenant.entitycode,
			tenant.paymentstyle,
			tenant.debtclassification,
			tenant.quality,
			tenant.varianceclassification,
			tenant.billingcontact,
			tenant.propertycontact,
			tenant.ah1contact,
			tenant.ah2contact,
			tenant.businesstype
		FROM tenant
		LEFT JOIN person ON
			( tenant.billingcontact = person.personcode )
		LEFT JOIN postaldetail ON
			( postaldetail.personcode = person.personcode )
		WHERE tenant.tenantcode = ?;
	/ );

	$sth->execute( $tenantcode );
	my $tenantrecord = $sth->fetchrow_hashref();
	$sth->finish;

	if ( $tenantrecord->{entitytype} =~ /^L|P$/ ) {
		$tenantrecord->{entityname} = $self->get_entity_name( $tenantrecord->{entitytype}, $tenantrecord->{entitycode} );
	}

	return $tenantrecord;
}

=item all_tenants

Returns all tenants from the DB

  my @tenants = $self->model->all_tenants();

=cut

sub all_tenants {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT tenantcode, active, entitytype, entitycode, name, legalname, cfbalance FROM tenant;
	) );

	$sth->execute();
	my $companies = $sth->fetchall_arrayref({});
	$sth->finish;

	return $companies;
}

=item active_tenants

Returns the active tenants from the DB

  my @tenants = $self->model->active_tenants();

=cut

sub active_tenants {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT tenantcode, active, entitytype, entitycode, name, legalname, cfbalance FROM tenant
			WHERE active = TRUE;
	) );

	$sth->execute();
	my $companies = $sth->fetchall_arrayref({});
	$sth->finish;

	return $companies;
}

=item inactive_tenants

Returns the inactive tenants from the DB

  my @tenants = $self->model->inactive_tenants();

=cut

sub inactive_tenants {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT tenantcode, active, entitytype, entitycode, name, legalname, cfbalance FROM tenant
			WHERE active = FALSE;
	) );

	$sth->execute();
	my $companies = $sth->fetchall_arrayref({});
	$sth->finish;

	return $companies;
}

sub tenant_paymentstyles {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT paymentstyle, description FROM paymentstyle WHERE receipts = TRUE;
	) );

	my $paymentstyles;
	$sth->execute();

	# Create a third column from the first two for combos
	while ( my $paymentstyle = $sth->fetchrow_hashref() ) {
		my $temp = {
			paymentstyle => $paymentstyle->{paymentstyle},
			description  => $paymentstyle->{description},
			paydesc      => $paymentstyle->{paymentstyle} . " - " . $paymentstyle->{description}
		};
		push @{ $paymentstyles }, $temp;
	}
	$sth->finish;

	return $paymentstyles;
}

sub tenant_debt_classifications {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT debtclassification, description FROM debtclassification;
	) );

	my $debtclassification;
	$sth->execute();

	# Create a third column from the first two for combos
	while ( my $paymentstyle = $sth->fetchrow_hashref() ) {
		my $temp = {
			debtclassification => $paymentstyle->{debtclassification},
			description        => $paymentstyle->{description},
			debtdesc           => $paymentstyle->{debtclassification} . " - " . $paymentstyle->{description}
		};
		push @{ $debtclassification }, $temp;
	}
	$sth->finish;

	return $debtclassification;
}

sub tenant_variance_classifications {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT varianceclassification, description FROM varianceclassification;
	) );

	my $varclassification;
	$sth->execute();

	# Create a third column from the first two for combos
	while ( my $varclass = $sth->fetchrow_hashref() ) {
		my $temp = {
			varianceclassification => $varclass->{varianceclassification},
			description            => $varclass->{description},
			variancedesc           => $varclass->{varianceclassification} . " - " . $varclass->{description}
		};
		push @{ $varclassification }, $temp;
	}
	$sth->finish;

	return $varclassification;
}

=item tenant_contacts

The contact types that appear in the tenant view window

=cut

sub tenant_contacts {
	my $self = shift;

	my $tenantcontacts = [
		{ name => 'Accounting Matters',      field => 'billingcontact' },
		{ name => 'Property Matters',        field => 'propertycontact' },
		{ name => 'After Hours Emergency 1', field => 'ah1contact' },
		{ name => 'After Hours Emergency 2', field => 'ah2contact' }
	];

	return $tenantcontacts;
}

=item available_tenantcode

Return the last available tenantcode. If tenant code is supplied then check
it, and return the last available if that code is already used.

=cut

sub available_tenantcode {
	my ( $self, $tenantcode ) = @_;

	my $getnewcode = 0;
	my $sth;

	if ( $tenantcode =~ /^\d+$/ ) {
		# Check if the supplied code exists already
		$sth = $self->{dbh}->prepare( qq(
			SELECT tenantcode FROM tenant WHERE tenantcode = ?;
		) );
		$sth->execute( $tenantcode );
		my $tenantrec = $sth->fetchall_arrayref({});

		if ( scalar @{ $tenantrec } > 0 ) {
			$getnewcode = 1;
		}
		$sth->finish;
	}
	else {
		$getnewcode = 1;
	}

	if ( $getnewcode ) {
		# Get a new code
		$sth = $self->{dbh}->prepare( qq(
			SELECT tenantcode FROM tenant ORDER BY tenantcode;
		) );
		$sth->execute();
		my $tenantrec = $sth->fetchall_arrayref({});
		my $lastrec = $tenantrec->[-1];
		$tenantcode = $lastrec->{tenantcode};
		$sth->finish;
	}

	return $tenantcode;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

