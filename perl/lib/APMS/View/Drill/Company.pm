package APMS::View::Drill::Company;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Panel;
use APMS::View::Browser;
use APMS::View::Buttons;

use base qw/ APMS::View::Drill /;

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	# Create the panel
	$self->{panel} = APMS::View::Panel->radio_choose(
		{
			name     => 'Filter By',
			options  => [ 'Active', 'All' ]
		},
		{
			name     => 'Sort By',
			options  => [ 'Code', 'Short Name' ]
		}
	);

	$self->{browser} = APMS::View::Browser->browse_window(
		fields => [
			{
				name   => 'Short Name',
				column => 'shortname',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				dosort => 1
			}, {
				name   => 'Active',
				column => 'active',
				type   => 'Glib::String',	
				widget => 'Gtk2::CellRendererText'
			}, {
				name   => 'Code',
				column => 'companycode',
				type   => 'Glib::Uint',
				widget => 'Gtk2::CellRendererText',
				dosort => 1
			}, {
				name   => 'Legal Name',
				column => 'legalname',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				expand => 1,
				minw   => 100,
				dosort => 1
			}, {
				name   => 'Tax No',
				column => 'taxno',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => $data{companylist},
	);

	$self->{buttons} = APMS::View::Buttons->button_pack( 'H', $data{buttons} );

	$self->drill_window(
		title     => $data{title},
		panel     => $self->{panel},
		browser   => $self->{browser},
		buttons   => $self->{buttons}
	);

	return $self;
}

=item set_selected

Callback for when the user double clicks a row

=cut

sub set_selected {
	my ( $self, $view, $path, $column ) = @_;

	my $treeselection = $view->get_selection();
	my $row = $treeselection->get_selected();

	print "Row: $row\n";
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

