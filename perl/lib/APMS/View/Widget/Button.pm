package APMS::View::Widget::Button;

sub save_button {
	my $self = shift;

    my $save = Gtk2::Button->new();
	$save->set_size_request( 90, 30 );

	my $savealign = Gtk2::Alignment->new( 0.5, 0.5, 0, 0 );
	$save->add( $savealign );

	my $savebox = Gtk2::HBox->new( 0, 2 );
	$savealign->add( $savebox );

	my $saveimage = Gtk2::Image->new_from_stock( 'gtk-save', 'button' );
	my $savelabel = Gtk2::Label->new( 'Save' );

	$savebox->pack_start( $saveimage, 0, 0, 0 );
	$savebox->pack_start( $savelabel, 0, 0, 0 );

	return $save;
}

sub cancel_button {
	my $self = shift;

    my $cancel = Gtk2::Button->new();
    $cancel->set_size_request( 90, 30 );

    my $cancelalign = Gtk2::Alignment->new( 0.5, 0.5, 0, 0 );
    $cancel->add( $cancelalign );

    my $cancelbox = Gtk2::HBox->new( 0, 2 );
    $cancelalign->add( $cancelbox );

    my $cancelimage = Gtk2::Image->new_from_stock( 'gtk-cancel', 'button' );
    my $cancellabel = Gtk2::Label->new( 'Cancel' );

    $cancelbox->pack_start( $cancelimage, 0, 0, 0 );
    $cancelbox->pack_start( $cancellabel, 0, 0, 0 );

	return $cancel;
}

sub add_button {
	my $self = shift;

    my $cancel = Gtk2::Button->new();
    $cancel->set_size_request( 90, 30 );

    my $cancelalign = Gtk2::Alignment->new( 0.5, 0.5, 0, 0 );
    $cancel->add( $cancelalign );

    my $cancelbox = Gtk2::HBox->new( 0, 2 );
    $cancelalign->add( $cancelbox );

    my $cancelimage = Gtk2::Image->new_from_stock( 'gtk-add', 'button' );
    my $cancellabel = Gtk2::Label->new( 'Add' );

    $cancelbox->pack_start( $cancelimage, 0, 0, 0 );
    $cancelbox->pack_start( $cancellabel, 0, 0, 0 );

	return $cancel;
}

sub jump_to_button {
	my $self = shift;

	my $button = Gtk2::Button->new();
    my $gotoimage = Gtk2::Image->new_from_stock( 'gtk-jump-to', 'button' );
    $gotoimage->set_alignment( 0.5, 0.5 );
    $button->add( $gotoimage );

	return $button;
}

sub clear_button {
	my $self = shift;

	my $clear = Gtk2::Button->new();
	$clear->set_size_request( 90, 30 );

	my $clearalign = Gtk2::Alignment->new( 0.5, 0.5, 0, 0 );
	$clear->add( $clearalign );

	my $clearbox = Gtk2::HBox->new( 0, 2 );
	$clearalign->add( $clearbox );

	my $clearimage = Gtk2::Image->new_from_stock( 'gtk-clear', 'button' );
	my $clearlabel = Gtk2::Label->new( 'Clear' );

	$clearbox->pack_start( $clearimage, 0, 0, 0 );
	$clearbox->pack_start( $clearlabel, 0, 0, 0 );

	return $clear;
}

sub date_selector {
	my $self = shift;

	my $dateselect = Gtk2::HBox->new( 0, 5 );
	my $monthbox = APMS::View::Widget::Combo->combo_select(
		fields => [
			{
				column => 'month',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => [
			{ month => 'Jan' },
			{ month => 'Feb' },
			{ month => 'Mar' },
			{ month => 'Apr' },
			{ month => 'May' },
			{ month => 'Jun' },
			{ month => 'Jul' },
			{ month => 'Aug' },
			{ month => 'Sep' },
			{ month => 'Oct' },	
			{ month => 'Nov' },
			{ month => 'Dec' }
		]
	);
	$dateselect->pack_start( $monthbox, 0, 0, 0 );

	return $dateselect;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

