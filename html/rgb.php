<html><head><meta/><title>RGB Colour table</title></head>
<body>
  <table width="100%">
    <tr>
      <td style='color: #FFF; background: #000000'>#000000</td>
      <td style='color: #FFF; background: #000000'>#000000</td>
      <td>Black </td>
    </tr><tr>
      <td style='color: #FFF; background: #000080'>#000080</td>
      <td style='color: #FFF; background: #0F0F0F'>#0F0F0F</td>
      <td>Navy </td>
    </tr><tr>

      <td style='color: #FFF; background: #00008B'>#00008B</td>
      <td style='color: #FFF; background: #101010'>#101010</td>
      <td>Dark blue </td>
    </tr><tr>
      <td style='color: #FFF; background: #0000CD'>#0000CD</td>
      <td style='color: #FFF; background: #171717'>#171717</td>
      <td>Medium blue </td>
    </tr><tr>
      <td style='color: #FFF; background: #0000FF'>#0000FF</td>

      <td style='color: #FFF; background: #1D1D1D'>#1D1D1D</td>
      <td>Blue </td>
    </tr><tr>
      <td style='color: #FFF; background: #006400'>#006400</td>
      <td style='color: #FFF; background: #3B3B3B'>#3B3B3B</td>
      <td>Dark green </td>
    </tr><tr>
      <td style='color: #FFF; background: #008000'>#008000</td>
      <td style='color: #FFF; background: #4B4B4B'>#4B4B4B</td>

      <td>Green </td>
    </tr><tr>
      <td style='color: #FFF; background: #008080'>#008080</td>
      <td style='color: #FFF; background: #5A5A5A'>#5A5A5A</td>
      <td>Teal </td>
    </tr><tr>
      <td style='color: #FFF; background: #008B8B'>#008B8B</td>
      <td style='color: #FFF; background: #616161'>#616161</td>
      <td>Dark cyan </td>

    </tr><tr>
      <td style='color: #000; background: #00BFFF'>#00BFFF</td>
      <td style='color: #000; background: #8D8D8D'>#8D8D8D</td>
      <td>Deep sky blue </td>
    </tr><tr>
      <td style='color: #000; background: #00DED1'>#00DED1</td>
      <td style='color: #000; background: #9A9A9A'>#9A9A9A</td>
      <td>Dark turquoise </td>
    </tr><tr>

      <td style='color: #000; background: #00FA9A'>#00FA9A</td>
      <td style='color: #000; background: #A4A4A4'>#A4A4A4</td>
      <td>Medium spring green </td>
    </tr><tr>
      <td style='color: #000; background: #00FF00'>#00FF00</td>
      <td style='color: #000; background: #969696'>#969696</td>
      <td>Lime </td>
    </tr><tr>
      <td style='color: #000; background: #00FF7F'>#00FF7F</td>

      <td style='color: #000; background: #A4A4A4'>#A4A4A4</td>
      <td>Spring green </td>
    </tr><tr>
      <td style='color: #000; background: #00FFFF'>#00FFFF</td>
      <td style='color: #000; background: #B3B3B3'>#B3B3B3</td>
      <td>Cyan </td>
    </tr><tr>
      <td style='color: #FFF; background: #191970'>#191970</td>
      <td style='color: #FFF; background: #232323'>#232323</td>

      <td>Midnight blue </td>
    </tr><tr>
      <td style='color: #FFF; background: #1E90FF'>#1E90FF</td>
      <td style='color: #FFF; background: #7B7B7B'>#7B7B7B</td>
      <td>Dodger blue </td>
    </tr><tr>
      <td style='color: #000; background: #20B2AA'>#20B2AA</td>
      <td style='color: #000; background: #858585'>#858585</td>
      <td>Light seagreen </td>

    </tr><tr>
      <td style='color: #FFF; background: #228B22'>#228B22</td>
      <td style='color: #FFF; background: #606060'>#606060</td>
      <td>Forest green </td>
    </tr><tr>
      <td style='color: #FFF; background: #2E8B57'>#2E8B57</td>
      <td style='color: #FFF; background: #696969'>#696969</td>
      <td>Sea green </td>
    </tr><tr>

      <td style='color: #FFF; background: #2F4F4F'>#2F4F4F</td>
      <td style='color: #FFF; background: #454545'>#454545</td>
      <td>Dark slate gray </td>
    </tr><tr>
      <td style='color: #000; background: #32CD32'>#32CD32</td>
      <td style='color: #000; background: #8D8D8D'>#8D8D8D</td>
      <td>Lime green </td>
    </tr><tr>
      <td style='color: #000; background: #3CB371'>#3CB371</td>

      <td style='color: #000; background: #888888'>#888888</td>
      <td>Medium sea green </td>
    </tr><tr>
      <td style='color: #000; background: #40E0D0'>#40E0D0</td>
      <td style='color: #000; background: #AEAEAE'>#AEAEAE</td>
      <td>Turquoise </td>
    </tr><tr>
      <td style='color: #FFF; background: #4169E1'>#4169E1</td>
      <td style='color: #FFF; background: #6B6B6B'>#6B6B6B</td>

      <td>Royal blue </td>
    </tr><tr>
      <td style='color: #FFF; background: #4682B4'>#4682B4</td>
      <td style='color: #FFF; background: #767676'>#767676</td>
      <td>Steelblue </td>
    </tr><tr>
      <td style='color: #FFF; background: #483D8B'>#483D8B</td>
      <td style='color: #FFF; background: #494949'>#494949</td>
      <td>Dark slate blue </td>

    </tr><tr>
      <td style='color: #000; background: #48D1CC'>#48D1CC</td>
      <td style='color: #000; background: #A7A7A7'>#A7A7A7</td>
      <td>Medium turquoise </td>
    </tr><tr>
      <td style='color: #FFF; background: #4B0082'>#4B0082</td>
      <td style='color: #FFF; background: #252525'>#252525</td>
      <td>Indigo </td>
    </tr><tr>

      <td style='color: #FFF; background: #556B2F'>#556B2F</td>
      <td style='color: #FFF; background: #5E5E5E'>#5E5E5E</td>
      <td>Dark olive green </td>
    </tr><tr>
      <td style='color: #000; background: #5F9EA0'>#5F9EA0</td>
      <td style='color: #000; background: #8B8B8B'>#8B8B8B</td>
      <td>Cadet blue </td>
    </tr><tr>
      <td style='color: #000; background: #6495ED'>#6495ED</td>

      <td style='color: #000; background: #909090'>#909090</td>
      <td>Cornflower blue </td>
    </tr><tr>
      <td style='color: #000; background: #66CDAA'>#66CDAA</td>
      <td style='color: #000; background: #AAAAAA'>#AAAAAA</td>
      <td>Medium aquamarine </td>
    </tr><tr>
      <td style='color: #FFF; background: #696969'>#696969</td>
      <td style='color: #FFF; background: #696969'>#696969</td>

      <td>Dim gray </td>
    </tr><tr>
      <td style='color: #FFF; background: #6A5ACD'>#6A5ACD</td>
      <td style='color: #FFF; background: #6C6C6C'>#6C6C6C</td>
      <td>Slate blue </td>
    </tr><tr>
      <td style='color: #FFF; background: #6B8E23'>#6B8E23</td>
      <td style='color: #FFF; background: #777777'>#777777</td>
      <td>Olive drab </td>

    </tr><tr>
      <td style='color: #000; background: #778899'>#778899</td>
      <td style='color: #000; background: #858585'>#858585</td>
      <td>Light slate gray </td>
    </tr><tr>
      <td style='color: #FFF; background: #7B68EE'>#7B68EE</td>
      <td style='color: #FFF; background: #7D7D7D'>#7D7D7D</td>
      <td>Medium slate blue </td>
    </tr><tr>

      <td style='color: #000; background: #7CFC00'>#7CFC00</td>
      <td style='color: #000; background: #B9B9B9'>#B9B9B9</td>
      <td>Lawngreen </td>
    </tr><tr>
      <td style='color: #000; background: #7FFF00'>#7FFF00</td>
      <td style='color: #000; background: #BCBCBC'>#BCBCBC</td>
      <td>Chartreuse </td>
    </tr><tr>
      <td style='color: #000; background: #7FFFD4'>#7FFFD4</td>

      <td style='color: #000; background: #D4D4D4'>#D4D4D4</td>
      <td>Aquamarine </td>
    </tr><tr>
      <td style='color: #FFF; background: #800000'>#800000</td>
      <td style='color: #FFF; background: #262626'>#262626</td>
      <td>Maroon </td>
    </tr><tr>
      <td style='color: #FFF; background: #800080'>#800080</td>
      <td style='color: #FFF; background: #353535'>#353535</td>

      <td>Purple </td>
    </tr><tr>
      <td style='color: #FFF; background: #808080'>#808080</td>
      <td style='color: #FFF; background: #808080'>#808080</td>
      <td>Gray </td>
    </tr><tr>
      <td style='color: #000; background: #87CEEB'>#87CEEB</td>
      <td style='color: #000; background: #BCBCBC'>#BCBCBC</td>
      <td>Sky blue </td>

    </tr><tr>
      <td style='color: #000; background: #87CEFA'>#87CEFA</td>
      <td style='color: #000; background: #BEBEBE'>#BEBEBE</td>
      <td>Light sky blue </td>
    </tr><tr>
      <td style='color: #FFF; background: #8A2BE2'>#8A2BE2</td>
      <td style='color: #FFF; background: #5C5C5C'>#5C5C5C</td>
      <td>Blue violet </td>
    </tr><tr>

      <td style='color: #FFF; background: #8B0000'>#8B0000</td>
      <td style='color: #FFF; background: #2A2A2A'>#2A2A2A</td>
      <td>Dark red </td>
    </tr><tr>
      <td style='color: #FFF; background: #8B008B'>#8B008B</td>
      <td style='color: #FFF; background: #393939'>#393939</td>
      <td>Dark magenta </td>
    </tr><tr>
      <td style='color: #FFF; background: #8B4513'>#8B4513</td>

      <td style='color: #FFF; background: #545454'>#545454</td>
      <td>Saddle brown </td>
    </tr><tr>
      <td style='color: #000; background: #8DBC8F'>#8DBC8F</td>
      <td style='color: #000; background: #A9A9A9'>#A9A9A9</td>
      <td>Dark seagreen </td>
    </tr><tr>
      <td style='color: #000; background: #90EE90'>#90EE90</td>
      <td style='color: #000; background: #C7C7C7'>#C7C7C7</td>

      <td>Light green </td>
    </tr><tr>
      <td style='color: #000; background: #9370DB'>#9370DB</td>
      <td style='color: #000; background: #878787'>#878787</td>
      <td>Medium purple </td>
    </tr><tr>
      <td style='color: #FFF; background: #9400D3'>#9400D3</td>
      <td style='color: #FFF; background: #444444'>#444444</td>
      <td>Dark violet </td>

    </tr><tr>
      <td style='color: #000; background: #98FB98'>#98FB98</td>
      <td style='color: #000; background: #D2D2D2'>#D2D2D2</td>
      <td>Pale green </td>
    </tr><tr>
      <td style='color: #FFF; background: #9932CC'>#9932CC</td>
      <td style='color: #FFF; background: #626262'>#626262</td>
      <td>Dark orchid </td>
    </tr><tr>

      <td style='color: #000; background: #9ACD32'>#9ACD32</td>
      <td style='color: #000; background: #ACACAC'>#ACACAC</td>
      <td>Yellow green </td>
    </tr><tr>
      <td style='color: #FFF; background: #A0522D'>#A0522D</td>
      <td style='color: #FFF; background: #656565'>#656565</td>
      <td>Sienna </td>
    </tr><tr>
      <td style='color: #FFF; background: #A52A2A'>#A52A2A</td>

      <td style='color: #FFF; background: #4F4F4F'>#4F4F4F</td>
      <td>Brown </td>
    </tr><tr>
      <td style='color: #000; background: #A9A9A9'>#A9A9A9</td>
      <td style='color: #000; background: #A9A9A9'>#A9A9A9</td>
      <td>Dark gray </td>
    </tr><tr>
      <td style='color: #000; background: #ADD8E6'>#ADD8E6</td>
      <td style='color: #000; background: #CDCDCD'>#CDCDCD</td>

      <td>Light blue </td>
    </tr><tr>
      <td style='color: #000; background: #ADFF2F'>#ADFF2F</td>
      <td style='color: #000; background: #CFCFCF'>#CFCFCF</td>
      <td>Green yellow </td>
    </tr><tr>
      <td style='color: #000; background: #AFEEEE'>#AFEEEE</td>
      <td style='color: #000; background: #DBDBDB'>#DBDBDB</td>
      <td>Pale turquoise </td>

    </tr><tr>
      <td style='color: #000; background: #B0C4DE'>#B0C4DE</td>
      <td style='color: #000; background: #C1C1C1'>#C1C1C1</td>
      <td>Light steel blue </td>
    </tr><tr>
      <td style='color: #000; background: #B0E0E6'>#B0E0E6</td>
      <td style='color: #000; background: #D2D2D2'>#D2D2D2</td>
      <td>Powder blue </td>
    </tr><tr>

      <td style='color: #FFF; background: #B22222'>#B22222</td>
      <td style='color: #FFF; background: #4D4D4D'>#4D4D4D</td>
      <td>Firebrick </td>
    </tr><tr>
      <td style='color: #000; background: #B8860B'>#B8860B</td>
      <td style='color: #000; background: #878787'>#878787</td>
      <td>Dark goldenrod </td>
    </tr><tr>
      <td style='color: #000; background: #BA55D3'>#BA55D3</td>

      <td style='color: #000; background: #828282'>#828282</td>
      <td>Medium orchid </td>
    </tr><tr>
      <td style='color: #000; background: #BC8F8F'>#BC8F8F</td>
      <td style='color: #000; background: #9C9C9C'>#9C9C9C</td>
      <td>Rosy brown </td>
    </tr><tr>
      <td style='color: #000; background: #BDB76B'>#BDB76B</td>
      <td style='color: #000; background: #B0B0B0'>#B0B0B0</td>

      <td>Dark khaki </td>
    </tr><tr>
      <td style='color: #000; background: #C0C0C0'>#C0C0C0</td>
      <td style='color: #000; background: #C0C0C0'>#C0C0C0</td>
      <td>Silver </td>
    </tr><tr>
      <td style='color: #FFF; background: #C71585'>#C71585</td>
      <td style='color: #FFF; background: #575757'>#575757</td>
      <td>Medium violet red </td>

    </tr><tr>
      <td style='color: #FFF; background: #CD5C5C'>#CD5C5C</td>
      <td style='color: #FFF; background: #7E7E7E'>#7E7E7E</td>
      <td>Indian red </td>
    </tr><tr>
      <td style='color: #000; background: #CD853F'>#CD853F</td>
      <td style='color: #000; background: #939393'>#939393</td>
      <td>Peru </td>
    </tr><tr>

      <td style='color: #FFF; background: #D2691E'>#D2691E</td>
      <td style='color: #FFF; background: #808080'>#808080</td>
      <td>Chocolate </td>
    </tr><tr>
      <td style='color: #000; background: #D2B48C'>#D2B48C</td>
      <td style='color: #000; background: #B8B8B8'>#B8B8B8</td>
      <td>Tan </td>
    </tr><tr>
      <td style='color: #000; background: #D3D3D3'>#D3D3D3</td>

      <td style='color: #000; background: #D3D3D3'>#D3D3D3</td>
      <td>Light grey </td>
    </tr><tr>
      <td style='color: #000; background: #D8BFD8'>#D8BFD8</td>
      <td style='color: #000; background: #C9C9C9'>#C9C9C9</td>
      <td>Thistle </td>
    </tr><tr>
      <td style='color: #000; background: #DA70D6'>#DA70D6</td>
      <td style='color: #000; background: #9B9B9B'>#9B9B9B</td>

      <td>Orchid </td>
    </tr><tr>
      <td style='color: #000; background: #DAA520'>#DAA520</td>
      <td style='color: #000; background: #A6A6A6'>#A6A6A6</td>
      <td>Goldenrod </td>
    </tr><tr>
      <td style='color: #000; background: #DB7093'>#DB7093</td>
      <td style='color: #000; background: #949494'>#949494</td>
      <td>Pale violet red </td>

    </tr><tr>
      <td style='color: #FFF; background: #DC143C'>#DC143C</td>
      <td style='color: #FFF; background: #545454'>#545454</td>
      <td>Crimson </td>
    </tr><tr>
      <td style='color: #000; background: #DCDCDC'>#DCDCDC</td>
      <td style='color: #000; background: #DCDCDC'>#DCDCDC</td>
      <td>Gainsboro </td>
    </tr><tr>

      <td style='color: #000; background: #DDA0DD'>#DDA0DD</td>
      <td style='color: #000; background: #B9B9B9'>#B9B9B9</td>
      <td>Plum </td>
    </tr><tr>
      <td style='color: #000; background: #DEB887'>#DEB887</td>
      <td style='color: #000; background: #BEBEBE'>#BEBEBE</td>
      <td>Burlywood </td>
    </tr><tr>
      <td style='color: #000; background: #E0B0FF'>#E0B0FF</td>

      <td style='color: #000; background: #C7C7C7'>#C7C7C7</td>
      <td>Mauve </td>
    </tr><tr>
      <td style='color: #000; background: #E0FFFF'>#E0FFFF</td>
      <td style='color: #000; background: #F6F6F6'>#F6F6F6</td>
      <td>Light cyan </td>
    </tr><tr>
      <td style='color: #000; background: #E6E6FA'>#E6E6FA</td>
      <td style='color: #000; background: #E8E8E8'>#E8E8E8</td>

      <td>Lavender </td>
    </tr><tr>
      <td style='color: #000; background: #E9967A'>#E9967A</td>
      <td style='color: #000; background: #ACACAC'>#ACACAC</td>
      <td>Dark salmon </td>
    </tr><tr>
      <td style='color: #000; background: #EE82EE'>#EE82EE</td>
      <td style='color: #000; background: #AFAFAF'>#AFAFAF</td>
      <td>Violet </td>

    </tr><tr>
      <td style='color: #000; background: #EEE8AA'>#EEE8AA</td>
      <td style='color: #000; background: #E3E3E3'>#E3E3E3</td>
      <td>Pale goldenrod </td>
    </tr><tr>
      <td style='color: #000; background: #F08080'>#F08080</td>
      <td style='color: #000; background: #A1A1A1'>#A1A1A1</td>
      <td>Light coral </td>
    </tr><tr>

      <td style='color: #000; background: #F0E68C'>#F0E68C</td>
      <td style='color: #000; background: #DFDFDF'>#DFDFDF</td>
      <td>Khaki </td>
    </tr><tr>
      <td style='color: #000; background: #F0F8FF'>#F0F8FF</td>
      <td style='color: #000; background: #F6F6F6'>#F6F6F6</td>
      <td>Alice blue </td>
    </tr><tr>
      <td style='color: #000; background: #F0FFF0'>#F0FFF0</td>

      <td style='color: #000; background: #F9F9F9'>#F9F9F9</td>
      <td>Honeydew </td>
    </tr><tr>
      <td style='color: #000; background: #F0FFFF'>#F0FFFF</td>
      <td style='color: #000; background: #FBFBFB'>#FBFBFB</td>
      <td>Azure </td>
    </tr><tr>
      <td style='color: #000; background: #F4A460'>#F4A460</td>
      <td style='color: #000; background: #B4B4B4'>#B4B4B4</td>

      <td>Sandy brown </td>
    </tr><tr>
      <td style='color: #000; background: #F5DEB3'>#F5DEB3</td>
      <td style='color: #000; background: #E0E0E0'>#E0E0E0</td>
      <td>Wheat </td>
    </tr><tr>
      <td style='color: #000; background: #F5F5DC'>#F5F5DC</td>
      <td style='color: #000; background: #F2F2F2'>#F2F2F2</td>
      <td>Beige </td>

    </tr><tr>
      <td style='color: #000; background: #F5F5F5'>#F5F5F5</td>
      <td style='color: #000; background: #F5F5F5'>#F5F5F5</td>
      <td>Whitesmoke </td>
    </tr><tr>
      <td style='color: #000; background: #F5FFFA'>#F5FFFA</td>
      <td style='color: #000; background: #FBFBFB'>#FBFBFB</td>
      <td>Mint cream </td>
    </tr><tr>

      <td style='color: #000; background: #F8F8FF'>#F8F8FF</td>
      <td style='color: #000; background: #F9F9F9'>#F9F9F9</td>
      <td>Ghost white </td>
    </tr><tr>
      <td style='color: #000; background: #FA8072'>#FA8072</td>
      <td style='color: #000; background: #A3A3A3'>#A3A3A3</td>
      <td>Salmon </td>
    </tr><tr>
      <td style='color: #000; background: #FAEBD7'>#FAEBD7</td>

      <td style='color: #000; background: #EDEDED'>#EDEDED</td>
      <td>Antique white </td>
    </tr><tr>
      <td style='color: #000; background: #FAF0E6'>#FAF0E6</td>
      <td style='color: #000; background: #F2F2F2'>#F2F2F2</td>
      <td>Linen </td>
    </tr><tr>
      <td style='color: #000; background: #FAFAD2'>#FAFAD2</td>
      <td style='color: #000; background: #F5F5F5'>#F5F5F5</td>

      <td>Light goldenrod yellow </td>
    </tr><tr>
      <td style='color: #000; background: #FDF5E6'>#FDF5E6</td>
      <td style='color: #000; background: #F6F6F6'>#F6F6F6</td>
      <td>Old lace </td>
    </tr><tr>
      <td style='color: #FFF; background: #FF0000'>#FF0000</td>
      <td style='color: #FFF; background: #4C4C4C'>#4C4C4C</td>
      <td>Red </td>

    </tr><tr>
      <td style='color: #FFF; background: #FF00FF'>#FF00FF</td>
      <td style='color: #FFF; background: #696969'>#696969</td>
      <td>Magenta </td>
    </tr><tr>
      <td style='color: #FFF; background: #FF1493'>#FF1493</td>
      <td style='color: #FFF; background: #696969'>#696969</td>
      <td>Deep pink </td>
    </tr><tr>

      <td style='color: #FFF; background: #FF4500'>#FF4500</td>
      <td style='color: #FFF; background: #757575'>#757575</td>
      <td>Orange red </td>
    </tr><tr>
      <td style='color: #000; background: #FF6347'>#FF6347</td>
      <td style='color: #000; background: #8E8E8E'>#8E8E8E</td>
      <td>Tomato </td>
    </tr><tr>
      <td style='color: #000; background: #FF69B4'>#FF69B4</td>

      <td style='color: #000; background: #9E9E9E'>#9E9E9E</td>
      <td>Hot pink </td>
    </tr><tr>
      <td style='color: #000; background: #FF7F50'>#FF7F50</td>
      <td style='color: #000; background: #A0A0A0'>#A0A0A0</td>
      <td>Coral </td>
    </tr><tr>
      <td style='color: #000; background: #FF8C00'>#FF8C00</td>
      <td style='color: #000; background: #9E9E9E'>#9E9E9E</td>

      <td>Dark orange </td>
    </tr><tr>
      <td style='color: #000; background: #FFA07A'>#FFA07A</td>
      <td style='color: #000; background: #B8B8B8'>#B8B8B8</td>
      <td>Light salmon </td>
    </tr><tr>
      <td style='color: #000; background: #FFA500'>#FFA500</td>
      <td style='color: #000; background: #ADADAD'>#ADADAD</td>
      <td>Orange </td>

    </tr><tr>
      <td style='color: #000; background: #FFB6C1'>#FFB6C1</td>
      <td style='color: #000; background: #CDCDCD'>#CDCDCD</td>
      <td>Light pink </td>
    </tr><tr>
      <td style='color: #000; background: #FFC8CB'>#FFC8CB</td>
      <td style='color: #000; background: #D9D9D9'>#D9D9D9</td>
      <td>Pink </td>
    </tr><tr>

      <td style='color: #000; background: #FFD700'>#FFD700</td>
      <td style='color: #000; background: #CACACA'>#CACACA</td>
      <td>Gold </td>
    </tr><tr>
      <td style='color: #000; background: #FFDAB9'>#FFDAB9</td>
      <td style='color: #000; background: #E1E1E1'>#E1E1E1</td>
      <td>Peach puff </td>
    </tr><tr>
      <td style='color: #000; background: #FFDEAD'>#FFDEAD</td>

      <td style='color: #000; background: #E2E2E2'>#E2E2E2</td>
      <td>Navajo white </td>
    </tr><tr>
      <td style='color: #000; background: #FFE4B5'>#FFE4B5</td>
      <td style='color: #000; background: #E7E7E7'>#E7E7E7</td>
      <td>Moccasin </td>
    </tr><tr>
      <td style='color: #000; background: #FFE4C4'>#FFE4C4</td>
      <td style='color: #000; background: #E8E8E8'>#E8E8E8</td>

      <td>Bisque </td>
    </tr><tr>
      <td style='color: #000; background: #FFE4E1'>#FFE4E1</td>
      <td style='color: #000; background: #ECECEC'>#ECECEC</td>
      <td>Misty rose </td>
    </tr><tr>
      <td style='color: #000; background: #FFEBCD'>#FFEBCD</td>
      <td style='color: #000; background: #EEEEEE'>#EEEEEE</td>
      <td>Blanched almond </td>

    </tr><tr>
      <td style='color: #000; background: #FFEFD5'>#FFEFD5</td>
      <td style='color: #000; background: #F1F1F1'>#F1F1F1</td>
      <td>Papaya whip </td>
    </tr><tr>
      <td style='color: #000; background: #FFF0F5'>#FFF0F5</td>
      <td style='color: #000; background: #F5F5F5'>#F5F5F5</td>
      <td>Lavender blush </td>
    </tr><tr>

      <td style='color: #000; background: #FFF5EE'>#FFF5EE</td>
      <td style='color: #000; background: #F7F7F7'>#F7F7F7</td>
      <td>Sea shell </td>
    </tr><tr>
      <td style='color: #000; background: #FFF8DC'>#FFF8DC</td>
      <td style='color: #000; background: #F7F7F7'>#F7F7F7</td>
      <td>Cornsilk </td>
    </tr><tr>
      <td style='color: #000; background: #FFFACD'>#FFFACD</td>

      <td style='color: #000; background: #F6F6F6'>#F6F6F6</td>
      <td>Lemon chiffon </td>
    </tr><tr>
      <td style='color: #000; background: #FFFAF0'>#FFFAF0</td>
      <td style='color: #000; background: #FAFAFA'>#FAFAFA</td>
      <td>Floral white </td>
    </tr><tr>
      <td style='color: #000; background: #FFFAFA'>#FFFAFA</td>
      <td style='color: #000; background: #FBFBFB'>#FBFBFB</td>

      <td>Snow </td>
    </tr><tr>
      <td style='color: #000; background: #FFFF00'>#FFFF00</td>
      <td style='color: #000; background: #E2E2E2'>#E2E2E2</td>
      <td>Yellow </td>
    </tr><tr>
      <td style='color: #000; background: #FFFFE0'>#FFFFE0</td>
      <td style='color: #000; background: #FBFBFB'>#FBFBFB</td>
      <td>Light yellow </td>

    </tr><tr>
      <td style='color: #000; background: #FFFFF0'>#FFFFF0</td>
      <td style='color: #000; background: #FDFDFD'>#FDFDFD</td>
      <td>Ivory </td>
    </tr><tr>
      <td style='color: #000; background: #FFFFFF'>#FFFFFF</td>
      <td style='color: #000; background: #FFFFFF'>#FFFFFF</td>
      <td>White </td>
    </tr>
</table>
</body></html>
