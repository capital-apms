<?php
// Ensure that ../inc is in our included paths as early as possible
set_include_path( '../inc'. PATH_SEPARATOR. get_include_path());

$c = (object) 'Configuration Data';
$c->started = microtime();
$c->messages = array();
$c->stylesheets = array('css/menus.css','css/apms.css');
$c->local_styles = array();
$c->print_styles = array('css/print.css');
$c->scripts = array('js/apms.js');
$c->page_title = "APMS";
$c->total_query_time = 0;

// Force some things to not be set...
unset($session);

error_log( "=============================================== Start $_SERVER[PHP_SELF] for $_SERVER[HTTP_HOST] on $_SERVER[SERVER_NAME]" );
if ( file_exists("/etc/apms/".$_SERVER['SERVER_NAME'].".conf") ) {
  include_once("/etc/apms/".$_SERVER['SERVER_NAME'].".conf");
}
else if ( file_exists("../config/config.php") ) {
  include_once("../config/config.php");
}
else {
  include_once("apms_configuration_missing.php");
  exit;
}

/**
* Work out our version
*
*/
$c->code_version = 0;
$c->version_string = '1.7.0'; // The actual version # is replaced into that during the build /release process
if ( isset($c->version_string) && preg_match( '/(\d+)\.(\d+)\.(\d+)(.*)/', $c->version_string, $matches) ) {
  $c->code_major = $matches[1];
  $c->code_minor = $matches[2];
  $c->code_patch = $matches[3];
  $c->code_version = (($c->code_major * 1000) + $c->code_minor).".".$c->code_patch;
}
header( sprintf("X-APMS-Version: %s/%d.%d", $c->version_string, $c->code_major, $c->code_minor) );

require_once("AWLUtilities.php");
require_once("AwlQuery.php");
require_once("APMSSession.php");

$session->LoginRequired();

/**
* Rather laboriously we have to look in the database to get the current financial year, used frequently
*/
$qry = new AwlQuery('SELECT financialyearcode FROM month WHERE startdate<=current_date AND enddate>=current_date LIMIT 1;');
if ( $qry->Exec("always") && $qry->rows() == 1 ) {
  $row = $qry->Fetch();
  $c->current_financial_year = $row->financialyearcode;
}
if ( isset($year) && $year < 1950 ) unset($year);
param_to_global('year','#[0-9]{4,5}#'); // just in case my code lasts 7992 years... :-)
if ( !isset($year) ) $year = $c->current_financial_year;
if ( $year < 1950 ) $year = $c->current_financial_year;

param_to_global('et','#[ACTLPJ]+#i'); if (isset($et)) $et = strtoupper($et);
param_to_global('ec','#[0-9]+#');
param_to_global('ac','#[0-9.]+#');

$component = param_to_global('t','#[a-z0-9_-]+#');
$idtype = $component;
param_to_global('id','#^[a-z, 0-9-]+$#i');
if ( isset($id) && preg_match( '#^([A-Z]+)[, -]?([0-9]+)$#', $id, $matches ) ) {
  $id = strtoupper($id);
  $id = intval($matches[2]);
  switch( $matches[1] ) {
    case 'T':   $idtype = 'tenant';     if (!isset($et)) { $et = 'T'; $ec = $id; }   break;
    case 'C':   $idtype = 'creditor';   if (!isset($et)) { $et = 'C'; $ec = $id; }   break;
    case 'P':   $idtype = 'property';   if (!isset($et)) { $et = 'P'; $ec = $id; }   break;
    case 'L':   $idtype = 'company';    if (!isset($et)) { $et = 'L'; $ec = $id; }   break;
    case 'J':   $idtype = 'project';    if (!isset($et)) { $et = 'J'; $ec = $id; }   break;
    case 'A':   $idtype = 'asset';      if (!isset($et)) { $et = 'A'; $ec = $id; }   break;

    case 'INV':   $idtype = 'invoice';  break;
    case 'VCHR':  $idtype = 'voucher';  break;
    case 'CHQ':   $idtype = 'cheque';   break;

    default:
      $idtype = $matches[1];
  }
}
else {
  if ( !isset($id) ) $id = 0; else $id = intval($id);  // Just to be certain
}


/**
* Function to retrieve a list of office settings into the $office_settings
* array.  If it's called a second time, only the new settings will be fetched
* from the database.
*
* @param strings $args  A list of strings which are the names of office settings.
*/
$office_settings = (object) array();
function get_office_settings( ) {
  global $office_settings;

  $args = func_get_args();
  $sql = "";
  $need_sql = false;
  foreach( $args AS $k => $v ) {
    if ( !isset($office_settings->{$v}) ) {
      $sql .= ", get_office_setting('$v') AS \"$v\"";
      $need_sql = true;
    }
    else {
      unset($args[$k]);
    }
  }
  if ( !$need_sql ) return;

  $qry = new AwlQuery( "SELECT".substr($sql,1) );
  if ( $qry->Exec('always') && $qry->rows() == 1 ) {
    $row = $qry->Fetch();
    foreach( $args AS $k => $v ) {
      $office_settings->{$v} = $row->{$v};
    }
  }
}

get_office_settings('GST-Rate');

/**
* Function to retrieve a office control accounts into the $office_accounts
* array.  If it's called a second time, only the new accounts will be fetched
* from the database.
*
* @param strings $args  A list of strings which are the names of office control accounts
*/
$office_accounts = array();
function get_office_accounts( ) {
  global $office_accounts;

  $args = func_get_args();
  $in_list = "";
  $need_sql = false;
  foreach( $args AS $k => $v ) {
    if ( !isset($office_accounts[$v]) ) {
      $in_list .= ", ". qpg($v);
      $need_sql = true;
    }
    else {
      unset($args[$k]);
    }
  }
  if ( !$need_sql ) return;

  $in_list = substr($in_list,2); // skip first comma
  $qry = new AwlQuery( "SELECT oca.* FROM officecontrolaccount oca JOIN office o USING (officecode) WHERE o.thisoffice AND oca.name IN ( $in_list )" );
  if ( $qry->Exec('always') && $qry->rows() > 0  ) {
    while( $row = $qry->Fetch() ) {
      $office_accounts[$row->name] = clone($row);
    }
  }
}

require_once("apms_menus.php");
