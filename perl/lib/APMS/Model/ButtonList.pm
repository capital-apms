package APMS::Model::ButtonList;

use strict;
use Data::Dumper;

=item buttons

Returns a list of buttons. Each button is a hash consisting of
all the details required to create an APMS button in a widget.

  my @buttons = $self->model->buttons( $username, $menucode );

Where $username is the username of the authenticated user, and
$menucode is the menu/screen that the button list should be
generated for.

=cut

sub buttons {
	my ( $self, $username, $menucode ) = @_;

	my $sth = $self->{dbh}->prepare( qq/
		SELECT
			programlink.buttonlabel,
			programlink.target,
			programlink.viewer,
			programlink.function,
			programlink.linktype
		FROM programlink
		WHERE programlink.linkcode IN (
			SELECT usrgroupmenuitem.linkcode
			FROM usrgroupmember
			JOIN usrgroup ON ( usrgroup.groupname = usrgroupmember.groupname )
			JOIN usrgroupmenu ON ( usrgroupmenu.groupname = usrgroup.groupname AND usrgroupmenu.nodecode = ? )
			JOIN usrgroupmenuitem ON ( usrgroupmenuitem.menuname = usrgroupmenu.menuname )
			WHERE usrgroupmember.username = ?
			ORDER BY usrgroup.sequence, usrgroupmenuitem.sequencecode
		);
	/ );

	$sth->execute( $menucode, $username );
	my $buttons = $sth->fetchall_arrayref({});
	$sth->finish;

	return $buttons;
}

sub buttons_old {
	my ( $self, $username, $menucode ) = @_;

	my $sth = $self->{dbh}->prepare( qq/
		SELECT DISTINCT
			UsrGroupMenuItem.ButtonLabel,
			UsrGroupMenuItem.SequenceCode,
			ProgramLink.LinkType,
			ProgramLink.Source,
			ProgramLink.Target,
			ProgramLink.LinkCode,
			ProgramLink.Viewer,
			ProgramLink.SortPanel,
			ProgramLink.FilterPanel,
			ProgramLink.Description,
			ProgramLink.Function,
			UsrGroupMenuItem.MenuName,
			UsrGroupMember.UserName,
			LinkNode.NodeCode
		FROM UsrGroupMenuItem
		JOIN UsrGroupMember
			ON (
				UsrGroupMember.GroupName = UsrGroupMenuItem.GroupName
				AND UsrGroupMember.UserName = ?
			)
		JOIN LinkNode
			ON (
				LinkNode.Description = UsrGroupMenuItem.MenuName
				AND LinkNode.NodeCode = ?
			)
		JOIN ProgramLink
			ON (
				ProgramLink.LinkCode = UsrGroupMenuItem.LinkCode
			)
		ORDER BY UsrGroupMenuItem.SequenceCode;
	/ );

	$sth->execute( $username, $menucode );
	my $buttons = $sth->fetchall_arrayref({});
	$sth->finish;

	return $buttons;
}

=item mainmenucode

Find the main menu code from the DB. Currently this is as-it-is in the original
APMS - finds the code by name.

=cut

sub mainmenucode {
	my $self = shift;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT NodeCode FROM LinkNode WHERE NodeType = 'MN' AND Description = 'Main Menu';
	) );

	my $result;
	$sth->execute();
	my $result = $sth->fetchrow_hashref;
	$sth->finish;

	return $result->{nodecode} if exists $result->{nodecode};
}

=item menu_links

This is used by the menu browser only, as the screen that calls this should only
be available to members of the administrators group

=cut

sub menu_links {
	my ( $self, $nodecode ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT
			UsrGroupMenuItem.SequenceCode,
			UsrGroupMenuItem.ButtonLabel,
			UsrGroupMenuItem.GroupName
		FROM LinkNode
		JOIN UsrGroupMenu ON ( UsrGroupMenu.NodeCode = LinkNode.NodeCode )
		JOIN UsrGroupMenuItem ON (
			UsrGroupMenuItem.GroupName = UsrGroupMenu.GroupName AND
			UsrGroupMenuItem.MenuName = UsrGroupMenu.MenuName
		)
		JOIN ProgramLink ON ( ProgramLink.LinkCode = UsrGroupMenuItem.LinkCode )
		WHERE LinkNode.NodeCode = ?;
	) );

=item

			ProgramLink.Source = ( IF LinkNode.NodeType = "MN" THEN ? ELSE LinkNode.NodeCode )

FOR EACH LinkNode WHERE ~{&KEY-PHRASE},
      EACH UsrGroupMenu WHERE UsrGroupMenu.NodeCode = LinkNode.NodeCode NO-LOCK,
      EACH UsrGroupMenuItem WHERE UsrGroupMenuItem.GroupName = UsrGroupMenu.GroupName
  AND UsrGroupMenuItem.MenuName = UsrGroupMenu.MenuName NO-LOCK,
      EACH ProgramLink WHERE ProgramLink.LinkCode = UsrGroupMenuItem.LinkCode
  AND ProgramLink.Source = (IF LinkNode.NodeType = "MN" THEN main-menu-node ELSE LinkNode.NodeCode) NO-LOCK

=cut

	$sth->execute( $nodecode );
	my $links = $sth->fetchall_arrayref({});
	$sth->finish;

	return $links;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

