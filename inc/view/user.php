<?php

// Viewer component for company records
$viewer = new Viewer("User");
$viewer->AddField( 'user_no' );
$viewer->AddField( 'username' );
$viewer->AddField( 'fullname' );
$viewer->AddField( 'email' );
$viewer->AddField( 'date_format_type' );
$viewer->SetJoins( "usr" );
$id = intval($_REQUEST['id']);
$viewer->SetWhere( 'user_no='.$id );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">User ID:</th>
  <td class="left">##user_no.enc##</td>
 </tr>
 <tr>
  <th class="right">Username:</th>
  <td class="left">##username.enc##</td>
 </tr>
 <tr>
  <th class="right">Full name:</th>
  <td class="left">##fullname.enc##</td>
 </tr>
 <tr>
  <th class="right">EMail:</th>
  <td class="left">##email.enc##</td>
 </tr>
 <tr>
  <th class="right">Date Format Type:</th>
  <td class="left">##date_format_type.enc##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("user: ".$viewer->Record->{'fullname'});

$page_elements[] = $viewer;
