-- Convert all instances of entitytype fields to uppercase.

CREATE SEQUENCE batch_seq;
SELECT setval('batch_seq',
   (SELECT batchcode FROM batch UNION SELECT batchcode FROM newbatch ORDER BY batchcode DESC LIMIT 1)
) AS batchcode;
GRANT UPDATE,SELECT ON batch_seq TO general;

-- Convert the batch table to use a timestamp rather than a date & a seconds
BEGIN;
ALTER TABLE batch RENAME COLUMN updatedat TO updated_seconds;
ALTER TABLE batch ADD COLUMN updatedat TIMESTAMP DEFAULT current_timestamp NOT NULL;
UPDATE batch SET updatedat = (updatedon::timestamp + (updated_seconds||' seconds')::interval)::timestamp;
ALTER TABLE batch DROP column updated_seconds;
ALTER TABLE batch DROP column updatedon;
COMMIT;
VACUUM FULL batch;

-- Convert the chartofaccount table to use a timestamp rather than a date & a seconds
BEGIN;
ALTER TABLE chartofaccount ADD COLUMN lastmodified TIMESTAMP DEFAULT current_timestamp NOT NULL;
UPDATE chartofaccount SET lastmodified = (lastmodifieddate::timestamp + (lastmodifiedtime||' seconds')::interval)::timestamp;
ALTER TABLE chartofaccount DROP column lastmodifiedtime;
ALTER TABLE chartofaccount DROP column lastmodifieddate;
COMMIT;
VACUUM FULL chartofaccount;

-- Convert the batchqueue table to use timestamp / interval
BEGIN;
ALTER TABLE batchqueue RENAME COLUMN elapsed TO el_secs;
ALTER TABLE batchqueue ADD COLUMN runafter TIMESTAMP DEFAULT current_timestamp;
ALTER TABLE batchqueue ADD COLUMN started TIMESTAMP;
ALTER TABLE batchqueue ADD COLUMN elapsed INTERVAL;
UPDATE batchqueue
   SET runafter = (rundate::timestamp + (runtime::text || ' seconds')::interval),
       started  = (startedon::timestamp + (startedat::text || ' seconds')::interval),
       elapsed  = (el_secs::text || ' seconds')::interval ;
ALTER TABLE batchqueue DROP COLUMN rundate;
ALTER TABLE batchqueue DROP COLUMN runtime;
ALTER TABLE batchqueue DROP COLUMN startedon;
ALTER TABLE batchqueue DROP COLUMN startedat;
ALTER TABLE batchqueue DROP COLUMN el_secs;
COMMIT;
VACUUM FULL batchqueue;

-- Convert the joblog table to use timestamp
BEGIN;
ALTER TABLE joblog RENAME COLUMN calltime TO call_seconds;
ALTER TABLE joblog ADD COLUMN calltime TIMESTAMP DEFAULT current_timestamp NOT NULL;
UPDATE joblog SET calltime = (calldate::timestamp + (call_seconds||' seconds')::interval)::timestamp;
ALTER TABLE joblog DROP column call_seconds;
ALTER TABLE joblog DROP column calldate;
COMMIT;
VACUUM FULL joblog;

-- Convert the tenantcall table to use timestamp
BEGIN;
ALTER TABLE tenantcall RENAME COLUMN timeofcall TO call_seconds;
ALTER TABLE tenantcall ADD COLUMN timeofcall TIMESTAMP DEFAULT current_timestamp NOT NULL;
ALTER TABLE tenantcall RENAME COLUMN timecomplete TO complete_seconds;
ALTER TABLE tenantcall ADD COLUMN timecomplete TIMESTAMP DEFAULT current_timestamp NOT NULL;
UPDATE tenantcall
   SET timeofcall = (dateofcall::timestamp + (call_seconds||' seconds')::interval)::timestamp,
       timecomplete = (datecomplete::timestamp + (complete_seconds||' seconds')::interval)::timestamp;
ALTER TABLE tenantcall DROP column call_seconds;
ALTER TABLE tenantcall DROP column dateofcall;
ALTER TABLE tenantcall DROP column complete_seconds;
ALTER TABLE tenantcall DROP column datecomplete;
COMMIT;
VACUUM FULL tenantcall;

-- Convert the topost table to use a timestamp rather than a date & a seconds
BEGIN;
ALTER TABLE topost RENAME COLUMN updatedat TO updated_seconds;
ALTER TABLE topost ADD COLUMN updatedat TIMESTAMP DEFAULT current_timestamp NOT NULL;
UPDATE topost SET updatedat = (updatedon::timestamp + (updated_seconds||' seconds')::interval)::timestamp;
ALTER TABLE topost DROP column updated_seconds;
ALTER TABLE topost DROP column updatedon;
COMMIT;
VACUUM FULL topost;

ALTER TABLE invoice ALTER COLUMN invoicestatus SET DEFAULT 'U';
DELETE FROM invoicestatus WHERE invoicestatus = 'D';
INSERT INTO invoicestatus VALUES( 'A', 'Approved');

UPDATE officecontrolaccount SET name = upper(name);

UPDATE newaccttrans SET reference = NULL WHERE reference = '';
UPDATE newaccttrans SET description = NULL WHERE description = '';
VACUUM FULL newaccttrans;

UPDATE accttran SET reference = NULL WHERE reference = '';
UPDATE accttran SET description = NULL WHERE description = '';
VACUUM FULL accttran;

DROP INDEX bankimportexception_sk1_bankaccount_trntype;

ALTER TABLE bankimportexception RENAME TO bankimportrule;
ALTER TABLE bankimportrule ADD COLUMN ruleseq INT;

CREATE INDEX bankimportrule_pk ON bankimportrule( bankaccountcode, ruleseq );

-- Convert the report parameters table
DELETE FROM rp;
ALTER TABLE rp DROP CONSTRAINT rp_pkey;
ALTER TABLE rp DROP COLUMN username;
ALTER TABLE rp ADD COLUMN user_no INT;
ALTER TABLE rp ADD PRIMARY KEY (user_no, reportid);

