<?php

$widget = new Widget();
$widget->AddField( 'filename', "char1" );
$widget->AddField( 'bankaccountcode', "char2", "SELECT bankaccountcode, bankaccountcode || ' - ' || accountname || ', ' || bankname || ', ' || bankbranchname FROM bankaccount WHERE active ORDER BY 1" );
$widget->AddField( 'description', 'char3' );
$widget->ReadWrite();
$widget->Defaults( array( 'filename' => '', 'bankaccountcode' => 'CHEQ', 'description' => 'Bank Import' ) );

$c->page_title = 'Bank File Import';

function apply_rule( &$txn, $rule ) {
  if ( isset($rule->entitytype) )  $txn['et'] = $rule->entitytype;
  if ( isset($rule->entitycode) )  $txn['ec'] = $rule->entitycode;
  if ( isset($rule->accountcode) ) $txn['ac'] = $rule->accountcode;
  if ( isset($rule->description) && $rule->description != '' ) {
    $txn['description'] = $rule->description;
    if ( isset($txn['person']) )  $txn['description'] = str_replace( '[[person]]', $txn['person'], $txn['description'] );
    if ( isset($txn['bankref']) ) $txn['description'] = str_replace( '[[bankref]]', $txn['bankref'], $txn['description'] );
    if ( isset($txn['type']) )    $txn['description'] = str_replace( '[[type]]', $txn['type'], $txn['description'] );
  }
  $txn['rowclass'] = 'r' . ($txn['doc'] % 2);
}

if ( $widget->IsSubmit() ) {
  $bank_file = fopen( $_FILES['filename']['tmp_name'], 'r' );
  $extension = preg_replace('#^[^.]+\.#', '', $_FILES['filename']['name'] );

  $transactions = array();
  switch ( strtolower($extension) ) {
    case 'qif':
      $state = 0;
      while( !feof($bank_file) ) {
        $line = fgets( $bank_file );
        $ltype = substr( $line, 0, 1);
        $line = trim(substr($line, 1));
        switch( $ltype ) {
          case '^':
            if ( $state > 0 ) {
              $transactions[] = $txn;
            }
            else {
              $state = 1;
            }
            $txn = array( 'bankref' => '' );
            break;
          case 'D':     $txn['date'] = $line;      break;
          case 'T':     $txn['amount'] = $line;    break;
          case 'L':     $txn['type'] = $line;      break;
          case 'P':     $txn['person'] = $line;    break;
          case 'M':
            if ( preg_match( '#^(Part=(.*?))?(Code=(.*?))?(Ref=(.*?))?$#i', $line, $parts ) ) {
              $txn['bankref'] = $parts[2] . (isset($parts[4]) ? $parts[4] : '') . (isset($parts[6]) ? $parts[6] : '');
            }
            else {
              $txn['bankref'] = $line;
            }
            break;
        }
      }
      break;
  }

  $sql = "SELECT * FROM bankimportrule WHERE bankaccountcode=? ORDER BY bankaccountcode, ruleseq";
  $qry = new PgQuery( $sql, $widget->Value('bankaccountcode') );
  $rules = array();
  if ( $qry->Exec('bank-import') && $qry->rows > 0 ) {
    while( $row = $qry->Fetch() ) $rules[] = $row;
  }

  $c->scripts[] = 'js/jquery.js';
  $c->scripts[] = 'js/bank-import.js';
  $localscripts = <<<EOSCRIPT
<script type="text/javascript">
$(document).ready(function(){
  $("input").on("focusin",function(e){e.target.select();});
  $("input").on("keypress",nextFieldOnEnter);
});
</script>

EOSCRIPT;

  $fields = array( 'description', 'reference', 'date', 'amount', 'et', 'ec', 'ac', 'person', 'bankref', 'type', 'rowclass', 'doc' );
  $footers = array( '<h2>Bank Import</h2>' );
  $footers[] = $localscripts;
  $footers[] = '<form method="POST" enctype="multipart/form-data" id="rowentry" onSubmit="return validateAll();"><table>';
  $footers[] = sprintf( '<input type="hidden" name="bankaccountcode" value="%s">', htmlentities($widget->Value('bankaccountcode')) );
  $footers[] = sprintf( '<input type="hidden" name="description" value="%s">', htmlentities($widget->Value('description')) );
  
  $footer_row = <<<EOT
 <tbody id="txn-##doc##" class="document">
  <tr class="##rowclass## rowsettop">
    <td colspan="5">Type: ##type##, Person: ##person##, BankRef: ##bankref##</td>
    <td><input type="hidden" readonly="readonly" id="gottotal-##doc##" name="doctotal" size="9" value="0.00"></td>
    <td><input type="hidden" readonly="readonly" id="wanttotal-##doc##" name="doctotal" size="9" value="##amount##"></td>
   </tr>
   <tr class="##rowclass##" id="row-##doc##-1">
    <td>&nbsp;</td>
    <td class="left" class="entityFields">
     <input type="text" class="entry" id="txn[##doc##][1][entitytype]" onFocus="this.select();" onBlur="onEntityTypeChange(this);" name="txn[##doc##][1][entitytype]" size="1" value="##et##">
     <input type="text" class="entry" name="txn[##doc##][1][entitycode]" size="5" value="##ec##">
     <input type="##accountfieldtype##" class="entry" id="txn[##doc##][1][accountcode]" name="txn[##doc##][1][accountcode]" size="7" value="##ac##">
    </td>
    <td class="left"><input type="text" class="entry" name="txn[##doc##][1][date]" size="10" value="##date##"></td>
    <td class="left"><input type="text" class="entry" name="txn[##doc##][1][reference]" size="8" value="##reference##"></td>
    <td class="left"><input type="text" class="entry" name="txn[##doc##][1][description]" size="25" value="##description##"></td>
    <td class="left"><input type="text" class="money" id="txn[##doc##][1][amount]" name="txn[##doc##][1][amount]" size="9" value="##amount##"></td>
    <td class="center">&nbsp; <button type="button" class="submit" id="split-txn[##doc##][1]" onClick="splitDocument('##doc##');return false;">Split</button> &nbsp; &nbsp;</td>
   </tr>
 </tbody>
EOT;

  $doc = 0;
  foreach( $transactions AS $k => $txn ) {
    if ( !isset($txn['amount']) ) continue;
    $doc++;

    $txn['reference'] = $txn['type'];
    $txn['doc'] = $doc;
    $txn['et'] = 'L';
    $txn['ec'] = '1';
    $txn['ac'] = '9999';
    $txn['rowclass'] = 'r-alert';
    $txn['description'] = (isset($txn['person']) ? $txn['person'] . ', ' : '') . trim($txn['bankref']);
    if ( !isset($txn['person']) ) $txn['person'] = '';

    foreach( $rules AS $k => $rule ) {
      if ( isset($rule->trntype) && $rule->trntype != '' && preg_match( '~'.$rule->trntype.'~', $txn['type'] ) ) {
        if ( ( !isset($rule->match1) || $rule->match1 == '' || preg_match( '~'.$rule->match1.'~', $txn['person'] ) )
              &&  ( !isset($rule->match2) || $rule->match2 == '' || preg_match( '~'.$rule->match2.'~', $txn['bankref'] ) ) ) {
          apply_rule( $txn, $rule );
        }
      }
      else if ( isset($rule->match1) && $rule->match1 != '' && preg_match( '~'.$rule->match1.'~', $txn['person'] ) ) {
        if ( !isset($rule->match2) || $rule->match2 == '' || preg_match( '~'.$rule->match2.'~', $txn['bankref'] ) ) {
          apply_rule( $txn, $rule );
        }
      }
      else if ( isset($rule->match2) && $rule->match2 != '' && preg_match( '~'.$rule->match2.'~', $txn['bankref'] ) ) {
        apply_rule( $txn, $rule );
      }
      if ( $txn['ec'] == '99999' ) {
        $txn['rowclass'] = 'r-partial';
      }
      else if ( $txn['et'] == 'X' ) {
        $txn['rowclass'] = 'r-skip';
      }
    }
    $txn['accountfieldtype'] = ( $txn['et'] == 'T' || $txn['et'] == 'C' ? 'hidden' : 'text' );

    $row = $footer_row;
    foreach( $txn AS $k => $v ) {
      $row = str_replace( "##$k##", $v, $row );
    }

    $footers[] = $row;
  }
  $footers[] = '<tr>
<td colspan="2"></td>
<td colspan="4"><input type="submit" class="submit" name="submit" value="Create Batch"></td>
</tr>
</form>
</table>';
}
else {

  $template = <<<EOTEMPLATE
<form method="POST" enctype="multipart/form-data" id="entry">
<table>
 <tr>
  <th class="right">Bank Account:</th>
  <td class="left">##bankaccountcode.select##</td>
 </tr>
 <tr>
  <th class="right">Bank File:</th>
  <td class="left">##filename.file##</td>
 </tr>
 <tr>
  <th class="right">Batch Description:</th>
  <td class="left">##description.input.45##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left">##Process File.submit##</td>
 </tr>
</table>
<form>

EOTEMPLATE;

  $widget->Layout( $template );
  $widget->Title = $c->page_title;
  $page_elements[] = $widget;
}