-- Convert all instances of entitytype fields to uppercase.

UPDATE accttran
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE cashflow
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE creditor
SET vchrentitytype = UPPER(vchrentitytype) WHERE vchrentitytype != UPPER(vchrentitytype);
UPDATE entitytype
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE project
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE tenant
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE invoiceline
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE voucherline
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE invoice
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE voucher
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE newaccttrans
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE accountbalance
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE officecontrolaccount
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE accountsummary
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE supplymeter
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE closinggroup
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE purchaseorder
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE fixedasset
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE rentcharge
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE entitycontact
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE entitycontacttype
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE flowstep
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE flowtask
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE propforecast
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE bankimportexception
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
UPDATE entitylistmember
SET entitytype = UPPER(entitytype) WHERE entitytype != UPPER(entitytype);
VACUUM FULL ANALYZE;
