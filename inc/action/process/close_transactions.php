<?php
/**
* Component for closing specified group of transactions
* @package   apms
* @subpackage   close_transactions
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/

param_to_global('et', '#^[TC]$#' );
param_to_global('ec', 'int' );
param_to_global('close', '#^\d+\.\d+\.\d+$#' );
param_to_global('description', '#.*#' );

if ( !isset($description) ) {
  $description = "Partly closed group";
}
$safe_description = qpg($description);

get_office_accounts( 'DEBTORS', 'CREDITORS' );
$ac = ( $et == 'T' ? $office_accounts['DEBTORS']->accountcode : $office_accounts['CREDITORS']->accountcode );

$in_list = "'" . implode( "','", array_keys($close) ) . "'";

$sql = <<<EOQ
SELECT sum(amount) AS checksum
  FROM accttran
 WHERE entitytype = '$et' AND entitycode = $ec AND accountcode = $ac AND closedstate IN ( 'O', 'P' )
   AND ( batchcode || '.' || documentcode || '.' || transactioncode ) IN ( $in_list )
EOQ;
$qry = new PgQuery( $sql );
if ( $qry->Exec("close_transactions") && $qry->rows == 1 ) {
  $r = $qry->Fetch();
  $closed_status = 'F';
  if ( $r->checksum != 0 ) {
    $c->messages[] = sprintf( "Transactions only part-closed since group does not sum to zero (%14.2lf)", $r->checksum );
    $closed_status = 'P';
  }
}
else {
  $c->messages[] = "Database problem - please correct and retry.";
  return true; // So we don't error the layer above
}


$sql = <<<EOQ
BEGIN;
INSERT INTO closinggroup( entitytype, entitycode, accountcode, dateclosed, closedstatus, description )
             VALUES( '$et', $ec, $ac, current_date, '$closed_status', $safe_description );
UPDATE accttran SET closedstate = '$closed_status', closinggroup = currval('closinggroup_closinggroup_seq')
 WHERE entitytype = '$et' AND entitycode = $ec AND accountcode = $ac AND closedstate IN ( 'O', 'P' )
   AND ( batchcode || '.' || documentcode || '.' || transactioncode ) IN ( $in_list );
COMMIT;
EOQ;

$qry = new PgQuery( $sql );
if ( !$qry->Exec("close_transactions") ) {
  $c->messages[] = "Database transaction failure - please correct problem and retry.";
}