<?php

/** @todo Really these should be fetched from the client record */
get_office_settings( 'GST-Number', 'Company-Logo' );

$gstno      = $office_settings->{"GST-Number"};
$logo_image = $office_settings->{"Company-Logo"};

$client_name = "Morphoss Ltd";
$client_address = "6 Karoro Place, Porirua";
$client_email = "receivables@morphoss.com";
$client_website = "http://www.morphoss.com/";
$client_telephone = "+64 (2) 7233 2426";
$client_country = "New Zealand";
$bank_branch_name = "NBNZ, Porirua";
$bank_account_no = "06-0549-0328767-01";

// Viewer component for invoice
$pdf = new PdfReport("a4","portrait");
$pdf->AddField( 'invoiceno' );
$pdf->AddField( 'etname', 'get_entity_type_name(i.entitytype)' );
$pdf->AddField( 'loweretname', 'lower(get_entity_type_name(i.entitytype))' );
$pdf->AddField( 'entitytype', 'i.entitytype' );
$pdf->AddField( 'entitycode', 'i.entitycode' );
$pdf->AddField( 'entityname', 'get_entity_name(i.entitytype,i.entitycode)' );
$pdf->AddField( 'invoice_date', "to_char(invoicedate,'FMDDth FMMonth, YYYY')" );
$pdf->AddField( 'interest_date', "to_char(invoicedate + '6 weeks'::interval,'FMDDth FMMonth, YYYY')" );
$pdf->AddField( 'topay' );
$pdf->AddField( 'todetail' );
$pdf->AddField( 'blurb' );
$pdf->AddField( 'total', "to_char(total,'FMMI999G999G999G999D00')" );
$pdf->AddField( 'taxamount', "to_char(taxamount,'FMMI999G999G999G999D00')" );
$pdf->AddField( 'includingtax', "to_char(total+taxamount,'FMMI999G999G999G999D00')" );
$pdf->AddField( 'billto', "pbill.firstname || ' ' || pbill.lastname" );
$pdf->AddField( 'billaddr', "pbilladdr.address" );
$pdf->AddField( 'billcity', "pbilladdr.city" );
$pdf->AddField( 'billstate', "pbilladdr.state" );
$pdf->AddField( 'billpostcode', "pbilladdr.zip" );
$pdf->AddField( 'billcountry', "pbilladdr.country" );
$joins = "invoice i LEFT JOIN tenant t ON t.tenantcode = i.entitycode ";
$joins .= "LEFT JOIN person pbill ON t.billingcontact = pbill.personcode ";
$joins .= "LEFT JOIN postaldetail pbilladdr ON t.billingcontact = pbilladdr.personcode AND pbilladdr.postaltype = 'BILL' ";
$pdf->SetJoins( $joins );

$pdf->SetWhere( "invoiceno=$id");

$pdf->GetRecord();

$pdf->SetFileName("Invoice-$id.pdf");

$pdf->p->addInfo(array("Creator"=>"Capital APMS","Title"=>sprintf("%s - Invoice %05d - %s", $client_name, $id, $pdf->Record->{'todetail'})));
$pdf->Line(21,193,200,193);

$pdf->MasterLayerOn();
$pdf->PlaceImage( 140, 265, $logo_image, 60 );
$pdf->MasterLayerOff();

$pdf->SetFont("Helvetica",9);

$pdf->SetMargins( 35, 140, 0, 10);
$pdf->WriteParagraphs( "Web:\nEmail:\nPhone:" );
$pdf->SetMargins( 35, 150, 0, 10);
$pdf->WriteParagraphs( "$client_website\n$client_email\n$client_telephone", array("justification"=>"right") );

$pdf->SetFont("Helvetica",11);

$pdf->SetMargins( 55, 21, 0, 10);
$text = sprintf( "%s\n%s\n%s", $pdf->Record->{'entityname'}, $pdf->Record->{'billaddr'}, $pdf->Record->{'billcity'} );
if ( !empty($pdf->Record->{'billstate'}) ) {
  $text .= "\n".$pdf->Record->{'billstate'};
}
if ( !empty( $pdf->Record->{'billpostcode'})) {
  $text .= " ".$pdf->Record->{'billpostcode'};
}
if ( !empty($pdf->Record->{'billcountry'}) && $pdf->Record->{'billcountry'} != $client_country ) {
  $text .= "\n".$pdf->Record->{'billcountry'};
}
$pdf->WriteParagraphs( $text );

$pdf->SetMargins( 55, 140, 0, 10);
$pdf->WriteParagraphs( "Tax Invoice No:\nGST NO." );

dbg_error_log( "ERROR", $gstno );
dbg_log_array( "ERROR", "ofcset", $office_settings, true );

$pdf->SetMargins( 55, 140, 0, 10);
$pdf->WriteParagraphs( sprintf("%d\n%s\n\n\n\n%s",
           $id,$gstno,$pdf->Record->{'invoice_date'}), array("justification"=>"right") );

$pdf->WriteAtJust( "right", 200, 194, "Page 1" );

$pdf->SetMargins( 228, 21, 0, 10);
$pdf->WriteParagraphs( "E. & O.E.  This account is now due.\nInterest may be charged on payments received after ".$pdf->Record->{'interest_date'}."." );


$pdf->WriteAtJust( 'centre',105, 194, "<b>TAX INVOICE</b>", "Helvetica", 19 );

$pdf->SetFont("Helvetica-Bold",11);
$pdf->SetMargins( 107, 21, 0, 10);
$pdf->WriteParagraphs("RE:");

$pdf->SetMargins( 107, 32, 0, 10);
$pdf->WriteParagraphs($pdf->Record->{'todetail'});

$pdf->SetFont("Helvetica",10);
$pdf->SetMargins( 115, 21, 0, 10);
$pdf->WriteParagraphs("TO:");

$pdf->SetMargins( 115, 32, 0, 10);
$pdf->WriteParagraphs($pdf->Record->{'blurb'});

$sql = "SELECT accounttext, amount, to_char(amount,'FMMI999G999G999G999D00') AS fmt_amount, ";
$sql .= "percent, to_char(percent,'FMMI99G999D00') AS fmt_percent, ";
$sql .= "yourshare, to_char(yourshare,'FMMI999G999G999G999D00') AS fmt_share, ";
$sql .= "quantity FROM invoiceline WHERE invoiceno = $id ORDER BY invoiceno, lineseq ASC";
$qry = new PgQuery($sql);
if ( $qry->Exec("PdfReport") && $qry->rows > 0 ) {
  $non_100_percent = false;
  $pdf->mmy -= 15;
  $top_line = $pdf->mmy;
  while( $row = $qry->Fetch() ) {
    $pdf->WriteAtJust( 'left', 32, $pdf->mmy, $row->accounttext );
    $pdf->WriteAtJust( 'right',190, $pdf->mmy, $row->fmt_share );
    if ( $row->percent != 100 ) {
      $pdf->WriteAtJust( 'right',160, $pdf->mmy, $row->fmt_amount );
      $pdf->WriteAtJust( 'right',170, $pdf->mmy, $row->fmt_percent );
      $non_100_percent = true;
    }

    $pdf->mmy -= $pdf->Pt2Mm($pdf->p->getFontHeight($pdf->points));
  }

  $pdf->SetLineStyle( 0.15, '', '', array( 3, 1.2 ));
  $pdf->Line(190,$pdf->mmy+2,173,$pdf->mmy+2);
  $pdf->mmy -= 2;

  if ( $pdf->Record->{'taxamount'} != 0 ) {
    $pdf->WriteAtJust( 'left', 32, $pdf->mmy, "Invoice SubTotal" );
    $pdf->WriteAtJust( 'right',190, $pdf->mmy, $pdf->Record->{'total'} );
    $pdf->mmy -= $pdf->Pt2Mm($pdf->p->getFontHeight($pdf->points));
  
    $pdf->WriteAtJust( 'left', 32, $pdf->mmy, "Plus GST" );
    $pdf->WriteAtJust( 'right',190, $pdf->mmy, $pdf->Record->{'taxamount'} );
    $pdf->mmy -= $pdf->Pt2Mm($pdf->p->getFontHeight($pdf->points));
  
    $pdf->Line(190,$pdf->mmy+2,173,$pdf->mmy+2);
    $pdf->mmy -= 2;
  }

  $pdf->WriteAtJust( 'left', 32, $pdf->mmy, "<b>Amount now due</b>" );
  $pdf->WriteAtJust( 'right',190, $pdf->mmy, "<b>".$pdf->Record->{'includingtax'}."</b>" );
  $pdf->mmy -= $pdf->Pt2Mm($pdf->p->getFontHeight($pdf->points));

  $pdf->Line(190,$pdf->mmy+2,173,$pdf->mmy+2);
  $pdf->Line(190,$pdf->mmy+1.5,173,$pdf->mmy+1.5);

  // Do column headers last
  $pdf->mmy = $top_line + $pdf->Pt2Mm($pdf->p->getFontHeight($pdf->points)) + 2;
  $pdf->WriteAtJust( 'left', 32, $pdf->mmy, "<b>Description</b>" );
  if ( $non_100_percent ) {
    $pdf->WriteAtJust( 'right',160, $pdf->mmy, "<b>Amount</b>" );
    $pdf->WriteAtJust( 'right',170, $pdf->mmy, "<b>%age</b>" );
    $pdf->WriteAtJust( 'right',190, $pdf->mmy, "<b>Share</b>" );
  }
  else {
    $pdf->WriteAtJust( 'right',190, $pdf->mmy, "<b>Amount</b>" );
  }
}

$pdf->SetLineStyle( 0.2, '', '', array( 9, 4 ));
$pdf->Line(6,57,204,57);

$pdf->SetFont("Times-Roman",16);
$pdf->WriteAtJust( 'left',21, 50, "<b>Remittance Advice</b>");

$pdf->SetFont("Times-Roman",11);
$pdf->SetMargins( 252, 21, 0, 10);
$pdf->WriteParagraphs("Please remit directly to our bank account:\n<b>$client_name\n$bank_branch_name, $bank_account_no</b>");
$pdf->WriteParagraphs("\nOr return this portion with your cheque to:\n$client_address");

$pdf->WriteAtJust( 'left',115, 50, "Our Ref:" );
$pdf->WriteAtJust( 'left',140, 50, sprintf("<b>%s%d/I%d</b>", $pdf->Record->{'entitytype'}, $pdf->Record->{'entitycode'}, $pdf->Record->{'invoiceno'}));
$pdf->WriteAtJust( 'right',200, 50, sprintf("%s", $pdf->Record->{'invoice_date'}));

$pdf->SetMargins( 252, 115, 0, 10);
$pdf->WriteParagraphs("From:\nRe:\nAmount Due:\n\nAmount Paid:");

$pdf->SetMargins( 252, 140, 0, 10);
$pdf->WriteParagraphs(sprintf("%s\n%s\n%s", $pdf->Record->{'entityname'}, substr($pdf->Record->{'todetail'},0,30), $pdf->Record->{'includingtax'}));

