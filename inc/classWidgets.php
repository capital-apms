<?php
/**
* Widgets for the top of a report or process screen
* @package   apms
* @subpackage   classWidgets
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Catalyst IT Ltd, Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2
*/
include_once("classEditor.php");

class Widget extends Editor
{
  var $WidgetID;

  function Widget( $widgetid = null ) {
    global $session, $component, $form_id_increment;
    parent::__construct( null );
    if ( !isset($widgetid) ) {
      $widgetid = preg_replace( '#/([^/]+)\.php#i', '$1', $_SERVER['PHP_SELF'] ) . '_' .$component . '_' . $form_id_increment;
    }
    $this->WidgetID = $this->Id($widgetid);
    $this->SetBaseTable( "rp" );
    $this->SetSubmitName("submit_".$this->WidgetID);
  }

  function WidgetId() {
    return $this->WidgetID;
  }

  function Defaults( $defaults ) {
    if ( !$this->Available() ) $this->Initialise($defaults);
  }

  function ReadWrite() {
    global $session;

    $this->SetWhere( "reportid='$this->WidgetID' AND user_no='$session->user_no'" );
    if ( $this->IsSubmit() ) {
      $_POST['reportid'] = $this->WidgetID;
      $_POST['user_no'] = $session->user_no;

      /** Assign the field aliases back to the real fields */
      foreach( $this->Fields AS $k => $v ) {
        if ( isset($_POST[$v->Field]) ) $_POST[$v->Sql] = $_POST[$v->Field];
      }
      $this->WhereNewRecord( "reportid='$this->WidgetID' AND user_no='$session->user_no'" );
      $this->Write();
    }
    else {
      $this->GetRecord();
    }
  }
}
