-- Conversions only applying to the Catalyst ledger

-- Fix up the months table, which I keep pointing out is wrong...
update month set financialyearcode = '2008' where startdate >= '2008-04-01' and startdate <= '2009-03-31';
update month set financialyearcode = '2009' where startdate >= '2009-04-01' and startdate <= '2010-03-31';
update month set financialyearcode = '2010' where startdate >= '2010-04-01' and startdate <= '2010-03-31';
update month set financialyearcode = '2011' where startdate >= '2011-04-01' and startdate <= '2011-03-31';

-- Clean up some messy data
insert into person values(108,'',null,'','New Zealand Growth Properties');
delete from creditor where creditorcode = 450;

BEGIN;
CREATE TEMP TABLE bad_vouchers
    AS SELECT DISTINCT voucherseq, 'VCHR' ||voucherseq::text AS reference FROM voucherline
        WHERE NOT EXISTS (SELECT 1 FROM voucher WHERE voucherline.voucherseq = voucher.voucherseq);
DELETE FROM bad_vouchers WHERE EXISTS (SELECT 1 FROM document WHERE document.reference = bad_vouchers.reference);
DELETE FROM bad_vouchers WHERE EXISTS (SELECT 1 FROM accttran WHERE accttran.reference = bad_vouchers.reference);
DELETE FROM voucherline WHERE voucherseq IN (SELECT voucherseq FROM bad_vouchers);
DROP TABLE bad_vouchers;
COMMIT;

DELETE FROM newaccttrans WHERE not exists (select 1 from newdocument where newdocument.batchcode = newaccttrans.batchcode and newdocument.documentcode = newaccttrans.documentcode);
