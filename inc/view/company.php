<?php

// Viewer component for company records
$viewer = new Viewer("Company");
$viewer->AddField( 'companycode', 'l.companycode' );
$viewer->AddField( 'active', "CASE WHEN l.active THEN 'Active' ELSE 'Inactive' END" );
$viewer->AddField( 'legalname', 'l.legalname' );
$viewer->AddField( 'parentcode', 'CASE WHEN l.parentcode > 0 THEN l.parentcode ELSE NULL END' );
$viewer->AddField( 'parentname', "COALESCE(p.legalname,'')" );
$viewer->AddField( 'clientcode', 'l.clientcode' );
$viewer->AddField( 'shortname', 'l.shortname' );
$viewer->AddField( 'registeredaddress', 'l.registeredaddress' );
$viewer->AddField( 'registeredno', 'l.registeredno' );
$viewer->AddField( 'incorporationdate', 'l.incorporationdate' );
$viewer->AddField( 'taxno', 'l.taxno' );
$joins = "company l LEFT JOIN client c USING ( clientcode ) LEFT OUTER JOIN company p ON (p.companycode=l.parentcode)";
$viewer->SetJoins( $joins );
$id = intval($_REQUEST['id']);
$viewer->SetWhere( 'l.companycode='.$id );

$fy_code = ( $c->current_financial_year - 1);
if ( isset($_GET['fy']) ) {
  $fy_code = intval($_GET['fy']);
}


$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Company:</th>
  <td class="left">##companycode.value##, "##shortname.value##"</td>
  <th class="right">Legal Name:</th>
  <td class="left">##legalname.value##</td>
 </tr>
 <tr>
  <th class="right">Registered Address:</th>
  <td class="left" style="width:15em">##registeredaddress.value##</td>
  <th class="right">Parent:</th>
  <td class="left">##parentcode.value## ##parentname.value##</td>
 </tr>
 <tr>
 </tr>
 <tr>
  <th class="right">Active:</th>
  <td class="left">##active.value##</td>
  <th class="right">Company No:</th>
  <td class="left">##registeredno.value##</td>
 </tr>
 <tr>
  <th class="right">IRD no:</th>
  <td class="left">##taxno.value##</td>
  <th class="right">Incorporation:</th>
  <td class="left">##incorporationdate.value##</td>
 </tr>
</table>
<br>
<h2>As at end of $fy_code financial year</h2>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Company: ".$viewer->Record->{'legalname'});
$page_elements[] = $viewer;

include_once("menus_entityaccount.php");


// Now list state at last year end
require_once('classBrowser.php');

$template = new Browser("Accounts");
$template->AddHidden( 'account', "entitytype || '-' || TO_CHAR(entitycode,'FM00009') || '-' || TO_CHAR(accountcode,'FM0009.00')" );
$template->AddHidden( 'entitytype' );
$template->AddHidden( 'entitycode' );
$template->AddColumn( 'accountgroupcode', 'Group', 'left', '<td class="left" style="width:5em;">%s</td>' );
$template->AddColumn( 'accountcode', 'Account', 'right', '', "TO_CHAR(accountcode,'FM0009.00')" );
$template->AddColumn( 'name', 'Name', 'left', '<td class="left" style="width:30em;">%s</td>', 'chartofaccount.name' );
$template->AddColumn( 'balance', 'Balance', 'right', '%0.2lf', '(CASE WHEN COALESCE(creditgroup,FALSE) THEN -1 ELSE 1 END * balance)' );
$template->AddTotal( 'balance' );
$template->SetWhere( "balance != 0.0" );
$rowurl = '/view.php?t=account&id=%s';
$template->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'account', '#even' );

/**
* Income and expenditure accounts just look at the current financial year
*/
$template->SetJoins( "year_end_balance('L',$id,$fy_code) LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode )" );

$browser = clone($template);
$browser->Title("Income Accounts");
$browser->AndWhere("grouptype = 'P' AND creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 1 );
$page_elements[] = $browser;

$browser = clone($template);
$browser->Title("Expense Accounts");
$browser->AndWhere("grouptype = 'P' AND NOT creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 2 );
$page_elements[] = $browser;


$sql = "SELECT sum(balance) FROM year_end_balance('L',$id,$fy_code) LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode ) WHERE grouptype = 'P'";
$qry = new PgQuery($sql);
if ( $qry->Exec('company_year_end') && $qry->rows == 1 ) {
  $r = $qry->Fetch();
  $net_type = ($r->sum < 0 ? 'Profit' : 'Loss');
  $page_elements[] = sprintf( "<h3>Net %s is %14.2lf</h3>\n", $net_type, abs($r->sum) );
}


/**
* Asset and liability accounts look at the full history, which is included in the
* function we are calling to get the balance.
*/

$browser = clone($template);
$browser->Title("Asset Accounts");
$browser->AndWhere("grouptype != 'P' AND NOT creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 3 );
$page_elements[] = $browser;

$browser = clone($template);
$browser->Title("Liability Accounts");
$browser->AndWhere("grouptype != 'P' AND creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 4 );
$page_elements[] = $browser;

$sql = "SELECT sum(balance) FROM year_end_balance('L',$id,$fy_code) LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode ) WHERE grouptype IS NULL";
$qry = new PgQuery($sql);

if ( $qry->Exec('company_year_end') && $qry->rows == 1 ) {
  $r = $qry->Fetch();
  if ( $r->sum != 0 ) {
    $browser = clone($template);
    $browser->Title("Bogus Accounts Incorrectly Set Up");
    $browser->AndWhere("grouptype IS NULL" );
    $browser->SetOrdering( 'accountcode', 'ASC', 5 );
    $page_elements[] = $browser;
  }
}


$sql = "SELECT sum(balance) FROM year_end_balance('L',$id,$fy_code) LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode ) WHERE grouptype != 'P'";
$qry = new PgQuery($sql);
if ( $qry->Exec('company_year_end') && $qry->rows == 1 ) {
  $r = $qry->Fetch();
  $net_type = ($r->sum < 0 ? 'Liabilities' : 'Assets' );
  $page_elements[] = sprintf( "<h3>Net %s are %14.2lf</h3>\n", $net_type, abs($r->sum) );
}

