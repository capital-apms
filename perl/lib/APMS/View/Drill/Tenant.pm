package APMS::View::Drill::Tenant;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Panel;
use APMS::View::Browser;
use APMS::View::Buttons;

use base qw/ APMS::View::Drill /;

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	# Create the panel
	$self->{panel} = APMS::View::Panel->radio_choose(
		{
			name     => 'Filter By',
			options  => [ 'All', 'Inactive', 'Active' ]
		},
		{
			name     => 'Sort By',
			options  => [ 'Name', 'Code', 'Property' ]
		}
	);

	$self->{alphabet} = APMS::View::Panel->alphabet_soup();

	# Create the browse window
	$self->{browser} = APMS::View::Browser->browse_window(
		fields => [
			{
				name   => 'Tenant',
				column => 'tenantcode',
				type   => 'Glib::Uint',
				widget => 'Gtk2::CellRendererText'
			}, {
				name   => 'A',
				column => 'active',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}, {
				name   => 'Entity',
				column => 'entitycode',
				type   => 'Glib::Uint',
				widget => 'Gtk2::CellRendererText',
				dosort => 1
			}, {
				name   => 'Name',
				column => 'name',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				expand => 1,
				minw   => 100,
				dosort => 1
			}, {
				name   => 'Balance',
				column => 'cfbalance',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => $data{tenantlist}
	);

	# Pack them into a button box
	$self->{buttons} = APMS::View::Buttons->button_pack( 'H', $data{buttons} );

	# Hand these objects to a generic drill window view
	$self->drill_window(
		title     => 'Tenants',
		panel     => $self->{panel},
		alphabet  => $self->{alphabet},
		browser   => $self->{browser},
		buttons   => $self->{buttons}
	);

	return $self;
}

=item selected_row_index

Return the selected index for the browser box

=cut

sub selected_tenantcode {
	my $self = shift;

	my $iter = 0;
	my $selection = $self->{browser}->get_selection();
	my ( $model, $iter ) = $selection->get_selected();
	return 0 unless $model;
	return $model->get_value( $iter, 0 );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

