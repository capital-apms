package APMS::Control::Maint::BankAccount;

use strict;
use APMS::Class qw/ new_object /;
use APMS::Constants qw/ APMS_VIEW APMS_MAINT APMS_ADD /;
use APMS::View::Maint::BankAccount;

use base qw/ APMS::Control::Window /;

=head1 APMS::Control::Maint::Invoice

Controller for the invoice maintenance screen

=cut

sub run {
	my ( $proto, $apms, $thisnode, %args ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;

	# What mode is this screen called in
	my $mode;

	if ( $args{mode} =~ /^view$/i ) {
		$mode = APMS_VIEW;
	}
	elsif ( $args{mode} =~ /^maintain$/i ) {
		$mode = APMS_MAINT;
	}
	elsif ( $args{mode} =~ /^add$/i ) {
		$mode = APMS_ADD;
	}

	# If not in ADD mode need the selected row in drill window
	if ( $mode != APMS_ADD ) {

		# Fetch the selected record id from the previous window
		return unless exists $args{recordid};

		# Its a function ref since %args was attached to the button before the
		# user pressed the button that brought them here.
		my $accountcode = $args{recordid}->();

		unless ( $accountcode ) {
			$apms->errormessage( "No bank account selected" );
			return;
		}

		# Used the (hopefully) passed recid to find the working record
		$self->{bankaccount} = $apms->model->bank_account( $accountcode );
	}

	# Create the edit window
	$self->{bankwindow} = APMS::View::Maint::BankAccount->new( bankaccount => $self->{bankaccount} );

	# Attach the destroy event
	$self->{bankwindow}->attach_destroy( sub { $self->destroy_window( @_ ) } );

	return $self->{bankwindow};
}

=item save_record

Callback for pushing save

=cut

sub save_record {
	print "saving record\n";
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

