
require 'dbi'
require 'gtk2'
require 'gconf2'

#
# == APMS
# The various classes and so forth used by APMS
#
# == Bugs
# Why!  The VERY idea!
#
# == Author
# Andrew McMillan <andrew@mcmillan.net.nz>
#
# == License
# Copyright (c) Andrew McMillan, 2007
# GNU General Public License version 2
#

require 'APMS/RecordBrowser'
require 'APMS/Window'
require 'APMS/Button'
require 'APMS/MenuLinks'
