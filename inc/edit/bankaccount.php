<?php
// The table we are maintaining here looks like this.
//
//     Column       |  Type   | Modifiers
// -----------------+---------+-----------
// bankaccountcode  | text    | default ''::text
// accountname      | text    | default ''::text
// bankbranchname   | text    | default ''::text
// bankname         | text    | default ''::text
// bankaccount      | text    | default ''::text
// companycode      | integer | default 0
// accountcode      | numeric | default (0)::numeric
// lastmodifieddate | date    | default '0001-01-01'::date
// lastmodifiedtime | integer | default 0
// lastmodifieduser | text    | default ''::text
// auditrecordid    | integer | default 0
// chequeaccount    | boolean | default false
// active           | boolean | default true
// deuserid         | text    | default ''::text
//

// Editor component for debtor records
$editor = new Editor("Bank Account", "bankaccount");

param_to_global('id', '#^[A-Z0-9]+$#', 'oldbankaccountcode', 'bankaccountcode');
$editor->SetWhere( "bankaccountcode=".qpg($id) );

param_to_global('newcode', '#^[A-Z0-9]+$#', 'bankaccountcode');
$editor->WhereNewRecord( "bankaccountcode=".qpg($newcode) );

if ( $editor->IsSubmit() ) {
  $editor->Write();
  if ( $id != $newcode ) {
    $id = $newcode;
    $editor->SetWhere( "bankaccountcode=".qpg($id) );
    $editor->GetRecord();
  }
}
else {
  $editor->GetRecord();
  if ( ! $editor->Available() ) {
    $editor->Initialise( array( 'bankaccountcode' => '', 'accountname' => '' ) );
  }
}

$editrow = new Editor("Bank Import Rules", "bankimportrule");
$editrow->AddField( 'rule_accountcode', 'bankimportrule.accountcode' );
$editrow->SetSubmitName( 'saverow' );
if ( $editrow->IsSubmit() ) {
  $_POST['bankaccountcode'] = $id;
  $_POST['accountcode'] = $_POST['rule_accountcode'];
  if ( ! $editrow->IsUpdate() ) {
    $sql = "SELECT ruleseq FROM bankimportrule WHERE bankaccountcode = ? ORDER BY ruleseq DESC LIMIT 1";
    $qry = new PgQuery($sql, $id);
    if ( $qry->Exec("bankaccount") &&  $qry->rows == 1 ) {
      $row = $qry->Fetch();
      $ruleseq = $row->ruleseq + 1;
    }
    else {
      $ruleseq = 1;
    }
    $_POST['ruleseq'] = $ruleseq;
  }
  else {
    $_POST['ruleseq'] = intval($_REQUEST['editseq']);
    $ruleseq = $_POST['ruleseq'];
  }
  $editrow->SetWhere( "bankaccountcode=".qpg($id)." AND ruleseq=$ruleseq");
  $editrow->Write( );
  unset($_GET['editseq']);
}



$template = <<<EOTEMPLATE
##form##
<table>
 <tr>
  <th class="right">Bank Account Code:</th>
  <td class="left"><input type="hidden" name="oldbankaccountcode" value="##bankaccountcode.enc##">##bankaccountcode.input.4##</td>
 </tr>
 <tr>
  <th class="right">Account Name</th>
  <td class="left">##accountname.input.50##</td>
 </tr>
 <tr>
  <th class="right">Bank Name</th>
  <td class="left">##bankname.input.50##</td>
 </tr>
 <tr>
  <th class="right">Branch Name</th>
  <td class="left">##bankbranchname.input.50##</td>
 </tr>
 <tr>
  <th class="right">Bank Account</th>
  <td class="left">##bankaccount.input.50##</td>
 </tr>
 <tr>
  <th class="right">Company Code</th>
  <td class="left">##companycode.input.5##</td>
 </tr>
 <tr>
  <th class="right">Account Code</th>
  <td class="left">##accountcode.input.7##</td>
 </tr>
 <tr>
  <th class="right">Cheque Account</th>
  <td class="left">##chequeaccount.checkbox##</td>
 </tr>
 <tr>
  <th class="right">Active</th>
  <td class="left">##active.checkbox##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left">##submit##</td>
 </tr>
</table>
</form>

EOTEMPLATE;

$editor->SetTemplate( $template );

$c->page_title = $editor->Title("Bank Account: ".$editor->Value('accountname') );

$page_elements[] = $editor;

$related_menu->AddOption("View Bank Account","/view.php?t=bankaccount&id=$id","View this Bank Account.");
$related_menu->AddOption("Edit Bank Account","/edit.php?t=bankaccount&id=$id","Edit this Bank Account.");


if ( $editor->IsUpdate() ) {

  function edit_row( $row_data ) {
    global $editrow, $id;

    $rowsubmittype = ($row_data->ruleseq == -1 ? 'Add' : 'Save');
    $form_url = preg_replace( '#&(edit|del)seq=\d+#', '', $_SERVER['REQUEST_URI'] );

    $template = <<<EOTEMPLATE
<form method="POST" enctype="multipart/form-data" id="rowentry" action="$form_url">
  <td class="left">##ruleseq.value##<input type="hidden" name="id" value="$id"><input type="hidden" name="editseq" value="$row_data->ruleseq"></td>
  <td class="left">##trntype.input.12##</td>
  <td class="left">##match1.input.12##</td>
  <td class="left">##match2.input.12##</td>
  <td class="left" style="white-space:nowrap">##entitytype.input.1####entitycode.input.3####rule_accountcode.input.7##</td>
  <td class="left">##description.input.30##</td>
  <td class="left">##$rowsubmittype.submit##</td>
</form>

EOTEMPLATE;

    $editrow->SetTemplate( $template );
    $editrow->Title("");
    if ( $row_data->ruleseq == -1 ) {
      $editrow->Initialise( (array) $row_data );
    }
    else
      $editrow->SetRecord( $row_data );

    return $editrow->Render();
  }

  require_once('classBrowser.php');
  $browser = new Browser("Bank Import Rules");
  $browser->AddHidden( 'bankaccountcode' );
  $browser->AddHidden( 'entitytype' );
  $browser->AddHidden( 'entitycode' );
  $browser->AddHidden( 'accountcode' );
  $browser->AddHidden( 'rule_accountcode', 'accountcode' );
  $browser->AddColumn( 'ruleseq', '#', 'right' );
  $browser->AddColumn( 'trntype', 'TxnType', 'left' );
  $browser->AddColumn( 'match1', 'Other Party', 'left' );
  $browser->AddColumn( 'match2', 'Bank Reference', 'left' );
  $browser->AddColumn( 'entity', 'Entity', 'left', '', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')" );
  $browser->AddColumn( 'description', 'Description', 'left' );
  $browser->SetOrdering( 'ruleseq' );
  $browser->SetJoins( "bankimportrule" );

  $rowurl = '/edit.php?t=bankaccount&id=%s&editseq=%d';
  $browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Edit\" class=\"r%d\">\n", "</tr>\n", 'bankaccountcode', 'ruleseq', '#even' );

  $edit_link = "<a href=\"/edit.php?t=bankaccount&id=$id&editseq=##ruleseq##\" class=\"submit\">Edit</a>";
  $del_link  = "<a href=\"/edit.php?t=bankaccount&id=$id&delseq=##ruleseq##\" class=\"submit\">Del</a>";
  $browser->AddColumn( 'action', 'Action', 'center', '', "'$edit_link&nbsp;$del_link'" );
  $browser->AddOrder( 'ruleseq', 'ASC' );
  $browser->SetJoins( "bankimportrule" );

  $browser->AndWhere( "bankaccountcode=".qpg($id) );
  $rowurl = '/edit.php?t=bankaccount&id=%s&editseq=%d';
  $browser->RowFormat( "<tr class=\"r%d\">\n", "</tr>\n", 'bankaccountcode', 'ruleseq', '#even' );
  $page_elements[] = $browser;

  if ( isset($_GET['delseq']) ) {
    $qry = new PgQuery("DELETE FROM bankimportrule WHERE bankaccountcode=? AND ruleseq = ?", $id, intval($_GET['delseq']) );
    $qry->Exec('bankaccount');
  }
  if ( isset($_GET['editseq']) ) {
    $browser->MatchedRow('ruleseq', intval($_GET['editseq']), 'edit_row');
  }
  else {
    $extra_row = array( 'ruleseq' => -1 );
    $browser->MatchedRow('ruleseq', -1, 'edit_row');
    $extra_row = (object) $extra_row;
    $browser->AddRow($extra_row);
  }

}
