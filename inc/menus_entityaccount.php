<?php
switch( $component ) {
  case 'creditor':   if ( isset($id) ) $creditorcode = $id;    break;
  case 'debtor':     if ( isset($id) ) $debtorcode = $id;      break;
  case 'company':    if ( isset($id) ) $companycode = $id;     break;
  case 'property':   if ( isset($id) ) $propertycode = $id;    break;
  case 'project':    if ( isset($id) ) $projectcode = $id;     break;
  case 'asset':      if ( isset($id) ) $assetcode = $id;       break;
  case 'voucher':    if ( isset($id) ) $vouchercode = $id;     break;
  case 'invoice':    if ( isset($id) ) $invoicecode = $id;     break;
  default:
   if ( isset($et) && isset($ec) ) {
     switch( $et ) {
       case 'C':   $creditorcode = $ec;    break;
       case 'T':   $debtorcode = $ec;      break;
       case 'J':   $projectcode = $ec;     break;
       case 'L':   $companycode = $ec;     break;
       case 'P':   $propertycode = $ec;    break;
       case 'A':   $assetcode = $ec;       break;
     }
   }
}

if ( isset($debtorcode) && $debtorcode > 0 ) {
  $related_menu->AddOption("View Debtor","/view.php?t=debtor&id=$debtorcode","View the details for this debtor", false, 1000);
  $related_menu->AddOption("Edit Debtor","/edit.php?t=debtor&id=$debtorcode","Edit the details for this debtor", false, 1001);

  $related_menu->AddOption("Debtor Invoices","/browse.php?t=invoices&et=T&ec=$debtorcode","Browse the invoices for this debtor", false, 3000);
  $related_menu->AddOption("New Invoice","/edit.php?t=invoice&et=T&ec=$debtorcode","Create a new invoice for this debtor", false, 3100);

  $related_menu->AddOption("Debtor Transactions","/browse.php?t=transactions&et=T&ec=$debtorcode","Browse the transactions for this debtor", false, 5000);
  $related_menu->AddOption("Close Transactions","/action.php?t=close_transactions&et=T&ec=$debtorcode","Close transactions for this debtor", false, 6000);
}

if ( isset($creditorcode) && $creditorcode > 0 ) {
  $related_menu->AddOption("View Creditor","/view.php?t=creditor&id=$creditorcode","View the details for this creditor", false, 1000);
  $related_menu->AddOption("Edit Creditor","/edit.php?t=creditor&id=$creditorcode","Edit the details for this creditor", false, 1001);

  $related_menu->AddOption("Creditor Vouchers","/browse.php?t=vouchers&creditorcode=$creditorcode","Browse the vouchers of this creditor", false, 3000);
  $related_menu->AddOption("New Voucher","/edit.php?t=voucher&creditorcode=$creditorcode","Create a new voucher for this creditor", false, 3100);

  $related_menu->AddOption("Creditor Transactions","/browse.php?t=transactions&et=C&ec=$creditorcode","Browse the transactions for this creditor", false, 5000);
  $related_menu->AddOption("Close Transactions","/action.php?t=close_transactions&et=C&ec=$creditorcode","Close transactions for this creditor", false, 6000);
}
if ( isset($vouchercode) ) {
  $related_menu->AddOption("Clone Voucher","/edit.php?t=voucher&copy=$vouchercode","Clone this voucher into a new voucher for this creditor", false, 3100);
}
if ( isset($invoicecode) ) {
  $related_menu->AddOption("Clone Invoice","/edit.php?t=invoice&copy=$invoicecode","Clone this invoice into a new invoice for this debtors", false, 3100);
}

if ( isset($propertycode) && $propertycode > 0 ) {
  $related_menu->AddOption("View Property","/view.php?t=property&id=$propertycode","View the details for this property", false, 1000);
  $related_menu->AddOption("Edit Property","/edit.php?t=property&id=$propertycode","Edit the details for this property", false, 1001);

  $related_menu->AddOption("Property Accounts","/browse.php?t=accounts&et=P&ec=$propertycode","Browse the accounts for this property", false, 7100);
}

if ( isset($companycode) && $companycode > 0 ) {
  $related_menu->AddOption("View Company","/view.php?t=company&id=$companycode","View the details for this company", false, 1000);
  $related_menu->AddOption("Edit Company","/edit.php?t=company&id=$companycode","Edit the details for this company", false, 1001);
  $related_menu->AddOption("Company Accounts","/browse.php?t=accounts&et=L&ec=$companycode","Browse the accounts for this company", false, 3000);
  $related_menu->AddOption("View Year End","/view.php?t=company_year_end&id=$companycode".($year > 0 ? "&year=$year" : ""),"View end of year balances for this company", false, 4000);
  $related_menu->AddOption("Monthly Balances","/view.php?t=company-by-month&id=$companycode".($year > 0 ? "&year=$year" : ""),"View detailed monthly balances for this company", false, 5000);
  $related_menu->AddOption("Run Year End","/action.php?t=year-end&companycode=$companycode","Create a batch of year end transactions for this company", false, 9800);
}

if ( isset($et) && isset($ec) && isset($ac) ) {
  $related_menu->AddOption("Account Balances","/view.php?t=account&et=$et&ec=$ec&ac=$ac","View the primary account for this $component", false, 7200);
  $related_menu->AddOption("Account Transactions","/browse.php?t=transactions&et=$et&ec=$ec&ac=$ac","Browse the transactions for this account", false, 7400);
}
else if ( isset($ac) ) {
  $related_menu->AddOption("Account Balances","/browse.php?t=accounts&ac=$ac","View balances for this account for different entities");
  $related_menu->AddOption("Account Transactions","/browse.php?t=transactions&ac=$ac","Browse transactions for this account");
}

if ( isset($ac) ) {
  $related_menu->AddOption("View Account Rec","/view.php?t=chartofaccount&ac=$ac","View the chart of accounts record");
  $related_menu->AddOption("Edit Account Rec","/edit.php?t=chartofaccount&ac=$ac","Edit the chart of accounts record");
}