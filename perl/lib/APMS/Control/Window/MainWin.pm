package APMS::Control::Window::MainWin;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Window::MainWin;

use base qw/
	APMS::Control::Window
	APMS::Control::ButtonsWindow
/;

sub run {
	my ( $proto, $apms ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;

	# Get the main window node code
	my $nodecode = $self->{apms}->model->mainmenucode();

	# Fetch the buttons for this window
	my $buttons = $self->{apms}->model->buttons( $self->{apms}->config->{UserName}, $nodecode );

	# Create the GUI view
	$self->{menuwindow} = APMS::View::Window::MainWin->new(
		apms    => $apms,
		buttons => $buttons,
		width      => $apms->config->{MainWinBG}->{Width},
		height     => $apms->config->{MainWinBG}->{Height},
		background => $apms->config->{MainWinBG}->{FileName}
	);

	# Attach the destroy event, with an extra call
	$self->{menuwindow}->attach_destroy( sub { $self->destroy_window( shift, $apms->quit ) } );

	$self->button_callbacks( $self->{menuwindow} );

	return $self->{menuwindow}->get_window();
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

