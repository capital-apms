<?php
require_once('../inc/always.php');
require_once('classBrowser.php');
require_once('classLinks.php');

$c->stylesheets[] = "css/browse.css";
$page_elements = array();

if ( ! @include_once( "browse/$component.php" ) ) {
  $c->messages[] = "browse/$component not found";
}

include('page-header.php');

// Page elements could be an array of viewers, browsers or something else
// that supports the Render() method...
$heading_level = null;
foreach( $page_elements AS $k => $page_element ) {
  if ( is_object($page_element) ) {
    echo $page_element->Render($heading_level);
    $heading_level = 'h2';
  }
  else {
    echo $page_element;
  }
}

if (function_exists("post_render_function")) {
  post_render_function();
}


include('page-footer.php');

