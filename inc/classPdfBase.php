<?php
/**
* Class for producing a formatted PDF report
*
* @package   apms
* @subpackage   classPdfBase
* @author    Andrew McMillan <andrew@catalyst.net.nz>
* @copyright Catalyst IT Ltd
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2
*/

class PdfPage {
  var $width;
  var $height;
  var $margin_top;
  var $margin_bottom;
  var $margin_left;
  var $margin_right;
  var $paper;

  function PdfPage( $paper = 'a4', $orientation = 'portrait' ) {
    $this->paper = $paper;
		switch (strtoupper($paper)){
			case 'A0': {$width = 840; $height = 1188; break;}
			case 'A1': {$width = 594; $height = 840; break;}
			case 'A2': {$width = 420; $height = 594; break;}
			case 'A3': {$width = 297; $height = 420; break;}
			case 'A4': default: {$width = 210; $height = 297; break;}
			case 'A5': {$width = 148.5; $height = 210; break;}
		}
		switch (strtolower($orientation)){
			case 'landscape':
				$a=$width;
				$width=$height;
				$height=$a;
				break;
		}
  }
}


class PdfBase {
  var $pages;
  var $current_paper;
  var $current_orientation;
  var $current_page;

  function PdfBase( $paper = 'a4', $orientation = 'portrait' ) {
    $this->pages = array();
    $this->current_page = -1;
    $this->NewPage($paper,$orientation);
  }

  function NewPage( $paper = 'a4', $orientation = 'portrait' ) {
    $this->pages[$this->current_page] &= new PdfPage( $paper, $orientation );
    $this->current_paper = $paper;
    $this->current_orientation = $orientation;
  }


}
?>