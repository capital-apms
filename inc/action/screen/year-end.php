<?php

if ( $widget->IsSubmit() ) return true;

param_to_global( 'companycode', 'int' );
param_to_global( 'monthcode', 'int' );

$c->page_title = $widget->Title( "Create Year End Journals" );
$widget->AddField( 'companycode', "int1", "SELECT companycode, legalname FROM company ORDER BY companycode;" );
$sql = <<<EOSQL
SELECT monthcode, to_char(enddate, 'FMDD Mon YYYY') || ' (' || financialyearcode || ' year)'FROM month
 WHERE NOT EXISTS( SELECT 1 FROM month m2 WHERE monthcode > month.monthcode AND financialyearcode = month.financialyearcode)
 ORDER BY monthcode;
EOSQL;
$widget->AddField( 'monthcode', 'int2', $sql );
$widget->Layout( <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Company:</th>
  <td class="left">##companycode.select##</td>
 </tr>
 <tr>
  <th class="right">Year End:</th>
  <td class="left">##monthcode.select##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left">##Run Year End.submit##</td>
 </tr>
</table>

EOTEMPLATE
);
$page_elements[] = $widget;

include_once("menus_entityaccount.php");
