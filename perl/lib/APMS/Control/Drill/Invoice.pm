package APMS::Control::Drill::Invoice;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Drill::Invoice;

use base qw/
	APMS::Control::Window
	APMS::Control::Drill
/;

sub run {
	my ( $proto, $apms, $nodecode, %args ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;

	# Fetch the buttons for this window
	$self->{buttons} = $apms->model->buttons( $apms->config->{UserName}, $nodecode );

	if ( exists $args{'key-name'} ) {
		if ( $args{'key-name'} = 'tenantcode' ) {
			if ( exists $args{recordid} ) {
				my $tenantcode = $args{recordid}->();

				return unless $tenantcode;
				$self->{invoicelist} = $apms->model->invoices_for_tenant( $tenantcode );
			}
		}
	}

	# Create the browse window
	$self->{drillwindow} = APMS::View::Drill::Invoice->new(
		title       => 'Invoices',
		buttons     => $self->{buttons},
		invoicelist => $self->{invoicelist}
	);

	# Attach the destroy event
	$self->{drillwindow}->attach_destroy( sub { $self->destroy_window( @_ ) } );

	# Connect the buttons
	$self->button_callbacks( $self->{drillwindow} );

	# Always return the view so it can be attached to the parent
	return $self->{drillwindow};
}

=item recordid

Callback for returning the selected record

=cut

sub recordid {
	my $self = shift;
	my $invoicenumber = $self->{drillwindow}->selected_invoiceseq();
	return $invoicenumber;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

