package APMS::View::Drill::BankAccount;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Panel;
use APMS::View::Browser;
use APMS::View::Buttons;

use base qw/ APMS::View::Drill /;

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	# Create the panel
	$self->{panel} = APMS::View::Panel->radio_choose(
		{
			name     => 'Filter By',
			options  => [ 'All', 'Inactive', 'Active' ]
		},
		{
			name     => 'Sort By',
			options  => [ 'Name', 'Code', 'Property' ]
		}
	);

	$self->{alphabet} = APMS::View::Panel->alphabet_soup();

	# Create the browse window
	$self->{browser} = APMS::View::Browser->browse_window(
		fields => [
			{
				name   => 'A/C',
				column => 'bankaccountcode',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				dosort => 1,
			}, {
				name   => 'Company',
				column => 'companycode',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				dosort => 1,
			}, {
				name   => 'Account',
				column => 'accountcode',
				type   => 'Glib::Uint',
				widget => 'Gtk2::CellRendererText',
				dosort => 1,
			}, {
				name   => 'Bank Name',
				column => 'bankname',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				expand => 1,
				minw   => 100,
				dosort => 1,
			}, {
				name   => 'Branch Name',
				column => 'branchname',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				expand => 1,
			}, {
				name   => 'Account Name',
				column => 'accountname',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				expand => 1,
			}, {
				name   => 'Bank A/C Code',
				column => 'bankaccount',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => $data{bankaccounts}
	);

	# Pack them into a button box
	$self->{buttons} = APMS::View::Buttons->button_pack( 'H', $data{buttons} );

	# Hand these objects to a generic drill window view
	$self->drill_window(
		title     => 'Tenants',
		panel     => $self->{panel},
		alphabet  => $self->{alphabet},
		browser   => $self->{browser},
		buttons   => $self->{buttons}
	);

	return $self;
}

=item selected_bankaccount

Return the selected bank account (the first column value - column 0)

=cut

sub selected_bankaccount {
	my $self = shift;

	my $iter = 0;
	my $selection = $self->{browser}->get_selection();
	my ( $model, $iter ) = $selection->get_selected();
	return 0 unless $model;
	return $model->get_value( $iter, 0 );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

