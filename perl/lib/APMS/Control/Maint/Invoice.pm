package APMS::Control::Maint::Invoice;

use strict;
use APMS::Class qw/ new_object /;
use APMS::Constants qw/ APMS_VIEW APMS_MAINT APMS_ADD /;
use APMS::View::Maint::Invoice;

use base qw/ APMS::Control::Window /;

=head1 APMS::Control::Maint::Invoice

Controller for the invoice maintenance screen

=cut

sub run {
	my ( $proto, $apms, $thisnode, %args ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;

	# What mode is this screen called in
	my $mode;

	if ( $args{mode} =~ /^view$/i ) {
		$mode = APMS_VIEW;
	}
	elsif ( $args{mode} =~ /^maintain$/i ) {
		$mode = APMS_MAINT;
	}
	elsif ( $args{mode} =~ /^add$/i ) {
		$mode = APMS_ADD;
	}

	#$mode = APMS_ADD;

	# Fetch the tenant record, or get a new tenant code if in ADD mode
	if ( $mode == APMS_ADD ) {
		$self->{invoicerecord}->{invoiceno} = $apms->model->available_invoice();
	}
	else {
		# Fetch the selected record id from the previous window
		return unless exists $args{recordid};

		# Its a function ref since %args was attached to the button before the
		# user pressed the button that brought them here.
		my $invoiceno = $args{recordid}->();

		unless ( $invoiceno ) {
			$apms->errormessage( "No invoice selected" );
			return;
		}

		# Used the (hopefully) passed recid to find the working record
		$self->{invoiceandlines} = $apms->model->invoice_record( $invoiceno );

		# This is the result of a join
		$self->{invoicerecord} = $self->{invoiceandlines}->[0];
		$self->{invoicelines} = $self->{invoiceandlines};
	}

	# Get required extra data for the invoice window select dropdowns
	$self->{invoiceterms} = $apms->model->invoice_terms();
	$self->{accountnames} = $apms->model->account_names();

	# Create the tenant window
	$self->{invoicewindow} = APMS::View::Maint::Invoice->new(
		invoicerecord  => $self->{invoicerecord},
		invoicelines   => $self->{invoicelines},
		accountnames   => $self->{accountnames},
		invoiceterms   => $self->{invoiceterms}
	);

	# Connect the inputs to the callbacks
	if ( $mode == APMS_ADD ) {
		# Check what the user just entered for a tenant ID
		#$self->{invoicewindow}->connect_signal( 'invoiceno', 'focus-out-event', sub { $self->check_invoice_no( @_ ) } );
	}
	else {
		# No callback and not editable
		$self->{invoicewindow}->disable_widgets( 'invoiceno' );
	}
	#$self->{invoicewindow}->connect_signal( 'save', 'clicked', sub { $self->save_record( @_ ) } );

	# Attach the destroy event
	$self->{invoicewindow}->attach_destroy( sub { $self->destroy_window( @_ ) } );

	$self->recalculate_topay();

	return $self->{invoicewindow};
}

=item save_record

Callback for pushing save

=cut

sub save_record {
	print "saving record\n";
}

sub recalculate_gst {
	my $self = shift;

	# Calculate the tax amount from the total and the GST
	if ( $self->{invoicewindow}->get_value( 'taxapplies' ) ) {
		$self->{invoicerecord}->{taxamount} = $self->{invoicerecord}->{total} * ( $self->{invoicerecord}->{gst} / 100 );
	}
	else {
		$self->{invoicerecord}->{taxamount} = 0;
	}
}

sub recalculate_topay {
	my $self = shift;
	$self->{invoicerecord}->{topay} = $self->{invoicerecord}->{total} + $self->{invoicerecord}->{taxamount};
	$self->{invoicewindow}->set_value( 'topay', $self->{invoicerecord}->{topay} );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

