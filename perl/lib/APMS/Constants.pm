package APMS::Constants;

require Exporter;
our @ISA    = qw/ Exporter /;
our @EXPORT = qw/ APMS_VIEW APMS_ADD APMS_MAINT /;

use constant APMS_VIEW  => 1;
use constant APMS_ADD   => 2;
use constant APMS_MAINT => 3;

1;
