<?php

// Viewer component for company records
$viewer = new Viewer("Company Year End");
$viewer->AddField( 'companycode' );
$viewer->AddField( 'name', 'l.legalname' );
$viewer->AddField( 'parentcode', 'l.parentcode' );
$joins = "company l LEFT JOIN client c USING ( clientcode ) ";
$viewer->SetJoins( $joins );
$id = intval($_REQUEST['id']);
$viewer->SetWhere( 'companycode='.$id );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Company:</th>
  <td class="center">##companycode.value##</td>
  <td class="left">##name.value##</td>
 </tr>
</table>

EOTEMPLATE;

param_to_global( 'fy_code', 'int', 'fy', 'year' );
if ( !isset($fy_code) ) $fy_code = $c->current_financial_year;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title($viewer->Record->{'name'}." at end of $fy_code financial year");
$page_elements[] = $viewer;

include_once("menus_entityaccount.php");

// Now list income accounts
require_once('classBrowser.php');

$template = new Browser("Accounts");
$template->AddHidden( 'account', "entitytype || '-' || TO_CHAR(entitycode,'FM00009') || '-' || TO_CHAR(accountcode,'FM0009.00')" );
$template->AddHidden( 'entitytype' );
$template->AddHidden( 'entitycode' );
$template->AddColumn( 'accountgroupcode', 'Group', 'left', '<td class="left" style="width:5em;">%s</td>' );
$template->AddColumn( 'accountcode', 'Account', 'right', '', "TO_CHAR(accountcode,'FM0009.00')" );
$template->AddColumn( 'name', 'Name', 'left', '<td class="left" style="width:30em;">%s</td>', 'chartofaccount.name' );
$template->AddColumn( 'balance', 'Balance', 'right', '%0.2lf', '(CASE WHEN COALESCE(creditgroup,FALSE) THEN -1 ELSE 1 END * balance)' );
$template->AddTotal( 'balance' );
$template->SetWhere( "balance != 0.0" );
$rowurl = '/view.php?t=account&id=%s';
$template->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'account', '#even' );

/**
* Income and expenditure accounts just look at the current financial year
*/
$template->SetJoins( "chartofaccount LEFT OUTER JOIN year_end_balance('L',$id,$fy_code) USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode )" );

$browser = clone($template);
$browser->Title("Income Accounts");
$browser->AndWhere("grouptype = 'P' AND creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 1 );
$page_elements[] = $browser;

$browser = clone($template);
$browser->Title("Expense Accounts");
$browser->AndWhere("grouptype = 'P' AND NOT creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 2 );
$page_elements[] = $browser;


$sql = "SELECT sum(balance) FROM year_end_balance('L',$id,$fy_code) LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode ) WHERE grouptype = 'P'";
$qry = new PgQuery($sql);
if ( $qry->Exec('company_year_end') && $qry->rows == 1 ) {
  $r = $qry->Fetch();
  $net_type = ($r->sum < 0 ? 'Profit' : 'Loss');
  $page_elements[] = sprintf( "<h3>Net %s is %14.2lf</h3>\n", $net_type, abs($r->sum) );
}


/**
* Asset and liability accounts look at the full history, which is included in the
* function we are calling to get the balance.
*/

$browser = clone($template);
$browser->Title("Asset Accounts");
$browser->AndWhere("grouptype != 'P' AND NOT creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 3 );
$page_elements[] = $browser;

$browser = clone($template);
$browser->Title("Liability Accounts");
$browser->AndWhere("grouptype != 'P' AND creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 4 );
$page_elements[] = $browser;

$sql = "SELECT sum(balance) FROM year_end_balance('L',$id,$fy_code) LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode ) WHERE grouptype IS NULL";
$qry = new PgQuery($sql);

if ( $qry->Exec('company_year_end') && $qry->rows == 1 ) {
  $r = $qry->Fetch();
  if ( $r->sum != 0 ) {
    $browser = clone($template);
    $browser->Title("Bogus Accounts Incorrectly Set Up");
    $browser->AndWhere("grouptype IS NULL" );
    $browser->SetOrdering( 'accountcode', 'ASC', 5 );
    $page_elements[] = $browser;
  }
}


$sql = "SELECT sum(balance) FROM year_end_balance('L',$id,$fy_code) LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode ) WHERE grouptype != 'P'";
$qry = new PgQuery($sql);
if ( $qry->Exec('company_year_end') && $qry->rows == 1 ) {
  $r = $qry->Fetch();
  $net_type = ($r->sum < 0 ? 'Liabilities' : 'Assets' );
  $page_elements[] = sprintf( "<h3>Net %s are %14.2lf</h3>\n", $net_type, abs($r->sum) );
}


function post_render_function() {
global $page_elements;

  $ledger_total = 0;
  foreach( $page_elements AS $element ) {
    if ( gettype($element) == 'object' && method_exists($element, 'GetTotal') ) {
      $ledger_total += $element->GetTotal( 'balance' );
      printf( "<p>Total for element is %14.2lf</p", $element->GetTotal( 'balance' ) );
    }

  }
  printf( "<p>Total for ledger is %14.2lf</p", $ledger_total );
}
