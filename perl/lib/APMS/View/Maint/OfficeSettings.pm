package APMS::View::Maint::OfficeSettings;

use strict;
use APMS::Class qw/ new_object /;
use Data::Dumper;

use base qw/ APMS::View /;

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	$self->{config} = $data{config};
	$self->new_window();
	$self->get_window->signal_connect( 'destroy' => sub { $self->end( @_ ) } );
	$self->get_window->set_default_size( 500, 500 );

	my $view = Gtk2::Viewport->new();
	my $scroll = Gtk2::ScrolledWindow->new();
	$scroll->set_policy( 'automatic', 'automatic' );
	$scroll->add( $view );
	$self->get_window->add( $scroll );

	if ( ref( $self->{config} ) =~ /HASH/ ) {
		$self->do_hash( $self->{config}, $view, () );
	}
	elsif ( ref( $self->{config} ) =~ /ARRAY/ ) {
		$self->do_array(  $self->{config}, $view, () );
	}

	$self->get_window->show_all();

	return $self;
}

=item do_hash

Given a hashref, and a widget to pack into, this turns the data structure into
a table of key value pairs. This function goes recursive if the hash value is a
reference to another hash.

=cut

sub do_hash {
	my ( $self, $hash, $frame, @path ) = @_;

	# Count the keys that are strings
	my @keys = ();
	foreach my $key ( keys %{ $hash } ) {
		unless ( ref( $key ) ) {
			push @keys, $key;
		}
	}
	my $keycount = scalar @keys;
	return 0 unless $keycount > 0;

	# Create a table for each of these keys
	my $ident = join '->', @path;
	$self->{tables}->{$ident} = Gtk2::Table->new( $keycount, 2, 0 );
	$frame->add( $self->{tables}->{$ident} );

	my $c = 0;
	foreach my $key ( @keys ) {

		if ( ref( $hash->{$key} ) =~ /HASH/ ) {
			push @path, $key;
			$self->{tables}->{$ident}->attach(
				$self->collapse_entry( $key, [ @path ] ),
				0, 1, $c, $c + 1, ['fill'], ['fill'], 3, 3
			);
			$self->do_hash( $hash->{$key}, $self->space_for_hash( $self->{tables}->{$ident}, 1, $c ), @path );
			pop @path;
		}
		elsif ( ref( $hash->{$key} ) =~ /ARRAY/ ) {
			push @path, $key;
			$self->{tables}->{$ident}->attach(
				$self->collapse_entry( $key, [ @path ] ),
				0, 1, $c, $c + 1, ['fill'], ['fill'], 3, 3
			);
			$self->do_array( $hash->{$key}, $self->space_for_hash( $self->{tables}->{$ident}, 1, $c ), @path );
			pop @path;
		}
		else {
			my $hbox = Gtk2::HBox->new( 0, 5 );
			my $label = Gtk2::Label->new( $key );
			$hbox->pack_start( $label, 0, 0, 0 );

			$self->{tables}->{$ident}->attach( $hbox, 0, 1, $c, $c + 1, ['fill'], ['fill'], 3, 3 );
			my $value = Gtk2::Entry->new();
			$value->set_text( $hash->{$key} );

			$value->signal_connect( 'changed' => sub { $self->value_changed( $key, [ @path ], @_ ) } );
			$self->{tables}->{$ident}->attach( $value, 1, 2, $c, $c + 1, ['fill','expand'], ['fill'], 3, 3 );
		}
		$c++;
	}
}

=item do_array

Given an array ref and a widget to pack into, create a graphical representation
of the data.

=cut

sub do_array {
	my ( $self, $array, $frame, @path ) = @_;

	# Count the elements
	my $arraycount = scalar @{ $array };
	my $ident = join '->', @path;
	$self->{tables}->{$ident} = Gtk2::Table->new( $arraycount, 1, 0 );
	$frame->add( $self->{tables}->{$ident} );

	my $c = 0;
	foreach my $val ( @{ $array } ) {

		if ( ref( $val ) =~ /HASH/ ) {
			push @path, '[' . $c . ']';
			$self->do_hash( $val, $self->space_for_hash( $self->{tables}->{$ident}, 0, $c ), @path );
			pop @path;
		}
		elsif ( ref( $val ) =~ /ARRAY/ ) {
			push @path, '[' . $c . ']';
			$self->do_array( $val, $self->space_for_hash( $self->{tables}->{$ident}, 0, $c ), @path );
			pop @path;
		}
		else {
			my $value = Gtk2::Entry->new();
			$value->set_text( $val );
			$value->signal_connect( 'changed' => sub { $self->value_changed( "[$c]", [ @path ], @_ ) } );
			$self->{tables}->{$ident}->attach( $value, 0, 1, $c, $c + 1, ['fill','expand'], ['fill'], 3, 3 );
		}
		$c++;
	}
}

sub collapse_entry {
	my ( $self, $text, $path, $hidden ) = @_;

	my $align = Gtk2::Alignment->new( 0, 0, 0, 0 );
	my $hbox = Gtk2::HBox->new( 0, 5 );
	my $label = Gtk2::Label->new( $text );

	my $ident = join( '->', @{ $path } );
	if ( $hidden ) {
		$self->{image}->{$ident} = Gtk2::Image->new_from_stock( 'gtk-zoom-in', 'small-toolbar' );
	}
	else {
		$self->{image}->{$ident} = Gtk2::Image->new_from_stock( 'gtk-zoom-out', 'small-toolbar' );
	}

	my $button = Gtk2::Button->new();
	$button->add( $self->{image}->{$ident} );
	$button->signal_connect( 'clicked' => sub { $self->compress_tree( $path, @_ ) } );
	$hbox->pack_start( $button, 0, 0, 0 );
	$hbox->pack_start( $label, 0, 0, 0 );
	$align->add( $hbox );

	return $align;
}

sub space_for_hash {
	my ( $self, $table, $c, $r ) = @_;
	my $nframe = Gtk2::Frame->new();
	$table->attach( $nframe, $c, $c + 1, $r, $r + 1, ['fill','expand'], ['fill'], 3, 3 );
	return $nframe;
}

sub compress_tree {
	my ( $self, $path ) = @_;
	my $ident = join( '->', @{ $path } );

	if ( exists $self->{tables}->{$ident} ) {
		if ( $self->{tables}->{$ident}->visible() ) {
			$self->{tables}->{$ident}->hide();
			if ( exists $self->{image}->{$ident} ) {
				$self->{image}->{$ident}->set_from_stock( 'gtk-zoom-in', 'small-toolbar' );
			}
		}
		else {
			$self->{tables}->{$ident}->show();
			if ( exists $self->{image}->{$ident} ) {
				$self->{image}->{$ident}->set_from_stock( 'gtk-zoom-out', 'small-toolbar' );
			}
		}
	}

	return 1;
}

=item

Callback to edit the original config structure when the user
changes the fields.

=cut

sub value_changed {
	my ( $self, $key, $path, $widget ) = @_;

	# Move along the config object path
	my $working = $self->{config};
	foreach my $bit ( @{ $path } ) {
		if ( $bit =~ /^\[(\d+)\]$/ ) {
			# References an array element
			my $element = $1;
			unless ( ref( $working ) =~ /ARRAY/ ) {
				print "path \"$path\" referenced an array element, but ref is not an array!\n";
			}
			else {
				$working = $working->[$element];
			}
		}
		else {
			# Must be a hash key then
			unless ( ref( $working ) =~ /HASH/ ) {
				print "path \"$path\" referenced by the key \"$bit\", but ref is not a hash!\n";
			}
			else {
				$working = $working->{$bit};
			}
		}
	}

	# Do the last element
	if ( $key =~ /^\[(\d+)\]$/ ) {
		my $element = $1;
		unless ( ref( $working ) =~ /ARRAY/ ) {
			print "path \"$path\" referenced an array element, but ref is not an array!\n";
		}
		else {
			$working->[$element] = $widget->get_text();
		}
	}
	else {
		unless ( ref( $working ) =~ /HASH/ ) {
			print "path \"$path\" referenced by the key \"$key\", but ref is not a hash!\n";
		}
		else {
			$working->{$key} = $widget->get_text();
		}
	}

	return 1;
}

sub end {
	my $self = shift;
	print Dumper( $self->{config} );
	$self->{window}->destroy();
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

