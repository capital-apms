package APMS::Control::Window::Menu;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Window::Menu;

# This needs some control bits for buttons
use base qw/
	APMS::Control::Window
	APMS::Control::ButtonsWindow
/;

sub run {
	my ( $proto, $apms, $nodecode ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;

	# Fetch the buttons for this window
	$self->{buttons} = $apms->model->buttons( $apms->config->{UserName}, $nodecode );

	# Create the GUI view
	$self->{menuwindow} = APMS::View::Window::Menu->new( buttons => $self->{buttons} );

	# Attach the callbacks to the buttons
	$self->button_callbacks( $self->{menuwindow} );

	# Attach this and sub-windows
	$self->{menuwindow}->attach_destroy( sub { $self->destroy_window( @_ ) } );

	# Return the view
	return $self->{menuwindow};
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

