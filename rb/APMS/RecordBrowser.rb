#
# == ApmsRecordBrowser
# A class which allows browsing a set of records.
#
# == Bugs
# Perish the thought!  Those are _features_!
#
# == Author
# Andrew McMillan <andrew@mcmillan.net.nz>
#
# == License
# Copyright (c) Andrew McMillan, 2007
# GNU General Public License version 2
#

# A basic field in an ApmsRecordBrowser
class ApmsField

  attr_reader :name, :heading, :type, :position
  attr_writer :heading, :type, :position

  def initialize(in_name)
    @name = in_name
    @heading = in_name
    @type = "Text"
    @position = nil
    puts " New field '#{in_name}'"
  end

  def set( what, value )
    case what
      when 'heading', '', nil
        @heading = value
        puts "#{@name} heading set to '#{@heading}'"
      when 'type'
        @type = value
        puts "#{@name} type set to '#{@type}'"
    end
  end

end

# Hash of ApmsField objects which is keyed on the field name
class ApmsFieldList
  def initialize
    @fields = Hash.new
    @positions = Array.new
  end

  def size
    return @positions.size
  end

  def fields
    return @fields
  end

  def positions
    return @positions
  end

  def each_field
    @positions.each_index do |idx|
      yield idx, @positions[idx], @fields[@positions[idx]]
    end
  end

  def set( name, what, value )
#    puts " Setting '#{what}' to '#{value}' in field '#{name}' "
    if ( @fields[name] == nil ) then
      @fields[name] = ApmsField.new(name)
      @positions.push(name)
      @fields[name].position = @positions.size
    end
    @fields[name].set(what,value)
  end
end

class ApmsRecordBrowser

  #
  # Creates an empty record browser with no columns and no rows, based on
  # Gtk::TreeView.  This class adds some utility functionality to load
  # the definition of the browser widget from a file.
  #
  def columns=(col_name_hash)
    set_columns(col_name_hash)
  end

  attr_reader :columns, :treeview, :size_x, :size_y

  def initialize(path)
    @fields = ApmsFieldList.new
    @sql = ""
    @size_x = 600
    @size_y = 100
    @treeview = Gtk::TreeView.new
    @liststore = Gtk::ListStore.new(String,String,String,String,String,String,String,String)
    load_browse_definition(path)
    @treeview.set_model(@liststore)
    @treeview.set_size_request(@size_x, @size_y)
    @treeview.set_headers_clickable(true)
    @treeview.set_enable_search(true)
    @treeview.set_rules_hint(true)

#    @treeview.signal_connect("event") do |w,e|
#      if e.event_type != Gdk::Event::MOTION_NOTIFY then
#        puts "Got event #{e} - '#{e.event_type.inspect}'"
#        puts e.inspect
#      end
#    end

#    @treeview.signal_connect("key-release-event") do |w,e|
#      puts "Got key-release event #{e} - '#{e.event_type.inspect}'"
#      puts e.inspect
#    end

  end

  def apply_fields
    n_columns = @fields.size
    @fields.each_field do |position,field,value|
      @treeview.append_column(Gtk::TreeViewColumn.new(value.heading, Gtk::CellRendererText.new, {:text => position} ))
    end
  end

  # This does kind of depend on the SQL returning the columns as named in the
  # call to set_columns
  def set_data( sql )
    puts "SQL is: #{sql}"
    rows_sth = $dbh.prepare(sql)
    rows_sth.execute()

    rows_sth.each do |row|
      values = @liststore.append
      @fields.each_field do |position,field,value|
        values.set_value(position,row[field].to_s)
      end
    end
  end

  #
  # Read the definitions of this browse window from a file
  #
  def load_browse_definition( filename )
    state = false
    File.foreach(filename) do |ln|

      if !state then
        if ln.match( /^([A-Z]+):\s*(#.*)?$/ )
          state = $1
        elsif ln.match( /^\s*(\S+)(:\S+)?\s*=\s*(\S.*\S?)\s*$/ ) then
          case $1
            when "X"
              @size_x = $3.to_i if ( $3.to_i > 100)

            when "Y"
              @size_y = $3.to_i if ( $3.to_i > 100)
          end
        end
        next
      end

      puts "State: '#{state}', Line: '#{ln.chomp}'"

      if ln.match( /^:#{state}$/ ) then
        state = false
        next
      end

      case state
        when "FIELDS"
          if ln.match( /^\s*(\S+)(:\S+)?\s*=\s*(\S.*\S?)\s*$/ ) then
            @fields.set($1, $2, $3 )
          end

        when "SQL"
          @sql += ln

        when "KEYS"
          if ln.match( /^\s*(\S+)\s*=\s*(\S.*\S?)\s*$/ ) then
            @keys[$1] = $2.split(/\s*,\s*/)
          end

      end

    end

    apply_fields
    set_data( @sql )

  end

  def selection
    return @treeview.selection
  end

  def get_key( key_name )
    iter = @treeview.selection
    key = Array.new
    @keys[key_name].each_index do |fieldname|
      key.push( iter[@fields.fields[fieldname].position] )
    end
  end

end
