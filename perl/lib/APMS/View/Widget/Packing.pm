package APMS::View::Widget::Packing;

=head1 APMS::View::Widget::Packing

Misc widget helpers. Mainly related to often-repeated packing jobs.

=cut

use strict;
use APMS::Class qw/ new_object /;

=item new_left_label new_right_label

Labels within table cells seem to float in the middle of the cell, without
putting them within expandable boxes and pack_start or pack_end'ng them, which
is what happends here. Put the objects returned from here inside fill and
expand table cells.

=cut

sub new_left_label {
	my ( $proto, $name ) = @_;

	my $box = Gtk2::HBox->new( 0, 0 );
	my $label = Gtk2::Label->new( $name );
	$box->pack_start( $label, 0, 0, 0 );

	return $box;
}

sub new_right_label {
	my ( $proto, $name ) = @_;

	my $box = Gtk2::HBox->new( 0, 0 );
	my $label = Gtk2::Label->new( $name );
	$box->pack_end( $label, 0, 0, 0 );

	return $box;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

