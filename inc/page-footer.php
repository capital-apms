<?php

$i_took = duration( $c->started, microtime() );   // calculate time taken for whole page generation

printf ('<p id="bottom_filler"><br clear="both">&nbsp;</p><div id="pagefooter"><p>Logged in as %s (%s),  Page generation took: %2.03lf, Queries took: %2.03lf</p></div>',
              $session->fullname, $session->username, $i_took, $c->total_query_time);

echo <<<EOFTR
<script><!--
window.defaultStatus = '$session->fullname';
--></script>
</body>
</html>
EOFTR;

