package APMS::Control::Window;

=item APMS::Control::Window

Generic controller functions for windows.

=cut

use strict;

=item destroy_window

This provides a controller method for dealing with a destroy event. This controller
method goes through each child window and fetches the callback for destroying that
window. Usually that callback will be this callback, hence windows are destroyed
recursively.

The $callback coderef is an optional callback that is used for the two windows in
in APMS that must call the main Gtk2->main_quit method when they are destroyed (via
$apms->quit).

=cut

sub destroy_window {
	my ( $self, $view, $callback ) = @_;

	foreach my $child ( $view->children ) {
		print "About to destroy " . $child->{id} . "\n";
		my $destroy = $child->destroy_callback();

		# Destroy the child (which may call this in recursion)
		next unless ref( $destroy ) =~ /CODE/;
		$destroy->( $self, $child );
	}

	# Destroy the GUI
	$view->get_window->destroy();
	$view->tell_parent_im_dead();

	if ( ref( $callback ) =~ /CODE/ ) {
		$callback->();
	}
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

