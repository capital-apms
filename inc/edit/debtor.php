<?php
// The flattened view we are maintaining here looks like this.
//
//         View "public.debtor_maintenance"
//          Column         |  Type   | Modifiers
// ------------------------+---------+-----------
//  debtorcode             | integer |
//  debtor_active          | boolean |
//  debtor_name            | text    |
//  debtclassification     | text    |
//  varianceclassification | text    |
//  debtor_et              | text    |
//  debtor_ec              | integer |
//  billingcontact         | integer |
//  billperson_title       | text    |
//  billperson_first       | text    |
//  billperson_last        | text    |
//  bill_company_name      | text    |
//  billperson_job         | text    |
//  bill_address           | text    |
//  bill_city              | text    |
//  bill_state             | text    |
//  bill_country           | text    |
//  bill_zip               | text    |
//  busphone_no            | text    |
//  busphone_isd           | text    |
//  busphone_std           | text    |
//

// Editor component for debtor records
$editor = new Editor("Debtor", "debtor_maintenance");
$editor->AddField( 'debtclassification', 'debtclassification', "SELECT '', 'Unclassified' UNION SELECT debtclassification, description FROM debtclassification" );
$editor->AddField( 'varianceclassification', 'varianceclassification', "SELECT '', 'Unclassified' UNION SELECT varianceclassification, description FROM varianceclassification" );
param_to_global('id', '#^\d+$#', 'olddebtorcode', 'debtorcode', 'tenantcode');

$editor->SetWhere( "debtorcode=$id" );

if ( $editor->IsSubmit() ) {
  $editor->WhereNewRecord( "debtorcode=currval('tenant_tenantcode_seq')" );
  $editor->Write();
}
else {
  $editor->GetRecord();
}
$id = $editor->Record->{'debtorcode'};

$editor->SetOptionList( 'billperson_title', array('Mr' => 'Mr', 'Ms' => 'Ms', 'Mrs' => 'Mrs' ), $editor->Record->{'billperson_title'} );
$editor->SetOptionList( 'debtor_et', array('L' => 'Company', 'J' => 'Project', 'P' => 'Property' ), $editor->Record->{'debtor_et'} );

include_once("menus_entityaccount.php");

$template = <<<EOTEMPLATE
##form##
<table>
 <tr>
  <th class="right">Debtor:</th>
  <td class="center">##debtorcode.enc##<input type="hidden" name="olddebtorcode" value="##debtorcode.enc##"></td>
  <td class="left">##debtor_name.input.50##</td>
 </tr>
 <tr>
  <th colspan="1">Billing Details</th>
  <th class="right">Address</th>
  <td class="left">##bill_address.textarea.40x2##<br>City: ##bill_city.input.16## State:##bill_state.input.16##<br>Country:##bill_country.input.20## Zip:##bill_zip.input.11##</td>
 </tr>
 <tr>
  <th colspan="1" rowspan="3">Contact</th>
  <th class="right">Person:</th>
  <td class="left"><select name="billperson_title">##billperson_title.options##</select> ##billperson_first.input.15## ##billperson_last.input.20##</td>
 </tr>
 <tr>
  <th class="right">Job Title:</th>
  <td class="left">##billperson_job.input.41##</td>
 </tr>
 <tr>
  <th class="right">Phone</th>
  <td class="left">+##busphone_isd.input.5## ( ##busphone_std.input.6## ) ##busphone_no.input.33##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">Active:</th>
  <td class="left">##debtor_active.checkbox## Debt Class:<select name="debtclassification">##debtclassification.options##</select> Variance Class:<select name="varianceclassification">##varianceclassification.options##</select></td>
 </tr>
 <tr>
  <th class="right" colspan="2">Account Ledger:</th>
  <td class="left"><select name="debtor_et">##debtor_et.options##</select> Code:##debtor_ec.input.5## ##debtor_entity_name.enc##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left" colspan="2">##submit##</td>
 </tr>
</table>
<form>

EOTEMPLATE;

$editor->SetTemplate( $template );

$c->page_title = $editor->Title("Debtor: ".$editor->Record->{'debtor_name'});

$page_elements[] = $editor;
