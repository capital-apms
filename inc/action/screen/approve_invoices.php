<?php
/**
* Component for browsing invoices for approval
* @package   apms
* @subpackage   approve_invoices
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/

param_to_global('status','#^[UA]#i');  if ( isset($status) ) $status = strtoupper($status); else $status = 'U';

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'year', 'int1', "SELECT 0, '--- All Years ---' UNION SELECT financialyearcode, description FROM financialyear ORDER BY 1;" );
$widget->AddField( 'status', 'char1', "SELECT invoicestatus, description FROM invoicestatus ORDER BY 1;" );
$widget->ReadWrite();
$widget->Defaults( array('status' => 'U', 'year' => $year) );
$widget->Layout( '<table><tr><th>For year:</th><td>##year.select##</td><th>Status:</th><td>##status.select##</td><td>##Show.submit##</td></tr></table>' );
$page_elements[] = $widget;


/**
* The main browse, which is a form, in this case.
*/
$browser = new Browser("Approve Invoices");
$c->page_title = $browser->Title();

if ( isset($et) && isset($ec) ) {
  $qry = new PgQuery( "SELECT get_entity_type_name(?) AS entitytypename, get_entity_name(?,?) AS entityname;", $et, $et, $ec);
  if ( $qry->Exec("action-$component") && $row = $qry->Fetch() ) {
    $browser->Title("$row->entitytypename Invoices for $row->entityname ($et$ec)");
  }
}

$browser->AddColumn( 'invoiceno', 'Inv#', 'center' );
$browser->AddColumn( 'invoicedate', 'Date', 'center' );
$browser->AddColumn( 'debtor', 'Debtor', 'left', '%s', 'get_entity_name(invoice.entitytype, invoice.entitycode)' );
$browser->AddColumn( 'todetail', 'Brief', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'invoicestatus', 'Approve', 'center', '<input type="checkbox" name="approve[##invoiceno##]" value="t">' );
$browser->AddColumn( 'total', 'Amount', 'right', '%0.2lf' );
$browser->SetJoins( "invoice" );
// $browser->SetJoins( "invoice JOIN invoiceline USING (invoiceno)" );
$browser->SetWhere( "invoice.invoicestatus=" . qpg($widget->Record->{'status'}) );
if ( isset($et) && isset($ec) )
  $browser->AndWhere( "invoice.entitytype='$et' AND invoice.entitycode=$ec" );
if ( $widget->Record->{'year'} > 0 ) {
  $browser->SetJoins("invoice JOIN month ON month.startdate <= invoice.invoicedate AND month.enddate >= invoice.invoicedate JOIN financialyear USING (financialyearcode)");
  $browser->AndWhere( "financialyearcode = " . qpg($widget->Record->{'year'}) );
}
$browser->AddOrder( 'invoiceno', 'ASC', 1 );
$rowurl = '/view.php?t=invoice&id=%s';
$browser->RowFormat( "<tr onclick=\"Toggle('approve[%s]');\" title=\"Click to Display Invoice Detail\" class=\"r%d\">\n", "</tr>\n", 'invoiceno', '#even' );

$browser->SetDiv( '<div class="browser"><form method="POST">', '<input class="submit" type="submit" name="submit" value="Approve"></div>' );

$page_elements[] = $browser;
