#!/usr/bin/perl -w
#
# Script to convert a progress definitions file (.df) into
# SQL suitable to construct a PostgreSQL database to match.
#

use strict;

my $current_table = undef;
my $current_field = undef;
my $current_index = 0;

my %xlate_types = (
      'character' => 'text',
      'decimal' => 'numeric',  # Safer to translate to Numeric than to double...
      'logical' => 'boolean',
      'raw' => 'text',
      'recid' => 'int8'
    );

my %xlate_names = (
      'limit' => 'limitation',
      'order' => 'purchaseorder'
    );

my %xlate_booleans = (
      'db' => 'FALSE',
      'no' => 'FALSE',
      'n' => 'FALSE',
      'yes' => 'TRUE',
      'y' => 'TRUE'
    );

my %other_field_data = ();

############################################################
# Called to add a sequence
############################################################
sub fix_name {
  my $name = shift ;
  $name =~ tr/A-Z-/a-z_/;
  $name = $xlate_names{$name} if ( defined($xlate_names{$name}) );
  return $name;
}

############################################################
# Called to close a table, if necessary
############################################################
sub possibly_close_table {

  print "\n" if ( defined($current_field) && $current_field > 0 );
  print ") WITHOUT OIDS;\n" if ( defined( $current_table) );

  $current_field = undef;
  $current_table = undef;
}

############################################################
# Called to add a sequence
############################################################
sub add_sequence {
  my $sname = fix_name(shift);

  my $initial   = undef;
  my $increment = undef;
  my $cycle     = undef;
  my $minimum   = undef;

  while( <> ) {
    /INITIAL ([0-9]+)/          && do { $initial   = $1 };
    /INCREMENT ([0-9]+)/        && do { $increment = $1 };
    /CYCLE-ON-LIMIT ([yesno]+)/ && do { $cycle     = $1 };
    /MIN-VAL ([0-9]+)/          && do { $minimum   = $1 };
    last if ( /^\s+$/ );
  }
  $minimum = $initial if ( !defined($minimum) || $minimum > $initial );

  my $ddl = "CREATE SEQUENCE $sname";
  $ddl .= " INCREMENT BY $increment" if ( defined( $increment) );
  $ddl .= " MINVALUE $minimum" if ( defined( $minimum) );
  $ddl .= " START WITH $initial" if ( defined( $initial) );
  $ddl .= ($cycle eq 'no'? " NO" : "") . " CYCLE" if ( defined( $cycle) );

  print $ddl, ";\n\n";
}


############################################################
# Called when we need a new field
############################################################
sub add_field {
  my $fname = fix_name(shift);
  my $tname = fix_name(shift);
  my $type = shift;

  my $initial   = "";
  my $mandatory = "";
  my %extra = ();

  while( <> ) {
    if ( /INITIAL "([^"]*)"/  ) { $initial   = "'$1'" }
    elsif ( /MANDATORY/       ) { $mandatory = " NOT NULL" }
    elsif ( /^\s+$/ ) { last; }
    elsif ( /(INITIAL|LENGTH|FIELD-TRIGGER|SQL-WIDTH) / ) { next; }
    else {
      /^\s*(\S+) +(\S.*)\s*$/ && do {
        my $attr = $1;
        my $fvalue = $2;
        $attr =~ /^VIEW-AS$/ && do {
          while ( $fvalue !~ /"$/ ) {
            $fvalue .= <>;
            chomp $fvalue;
          };
        };
        $fvalue =~ s/^"// && $fvalue =~ s/"$//;
        $fvalue =~ s/'/''/g;
        $fvalue =~ s/""/"/g;
        $fvalue =~ s/\\/\\\\/g;
        $other_field_data{"$tname.$fname"}{$attr} = $fvalue;
      };
    }
  }

  $type = $xlate_types{$type} if ( defined($xlate_types{$type}) );

  if ( $type eq 'date' && $initial ne '' ) {
    $initial =~ /'([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4})'/ && do {
      # Convert from US date to ISO date
      $initial = sprintf( "'%04d-%02d-%02d'", $3, $1, $2 );
    };
  }
  elsif ( $type eq 'boolean' ) {
    $initial =~ tr/A-Z/a-z/;
    $initial =~ tr/'//d;
    $initial = $xlate_booleans{$initial} if ( defined($xlate_booleans{$initial}) );
  }

  if ( $initial eq "" || $initial eq "?" ) {
    $initial = ""
  }
  else {
    $initial = " DEFAULT $initial";
  }

  print ",\n" if ( $current_field++ > 0 );
  print "  $fname $type$initial";
}


############################################################
# Called when we need an index for a table
############################################################
sub add_index {
  my $iname = fix_name(shift);
  my $tname = fix_name(shift);

  possibly_close_table();

  my $unique  = "";
  my $primary = 0;
  my $fields  = "";

  while( <> ) {
    /INDEX-FIELD "([^"]*)" ([AD])/      && do {
      $fields .= ($fields eq "" ? "" : ", ") . fix_name($1);
    };
    /UNIQUE/          && do { $unique = " UNIQUE" };
    /PRIMARY/         && do { $primary = 1 };
    last if ( /^\s+$/ );
  }

  if ( $primary && $unique ne "" ) {
    # In PostgreSQL it can't be a primary key unless it
    # is unique.  Progress allows non-unique primary indexes.
    print "ALTER TABLE $tname ADD PRIMARY KEY ( $fields );\n";
  }
  else {
    $current_index++;
    $iname = $tname."_sk$current_index"."_$fields";
    $iname =~ tr/ ,/_/d;
    $iname =~ s/code//g;
    print "CREATE$unique INDEX $iname ON $tname ( $fields );\n";
  }
}


############################################################
# Called to start a new table
############################################################
sub add_table {
  my $tname = fix_name(shift);

  possibly_close_table();

  print "\nCREATE TABLE $tname (\n";
  $current_table = $tname;
  $current_field = 0;
  $current_index = 0;
}

############################################################
# Main routine
############################################################
while( <> ) {
  /^ADD TABLE "(.+)"/ && do { add_table( $1 ); };
  /^ADD SEQUENCE "(.+)"/ && do { add_sequence( $1 ); };
  /^ADD FIELD "(.+)" OF "(.+)" AS (\S+)/ && do { add_field( $1, $2, $3 ); };
  /^ADD INDEX "(.+)" ON "(.+)"/ && do { add_index( $1, $2 ); };
}

possibly_close_table();

print <<EOSQL ;

-- Table to capture additional Progress field information
CREATE TABLE _progress_field_data (
  tname TEXT,
  fname TEXT,
  attribute TEXT,
  value TEXT,
  PRIMARY KEY ( tname, fname, attribute )
);

BEGIN;
EOSQL

foreach my $tfname ( sort keys %other_field_data ) {
  my ($tname, $fname) = split '\.', $tfname, 2;
  print  "-- $tname $fname\n";
  foreach my $attr ( sort keys %{ $other_field_data{$tfname} } ) {
    print "INSERT INTO _progress_field_data VALUES( '$tname', '$fname', '$attr', '$other_field_data{$tfname}{$attr}' );\n";
  }
}
print "COMMIT;\n";

