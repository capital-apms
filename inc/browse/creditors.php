<?php
$related_menu->AddOption("Create Creditor","/edit.php?t=creditor","Create a new creditor", false, 7001);

require_once("classWidgets.php");

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'active', 'log1' );
$widget->AddField( 'showzero', 'log2' );
$widget->ReadWrite();
$widget->Defaults( array('active' => 't', 'showzero' => 'f' ) );
$widget->Layout( '<table> <tr>
<th>Active:</th> <td>##active.checkbox##</td>
<th>Show zero balances:</th> <td>##showzero.checkbox##</td>
<td>##Show.submit##</td> </tr></table>'
);
$page_elements[] = $widget;

$show_zero = ($widget->Record->{'showzero'} == 't');
$show_active = ($widget->Record->{'active'} == 't');


/**
* Now build the browser
*/
$browser = new Browser("Creditors");
$browser->AddColumn( 'creditorcode', '#', 'center' );
$browser->AddColumn( 'name', 'Creditor Name' );
$browser->AddColumn( 'paymentstyle', 'Pymt Style', 'center', null, 'c.paymentstyle' );
$browser->AddColumn( 'unapproved_vouchers', 'Vchr?', 'center', null, "(CASE WHEN EXISTS (SELECT 1 FROM voucher uv WHERE uv.creditorcode = c.creditorcode AND voucherstatus IN ('U','H')) THEN 'Yes' ELSE '-'END)");
param_to_global( 'asat', '#^\d+$#' );
if ( isset($asat) && $asat > 0 ) {
  $browser->AddColumn( 'balance', 'Balance', 'right', '%0.2lf', 'SUM(COALESCE(balance,0.0))' );
  $browser->AddGrouping( 'creditorcode, name, paymentstyle' );
  $browser->SetJoins( "creditor c LEFT JOIN accountbalance a ON a.entitytype = 'C' AND a.entitycode = c.creditorcode AND a.monthcode <= $asat" );
}
else {
  $browser->AddColumn( 'balance', 'Balance', 'right', '%0.2lf', 'COALESCE(balance,0.0)' );
  $browser->SetJoins( "creditor c LEFT JOIN accountsummary a ON a.entitytype = 'C' AND a.entitycode = c.creditorcode" );
}

$new_voucher_link = '<a href="/edit.php?t=voucher&creditorcode=##creditorcode##" class="submit">New Voucher</a>';
$browser->AddColumn( 'actions', 'Actions', 'center', '&nbsp;%s&nbsp;', "'$new_voucher_link'");
$browser->AddTotal( 'balance' );

$browser->SetWhere( 'TRUE' );
if ( ! $show_zero )  $browser->AndWhere( '(ABS(balance) > 1.0)' );
if ( $show_active )  $browser->AndWhere( 'active' );
$browser->SetOrdering( 'name' );

$rowurl = '/view.php?t=creditor&id=%d';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Display Creditor Detail\" class=\"r%d\">\n", "</tr>\n", 'creditorcode', '#even' );
$browser->DoQuery();

$page_elements[] = $browser;
$c->page_title = "Browse Creditors";

