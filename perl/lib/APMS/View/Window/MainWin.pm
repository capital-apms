package APMS::View::Window::MainWin;

use strict;

use APMS::Class qw/ new_object /;
use APMS::View::Buttons;

use base qw/
	APMS::View::Window
	APMS::View::ButtonsWindow
/;

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	# Create the window
	$self->new_window( 1 );
	$self->get_window->set_default_size( $data{width}, $data{height} );

	# Set the background image
	my $rc_style = Gtk2::RcStyle->new;
	$rc_style->bg_pixmap_name( 'normal', $data{background} );
	$self->get_window->modify_style( $rc_style );

	# Pack them into a box. We pass the $apms object into here as some of
	# the button presses need to call new window classes that need it.
	$self->{buttons} = APMS::View::Buttons->button_pack( 'H', $data{buttons} );

	# Create a new VBox for the window, and pack the buttons
	# at the end.
	my $wholebox = Gtk2::VBox->new( 0, 0 );
	$wholebox->pack_end( $self->{buttons}->box, 0, 0, 0 );

	# Pack the box into the window
	$self->get_window->add( $wholebox );
	$self->get_window->show_all;

	return $self;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

