#!/bin/sh

DUMPDIR=/home/apms/dump/jacks
cd ${DUMPDIR}
rsync -a --verbose apms@binney:dump//jacks/* .

CONVERTDIR=/home/andrew/projects/apms2/convert/sql
cd ${CONVERTDIR}
mkdir -p oldsql
mv * oldsql

dropdb apms
createdb apms

echo -n "Processing database definitions: converting... "
../df-2-postgresql.pl ${DUMPDIR}/all.df >apms.psql
echo -n "loading... "
psql -q apms -f apms.psql 2>&1 | grep -v ' NOTICE: '
echo "done. "

for F in ${DUMPDIR}/*.d ; do
  BASE="`basename ${F} .d`"
  echo -n "Processing ${BASE}: converting... "
  ../data-2-postgresql.pl --file ${F} --dbname apms >${BASE}.sql
  echo -n "loading... "
  psql -q apms -f ${BASE}.sql
  echo "done. "
done

../post-load-conversion.sh
