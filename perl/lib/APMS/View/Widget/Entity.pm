package APMS::View::Widget::Entity;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Widget::Button;

sub entity_select {
	my ( $proto ) = @_;
	my $self = new_object( $proto );

	$self->{entitybox} = Gtk2::HBox->new( 0, 5 );

	$self->{entitytype} = Gtk2::Entry->new();
	$self->{entitytype}->set_size_request( 25, 25 );
	$self->{entitybox}->pack_start( $self->{entitytype}, 0, 1, 0 );

	$self->{entitycode} = Gtk2::Entry->new();
	$self->{entitycode}->set_size_request( 50, 25 );
	$self->{entitybox}->pack_start( $self->{entitycode}, 0, 1, 0 );

	$self->{chooseentity} = APMS::View::Widget::Button->jump_to_button();
	$self->{entitybox}->pack_start( $self->{chooseentity}, 0, 1, 0 );

	$self->{entityname} = Gtk2::Label->new( '(select parent entity)' );
	$self->{entitybox}->pack_start( $self->{entityname}, 0, 1, 0 );

	return $self;
}

sub box {
	my $self = shift;
	return $self->{entitybox};
}

sub set_entity_details {
	my ( $self, $type, $code, $name ) = @_;

	$self->{entitytype}->set_text( $type );
	$self->{entitycode}->set_text( $code );
	$self->{entityname}->set_text( $name );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

