#!/bin/sh

[ "${PGDBNAME}"    = "" ] && export PGDBNAME="catalyst"
[ "${PSQLNAME}"    = "" ] && export PSQLNAME="apms_${PGDBNAME}"
[ "${APPUSRNAME}"  = "" ] && export APPUSRNAME="apms_app"
[ "${DBAUSRNAME}"  = "" ] && export DBAUSRNAME="apms_dba"

TABLES="SELECT relname FROM pg_class WHERE relkind = 'r' AND relowner > 10;"
for T in `psql ${PSQLNAME} -qAt -c "$TABLES" `; do
  psql ${PSQLNAME} -qAt -c "grant insert, select, update ON ${T} TO ${APPUSRNAME}; GRANT ALL ON ${T} TO ${DBAUSRNAME};"
done


SEQUENCES="SELECT relname FROM pg_class WHERE relkind = 's' AND relowner > 10;"
for S in `psql ${PSQLNAME} -qAt -c "$SEQUENCES" `; do
  psql ${PSQLNAME} -qAt -c "grant select, update ON ${S} TO ${APPUSRNAME}; GRANT ALL ON ${T} TO ${DBAUSRNAME};"
done

DELETE_TABLES="newbatch newdocument newaccttrans accountsummary accountbalance"
for T in ${DELETE_TABLES}; do
  psql ${PSQLNAME} -qAt -c "grant delete ON ${T} TO ${APPUSRNAME}; GRANT ALL ON ${T} TO ${DBAUSRNAME};"
done
