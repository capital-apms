--
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET search_path = public, pg_catalog;

--
-- Data for TOC entry 3 (OID 28426491)
-- Name: organisation; Type: TABLE DATA; Schema: public; Owner: andrew
--

COPY organisation (org_code, active, abbreviation, org_name) FROM stdin;
1	t	CIT	Catalyst IT Ltd
\.


--
-- TOC entry 2 (OID 28426489)
-- Name: organisation_org_code_seq; Type: SEQUENCE SET; Schema: public; Owner: andrew
--

SELECT pg_catalog.setval('organisation_org_code_seq', 1, true);


