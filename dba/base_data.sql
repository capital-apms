
BEGIN;
DELETE FROM invoicestatus;
INSERT INTO invoicestatus VALUES( 'A', 'Approved');
INSERT INTO invoicestatus VALUES( 'U', 'Unapproved');
COMMIT;

BEGIN;
DELETE FROM entitytype;
INSERT INTO entitytype VALUES( 'C', 'Creditor');
INSERT INTO entitytype VALUES( 'T', 'Debtor');
INSERT INTO entitytype VALUES( 'L', 'Company');
INSERT INTO entitytype VALUES( 'P', 'Property');
INSERT INTO entitytype VALUES( 'J', 'Project');
INSERT INTO entitytype VALUES( 'A', 'Asset');
COMMIT;
