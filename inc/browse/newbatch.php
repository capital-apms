<?php

$browser = new Browser("Unposted Batches");
$browser->AddColumn( 'batchcode', 'Batch', 'center' );
$browser->AddColumn( 'batchtype', 'Type' );
$browser->AddColumn( 'documentcount', 'Documents' );
$browser->AddColumn( 'description', 'Description' );
$browser->AddColumn( 'total', 'Total', 'right', '%0.2lf', 'COALESCE(total,0.0)' );
$browser->AddColumn( 'update', 'Action', 'center', '<a href="/action.php?t=batch-update&batchcode=%d" class="submit">Update</a>', 'batchcode' );

$browser->SetJoins( "newbatch b LEFT JOIN usr u ON u.user_no = b.personcode" );

$browser->AddOrder( 'batchcode', 'ASC' );

$rowurl = '/view.php?t=newbatch&id=%d';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Display Unposted Batch Detail\" class=\"r%d\">\n", "</tr>\n", 'batchcode', '#even' );
$browser->DoQuery();

$related_menu->AddOption("Create Batch","/edit.php?t=newbatch","Create a new batch of transactions.");

$c->page_title = "Browse Unposted Batches";

$page_elements[] = $browser;