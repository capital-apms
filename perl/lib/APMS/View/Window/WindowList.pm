package APMS::View::Window::WindowList;

use strict;
use APMS::View::Buttons;

sub run {
	my ( $proto, $self ) = @_;
	my $class = ref( $proto ) || $proto;

	# Create the window
	$self->{windowlist} = Gtk2::Window->new( 'toplevel' );

	# Create a box and a button
	my $vbox = Gtk2::VBox->new( 0, 0 );
	my $button = Gtk2::Button->new( 'Main Menu' );

	$vbox->pack_start( $button, 0, 0, 0 );

	# Pack the box into the window
	$self->{windowlist}->add( $vbox );
	$self->{windowlist}->show_all;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

