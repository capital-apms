<?php

// Viewer component for company records
$viewer = new Viewer("Property");
$viewer->AddField( 'propertycode' );
$viewer->AddField( 'shortname', 'p.shortname' );
$viewer->AddField( 'name', 'p.name' );
$viewer->AddField( 'companycode', 'p.companycode' );
$joins = "property p LEFT JOIN company l USING ( companycode ) ";
$viewer->SetJoins( $joins );
$id = intval($_REQUEST['id']);
$viewer->SetWhere( 'propertycode='.$id );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Property:</th>
  <td class="center">##propertycode.value##</td>
  <td class="left">##name.value##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Company: ".$viewer->Record->{'name'});
$page_elements[] = $viewer;


include_once("menus_entityaccount.php");

// Now list income accounts
require_once('classBrowser.php');

$template = new Browser("Accounts");
$template->AddHidden( 'account', "entitytype || '-' || TO_CHAR(entitycode,'FM00009') || '-' || TO_CHAR(accountcode,'FM0009.00')" );
$template->AddHidden( 'entitytype' );
$template->AddHidden( 'entitycode' );
$template->AddColumn( 'accountgroupcode', 'Group', 'left' );
$template->AddColumn( 'accountcode', 'Account', 'right', '', "TO_CHAR(accountcode,'FM0009.00')" );
$template->AddColumn( 'name', 'Name', 'left', '<td class="left" style="width:30em;">%s</td>', 'chartofaccount.name' );
$template->AddColumn( 'balance', 'Balance', 'right', '%0.2lf', '(CASE WHEN COALESCE(creditgroup,FALSE) THEN -1 ELSE 1 END * balance)' );
$template->AddTotal( 'balance' );
$template->SetJoins( "accountsummary LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode )" );
$template->SetWhere( "entitytype='P' AND entitycode=$id AND balance != 0.0" );
$rowurl = '/view.php?t=account&id=%s';
$template->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'account', '#even' );

// Now, based on the template lets add that five times...
$browser = clone($template);
$browser->Title("Income Accounts");
$browser->AndWhere("grouptype = 'P' AND creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 1 );
$browsers[] = $browser;

$browser = clone($template);
$browser->Title("Expense Accounts");
$browser->AndWhere("grouptype = 'P' AND NOT creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 2 );
$browsers[] = $browser;

$browser = clone($template);
$browser->Title("Asset Accounts");
$browser->AndWhere("grouptype != 'P' AND NOT creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 3 );
$browsers[] = $browser;

$browser = clone($template);
$browser->Title("Liability Accounts");
$browser->AndWhere("grouptype != 'P' AND creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 4 );
$browsers[] = $browser;

$browser = clone($template);
$browser->Title("Bogus Accounts Incorrectly Set Up");
$browser->AndWhere("grouptype IS NULL" );
$browser->SetOrdering( 'accountcode', 'ASC', 5 );
$page_elements[] = $browser;

