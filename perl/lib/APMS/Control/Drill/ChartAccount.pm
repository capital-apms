package APMS::Control::Drill::ChartAccount;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Drill::ChartAccount;

use base qw/
	APMS::Control::Window
	APMS::Control::Drill
/;

sub run {
	my ( $proto, $apms, $nodecode ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;

	# Call the appropriate model method
	$self->{chartofaccounts} = $apms->model->all_accounts();

	# Fetch the buttons for this window
	$self->{buttons} = $apms->model->buttons( $apms->config->{UserName}, $nodecode );

	# Create the view
	$self->{drillwindow} = APMS::View::Drill::ChartAccount->new(
		buttons         => $self->{buttons},
		chartofaccounts => $self->{chartofaccounts}
	);

	# Attach the destroy event
	$self->{drillwindow}->attach_destroy( sub { $self->destroy_window( @_ ) } );

	# Connect the generic button signal to every button in the drill view
	$self->button_callbacks( $self->{drillwindow} );

	return $self->{drillwindow};
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

