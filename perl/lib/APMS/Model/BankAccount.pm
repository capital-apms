package APMS::Model::BankAccount;

use strict;
#use Data::Dumper;

=item all_bank_accounts

=cut

sub all_bank_accounts {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT
			bankaccountcode,
			companycode,
			accountcode,
			bankname,
			bankbranchname,
			accountname,
			bankaccount
		FROM bankaccount;
	) );

	$sth->execute();
	my $accounts = $sth->fetchall_arrayref({});
	$sth->finish;

	return $accounts;
}

sub bank_account {
	my ( $self, $accountcode ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT
			bankaccount.bankaccountcode,
			bankaccount.companycode,
			company.legalname AS companyname,
			bankaccount.accountcode,
			chartofaccount.name AS accountname,
			bankaccount.bankname,
			bankaccount.bankbranchname,
			bankaccount.accountname,
			bankaccount.bankaccount,
			bankaccount.active,
			bankaccount.chequeaccount
		FROM bankaccount
		JOIN company ON ( company.companycode = bankaccount.companycode )
		JOIN chartofaccount ON ( chartofaccount.accountcode = bankaccount.accountcode )
		WHERE bankaccount.bankaccountcode = ?;
	) );

	$sth->execute( $accountcode );
	my $account = $sth->fetchrow_hashref();
	$sth->finish;

	return $account;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

