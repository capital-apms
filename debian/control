Source: apms
Section: catalyst
Priority: extra
Maintainer: Andrew McMillan <andrew@catalyst.net.nz>
Standards-Version: 3.7.3
Build-Depends: debhelper, php5-cli

Package: apms
Architecture: all
Depends: debconf (>= 1.0.32), php5, php5-pgsql, postgresql-client (>=8.1) | postgresql-client-8.1 | postgresql-client-8.2 | postgresql-client-8.3, libawl-php (>=0.26), libdbd-pg-perl
Description: Accounting and Property Management System
 The Accounting and Property Management System is an accounting system
 with a number of features that are particularly relevant to organisations
 owning and managing large multi-tenanted office buildings.
 .
 Features include:
  - Multi-company General Ledger accounting.
  - Property ledgers
  - Accounts Receivable / Payable
  - Specialist Property Ledgers
  - Specialist Project Management Ledgers
  - Asset Ledgers
  - Flexible accounting periods.
  - Recording of leases, service contracts and property maintenance
    activities specifically for property management.
  - Recording of accounting doesn't have to stop for year end or
    month end rollover activities.
  - Holds effectively unlimited accounting history without archiving.
 .
 Capital APMS was originally developed in the mid 1980s. In the late
 1980s it was rewritten and was sold by Coopers and Lybrand to a number
 of commercial property owning and managing companies in New Zealand
 and Australia.  In the mid '90s it was rewritten a second time against
 a different database / programming language and as a client-server
 Windows application.  This implementation is a further comprehensive
 rewrite using an open source application stack.
