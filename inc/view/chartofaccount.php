<?php

param_to_global( 'ac', '#^\d+(\.\d{0,2})?$#', 'ac', 'accountcode', 'id' );

// Viewer component for account
$viewer = new Viewer("Account", 'chartofaccount');
$viewer->SetWhere( "accountcode=$ac");
$viewer->GetRecord();

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Account:</th>
  <td class="left">##accountcode.format.%07.2lf##</td>
 </tr>
 <tr>
  <th class="right">Short name:</th>
  <td class="left">##shortname##</td>
 </tr>
 <tr>
  <th class="right">Name:</th>
  <td class="left">##name##</td>
 </tr>
 <tr>
  <th class="right">Group:</th>
  <td class="left"><a href="/view.php?t=accountgroup&id=##accountgroupcode.urlencode##">##accountgroupcode##</a></td>
 </tr>
 <tr>
  <th class="right">High volume:</th>
  <td class="left">##highvolume##</td>
 </tr>
 <tr>
  <th class="right">Update to:</th>
  <td class="left">##updateto##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$c->page_title = $viewer->Title("Account $ac - " . $viewer->Record->{'name'});
$page_elements[] = $viewer;

include_once("menus_entityaccount.php");

