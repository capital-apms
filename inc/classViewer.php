<?php
/**
* APMS Record Views
*
* @package   apms
* @subpackage   classViewer
* @author    Andrew McMillan <andrew@catalyst.net.nz>
* @copyright Catalyst IT Ltd
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2
*/

/**
* First we need a class for the fields in the viewer
* @package   apms
* @subpackage   classViewer
*/
class ViewerField
{
  var $Field;
  var $Sql;
  var $Value;

  function ViewerField( $field, $sql="" ) {
    $this->Field  = $field;
    $this->Sql    = $sql;
  }

  function Set($value) {
    $this->Value = $value;
  }

  function GetTarget() {
    if ( $this->Sql == "" ) return $this->Field;
    return "$this->Sql AS $this->Field";
  }
}


/**
* Class for the actual viewer
* @package   apms
* @subpackage   classViewer
*/
class Viewer
{
  protected $Title;
  var $Fields;
  var $OrderedFields;
  var $Joins;
  var $Where;
  var $Order;
  var $Limit;
  var $Query;
  var $Template;
  var $Record;

  function Viewer( $title = "", $fields = null ) {
    global $c, $session;
    $this->Title = $title;
    $this->Order = "";
    $this->Limit = "";
    $this->Template = "";

    if ( isset($fields) ) {
      if ( is_array($fields) ) {
        foreach( $fields AS $k => $v ) {
          $this->AddField($v);
        }
      }
      else if ( is_string($fields) ) {
        // We've been given a table name, so get all fields for it.
        $this->Joins = $fields;
        $field_list = get_fields($fields);
        foreach( $field_list AS $k => $v ) {
          $this->AddField($k, "$fields.$k");
        }
      }
    }

    $session->Log("DBG: New viewer called $title");
  }

  function AddField( $field, $sql="" ) {
    $this->Fields[$field] = new ViewerField( $field, $sql );
    $this->OrderedFields[] = $field;
  }

  function SetJoins( $join_list ) {
    $this->Joins = $join_list;
  }

  function Title( $new_title = null ) {
    if ( isset($new_title) ) $this->Title = $new_title;
    return $this->Title;
  }

  function SetTitle( $new_title ) {
    $this->Title = $new_title;
  }

  function SetWhere( $where_clause ) {
    $this->Where = $where_clause;
  }

  function MoreWhere( $operator, $more_where ) {
    if ( $this->Where == "" ) {
      $this->Where = $more_where;
      return;
    }
    $this->Where = "$this->Where $operator $more_where";
  }

  function AndWhere( $more_where ) {
    $this->MoreWhere("AND",$more_where);
  }

  function OrWhere( $more_where ) {
    $this->MoreWhere("OR",$more_where);
  }

  function SetTemplate( $template ) {
    $this->Template = $template;
  }


  /**
  * Callback function used for replacing parts into the template
  */
  function ReplaceFieldPart($matches)
  {
    // $matches[0] is the complete match
    // $matches[1] the match for the first subpattern
    // enclosed in '(...)' and so on
    $field_name = $matches[1];
    $what_part = '';
    $what_part = (isset($matches[3]) ? $matches[3] : '');
    $modifier = (isset($matches[5]) ? $matches[5] : '');

    $field_value = (isset($this->Record->{$field_name}) ? $this->Record->{$field_name} : '');

    switch( $what_part ) {
      case 'format':
        $result = sprintf( $modifier, $field_value );
        return str_replace( "\n", "<br />", $result );

      case 'yesno':
        return ( $field_value == 't' ? 'Yes' : 'No' );

      case 'urlencode':
        return rawurlencode($field_value);

      default:
        return str_replace( "\n", "<br />", $field_value );
    }
  }


  function Value( $value_field_name ) {
    if ( !isset($this->Record->{$value_field_name}) ) return null;
    return $this->Record->{$value_field_name};
  }


  function GetRecord() {
    $target_fields = "";
    foreach( $this->Fields AS $k => $column ) {
      if ( $target_fields != "" ) $target_fields .= ", ";
      $target_fields .= $column->GetTarget();
    }
    $sql = sprintf( "SELECT %s FROM %s WHERE %s %s %s", $target_fields, $this->Joins, $this->Where, $this->Order, $this->Limit);
    $this->Query = new PgQuery( $sql );
    if ( $this->Query->Exec("Browse:$this->Title:DoQuery") ) {
      $this->Record = $this->Query->Fetch();
    }
    return $this->Record;
  }


  function Render( $title_tag = null ) {
    global $c, $session;
    global $ReplaceFields, $ReplaceValues;

    $session->Log("DBG: Rendering viewer $this->Title");
    if ( $this->Template == "" ) $this->DefaultTemplate();

    $html = '<div id="viewer">';
    if ( $this->Title != "" ) {
      if ( !isset($title_tag) ) $title_tag = 'h1';
      $html = "<$title_tag>$this->Title</$title_tag>\n";
    }

    // Stuff like "##fieldname.part## gets converted to the appropriate value
    $replaced = preg_replace_callback("/##([^.#]+)(\.([^.#]+)(\.([^#]*))?)?##/", array(&$this,"ReplaceFieldPart"), $this->Template);
    $html .= $replaced;

    $html .= '</div>';
    return $html;
  }

}
