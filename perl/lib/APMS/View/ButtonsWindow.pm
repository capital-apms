package APMS::View::ButtonsWindow;

=head1 APMS::View::ButtonsWindow

Defines a base class for windows with DB buttons

=cut

use strict;

=item buttons

Return the buttons associated with this window.

=cut

sub buttons {
	my ( $self ) = @_;
	return $self->{buttons}->buttonlist;
}

=item attach_button_callback

Attach the controller callback to a button.

=cut

sub attach_button_callback {
	my ( $self, $button, $callback ) = @_;
	$button->{widget}->signal_connect( clicked => $callback );
}

=item disable_button

When the buttons are being set up, some of them may be disabled at that
time due to the target not being supported.

=cut

sub disable_button {
	my ( $self, $button ) = @_;
	$button->{widget}->set_sensitive( 0 );
}

=item set_tooltip

Set the tooltip hint for a button

=cut

sub set_tooltip {
	my ( $self, $button, $text ) = @_;
	$button->{tooltip}->set_tip( $button->{widget}, $text );
}

sub set_bg_dark {
	my ( $self, $button ) = @_;
	my $color = Gtk2::Gdk::Color->new( 32639, 32639, 32639 );
	$button->{widget}->modify_bg( 'normal', $color );
	$button->{widget}->modify_bg( 'prelight', $color );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

