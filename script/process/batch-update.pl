#!/usr/bin/perl -w
#
# This process builds a batch (which we will then post) which
# transfers the amounts in all income and expenditure accounts
# into the retained earnings account.
#

use strict;

use Sys::Syslog;
use DBI;
use POSIX qw(floor);
use Getopt::Long qw(:config permute);  # allow mixed args.

# Options variables
my $batchcode;
my $forcemonth;
my $dbname ="apms";
my $dbuser ="";
my $debug = 0;
my $helpmeplease = 0;
my $syslog = 1;
my $log_open = 0;
my $update_user_no = 0;
my $update_user_name = "Unknown";

GetOptions ('debug!'    => \$debug,
            'batch=s'   => \$batchcode,
            'forcemonth=s'   => \$forcemonth,
            'updater=s' => \$update_user_no,
            'username=s' => \$update_user_name,
            'dbname=s'  => \$dbname,
            'user=s'    => \$dbuser,
            'help'      => \$helpmeplease  );

show_usage() if ( $helpmeplease );

# General global variables
my %ic;  # Intercompany matrix

# Various accounts / entities we will get from the control accounts table
my $default_db_account;
my $default_cr_entity;
my $default_cr_account;
my $gst_in_entity;
my $gst_in_account;
my $ic_base_ledger;
my $ic_base_account;
my $ic_base_offset;
my $ic_suspense_account;

############################################################
# Open database connection and set up our queries
############################################################
my $dbh = DBI->connect("dbi:Pg:dbname=$dbname", $dbuser, "", { AutoCommit => 0 } ) or die "Can't connect to database $dbname";

my $maxdoc = 0;

get_control_accounts();
create_batch_record();

my $last_document;
my @to_post;
my $unposted;
my $txns = get_transactions_to_post();

############################################################
# Main loop.  We keep this simple.
############################################################
while( $unposted = $txns->fetchrow_hashref() ) {
  next if ( !defined($unposted->{'transactioncode'}) || $unposted->{'transactioncode'} eq "" );
  if ( !defined($last_document) || $last_document->{'documentcode'} != $unposted->{'documentcode'} ) {
    commit_document( \@to_post, $last_document );  # Will ignore it if this is empty
    $last_document = $unposted;
    @to_post = undef;
  }
  $unposted->{'monthcode'} = get_posting_month( $unposted->{'monthcode'}, $unposted->{'date'} );
  logf( 'info', "Posting transaction to month %d.", $unposted->{'monthcode'});
  add_transaction_to_document( \@to_post, $unposted);
}
commit_document( \@to_post, $last_document );  # Will ignore it if this is empty
do_intercompany( $maxdoc );


############################################################
# Get the control accounts
############################################################
sub get_control_accounts {

  my $control_accounts = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
  SELECT upper(officecontrolaccount.name) AS name, entitytype, entitycode, accountcode
    FROM office JOIN officecontrolaccount USING (officecode) WHERE thisoffice;
EOQ

  if ( $control_accounts->execute() ) {
    while ( my $row = $control_accounts->fetchrow_hashref() ) {
      if ( $row->{'name'} eq 'DEBTORS' ) {
        $default_db_account = $row->{'accountcode'};
      }
      elsif ( $row->{'name'} eq 'CREDITORS' ) {
        $default_cr_entity  = $row->{'entitycode'};
        $default_cr_account = $row->{'accountcode'};
      }
      elsif ( $row->{'name'} eq 'GST-IN' ) {
        $gst_in_entity  = $row->{'entitycode'};
        $gst_in_account = $row->{'accountcode'};
      }
      elsif ( $row->{'name'} eq 'IC-BASE' ) {
        $ic_base_ledger  = $row->{'entitycode'};
        $ic_base_account = $row->{'accountcode'};
        $ic_base_offset = floor( $ic_base_account - ( floor( $ic_base_account / 1000 ) * 1000) );
        logf( "info", "Intercompany base ledger=%d, account=%06.1lf, offset=%06.1lf", $ic_base_ledger, $ic_base_account, $ic_base_offset );
      }
      elsif ( $row->{'name'} eq 'IC-SUSPENSE' ) {
        $ic_suspense_account = $row->{'accountcode'};
      }
    }
  }
  else {
    logf( 'err', "ERROR: Cannot read control accounts from database.");
  }
}

############################################################
# Log to syslog or stdout, as selected
############################################################
sub get_transactions_to_post {
  my $get_transactions = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
SELECT newdocument.reference AS documentreference,
       newdocument.description AS documentdescription,
       newaccttrans.*
  FROM newdocument LEFT JOIN newaccttrans USING (batchcode,documentcode)
 WHERE newdocument.batchcode = ?
 ORDER BY newdocument.batchcode, newdocument.documentcode, transactioncode;
EOQ

  $get_transactions->execute( $batchcode ) or die $dbh->errstr;

  return $get_transactions;
}


############################################################
# Log to syslog or stdout, as selected
############################################################
sub create_batch_record {
  my $create_batch = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
INSERT INTO batch (batchcode, operatorcode, description, updatedby )
  SELECT batchcode, $update_user_no, description, ?
    FROM newbatch WHERE newbatch.batchcode = $batchcode
EOQ

  logf( "info", "Executing batch creation" ) if ( $debug );
  $create_batch->execute($update_user_name) or do {
    logf( "warning", "Batch $batchcode already exists - assuming continuation of failed update." );
    $dbh->rollback;
    seed_intercompany_from_posted();
  };

  logf( "info", "Committing changes" ) if ( $debug );
  $dbh->commit;

}



############################################################
# Add this transaction and any consequences onto the list to be posted
############################################################
sub get_posting_month {
  my $month = shift;
  my $date = shift;
  return $month if ( defined($month) && $month > 0 );

  # We look for the ideal month, then the earliest later month, and finally the latest earlier month
  my $get_month = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
SELECT monthcode, -99999999 AS sorter FROM month WHERE monthstatus = 'Open' AND '2008-11-15'::date BETWEEN startdate AND enddate
   UNION
SELECT monthcode, monthcode AS sorter FROM month WHERE monthstatus = 'Open' AND startdate >= '2008-11-15'::date
   UNION
SELECT monthcode, (99999999 - monthcode) AS sorter FROM month WHERE monthstatus = 'Open' AND enddate < '2008-11-15'::date
   ORDER BY 2 LIMIT 1
EOQ
  $get_month->execute() or die $dbh->errstr;

  if ( $get_month->rows == 0 ) {
    logf( "error", "No open months.  Giving up.");
    die "No open months.  Giving up.";
  }
  $month = $get_month->fetchrow_hashref();

  logf( "info", "Posting month for $date is %d", $month->{'monthcode'} ) if ( $debug );

  return $month->{'monthcode'};
}


############################################################
# Add the amount to the intercompany array for this code / month
############################################################
sub add_intercompany {
  my $ec     = shift;
  my $month  = shift;
  my $amount = shift;
  if ( defined($ic{$month}{$ec}) ) {
    if ( -$amount == $ic{$month}{$ec} ) {
      delete( $ic{$month}{$ec} );
    }
    else {
      $ic{$month}{$ec} += $amount;
    }
  }
  else {
    $ic{$month}{$ec} = $amount;
  }
}


############################################################
# Seed the intercompany array with any already posted txns
############################################################
sub seed_intercompany_from_posted( ) {
  my $posted_txn_sql = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
SELECT documentcode, entitycode, monthcode, amount FROM accttran
 WHERE accttran.batchcode = ? AND entitytype = 'L';
EOQ

  logf( "info", "Seeding intercompany from already posted transactions" );
  $posted_txn_sql->execute( $batchcode ) or die $dbh->errstr;

  while( my $row = $posted_txn_sql->fetchrow_hashref() ) {
    add_intercompany( $row->{'entitycode'}, $row->{'monthcode'}, $row->{'amount'} );
    $maxdoc = $row->{'documentcode'} if ( $row->{'documentcode'} > $maxdoc );
  }
}

############################################################
# Add a transaction to the document
############################################################
sub add_transaction_to_document {
  my $docsql = shift;
  my $tx = shift;

  return if ( $tx->{'amount'} == 0 );   # Nothing to do :-)

  my $txncount = $#$docsql + 1;

  push @$docsql, <<EOQ ;
INSERT INTO accttran (batchcode, documentcode, date, reference, description, transactioncode,
            monthcode, entitytype, entitycode, accountcode, amount, consequenceof )
     SELECT batchcode, documentcode, date, reference, description, $txncount,
            $tx->{'monthcode'}, '$tx->{'entitytype'}', $tx->{'entitycode'}, $tx->{'accountcode'}, $tx->{'amount'}::numeric(20,2), $tx->{'consequenceof'}
       FROM newaccttrans
      WHERE batchcode=$tx->{'batchcode'} AND documentcode=$tx->{'documentcode'} AND transactioncode=$tx->{'transactioncode'};

EOQ
#  logf( "info", "Added transaction $tx->{'batchcode'}-$tx->{'documentcode'}-$txncount: $tx->{'entitytype'}-%05d-%06.1lf on %s (%d): \$%0.2lf, %s: %s",
#                        $tx->{'entitycode'}, $tx->{'accountcode'}, $tx->{'date'}, $tx->{'monthcode'}, $tx->{'amount'}, $tx->{'reference'}, $tx->{'description'} );

  if ( $tx->{'entitytype'} eq "L" ) {
    add_intercompany( $tx->{'entitycode'}, $tx->{'monthcode'}, $tx->{'amount'} );
    return;  # We got to the GL, so we can return now.
  }

  my $sql = "";

  if ( $tx->{'entitytype'} eq 'T' ) {
    $sql = <<EOQ
SELECT 'tenant' AS resulttype, entitytype, entitycode, $tx->{'accountcode'} AS entityaccount
       FROM tenant WHERE tenantcode = $tx->{'entitycode'}
EOQ
  }
  elsif ( $tx->{'entitytype'} eq 'C' ) {
    $sql = <<EOQ
SELECT 'creditor' AS resulttype, 'L' AS entitytype, $default_cr_entity AS entitycode, $tx->{'accountcode'} AS entityaccount
EOQ
  }
  elsif ( $tx->{'entitytype'} eq 'P' ) {
    $sql = <<EOQ
SELECT 'property' AS resulttype, 'L' AS entitytype, companycode AS entitycode, $tx->{'accountcode'} AS entityaccount
       FROM property WHERE propertycode = $tx->{'entitycode'}
EOQ
  }
  elsif ( $tx->{'entitytype'} eq 'F' ) {
    $sql = <<EOQ
SELECT 'fixedasset' AS resulttype, entitytype, entitycode, $tx->{'accountcode'} AS entityaccount
       FROM fixedasset WHERE assetcode = $tx->{'entitycode'}
EOQ
  }
  elsif ( $tx->{'entitytype'} eq 'J' ) {
    # This is the really complicated one, because we could be directed by the ProjectBudget
    # record (if it exists) or the Project record (if it doesn't).
    $sql = <<EOQ
SELECT 'budget' AS resulttype, entitytype, entitycode, entityaccount
       FROM projectbudget WHERE projectcode = $tx->{'entitycode'} AND accountcode = $tx->{'accountcode'}
UNION
SELECT 'project' AS resulttype, entitytype, entitycode, entityaccount
       FROM project WHERE projectcode = $tx->{'entitycode'}
ORDER BY 1
EOQ
  }

  my $qry = $dbh->prepare( $sql ) or die $dbh->errstr;
  logf( "info", "Finding consequences of %s-%05d-%06.1lf", $tx->{'entitytype'}, $tx->{'entitycode'}, $tx->{'accountcode'} ) if ( $debug );
  $qry->execute() or die $dbh->errstr;
  my $cons = $qry->fetchrow_hashref();
  $tx->{'consequenceof'} = $txncount if ( $tx->{'consequenceof'} == 0 );
  $tx->{'entitytype'} = $cons->{'entitytype'};
  $tx->{'entitycode'} = $cons->{'entitycode'};
  $tx->{'accountcode'} = $cons->{'entityaccount'};

  add_transaction_to_document($docsql,$tx);
}


############################################################
# When we've built up all of the transactions for a document, commit it to the database
############################################################
sub commit_document {
  my $docsql = shift;
  my $doc = shift;

  my $txncount = $#$docsql;
  return if ( $txncount < 1 );

  logf( "info", "Committing document $doc->{'batchcode'}-$doc->{'documentcode'} with $txncount transactions" );

  my $post_sql = <<EOQ ;
BEGIN;
INSERT INTO document ( batchcode, documentcode, description, reference, transactioncount, documenttype )
    SELECT batchcode, documentcode, description, reference, $txncount, documenttype
      FROM newdocument WHERE batchcode = $doc->{'batchcode'} AND documentcode = $doc->{'documentcode'};

EOQ
  foreach my $txsql ( @$docsql ) {
    $post_sql .= $txsql if ( defined($txsql) );
  }
  $post_sql .= <<EOQ ;
DELETE FROM newaccttrans WHERE batchcode=$doc->{'batchcode'} AND documentcode=$doc->{'documentcode'};
DELETE FROM newdocument WHERE batchcode=$doc->{'batchcode'} AND documentcode=$doc->{'documentcode'};
COMMIT;

EOQ
  $maxdoc = $doc->{'documentcode'} if ( $doc->{'documentcode'} > $maxdoc );
  print $post_sql;
}


############################################################
# Go through the intercompany matrix balancing all that stuff.
############################################################
sub do_intercompany {
  my $doccode = shift;
  my @icdoc;

  $doccode++;

  my $txncount = 0;

  foreach my $monthcode ( sort keys %ic ) {

    my $let_me_out = 500;  # We will escape from the loop if we ever go round this many times...

    my $eamounts = \%{$ic{$monthcode}};
    my @entities = (sort keys %$eamounts);
    next unless $#entities > 0;

    logf( "info", "Handling intercompany for month %d", $monthcode );
    while ( $let_me_out-- > 0 && $#entities > 0 ) {
      my $entitycode = $entities[0];
      $entitycode = $entities[1] if( $entitycode == $ic_base_ledger );
      my $amount = $ic{$monthcode}{$entitycode};
      my $reversal   = find_matching_intercompany( \@entities, $monthcode, $entitycode );

      my $accountcode = $ic_base_account + $reversal - $ic_base_offset;
      add_intercompany_transaction( \@icdoc, $doccode, ++$txncount, $monthcode, $entitycode, $accountcode, -$amount, "", "" );

      $accountcode = $ic_base_account + $entitycode - $ic_base_offset;
      add_intercompany_transaction( \@icdoc, $doccode, ++$txncount, $monthcode, $reversal, $accountcode, $amount, "", "" );

      @entities = (sort keys %$eamounts);
    }

    if ( $#entities == 0 ) {
      foreach my $entitycode ( @entities ) {
        logf( "info", "Posting unbalanced difference to suspense for L%d, month %d", $entitycode, $monthcode );
        my $amount = $ic{$monthcode}{$entitycode};
        add_intercompany_transaction( \@icdoc, $doccode, ++$txncount, $monthcode, $entitycode,
                                    $ic_suspense_account, -$amount, "Imbalance", "Automatically re-balance batch/month" );
      }
    }
  }

  my $post_sql = <<EOQ ;
BEGIN;
EOQ

  # Don't do anything unless we actually have some transactions to do
  if ( $txncount > 0 ) {
    $post_sql .= <<EOQ ;
INSERT INTO document ( batchcode, documentcode, description, reference, transactioncount, documenttype )
    VALUES( $batchcode, $doccode, 'InterCompany for batch $batchcode', 'InterCompany', $txncount, 'ICOA' );

EOQ

    foreach my $txsql ( @icdoc ) {
      $post_sql .= $txsql if ( defined($txsql) );
    }
  }
  else {
    $doccode--;
  }

  $post_sql .= <<EOQ ;
DELETE FROM newaccttrans WHERE batchcode=$batchcode;
DELETE FROM newdocument WHERE batchcode=$batchcode;
DELETE FROM newbatch WHERE batchcode=$batchcode;
UPDATE batch
   SET documentcount=$doccode, updatedat=current_timestamp::timestamp without time zone
 WHERE batchcode=$batchcode;
COMMIT;

EOQ

  print $post_sql;

}


############################################################
# Find a matching company to exchange the intercompany with
############################################################
sub find_matching_intercompany {
  my $entities = shift;
  my $monthcode = shift;
  my $ec = shift;

  # At this point we don't do anything fancy - all intercompany goes via
  # the base ledger...
  return $ic_base_ledger;
}

############################################################
# Add a single intercompany transaction to our array
############################################################
sub add_intercompany_transaction {
  my $icdoc = shift;
  my $doc=shift;
  my $txn=shift;
  my $mth=shift;
  my $ec=shift;
  my $ac=shift;
  my $amt=shift;
  my $ref=shift;
  my $desc=shift;

  push @$icdoc, <<EOQ ;
INSERT INTO accttran (batchcode, documentcode, transactioncode, date, monthcode, entitytype, entitycode, accountcode, amount, reference, description )
    VALUES( $batchcode, $doc, $txn, (SELECT enddate FROM month WHERE monthcode = $mth), $mth, 'L', $ec, $ac, $amt\:\:numeric(20,2), '$ref', '$desc' );

EOQ
  logf( "info", "Added intercompany transaction $batchcode-$doc-$txn L-%05d-%06.1lf month %d for %.2lf", $ec, $ac, $mth, $amt, $ref );
  add_intercompany( $ec, $mth, $amt );
}


############################################################
# Tell the nice user how we do things.  Short and sweet.
############################################################
sub show_usage {
    print <<OPTHELP;

batch-update.pl [options] --updater <user_no> --batch <batchno>

Options are:
    --forcemonth  Force processing to a particular accounting period (by month code)
    --debug       Turn on debugging
    --dbname      The database to dig into
    --user        Connect to the database as this user.
    --updater     Mark the batch as updated by this user.

The program will post the specified batch of transactions, with
a database commit after each document is processed.

OPTHELP
    exit 0;
}

############################################################
# Log to syslog or stdout, as selected
############################################################
sub logf {
my $severity = shift;

  if ( $syslog && ! $log_open ) {
    openlog( "logacct", "pid", "local0" );
    $log_open = 1;
  }
  if ( $syslog ) {
    syslog( $severity, @_ );
  }
  else {
    print scalar localtime, "[", $$, "]: ";
    printf( @_ );
    print "\n";
  }
}
