#!/bin/sh

[ "${PGDBNAME}"     = "" ] && export PGDBNAME="catalyst"
if [ "${APMSROOT}"     = "" ]; then
  if [ -f "dba/update-apms-database" ]; then
    export APMSROOT=`pwd`
  else
    export APMSROOT=/usr/share/apms
  fi
fi
[ "${AWLROOT}"      = "" ] && export AWLROOT=/usr/share/awl
[ "${DUMPDIR}"      = "" ] && export DUMPDIR=/home/apms/dump/${PGDBNAME}
[ "${LOCALDIR}"     = "" ] && export LOCALDIR="${DUMPDIR}/local"
[ "${DBNAME}"       = "" ] && export DBNAME="apms_${PGDBNAME}"
[ "${APMS_APPUSER}" = "" ] && export APMS_APPUSER="apms_app"
[ "${APMS_DBAUSER}" = "" ] && export APMS_DBAUSER="apms_dba"

try_db_user() {
  [ "XtestX`psql -U "${1}" -qAt "${DBNAME}" -c "SELECT usename FROM pg_user;" 2>/dev/null`" != "XtestX" ]
}

if [ "${DBA}" = "" ]; then
  #
  # Try a few alternatives for a database user or give up...
  if try_db_user "${APMS_DBAUSER}" ; then
    export DBA="-U ${APMS_DBAUSER}"
  else
    if try_db_user "postgres" ; then
      export DBA="-U postgres"
    else
      export DBA=""
    fi
  fi
fi

[ -n "${DEBUG}" ] && set -o xtrace

psql ${DBA} -q ${DBNAME} -f ${APMSROOT}/convert/rename-usr.sql

psql ${DBA} -q ${DBNAME} -f ${AWLROOT}/dba/schema-management.sql
psql ${DBA} -q ${DBNAME} -f ${AWLROOT}/dba/awl-tables.sql

if [ -f "${LOCALDIR}/usr.sql" ]; then
  psql ${DBA} -q ${DBNAME} -f "${LOCALDIR}/usr.sql"
else
  psql ${DBA} -q ${DBNAME} -f ${APMSROOT}/convert/usr.sql
fi

${APMSROOT}/dba/set-minimum-privileges.sh

psql ${DBA} -q ${DBNAME} -f ${APMSROOT}/convert/et-to-upper.sql
psql ${DBA} -q ${DBNAME} -f ${APMSROOT}/convert/conversions.sql
if [ -f "${APMSROOT}/convert/specific-${PGDBNAME}.sql" ]; then
  psql ${DBA} -q ${DBNAME} -f ${APMSROOT}/convert/specific-${PGDBNAME}.sql
fi

cd ${APMSROOT}
dba/update-apms-database --dbname ${DBNAME} --dbuser ${APMS_DBAUSER} --appuser ${APMS_APPUSER} --owner ${APMS_DBAUSER}
