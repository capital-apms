package APMS::Model::Persons;

use strict;
#use Data::Dumper;

=item person_address_records

Pre-fetch all the address records for a person

=cut

sub person_contact_records {
	my ( $self, $personcode ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT
			postaldetail.postaltype,
			postaldetail.address,
			postaldetail.city,
			postaldetail.state,
			postaldetail.country,
			postaldetail.zip,
			phonedetail.countrycode,
			phonedetail.stdcode,
			phonedetail.ccountrycode,
			phonedetail.cstdcode,
			phonedetail.number,
			phonedetail.phonetype,
			person.persontitle,
			person.firstname,
			person.lastname,
			person.company
		FROM person
		LEFT JOIN postaldetail ON ( postaldetail.personcode = person.personcode )
		LEFT JOIN phonedetail ON ( phonedetail.personcode = person.personcode )
		WHERE person.personcode = ?;
	) );

	my $addresses;
	$sth->execute( $personcode );

	# Use the first row to fill in the base person details
	my $row = $sth->fetchrow_hashref();
	$addresses = {
		persontitle => $row->{persontitle},
		firstname   => $row->{firstname},
		lastname    => $row->{lastname},
		company     => $row->{company}
	};

	# And grab the address and phone details in the first row
	my $posttype = $row->{postaltype};
	if ( $posttype ) {
		$addresses->{address}->{"$posttype"} = {
			address => $row->{address},
			city    => $row->{city},
			state   => $row->{state},
			country => $row->{country},
			zip     => $row->{zip}
		};
	}
	my $phonetype = $row->{phonetype};
	if ( $phonetype ) {
		$addresses->{phone}->{"$phonetype"} = {
			countrycode  => $row->{countrycode},
			stdcode      => $row->{stdcode},
			ccountrycode => $row->{ccountrycode},
			cstdcode     => $row->{cstdcode},
			number       => $row->{number}
		};
	}

	# Go through subsequent records to fill in the other addresses
	while ( my $row = $sth->fetchrow_hashref() ) {
		my $posttype = $row->{postaltype};
		if ( $posttype ) {
			$addresses->{address}->{"$posttype"} = {
				address => $row->{address},
				city    => $row->{city},
				state   => $row->{state},
				country => $row->{country},
				zip     => $row->{zip}
			};
		}
		my $phonetype = $row->{phonetype};
		if ( $phonetype ) {
			$addresses->{phone}->{"$phonetype"} = {
				countrycode  => $row->{countrycode},
				stdcode      => $row->{stdcode},
				ccountrycode => $row->{ccountrycode},
				cstdcode     => $row->{cstdcode},
				number       => $row->{number}
			};
		}
	}
	$sth->finish;

	#print Dumper( $addresses );
	return $addresses;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

