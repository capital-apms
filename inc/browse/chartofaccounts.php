<?php

require_once('classWidgets.php');

param_to_global('accountgroup', '#^[a-z0-9_-]+$#i'); // if (isset($accountgroup) ) $accountgroup = strtoupper($accountgroup);

print "1\n";
/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget('accounts');
$widget->AddField( 'updates', 'char1', "SELECT ' ', '--Show All--' UNION SELECT 'Z', '--No Manual Posting--' UNION SELECT entitytype, description FROM entitytype ORDER BY 1" );
$widget->ReadWrite();
$widget->Defaults( array('updates' => ' ' ) );
$widget->Layout( '<table> <tr> <th>Update to:</th> <td>##updates.select##</td> <td>##Show.submit##</td> </tr></table>' );

$allow_updates = ' ';
if ( isset($widget->Record->{'updates'}) && preg_match( '#([ACTLPJZ]+)#i', $widget->Record->{'updates'}, $matches) ) $allow_updates = strtoupper($matches[1]);

$page_elements[] = $widget;


$browser = new Browser("Chart of Accounts");

if ( !isset($accountgroup) ) $browser->AddColumn( 'accountgroupcode', 'Group', 'left' );
$browser->AddColumn( 'accountcode', 'Account', 'right', '', "TO_CHAR(accountcode,'FM0009.00')" );
$browser->AddColumn( 'shortname', 'ShortName' );
$browser->AddColumn( 'name', 'Name', 'left', '<td class="left" style="width:30em;" title="##alternativecode##">%s</td>', 'chartofaccount.name' );
$browser->AddHidden( 'alternativecode' );
$browser->AddColumn( 'updateto', 'Updates' );
$browser->AddColumn( 'highvolume', 'Volume', 'left', '', "CASE WHEN highvolume THEN 'Yes' ELSE 'No' END" );
$browser->SetJoins( "chartofaccount LEFT JOIN accountgroup USING ( accountgroupcode )" );
$rowurl = '/view.php?t=chartofaccount&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'accountcode', '#even' );
$browser->SetOrdering( 'accountcode' );

if ( isset($accountgroup) )   $browser->AndWhere( "accountgroupcode = ".qpg($accountgroup) );

if ( $allow_updates != ' ' ) {
  if ( $allow_updates != 'Z' )
    $browser->AndWhere( "updateto ~ '[$allow_updates]'" );
  else
    $browser->AndWhere( "updateto IS NULL OR updateto = ''" );
}

include_once("menus_entityaccount.php");

$rowurl = '/view.php?t=document&id=%s';
$c->page_title = $browser->Title();
$browser->DoQuery();
$page_elements[] = $browser;
