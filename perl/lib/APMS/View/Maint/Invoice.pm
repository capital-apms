package APMS::View::Maint::Invoice;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Widget::Packing;
use APMS::View::Widget::Button;
use APMS::View::Widget::Tenant;
use APMS::View::Widget::Combo::Payment;
use APMS::View::Widget::Invoice;

use base qw/ APMS::View /;

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	my $invoicerecord = $data{invoicerecord};

	# NOTE: The intent of placing widgets inside $self is to make them
	# available for manipulation by logic elsewhere. If its not in $self
	# then its just a formatting widget, not something that will have
	# callbacks attached

	# Working VBox or HBox and a working label
	my ( $wbox, $label );

	# The window
	$self->new_window();
	$self->get_window->set_default_size( 600, 500 );
	$self->get_window->set_position( 'none' );
	$self->get_window->realize();

	# The entire window holds a vbox for all the sub-components
	my $rootbox = Gtk2::VBox->new( 0, 5 );
	$rootbox->set_border_width( '5' );
	$self->get_window->add( $rootbox );

	######################################################
	# First row: Date, Tenant, Attn etc
	my $detailstable = Gtk2::Table->new( 6, 2, 0 );
	$detailstable->set_row_spacings( '5' );
	$detailstable->set_col_spacings( '5' );
	$detailstable->set_border_width( '5' );
	$rootbox->pack_start( $detailstable, 0, 0, 0 );

	# Table labels
	$label = APMS::View::Widget::Packing->new_right_label( 'Date:' );
	$detailstable->attach( $label, 0, 1, 0, 1, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Tenant:' );
	$detailstable->attach( $label, 0, 1, 1, 2, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Attention:' );
	$detailstable->attach( $label, 0, 1, 2, 3, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'GL Text:' );
	$detailstable->attach( $label, 0, 1, 3, 4, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Blurb:' );
	$detailstable->attach( $label, 0, 1, 4, 5, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Terms:' );
	$detailstable->attach( $label, 0, 1, 5, 6, ['fill'], ['fill', 'expand'], 0, 0 );

	# Entry elements - Date and invoice number
	my $datenobox = Gtk2::HBox->new( 0, 5 );
	$detailstable->attach( $datenobox, 1, 2, 0, 1, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	$self->{date} = Gtk2::Entry->new();
	$self->{date}->set_text( $invoicerecord->{invoicedate} );
	$datenobox->pack_start( $self->{date}, 0, 1, 0 );

	$self->{invoiceno} = Gtk2::Entry->new();
	$self->{invoiceno}->set_text( $invoicerecord->{invoiceno} );
	$datenobox->pack_end( $self->{invoiceno}, 0, 1, 0 );

	# Tenant box
	$self->{tenant} = APMS::View::Widget::Tenant->tenant_select();
	$self->{tenant}->set_tenant_details( $invoicerecord->{tenantcode}, $invoicerecord->{tenantname} );
	$detailstable->attach( $self->{tenant}->box(), 1, 2, 1, 2, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	# Attn field
	$self->{attention} = Gtk2::Entry->new();
	$self->{attention}->set_text( $invoicerecord->{attnto} );
	$detailstable->attach( $self->{attention}, 1, 2, 2, 3, ['fill'], ['fill', 'expand'], 0, 0 );

	# GL text
	$self->{gltext} = Gtk2::Entry->new();
	$self->{gltext}->set_text( $invoicerecord->{blurb} );
	$detailstable->attach( $self->{gltext}, 1, 2, 3, 4, ['fill'], ['fill', 'expand'], 0, 0 );

	# Blurb text
	$self->{blurb} = Gtk2::TextBuffer->new();
	$self->{blurbedit} = Gtk2::TextView->new_with_buffer( $self->{blurb} );

	# Scrolled win for blurb
	my $scrolledwindow = Gtk2::ScrolledWindow->new( undef, undef );
	$scrolledwindow->set_shadow_type( 'in' );
	$scrolledwindow->add( $self->{blurbedit} );
	$detailstable->attach( $scrolledwindow, 1, 2, 4, 5, ['fill'], ['fill', 'expand'], 0, 0 );

	# Terms select
	$self->{invoiceterms} = APMS::View::Widget::Combo::Payment->terms_combo( $data{invoiceterms} );
	$detailstable->attach( $self->{invoiceterms}, 1, 2, 5, 6, ['fill'], ['fill', 'expand'], 0, 0 );

	# Special Invoice widget
	$self->{invoicelines} = APMS::View::Widget::Invoice->invoice_lines(
		invoicelines => $data{invoicelines},
		accountnames => $data{accountnames}
	);
	$rootbox->pack_start( $self->{invoicelines}->box(), 1, 1, 0 );

	# Totals
	my $endbox = Gtk2::HBox->new( 0, 0 );
	my $totalstable = Gtk2::Table->new( 3, 3, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Total:' );
    $totalstable->attach( $label, 1, 2, 0, 1, ['fill'], ['fill', 'expand'], 3, 3 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Tax:' );
	$totalstable->attach( $label, 1, 2, 1, 2, ['fill'], ['fill', 'expand'], 3, 3 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Amt Due:' );
	$totalstable->attach( $label, 1, 2, 2, 3, ['fill'], ['fill', 'expand'], 3, 3 );

	# Hand roll these labels, as we need the label modifiable
	$self->{total} = Gtk2::Label->new( $invoicerecord->{total} );
	my $totalbox = Gtk2::HBox->new( 0, 0 );
	$totalbox->pack_end( $self->{total}, 0, 0, 0 );
	$totalstable->attach( $totalbox, 2, 3, 0, 1, ['fill'], ['fill', 'expand'], 3, 3 );

	$self->{taxamount} = Gtk2::Entry->new();
	$self->{taxamount}->set_alignment( 1.0 );
	$self->{taxamount}->set_text( $invoicerecord->{taxamount} );
	$totalstable->attach( $self->{taxamount}, 2, 3, 1, 2, ['fill'], ['fill', 'expand'], 3, 3 );

	$self->{topay} = Gtk2::Label->new( $invoicerecord->{topay} );
	my $paybox = Gtk2::HBox->new( 0, 0 );
	$paybox->pack_end( $self->{topay}, 0, 0, 0 );
	$totalstable->attach( $paybox, 2, 3, 2, 3, ['fill'], ['fill', 'expand'], 3, 3 );

	$self->{taxapplies} = Gtk2::CheckButton->new_with_label( 'Taxable' );
	$self->{taxapplies}->set_active( $invoicerecord->{taxapplies} );
	$totalstable->attach( $self->{taxapplies}, 0, 1, 0, 3, ['fill'], [], 3, 3 );

	$endbox->pack_end( $totalstable, 0, 0, 0 );
	$rootbox->pack_start( $endbox, 0, 0, 0 );

	# Add, Save and Cancel buttons
	my $buttonbox = Gtk2::HBox->new( 0, 5 );
	$self->{add} = APMS::View::Widget::Button->add_button();
	$buttonbox->pack_start( $self->{add}, 0, 0, 0 );
	$self->{save} = APMS::View::Widget::Button->save_button();
	$buttonbox->pack_end( $self->{save}, 0, 0, 0 );
	$self->{cancel} = APMS::View::Widget::Button->cancel_button();
	$buttonbox->pack_end( $self->{cancel}, 0, 0, 0 );
	$rootbox->pack_end( $buttonbox, 0, 0, 0 );

	# Show everything
	$self->get_window->show_all();

	return $self;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

