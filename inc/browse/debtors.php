<?php
$related_menu->AddOption("Create Debtor","/edit.php?t=debtor","Create a new debtor", false, 7001);

include_once("classWidgets.php");

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'active', 'log1' );
$widget->AddField( 'showzero', 'log2' );
$widget->ReadWrite();
$widget->Defaults( array('active' => 't', 'showzero' => 'f' ) );
$widget->Layout( '<table> <tr>
<th>Active:</th> <td>##active.checkbox##</td>
<th>Show zero balances:</th> <td>##showzero.checkbox##</td>
<td>##Show.submit##</td> </tr></table>'
);
$page_elements[] = $widget;

$show_zero = ($widget->Record->{'showzero'} == 't');
$show_active = ($widget->Record->{'active'} == 't');


/**
* Now build the browser
*/
$browser = new Browser("Debtors");
$browser->AddColumn( 'tenantcode', 'Debtor', 'center' );
$browser->AddColumn( 'name', 'Debtor Name' );
$browser->AddColumn( 'parent', 'Parent', 'right', '%s', '(t.entitytype || t.entitycode::text)' );
$browser->AddColumn( 'balance', 'Balance', 'right', '%0.2lf', 'COALESCE(balance,0.0)' );
$browser->AddTotal( 'balance' );

$new_invoice_link = '<a href="/edit.php?t=invoice&et=T&ec=##tenantcode##" class="submit">New Invoice</a>';
$browser->AddColumn( 'actions', 'Actions', 'center', '&nbsp;%s&nbsp;', "'$new_invoice_link'");

$browser->SetJoins( "tenant t LEFT JOIN accountsummary a ON a.entitytype = 'T' AND a.entitycode = t.tenantcode" );
$browser->SetWhere( 'TRUE' );
if ( ! $show_zero )  $browser->AndWhere( '(ABS(balance) > 1.0)' );
if ( $show_active )  $browser->AndWhere( 'active' );
$browser->SetOrdering( 'name' );
$rowurl = '/view.php?t=debtor&id=%d';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Display Debtor Detail\" class=\"r%d\">\n", "</tr>\n", 'tenantcode', '#even' );
$browser->DoQuery();

$c->page_title = "Browse Debtors";

$page_elements[] = $browser;