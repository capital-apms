<?php
// The table we are displaying here looks like this.
//
//     Column       |  Type   | Modifiers
// -----------------+---------+-----------
// bankaccountcode  | text    | default ''::text
// accountname      | text    | default ''::text
// bankbranchname   | text    | default ''::text
// bankname         | text    | default ''::text
// bankaccount      | text    | default ''::text
// companycode      | integer | default 0
// accountcode      | numeric | default (0)::numeric
// lastmodifieddate | date    | default '0001-01-01'::date
// lastmodifiedtime | integer | default 0
// lastmodifieduser | text    | default ''::text
// auditrecordid    | integer | default 0
// chequeaccount    | boolean | default false
// active           | boolean | default true
// deuserid         | text    | default ''::text
//

// Editor component for debtor records
$viewer = new Viewer("Bank Account", "bankaccount");

param_to_global('id', '#^[A-Z0-9]+$#', 'bankaccountcode');
$viewer->SetWhere( "bankaccountcode=".qpg($id) );

$viewer->GetRecord();

// $viewer->SetOptionList( 'billperson_title', array('Mr' => 'Mr', 'Ms' => 'Ms', 'Mrs' => 'Mrs' ), $viewer->Record->{'billperson_title'} );
// $viewer->SetOptionList( 'debtor_et', array('L' => 'Company', 'J' => 'Project', 'P' => 'Property' ), $viewer->Record->{'debtor_et'} );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Bank Account Code:</th>
  <td class="left"><input type="hidden" name="oldbankaccountcode" value="##bankaccountcode.enc##">##bankaccountcode.input.4##</td>
 </tr>
 <tr>
  <th class="right">Account Name</th>
  <td class="left">##accountname.input.50##</td>
 </tr>
 <tr>
  <th class="right">Bank Name</th>
  <td class="left">##bankname.input.50##</td>
 </tr>
 <tr>
  <th class="right">Branch Name</th>
  <td class="left">##bankbranchname.input.50##</td>
 </tr>
 <tr>
  <th class="right">Bank Account</th>
  <td class="left">##bankaccount.input.50##</td>
 </tr>
 <tr>
  <th class="right">Company Code</th>
  <td class="left">##companycode.input.5##</td>
 </tr>
 <tr>
  <th class="right">Account Code</th>
  <td class="left">##accountcode.input.7##</td>
 </tr>
 <tr>
  <th class="right">Cheque Account</th>
  <td class="left">##chequeaccount.checkbox##</td>
 </tr>
 <tr>
  <th class="right">Active</th>
  <td class="left">##active.checkbox##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );

$c->page_title = $viewer->Title("Bank Account: ".$viewer->Record->{'accountname'});

$page_elements[] = $viewer;

$related_menu->AddOption("View Bank Account","/view.php?t=bankaccount&id=$id","View this Bank Account.");
$related_menu->AddOption("Edit Bank Account","/edit.php?t=bankaccount&id=$id","Edit this Bank Account.");

require_once('classBrowser.php');
$browser = new Browser("Bank Import Rules");
$browser->AddHidden( 'bankaccountcode' );
$browser->AddHidden( 'entitytype' );
$browser->AddHidden( 'entitycode' );
$browser->AddHidden( 'accountcode' );
$browser->AddColumn( 'ruleseq', '#', 'right' );
$browser->AddColumn( 'trntype', 'TxnType', 'left' );
$browser->AddColumn( 'match1', 'Other Party', 'left' );
$browser->AddColumn( 'match2', 'Bank Reference', 'left' );
$browser->AddColumn( 'entity', 'Entity', 'left', '', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')" );
$browser->AddColumn( 'description', 'Description', 'left' );
$browser->SetOrdering( 'ruleseq' );
$browser->SetJoins( "bankimportrule" );

$browser->AndWhere( "bankaccountcode=".qpg($id) );
$rowurl = '/edit.php?t=bankaccount&id=%s&editseq=%d';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Edit\" class=\"r%d\">\n", "</tr>\n", 'bankaccountcode', 'ruleseq', '#even' );
$page_elements[] = $browser;
