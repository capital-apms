package APMS::View::Widget::Account;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Widget::Button;

sub account_select {
	my ( $proto ) = @_;
	my $self = new_object( $proto );

	$self->{accountbox} = Gtk2::HBox->new( 0, 5 );

	$self->{accountcode} = Gtk2::Entry->new();
	$self->{accountcode}->set_size_request( 60, 25 );
	$self->{accountbox}->pack_start( $self->{accountcode}, 0, 1, 0 );

	$self->{chooseaccount} = APMS::View::Widget::Button->jump_to_button();
	$self->{accountbox}->pack_start( $self->{chooseaccount}, 0, 1, 0 );

	$self->{accountname} = Gtk2::Label->new( '(select account)' );
	$self->{accountbox}->pack_start( $self->{accountname}, 0, 1, 0 );

	return $self;
}

sub box {
	my $self = shift;
	return $self->{accountbox};
}

sub set_account_details {
	my ( $self, $code, $name ) = @_;

	$self->{accountcode}->set_text( $code );
	$self->{accountname}->set_text( $name );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

