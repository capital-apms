<?php
include_once("classWidgets.php");

param_to_global( 'creditorcode', 'int' );
param_to_global( 'id', '#^[ACTLPJ]-\d+-\d+(\.\d+)?$#i' );
if ( isset($creditorcode) ) {
  $et = 'C';
  $ec = $creditorcode;
}
else if ( isset($id) ) {
  list( $et, $ec, $ac ) = explode('-',$id,3);
  $et = strtoupper(substr($et,0,1));
  $ec = intval($ec);
  $ac = floatval($ac);
}

param_to_global('month', '#[0-9:/-]+#');

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'year', 'int1', "SELECT 0, '--- All Years ---' UNION SELECT financialyearcode, description FROM financialyear ORDER BY 1;" );
$widget->ReadWrite();
$widget->Defaults( array('year' => $year ) );
$widget->Layout( '<table> <tr> <th>For year:</th> <td>##year.select##</td> <td>##Show.submit##</td> </tr></table>' );
$page_elements[] = $widget;


$browser = new Browser("Browse Cheques");

if ( isset($et) && isset($ec) ) {
  $qry = new PgQuery( "SELECT get_entity_type_name(?) AS entitytypename, get_entity_name(?,?) AS entityname;", $et, $et, $ec);
  if ( $qry->Exec("browse-$component") && $row = $qry->Fetch() ) {
    $browser->Title("$row->entitytypename Cheques for $row->entityname ($et$ec)");
  }
}
include_once("menus_entityaccount.php");

//  chequeno | bankaccountcode | creditorcode | amount | batchcode | documentcode | payeename | date | cancelled | datepresented | presentedamount | datesent

$browser->AddColumn( 'bankaccountcode', 'Bank', 'center' );
$browser->AddColumn( 'chequeno', 'Cheque#', 'center' );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'payeename', 'Payee', 'left', null, 'chq.payeename' );
$browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$browser->AddTotal( 'amount' );
$browser->SetJoins( "cheque chq JOIN creditor cr USING(creditorcode)" );
if ( $widget->Record->{'year'} > 0 ) {
  $browser->SetJoins("cheque chq JOIN creditor cr USING(creditorcode) JOIN month ON chq.date BETWEEN month.startdate AND month.enddate JOIN financialyear USING (financialyearcode)");
  $browser->AndWhere( "financialyearcode =".qpg($widget->Record->{'year'}) );
}
if ( isset($month) ) $browser->AndWhere( sprintf("(month.startdate <= %s AND month.enddate >= %s) ", qpg($month), qpg($month)) );
$browser->SetOrdering( 'chequeno', 'DESC', 1 );
$rowurl = '/view.php?t=cheque&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Display Cheque Detail\" class=\"r%d\">\n", "</tr>\n", 'chequeno', '#even' );

$c->page_title = $browser->Title();
$page_elements[] = $browser;
