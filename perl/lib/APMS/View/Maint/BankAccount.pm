package APMS::View::Maint::BankAccount;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Widget::Packing;
use APMS::View::Widget::Button;
use APMS::View::Widget::Company;
use APMS::View::Widget::Account;

use base qw/ APMS::View /;

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	my $bankaccount = $data{bankaccount};

	# NOTE: The intent of placing widgets inside $self is to make them
	# available for manipulation by logic elsewhere. If its not in $self
	# then its just a formatting widget, not something that will have
	# callbacks attached

	# Working VBox or HBox and a working label
	my ( $wbox, $label );

	# The window
	$self->new_window();
	$self->get_window->set_default_size( 500, 300 );
	$self->get_window->set_position( 'none' );
	$self->get_window->realize();

	# The entire window holds a vbox for all the sub-components
	my $rootbox = Gtk2::VBox->new( 0, 5 );
	$rootbox->set_border_width( '5' );
	$self->get_window->add( $rootbox );

	######################################################
	# First row: Date, Tenant, Attn etc
	my $detailstable = Gtk2::Table->new( 8, 2, 0 );
	$detailstable->set_row_spacings( '5' );
	$detailstable->set_col_spacings( '5' );
	$detailstable->set_border_width( '5' );
	$rootbox->pack_start( $detailstable, 0, 0, 0 );

	# Table labels
	$label = APMS::View::Widget::Packing->new_right_label( 'Bank A/C:' );
	$detailstable->attach( $label, 0, 1, 0, 1, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Company:' );
	$detailstable->attach( $label, 0, 1, 1, 2, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'GL Account:' );
	$detailstable->attach( $label, 0, 1, 2, 3, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Bank Name:' );
	$detailstable->attach( $label, 0, 1, 3, 4, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Branch Name:' );
	$detailstable->attach( $label, 0, 1, 4, 5, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Account Name:' );
	$detailstable->attach( $label, 0, 1, 5, 6, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Account No:' );
	$detailstable->attach( $label, 0, 1, 6, 7, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'DE User:' );
	$detailstable->attach( $label, 0, 1, 7, 8, ['fill'], ['fill', 'expand'], 0, 0 );

	# Entry elements - Bank account and Active flag
	my $datenobox = Gtk2::HBox->new( 0, 5 );
	$detailstable->attach( $datenobox, 1, 2, 0, 1, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	$self->{bankaccountcode} = Gtk2::Entry->new();
	$self->{bankaccountcode}->set_text( $bankaccount->{bankaccountcode} );
	$datenobox->pack_start( $self->{bankaccountcode}, 0, 1, 0 );

	$self->{active} = Gtk2::Entry->new();
	$self->{active}->set_text( $bankaccount->{active} );
	$datenobox->pack_end( $self->{active}, 0, 1, 0 );

	# Company select button
	$self->{companycode} = APMS::View::Widget::Company->company_select();
	$self->{companycode}->set_company_details( $bankaccount->{companycode}, $bankaccount->{companyname} );
	$detailstable->attach( $self->{companycode}->box(), 1, 2, 1, 2, ['fill'], ['fill', 'expand'], 0, 0 );

	# Account select button
	$self->{accountcode} = APMS::View::Widget::Account->account_select();
	$self->{accountcode}->set_account_details( $bankaccount->{accountcode}, $bankaccount->{accountname} );
	$detailstable->attach( $self->{accountcode}->box(), 1, 2, 2, 3, ['fill'], ['fill', 'expand'], 0, 0 );

	# Bank name
	$self->{bankname} = Gtk2::Entry->new();
	$self->{bankname}->set_text( $bankaccount->{bankname} );
	$detailstable->attach( $self->{bankname}, 1, 2, 3, 4, ['fill'], ['fill', 'expand'], 0, 0 );

	# Branch Name
	$self->{bankbranchname} = Gtk2::Entry->new();
	$self->{bankbranchname}->set_text( $bankaccount->{bankbranchname} );
	$detailstable->attach( $self->{bankbranchname}, 1, 2, 4, 5, ['fill'], ['fill', 'expand'], 0, 0 );

	# Account Name
	$self->{accountname} = Gtk2::Entry->new();
	$self->{accountname}->set_text( $bankaccount->{accountname} );
	$detailstable->attach( $self->{accountname}, 1, 2, 5, 6, ['fill'], ['fill', 'expand'], 0, 0 );

	# Account Number
	$self->{bankaccount} = Gtk2::Entry->new();
	$self->{bankaccount}->set_text( $bankaccount->{bankaccount} );
	$detailstable->attach( $self->{bankaccount}, 1, 2, 6, 7, ['fill'], ['fill', 'expand'], 0, 0 );

	# DE User
	#$self->{} = Gtk2::Entry->new();
	#$self->{}->set_text( $bankaccount->{} );
	#$detailstable->attach( $self->{}, 1, 2, 5, 6, ['fill'], ['fill', 'expand'], 0, 0 );

	# Add, Save and Cancel buttons
	my $buttonbox = Gtk2::HBox->new( 0, 5 );
	$self->{save} = APMS::View::Widget::Button->save_button();
	$buttonbox->pack_end( $self->{save}, 0, 0, 0 );
	$self->{cancel} = APMS::View::Widget::Button->cancel_button();
	$buttonbox->pack_end( $self->{cancel}, 0, 0, 0 );
	$rootbox->pack_end( $buttonbox, 0, 0, 0 );

	# Show everything
	$self->get_window->show_all();

	return $self;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

