#!/bin/sh

[ "${PGDBNAME}"     = "" ] && export PGDBNAME="catalyst"
if [ "${APMSROOT}"     = "" ]; then
  if [ -f "dba/update-apms-database" ]; then
    export APMSROOT=`pwd`
  else
    export APMSROOT=/usr/share/apms
  fi
fi
[ "${AWLROOT}"      = "" ] && export AWLROOT=/usr/share/awl
[ "${DUMPDIR}"      = "" ] && export DUMPDIR=/home/apms/dump/${PGDBNAME}
[ "${CONVERTDIR}"   = "" ] && export CONVERTDIR="${DUMPDIR}/sql"
[ "${LOCALDIR}"     = "" ] && export LOCALDIR="${DUMPDIR}/local"
[ "${DBNAME}"       = "" ] && export DBNAME="apms_${PGDBNAME}"
[ "${APMS_APPUSER}" = "" ] && export APMS_APPUSER="apms_app"
[ "${APMS_DBAUSER}" = "" ] && export APMS_DBAUSER="apms_dba"

mkdir -p ${DUMPDIR}
mkdir -p ${CONVERTDIR}
cd ${CONVERTDIR}

if [ "$1" = "refresh" ] ; then
  rsync -a --verbose apms@boole:dump/${PGDBNAME}/* ${DUMPDIR}/
  mkdir -p oldsql
  mv *.sql *.psql oldsql
fi

if [ "$2" = "keepusers" ] ; then
  mkdir -p ${LOCALDIR}
  pg_dump ${DBNAME} -t usr >"${LOCALDIR}/usr.sql"
fi

[ -n "${DEBUG}" ] && set -o xtrace

dropdb ${DBNAME}


DBADIR="`dirname \"$0\"`"
INSTALL_NOTE_FN="`mktemp`"

testawldir() {
  [ -f "${1}/dba/awl-tables.sql" ]
}

#
# Attempt to locate the AWL directory
AWLDIR="${DBADIR}/../../awl"
if ! testawldir "${AWLDIR}"; then
  AWLDIR="/usr/share/awl"
  if ! testawldir "${AWLDIR}"; then
    AWLDIR="/usr/local/share/awl"
    if ! testawldir "${AWLDIR}"; then
      echo "Unable to find AWL libraries"
      exit 1
    fi
  fi
fi

# Get the major version for PostgreSQL
export DBVERSION="`psql -qAt template1 -c "SELECT version();" | cut -f2 -d' ' | cut -f1-2 -d'.'`"

install_note() {
  cat >>"${INSTALL_NOTE_FN}"
}

db_users() {
  psql -qAt template1 -c "SELECT usename FROM pg_user;";
}

create_db_user() {
  if ! db_users | grep "^${1}$" >/dev/null ; then
    psql -qAt template1 -c "CREATE USER ${1} NOCREATEDB NOCREATEROLE;"
    cat <<EONOTE | install_note
*  You will need to edit the PostgreSQL pg_hba.conf to allow the
   '${1}' database user access to the 'davical' database.

EONOTE
  fi
}

create_plpgsql_language() {
  if ! psql ${DBA} -qAt "${DBNAME}" -c "SELECT lanname FROM pg_language;" | grep "^plpgsql$" >/dev/null; then
    if ! createlang plpgsql "${DBNAME}"; then
      echo "Unable to create the plpgsql language - does the '${USER}' user have sufficient database rights?"
      exit 1
    fi
  fi
}

try_db_user() {
  [ "XtestX`psql -U "${1}" -qAt "${DBNAME}" -c "SELECT usename FROM pg_user;" 2>/dev/null`" != "XtestX" ]
}

create_db_user "${APMS_DBAUSER}"
create_db_user "${APMS_APPUSER}"


# FIXME: Need to check that the database was actually created.
if ! createdb --encoding UTF8 "${DBNAME}" --template template0 --owner "${APMS_DBAUSER}"; then
  echo "Unable to create database - does the '${USER}' user have sufficient database rights?"
  exit 1
fi

#
# Try a few alternatives for a database user or give up...
if try_db_user "${APMS_DBAUSER}" ; then
  export DBA="-U ${APMS_DBAUSER}"
else
  if try_db_user "postgres" ; then
    export DBA="-U postgres"
  else
    if try_db_user "${USER}" ; then
      export DBA=""
    else
      if try_db_user "${PGUSER}" ; then
        export DBA=""
      else
        cat <<EOFAILURE
* * * * ERROR * * * *
I cannot find a usable database user to construct the APMS database with, but
may have successfully created the apms_app and apms_dba users (I tried :-).

You should edit your pg_hba.conf file to give permissions to the apms_app and
apms_dba users to access the database and run this script again.  If you still
continue to see this message then you will need to make sure you run the script
as a user with full permissions to access the local PostgreSQL database.

If your PostgreSQL database is non-standard then you will need to set the PGHOST,
PGPORT and/or PGCLUSTER environment variables before running this script again.

EOFAILURE
        exit 1
      fi
    fi
  fi
fi

create_plpgsql_language


echo -n "Processing database definitions: "
if [ ${DUMPDIR}/all.df -nt apms.psql ]; then
  echo -n "converting... "
  "${APMSROOT}/convert/df-2-postgresql.pl" ${DUMPDIR}/all.df >apms.psql
fi
echo -n "loading... "
psql ${DBA} -q ${DBNAME} -f apms.psql 2>&1 | grep -v ' NOTICE: '
echo "done. "

for F in ${DUMPDIR}/*.d ; do
  BASE="`basename ${F} .d`"
  echo -n "Processing ${BASE}: "
  if [ "${F}" -nt "${BASE}.sql" ]; then
    echo -n "converting... "
    "${APMSROOT}/convert/data-2-postgresql.pl" ${DBA} --file ${F} --dbname ${DBNAME} >${BASE}.sql
  fi
  echo -n "loading... "
  if [ -f "${LOCALDIR}/${BASE}.sql" ]; then
    case ${BASE} in
      usr)
        # Do nothing - we load it later
        ;;
      *)
        psql ${DBA} -q ${DBNAME} -f "${LOCALDIR}/${BASE}.sql"
        ;;
    esac
  else
    psql ${DBA} -q ${DBNAME} -f ${BASE}.sql
  fi
  echo "done. "
done

${APMSROOT}/convert/post-load-conversion.sh

echo "NOTE"
echo "===="
cat "${INSTALL_NOTE_FN}"
rm "${INSTALL_NOTE_FN}"

