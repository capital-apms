package APMS::View::Widget::Combo::Payment;

use strict;
use APMS::View::Widget::Combo;

sub paymentstyle_combo {
	my ( $self, $data ) = @_;

	my $combobox = APMS::View::Widget::Combo->combo_select(
		fields => [
			{
				column => 'paydesc',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => $data
	);

	return $combobox;
}

sub debtclass_combo {
	my ( $self, $data ) = @_;

	# Shove a blank selection in front
	unshift @{ $data }, { debtdesc => "", debtclassification => "", description => "" };

	my $debtclass = APMS::View::Widget::Combo->combo_select(
		fields => [
			{
				column => 'debtdesc',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data =>	$data
	);

	return $debtclass;
}

sub variance_combo {
	my ( $self, $data ) = @_;

	# Shove a blank selection in front
	unshift @{ $data }, { variancedesc => "", varianceclassification => "", description => "" };

	my $debtclass = APMS::View::Widget::Combo->combo_select(
		fields => [
			{
				column => 'variancedesc',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data =>	$data
	);

	return $debtclass;
}

sub terms_combo {
	my ( $self, $data ) = @_;

	my $terms = APMS::View::Widget::Combo->combo_select(
		fields => [
			{
				column => 'description',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data =>	$data
	);

	return $terms;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

