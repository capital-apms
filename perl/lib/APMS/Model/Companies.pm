package APMS::Model::Companies;

use strict;

=item all_companies

Returns all companies from the DB

  my @companies = $self->model->all_companies();

=cut

sub all_companies {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT ShortName, Active, CompanyCode, LegalName, TaxNo FROM Company;
	) );

	$sth->execute();
	my $companies = $sth->fetchall_arrayref({});
	$sth->finish;

	return $companies;
}

=item active_companies

Returns the active companies from the DB

  my @companies = $self->model->active_companies();

=cut

sub active_companies {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT ShortName, Active, CompanyCode, LegalName, TaxNo FROM Company
			WHERE Active = 'yes';
	) );

	$sth->execute();
	my $companies = $sth->fetchall_arrayref({});
	$sth->finish;

	return $companies;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

