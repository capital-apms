<?php

param_to_global( 'id', '#^[ACTLPJ]-\d+-\d+(\.\d+)?$#i');
param_to_global( 'et', '#^[ACTLPJ]$#i', 'entitytype' );
param_to_global( 'ec', '#^\d+$#', 'entitycode' );
param_to_global( 'ac', '#^\d+(\.\d{0,2})?$#', 'accountcode' );

if ( isset($id) ) {
  list( $et, $ec, $ac ) = explode('-',$id,3);
  $et = strtoupper(substr($et,0,1));
  $ec = intval($ec);
  $ac = floatval($ac);
}
param_to_global('month', '#[0-9:/-]+#');

// Viewer component for account
$viewer = new Viewer("Account");
$viewer->AddField( 'etname', "get_entity_type_name('$et')" );
$viewer->AddField( 'entityname', "get_entity_name('$et',$ec)" );
$viewer->AddField( 'entitycode' );
$viewer->AddField( 'accountcode', "coa.accountcode");
$viewer->AddField( 'accountname', "coa.name" );
$viewer->AddField( 'balance', "to_char(balance,'FML999G999G999G990D00PR')" );  // Such an optimist :-)
$viewer->AddField( 'current_year','(SELECT financialyearcode FROM month WHERE startdate<=current_date AND enddate>=current_date)' );
$joins = "chartofaccount coa LEFT JOIN accountsummary a ON a.entitytype='$et' AND a.entitycode=$ec AND a.accountcode=coa.accountcode";
$viewer->SetJoins( $joins );

unset($id); if ( isset($_REQUEST['id']) ) $id = $_REQUEST['id'];
$viewer->SetWhere( "coa.accountcode=$ac");
$viewer->GetRecord();

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">##etname.value## Name:</th>
  <td class="left">$et-$ec</td>
  <td class="left">##entityname.value##</td>
 </tr>
 <tr>
  <th class="right">Account Name:</th>
  <td class="left">##accountcode.format.%07.2lf##</td>
  <td class="left">##accountname.value##</td>
 </tr>
 <tr>
  <th class="right">Balance:</th>
  <td class="right">##balance.value##</td>
  <td class="left"></td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$c->page_title = $viewer->Title($viewer->Record->{'etname'}." Account $id");
$page_elements[] = $viewer;

include_once("menus_entityaccount.php");

require_once('classBrowser.php');

$browser = new Browser("Total of Transactions, by Month");
$month_format = '<td class="left" style="width:8em">%s</td>';
$money_fmt = '<td class="money"><a href="/browse.php?t=transactions&et='.$et.'&ec='.$ec.'&ac='.$ac.'&year=%s" class="invisilink">%%s</a></td>';
$browser->RowFormat( "<tr class=\"r%d\">\n", "</tr>\n", 'entitytype', 'entitycode', 'accountcode', '#even' );
$browser->AddHidden( 'entitytype', "'$et'" );
$browser->AddHidden( 'entitycode', "$ec" );
$browser->AddHidden( 'accountcode', "$ac" );
$browser->AddHidden( 'monthcode', 'm.monthcode' );
$browser->AddHidden( 'monthpart', "substring( startdate::text from 6 for 5 )" );
$browser->AddColumn( 'month', 'Month', 'left', $month_format, "to_char( startdate, 'YYYY, FMMonth' )" );
$browser->AddColumn( 'balance', $year, 'right', sprintf($money_fmt,$year), "to_char(balance,'FMMI999G999G999G990D00')" );
$browser->AddTotal( 'balance' );
for ( $i=1; $i<8; $i++ ) {
  $browser->AddColumn( "year_$i", $year - $i, 'right', sprintf($money_fmt, $year - $i), "to_char(previous_balance('$et',$ec,$ac,m.monthcode,$i),'FMMI999G999G999G990D00')" );
  $browser->AddTotal( "year_$i" );
}

$joins = 'chartofaccount coa, month m ';
$joins .= "LEFT JOIN accountbalance ab ON $ac=ab.accountcode AND ab.entitytype='$et' AND ab.entitycode=$ec AND ab.monthcode=m.monthcode ";
$browser->SetJoins( $joins );

$browser->SetWhere( "coa.accountcode = $ac" );
$browser->AndWhere( "m.financialyearcode = $year" );

$browser->ForceOrder( 'monthcode', 'ASC' );
$rowurl = '/browse.php?t=transactions&et=%s&ec=%s&ac=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'entitytype', 'entitycode', 'accountcode', '#even' );
$page_elements[] = $browser;


$browser = new Browser("Balance of Account, by Month");
$money_fmt = '<td class="money"><a href="/browse.php?t=transactions&et='.$et.'&ec='.$ec.'&ac='.$ac.'&year=%s" class="invisilink">%%s</a></td>';
$browser->RowFormat( "<tr class=\"r%d\">\n", "</tr>\n", 'entitytype', 'entitycode', 'accountcode', '#even' );
$browser->AddHidden( 'entitytype', "'$et'" );
$browser->AddHidden( 'entitycode', "$ec" );
$browser->AddHidden( 'accountcode', "$ac" );
$browser->AddHidden( 'monthcode', 'm.monthcode' );
$browser->AddHidden( 'monthpart', "substring( startdate::text from 6 for 5 )" );
$browser->AddColumn( 'month', 'Month', 'left', $month_format, "to_char( startdate, 'YYYY, FMMonth' )" );
for ( $i=0; $i<8; $i++ ) {
  $browser->AddColumn( "year_$i", $year - $i, 'right', sprintf($money_fmt, $year - $i), "to_char(previous_summary('$et',$ec,$ac,m.monthcode,$i),'FMMI999G999G999G990D00')" );
}

$joins = 'chartofaccount coa, month m ';
$joins .= "LEFT JOIN accountbalance ab ON $ac=ab.accountcode AND ab.entitytype='$et' AND ab.entitycode=$ec AND ab.monthcode=m.monthcode ";
$browser->SetJoins( $joins );

$browser->SetWhere( "coa.accountcode = $ac" );
$browser->AndWhere( "m.financialyearcode = $year" );

$browser->ForceOrder( 'monthcode', 'ASC' );
$rowurl = '/browse.php?t=transactions&et=%s&ec=%s&ac=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'entitytype', 'entitycode', 'accountcode', '#even' );
$page_elements[] = $browser;


$browser = new Browser("Balance of Account, by Day");
$browser->AddColumn( 'day', 'Date' );
$browser->AddColumn( 'balance', 'Balance', 'money', '', "to_char(balance,'FMMI999G999G999G990D00')" );
$browser->SetJoins( "daily_balance( '$et'::char, $ec, $ac::numeric, (current_date - '100 days'::interval)::date, 100)" );
$browser->SetOrdering( 'day' );
$rowurl = "/browse.php?t=transactions&et=$et&ec=$ec&ac=$ac";
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", '#even' );
$page_elements[] = $browser;
