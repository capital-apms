package APMS;

use strict;

use Gtk2 qw/ -init /;
use APMS::Class qw/ new_object /;
use APMS::Model;
use APMS::Control::Window::MainWin;
use APMS::Control::Window::Menu;
use APMS::Control::Drill::BankAccount;
use APMS::Control::Drill::Tenant;
use APMS::Control::Drill::Company;
use APMS::Control::Drill::ChartAccount;
use APMS::Control::Drill::Invoice;
use APMS::Control::Maint::BankAccount;
use APMS::Control::Maint::Tenant;
use APMS::Control::Maint::Invoice;
use APMS::Control::Maint::OfficeSettings;
#use APMS::Control::Security::MenuBrowser;

# What windows can only have one instance?
my $only_one_window = 'APMS::Controller::Viewer::Drill';

sub new {
	my ( $proto, $config ) = @_;
	my $self = new_object( $proto );

	# Connecto to the database
	$self->{model} = APMS::Model->connect( $config->{Database} );
	$self->{config} = $config;

	return $self;
}

=item run

Run an APMS window

  $self->run( class => 'APMS::Control::Some::Thing' );

Where the class name refers to some APMS window class. This method
simply calls the class method "run" and passes $self to it. $self
has the DB model assiciated with it, as well as the window that lists
the other open windows.

In this step, apms also checks if this class of window can have
multiple instances open at the same time. for example, maint screens
generally can, but not drill windows. See $only_one_window for what
can only have one window.

=cut

sub run {
	my ( $self, %args ) = @_;

	# Should this window have a parent?
	if ( $args{parentview} && $args{parentview}->has_child( $args{class} ) ) {
		# Check for restriction on number of same class windows
		return 0 if $args{class} =~ /$only_one_window/;
	}

	# Initialise the controller (which should create a view)
	my $view = $args{class}->run( $self, $args{nodecode}, %{ $args{args} } );

	# Append to the parent window
	if ( $view && $args{parentview} ) {
		$args{parentview}->append_child( $view );
	}

	return 1;
}

sub runmenu {
	my ( $self, %args ) = @_;
	$self->run( class => 'APMS::Control::Window::Menu', %args );
}

=item model

Returns the data model object

  my @returndata = $self->model->somequery( @parameters );

Enables the above call to work from anything that sets $self.

=cut

sub model {
	my $self = shift;
	return $self->{model};
}

=item config

Returns the configuration hash

  my $something = $self->config->{SomeConfigItem}

for making the configuration available from within other classes.

=cut

sub config {
	my $self = shift;
	return $self->{config};
}

=item mainloop

The GTK mainloop

  $self->mainloop;

Initiate the GTK loop, thus the interface.

=cut

sub mainloop {
	Gtk2->main;
}

sub errormessage {
	my ( $self, $message ) = @_;

	my $window = Gtk2::Window->new( 'toplevel' );
	my ( $amount, $response );
	my $dialog = Gtk2::MessageDialog->new(
		$window,
		'destroy-with-parent',
		'error',
		'ok',
		"%s", $message
	);

	$response = $dialog->run;
	$dialog->destroy;

}

sub quit {
	Gtk2->main_quit;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

