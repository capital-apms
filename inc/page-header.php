<?php

function make_help_link($matches)
{
  // as usual: $matches[0] is the complete match
  // $matches[1] the match for the first subpattern
  // enclosed in '##...##' and so on
  // Use like: $s = preg_replace_callback("/##([^#]+)##", "make_help_link", $s);
//  $help_topic = preg_replace( '/^##(.+)##$/', '$1', $matches[1]);
  $help_topic = $matches[1];
  $display_url = $help_topic;
  if ( $GLOBALS['session']->AllowedTo("Admin") || $GLOBALS['session']->AllowedTo("Support") ) {
    if ( strlen($display_url) > 30 ) {
      $display_url = substr( $display_url, 0, 28 ) . "..." ;
    }
  }
  else {
    $display_url = "help";
  }
  return " <a class=\"help\" href=\"/help.php?h=$help_topic\" title=\"Show help on '$help_topic'\" target=\"_new\">[$display_url]</a> ";
}


function send_page_header() {
  global $c, $main_menu, $office_settings, $office_accounts;

  echo <<<EOHDR
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>$c->page_title</title>

EOHDR;

  foreach ( $c->stylesheets AS $stylesheet ) {
    echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$stylesheet\" />\n";
  }
  if ( isset($c->local_styles) ) {
    // Always load local styles last, so they can override prior ones...
    foreach ( $c->local_styles AS $stylesheet ) {
      echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$stylesheet\" />\n";
    }
  }

  if ( isset($c->print_styles) ) {
    // Finally, load print styles last, so they can override all of the above...
    foreach ( $c->print_styles AS $stylesheet ) {
      echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$stylesheet\" media=\"print\"/>\n";
    }
  }

  if (isset($office_settings) || isset($office_accounts) ) {
    $gst_rate = $office_settings->{'GST-Rate'};
    echo "<script language=\"JavaScript\">\napms = {\n";
    if ( isset($office_settings) ) {
      foreach( $office_settings AS $k => $v ) {
        printf( '  %s:"%s",%s', str_replace('-','_',(strtolower($k))), str_replace( '\\', '\\\\', $v), "\n" );
      }
    }
    if ( isset($office_accounts) ) {
      foreach( $office_accounts AS $k => $v ) {
        printf( "  %s:{\n", str_replace('-','_',(strtolower($k))) );
        foreach( $v AS $fname => $fval ) {
          printf( "    %s:\"%s\",\n", $fname, $fval );
        }
        echo "  },\n";
      }
    }
    echo "};\n</script>\n";
  }

  foreach ( $c->scripts AS $script ) {
    echo "<script language=\"JavaScript\" src=\"$script\"></script>\n";
  }

  echo "</head>\n<body>\n";

  if ( isset($main_menu) && is_object($main_menu) ) {
    $main_menu->LinkActiveSubMenus();
    if ( function_exists("local_menu_bar") ) {
      local_menu_bar($main_menu);
    }
    else {
      echo $main_menu->RenderAsCSS();
    }
  }

  if ( isset($c->messages) && is_array($c->messages) && count($c->messages) > 0 ) {
    echo "<div id=\"messages\"><ul class=\"messages\">\n";
    foreach( $c->messages AS $i => $msg ) {
      // ##HelpTextKey## gets converted to a "/help.phph=HelpTextKey" link
      $msg = preg_replace_callback("/##([^#]+)##/", "make_help_link", $msg);
      echo "<li class=\"messages\">$msg</li>\n";
    }
    echo "</ul></div>\n";
  }
}

send_page_header();

