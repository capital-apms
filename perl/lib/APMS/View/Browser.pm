package APMS::View::Browser;

use Glib qw/ TRUE FALSE /;

=head1 APMS::View::BrowseTable

Manage the browse window widgets for APMS

=cut

# Provide a map between data types, and Gtk2::TreeViewColumn
# attributes.
my %attrmap = (
	'Glib::Boolean' => 'active',
	'Glib::Uint'    => 'text',
	'Glib::String'  => 'text',
);

use strict;

=item browse_window

A generic interface to the Gtk2::TreeView widget. Creates an APMS style
browser box using TreeView. Basicly you pass this method a definition of
the columns, some callbacks, and the data you want to insert into the view.

=cut

sub browse_window {
	my ( $self, %def ) = @_;

	# Create a list of columns, ordered
	my @columns;
	map { push @columns, $_->{column} } @{ $def{fields} };

	# List of column types, ordered for Gtk2::ListStore
	my @columntypes;
	map { push @columntypes, $_->{type} } @{ $def{fields} };

	# Create the store object
	my $store = Gtk2::ListStore->new( @columntypes );

	# Create a sortable wrapper around it
	my $sortstore = Gtk2::TreeModelSort->new( $store );

	# Put the data into the store
	foreach my $row ( @{ $def{data} } ) {

		# Order by column def
		my @data;
		my $count = 0;
		foreach my $column ( @columns ) {
			push @data, ( $count, $row->{$column} );
			$count++;
		}

		# Append row to store, and fill the row with data
		my $iter = $store->append;
		$store->set( $iter, @data );
	}

	# The View object
	my $treeview = Gtk2::TreeView->new( $sortstore );

	# Attach a callback to row selection
	if ( exists $def{row_clicked} ) {
		#$treeview->signal_connect( 'row-activated' => $def{row_clicked} );
		my $selection = $treeview->get_selection();
		$selection->set_select_function( $def{row_clicked} );
	}

	# Add the field widgets to the view
	my $count = 0;
	foreach my $field ( @{ $def{fields} } ) {

		# Create the field widget
		my $class = $field->{widget};
		my $renderer = $class->new;

		my $column = Gtk2::TreeViewColumn->new_with_attributes(
			$field->{name},
			$renderer,
			$attrmap{"$field->{type}"} => $count
		);

		# Set various width attributes if their defined
		$column->set_min_width( $field->{minw} ) if exists $field->{minw};
		$column->set_expand( $field->{minw} ) if exists $field->{expand};

		# Should this column be sortable
		if ( exists $field->{dosort} && $field->{dosort} ) {
			$column->set_sort_column_id( $count );
			$column->set_sort_order( 'ascending' );
			$column->set_sort_indicator( TRUE ); # Show the arrow
		}

		# Append the column to the treeview
		$treeview->append_column( $column );

		$count++;
	}

	# Return the completed object
	return $treeview;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

