#
# == Apms::Window
# A base class which all APMS windows inherit from which handles
# stuff like saving it's last position on close, and adding itself
# to the system wide navigator menu and such.
#
# == Bugs
# Perish the thought!  Those are _features_!
#
# == Author
# Andrew McMillan <andrew@mcmillan.net.nz>
#
# == License
# Copyright (c) Andrew McMillan, 2007
# GNU General Public License version 2
#

class ApmsWindow < Gtk::Window
  def tips
    @@tips
  end
  def tips=(tips_ptr)
    @@tips = tips_ptr
  end

  attr_reader :control, :content, :viewer, :menu, :position

  #
  # Window.new( dbh, &tooltips, source_id, fill='H', size=500 )
  #
  # Creates a window into which we shall stuff things.
  #
  def initialize( in_title = "" )
    super
    border_width = 2
    title = ( in_title == "" ? "APMS" : in_title)

    # Everything ends up in a single VBox container which has one HBox per major element
    @container = Gtk::VBox.new
    add(@container)
    @control = Gtk::HBox.new
    @container.pack_start(@control, false, false)
    @content = Gtk::HBox.new
    @container.add(@content)
    @viewer  = Gtk::HBox.new
    @container.pack_end(@viewer, false, false)
    @menu    = Gtk::HBox.new
    @container.pack_end(@menu, false, false)

    # FIXME:
    # Convenience during development... Remove this later.  Ctrl+Q should only work in the main menu really.
    signal_connect("key_release_event") do |w,e|
      if e.state == Gdk::Window::CONTROL_MASK then
        if e.keyval == 113 then
          puts "Got ^Q - quitting"
          Gtk.main_quit
        end
      end
    end


  end
end

#
# Most APMS Windows actuall have a menu across the bottom.  Some have only
# this, so this class is used directly in some cases, and inherited from
# for drill windows and maintenance windows
#
class ApmsMenuWindow < ApmsWindow

  attr_reader :direction, :size
  attr_writer :direction, :size

  def node_id
    @node_id
  end

  def initialize( in_title = "" )
    super
    @direction = 'V'
    @size = -1
  end

  def set_target( in_node_id )
    @node_id = in_node_id
    menu = ApmsMenuLinks.new($dbh,@@tips,@node_id,@direction,@size )
    @menu.add(menu.buttons)
  end

end

#
# Drill Windows have a control panel at the top, a record browser below
# and (in some cases) a record detail viewer below that.  They will almost
# always have a menu at the bottom as well.
#
class ApmsDrillWindow < ApmsMenuWindow

  attr_reader :scroller, :table

  def initialize( in_title = "" )
    super
    @direction = 'H'
    @size = 500
    @browse_name = nil
    #if programlink != nil then
    #  @browse_name = programlink[""]
    #end
  end

  def set_target( in_node_id )
    @node_id = in_node_id
    sth = $dbh.prepare("SELECT rbfile, nodetype, description FROM linknode WHERE nodecode = " + @node_id.to_s)
    sth.execute()
    linknode = sth.fetch
    sth.finish

    @browserdef = linknode[0]
    @nodetype = linknode[1]
    @description = linknode[2]
    if @browserdef == "" then
      puts "Node #{@node_id} has no target rbfile"
    else
      puts "Loading node id " + @node_id.to_s + " from file " + @browserdef
      @records = ApmsRecordBrowser.new(linknode[0])
      @scroller = Gtk::ScrolledWindow.new
      @scroller.add(@records.treeview)
      @content.add( @scroller )
      @size = @records.size_x
    end
    super
  end

  def set_source( in_node_id )
  end

end
