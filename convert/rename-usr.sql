BEGIN;
ALTER TABLE usr RENAME TO appuser;
ALTER TABLE appuser DROP CONSTRAINT usr_pkey;
ALTER TABLE appuser ADD PRIMARY KEY (username);
COMMIT;
ALTER TABLE appuser DROP CONSTRAINT usr_username_key;
DROP INDEX usr_sk1_unique_username;

