<?php
require_once('../inc/always.php');

require_once('classEditor.php');
require_once('classLinks.php');

require_once('apms_menus.php');

// $c->stylesheets[] = "css/edit.css";
$page_elements = array();

if ( ! @include_once( "edit/$component.php" ) ) {
  $c->messages[] = "edit/$component not found";
}

///////////////////////////////////////////////////////
// From here we start rendering the page to the user...
///////////////////////////////////////////////////////
include('page-header.php');

$heading_level = null;
foreach( $page_elements AS $k => $page_element ) {
  echo $page_element->Render($heading_level);
  $heading_level = 'h2';
}

include('page-footer.php');

