<?php
/**
* Component for browsing vouchers for approval
* @package   apms
* @subpackage   approve_vouchers
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/

param_to_global('status','#^[UA]#i');  if ( isset($status) ) $status = strtoupper($status); else $status = 'U';

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'year', 'int1', "SELECT 0, '--- All Years ---' UNION SELECT financialyearcode, description FROM financialyear ORDER BY 1;" );
$widget->AddField( 'status', 'char1', "SELECT voucherstatus, description FROM voucherstatus ORDER BY 1;" );
$widget->ReadWrite();
$widget->Defaults( array('status' => 'U', 'year' => $year) );
$widget->Layout( '<table><tr><th>For year:</th><td>##year.select##</td><th>Status:</th><td>##status.select##</td><td>##Show.submit##</td></tr></table>' );
$page_elements[] = $widget;


/**
* The main browse, which is a form, in this case.
*/
$browser = new Browser("Approve Vouchers");
$c->page_title = $browser->Title();

if ( isset($et) && isset($ec) ) {
  $qry = new PgQuery( "SELECT get_entity_type_name(?) AS entitytypename, get_entity_name(?,?) AS entityname;", $et, $et, $ec);
  if ( $qry->Exec("action-$component") && $row = $qry->Fetch() ) {
    $browser->Title("$row->entitytypename Vouchers for $row->entityname ($et$ec)");
  }
}

$browser->AddColumn( 'voucherseq', '#', 'center' );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'creditor_name', 'Creditor', 'center', '', 'creditor.name' );
$browser->AddColumn( 'voucherstatus', 'Approve', 'center', '<input type="checkbox" name="approve[##voucherseq##]" value="t" onclick="Toggle(\'approve[##voucherseq##]\');">' );
$browser->AddColumn( 'invoicereference', 'Invoice#', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'ourorderno', 'Order', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'description', 'Description' );
$browser->AddColumn( 'goodsvalue', 'Goods', 'right', '%0.2lf' );
$browser->AddColumn( 'taxvalue', 'Tax', 'right', '%0.2lf' );
$browser->AddColumn( 'total', 'Total', 'right', '%0.2lf', '(goodsvalue + taxvalue)' );
$browser->SetJoins( "voucher JOIN creditor USING (creditorcode) " );
// $browser->SetJoins( "voucher JOIN voucherline USING (voucherseq)" );
$browser->SetWhere( "voucher.voucherstatus=" . qpg($widget->Record->{'status'}) );
if ( isset($et) && isset($ec) )
  $browser->AndWhere( "voucher.entitytype='$et' AND voucher.entitycode=$ec" );
if ( $widget->Record->{'year'} > 0 ) {
  $browser->SetJoins("voucher JOIN month ON voucher.date BETWEEN month.startdate AND month.enddate JOIN creditor USING (creditorcode) ");
  $browser->AndWhere( "financialyearcode = " . qpg($widget->Record->{'year'}) );
}
$browser->AddOrder( 'voucherseq', 'ASC', 1 );
$rowurl = '/view.php?t=voucher&id=%s';
$browser->RowFormat( "<tr onclick=\"Toggle('approve[%s]');\" title=\"Click to Display Voucher Detail\" class=\"r%d\">\n", "</tr>\n", 'voucherseq', '#even' );

$browser->SetDiv( '<div class="browser"><form method="POST">', '<input class="submit" type="submit" name="submit" value="Approve"></div>' );

$page_elements[] = $browser;
