<?php

// Editor component for unposted batches
$editor = new Editor("Unposted Batch", 'newbatch');
$editor->AddField( 'orig_batchcode', 'batchcode' );

param_to_global( 'id', 'int', 'orig_batchcode', 'batchcode' );
if ( isset($id ) ) $editor->SetWhere( "batchcode=$id" );
$editor->Initialise( array('description' => '', 'documentcount' => 0 ) );

// Editor component for invoice line
$editrow = new Editor("Transaction", 'newaccttrans');
$editrow->AddField( 'txn_batchcode', 'newaccttrans.batchcode' );
$editrow->AddField( 'txn_documentcode', 'newaccttrans.documentcode' );
$editrow->AddField( 'etname', 'get_entity_type_name(entitytype)' );
$editrow->AddField( 'my_transactioncode', 'transactioncode' );
$editrow->SetSubmitName( 'saverow' );

if ( $editor->IsSubmit() ) {
  $editor->WhereNewRecord( "batchcode=currval('newbatch_batchcode_seq')" );
  $editor->Write();
  $id = $editor->Value('batchcode');
}
else if ( isset($id) ) {
  $editor->GetRecord();
}
else {
  $id = 0;
}

$doc = 1;

if ( $editrow->IsSubmit() ) {
  error_log("submitting");
  $_POST['batchcode'] = $id;
  $_POST['documentcode'] = $doc;
  if ( ! $editrow->IsUpdate() ) {
    error_log("Not updating");
    $qry = new PgQuery("SELECT transactioncode FROM newaccttrans WHERE batchcode = ? AND documentcode = ? ORDER BY transactioncode DESC LIMIT 1", $id, $doc);
    if ( $qry->Exec("newbatch") &&  $qry->rows == 1 ) {
      $row = $qry->Fetch();
      $transactioncode = $row->transactioncode + 1;
    }
    else {
      $qry = new PgQuery('INSERT INTO newdocument ( batchcode, documentcode, documenttype, reference, description ) VALUES(?,?,?,?,?);',
                           $id, $doc, 'JRNL', "J-$id", $editor->Value('description') );
      $qry->Exec('newbatch');
      $transactioncode = 1;
    }
    $_POST['transactioncode'] = $transactioncode;
  }
  else {
    $transactioncode = $_POST['transactioncode'];
  }
  $editrow->SetWhere( "batchcode=$id AND documentcode=$doc AND transactioncode=$transactioncode");
  $editrow->WhereNewRecord( "batchcode=$id AND documentcode=$doc AND transactioncode=$transactioncode" );
  $editrow->Write();

  $qry = new PgQuery('UPDATE newbatch SET documentcount = (SELECT count(1) FROM newdocument WHERE newdocument.batchcode=?);', $id );
  $qry->Exec('newbatch');
  unset($_GET['editseq']);
}


$submittype = 'Update';
if ( ! $editor->Available() ) {
  $submittype = 'Create';
  $editor->Initialise( array('batchtype' => 'NORM', 'description' => '' ) );
}

$editor->SetLookup( 'batchtype', "SELECT batchtype, batchtype || ' - ' || description FROM batchtype ORDER BY 1;");

$template = <<<EOTEMPLATE
<form method="POST" enctype="multipart/form-data" id="entry">
<table>
 <tr>
  <th class="right">Unposted Batch:</th>
  <td class="center">##orig_batchcode.value####orig_batchcode.hidden##</td>
  <th class="right">Type:</th>
  <td class="center">##batchtype.select##</td>
 </tr>
 <tr>
  <th class="right">Description:</th>
  <td class="left" colspan="3">##description.input.50##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left" colspan="3">##submit##</td>
 </tr>
</table>
</form>

EOTEMPLATE;

$editor->SetTemplate( $template );
$c->page_title = $editor->Title("Unposted Batch: $id - ".$editor->Record->{'description'});
$documentcount = $editor->Record->{'documentcount'};
$page_elements[] = $editor;

$related_menu->AddOption("View Batch","/view.php?t=newbatch&id=$id","View this unposted batch of transactions.");
$related_menu->AddOption("Edit Batch","/edit.php?t=newbatch&id=$id","Edit this unposted batch of transactions.");
$related_menu->AddOption("Create Batch","/edit.php?t=newbatch","Create a new batch of transactions.");
if ( $editor->Record->{'batchtype'} == 'ACCR' ) {
  $related_menu->AddOption("Copy Batch","/action.php?t=batch-copy&batchcode=$id","Copy this batch of accruals transactions");
}
else {
  $related_menu->AddOption("Update Batch","/action.php?t=batch-update&batchcode=$id","Update this batch of transactions");
}


// Now list income accounts
require_once('classBrowser.php');

if ( $submittype == 'Update' ) {

  function edit_row( $row_data ) {
    global $editrow, $id;

    $rowsubmittype = ($row_data->transactioncode == -1 ? 'Add' : 'Save');
    $form_url = preg_replace( '#&(edit|del)seq=\d+#', '', $_SERVER['REQUEST_URI'] );
    if ( ! preg_match( '/&id=\d+/', $form_url ) ) {
      $form_url .= "&id=$id";
    }

    $template = <<<EOTEMPLATE
<form method="POST" enctype="multipart/form-data" id="rowentry" action="$form_url">
  <td class="left">##transactioncode.value####transactioncode.hidden##<input type="hidden" name="id" value="$id"></td>
  <td class="left" style="white-space:nowrap">##entitytype.input.1## ##entitycode.input.5## ##accountcode.input.7##</td>
  <td class="left">##date.date##</td>
  <td class="left">##reference.input.8##</td>
  <td class="left">##description.input.25##</td>
  <td class="left">##amount.money.8##</td>
  <td class="left">##$rowsubmittype.submit##</td>
</form>

EOTEMPLATE;

    $editrow->SetTemplate( $template );
    if ( $row_data->transactioncode == -1 )
      $editrow->Initialise( (array) $row_data );
    else
      $editrow->SetRecord( $row_data );

    $editrow->Title("");
    return $editrow->Render();
  }

  // And the invoice line details
  require_once('classBrowser.php');
  $browser = new Browser("Transaction Detail");
  $browser->AddHidden( 'etname', 'lower(get_entity_type_name(entitytype))' );
  $browser->AddColumn( 'transactioncode', '#', 'right' );
  $browser->AddHidden( 'entitytype' );
  $browser->AddHidden( 'entitycode' );
  $browser->AddHidden( 'entityname', 'get_entity_name(entitytype,entitycode)' );
  $browser->AddHidden( 'accountcode' );
//  $browser->AddHidden( 'account', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')" );
  $browser->AddHidden( 'accountname', 'chartofaccount.name');
  $browser->AddColumn( 'account', 'Account', 'left', '', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')");
  $browser->AddColumn( 'date', 'Date', 'center' );
  $browser->AddColumn( 'reference', 'Reference', 'left' );
  $browser->AddColumn( 'description', 'Description', 'left' );
  $browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
  $edit_link = "<a href=\"/edit.php?t=newbatch&id=$id&editseq=##transactioncode##\" class=\"submit\">Edit</a>";
  $del_link  = "<a href=\"/edit.php?t=newbatch&id=$id&delseq=##transactioncode##\" class=\"submit\">Del</a>";
  $browser->AddColumn( 'action', 'Action', 'center', '', "'$edit_link&nbsp;$del_link'" );
  $browser->AddOrder( 'transactioncode', 'ASC' );
  $browser->SetJoins( "newaccttrans LEFT OUTER JOIN chartofaccount USING ( accountcode )" );

  $browser->AndWhere( "batchcode=$id AND documentcode=$doc" );
  $rowurl = '/view.php?t=account&id=%s';
  $browser->RowFormat( "<tr class=\"r%d\" title=\"%s: %s - %s\">\n", "</tr>\n", '#even', 'entitytype', 'entityname', 'accountname' );
  $page_elements[] = $browser;

  if ( isset($_GET['delseq']) ) {
    $qry = new PgQuery("DELETE FROM newaccttrans WHERE batchcode=? AND documentcode=? AND transactioncode = ?", $id, $doc, intval($_GET['delseq']) );
    $qry->Exec('newbatch');
  }
  if ( isset($_GET['editseq']) ) {
    $browser->MatchedRow('transactioncode', intval($_GET['editseq']), 'edit_row');
  }
  else {
    $remaining = $editor->Record->{'total'};
    $extra_row = array( 'transactioncode' => -1, 'percent' => 100, 'amount' => $remaining, 'yourshare' => $remaining );
    $qry = new PgQuery("SELECT entitytype, entitycode, accountcode, date, reference, description FROM newaccttrans WHERE batchcode=$id AND documentcode=$doc ORDER BY transactioncode DESC LIMIT 1");
    if ( $qry->Exec('newbatch') && $qry->rows == 1 ) {
      $row = $qry->Fetch();
      $extra_row['entitytype']  = $row->entitytype;
      $extra_row['entitycode']  = $row->entitycode;
      $extra_row['accountcode'] = $row->accountcode;
      $extra_row['date']        = $row->date;
      $extra_row['reference']   = $row->reference;
      $extra_row['description'] = $row->description;
    }
    else {
      $extra_row['entitytype']  = 'L';
      $extra_row['entitycode']  = 1;
      $extra_row['accountcode'] = '';
    }
    $browser->MatchedRow('transactioncode', -1, 'edit_row');
    $extra_row = (object) $extra_row;
    $browser->AddRow($extra_row);
  }

}
