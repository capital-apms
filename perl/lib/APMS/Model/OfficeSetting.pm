package APMS::Model::OfficeSetting;

use strict;

=item all_settings

Returns all the office settings from the DB

  my $settings = $self->model->all_settings();

=cut

sub all_settings {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT setname, setvalue FROM officesettings;
	) );

	my $settings;
	$sth->execute();
	map { $settings->{"$_->{setname}"} = $_->{setvalue} } @{ $sth->fetchall_arrayref({}) };
	$sth->finish;

	return $settings;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

