<?php
$related_menu->AddOption("Create Property","/edit.php?t=property","Create a new property", false, 7001);

$browser = new Browser("Properties");
$browser->AddColumn( 'propertycode', 'Property', 'center' );
$browser->AddColumn( 'companycode', 'Company', 'center' );
$browser->AddColumn( 'shortname', 'Short name', 'left' );
$browser->AddColumn( 'region', 'Region' );
$browser->SetJoins( "property" );
$browser->SetWhere( 'active' );
$browser->AddOrder( 'propertycode', 'A', 1);

$rowurl = '/view.php?t=property&id=%d';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'propertycode', '#even' );
$browser->DoQuery();

$c->page_title = "Browse Properties";

$page_elements[] = $browser;