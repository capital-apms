package APMS::View::Drill;

=head1 APMS::View::Drill

Defines a base class for drill windows

=cut

use strict;

use base qw/
	APMS::View::Window
	APMS::View::ButtonsWindow
/;

=item drill_window

This defines the drill window. The %object hash contains any of the following
keys:

panel   - An APMS::View::Panel object
browser - An APMS::View::Browser object (probably via APMS::Browser::<type>)
buttons - An APMS::View::Buttons object

=cut

sub drill_window {
	my ( $self, %object ) = @_;

	# Create the window
	$self->new_window();
	$self->get_window->set_default_size( 700, 500 );

	$self->get_window->set_title( $object{title} );
	# Create a global VBox
	my $vbox = Gtk2::VBox->new( 0, 5 );

	# Put the browse widget into a scrolled window
	my ( $scroll, $viewport );
	if ( exists $object{browser} ) {
		$scroll = Gtk2::ScrolledWindow->new();
		$scroll->set_policy( 'never', 'always' );
		$scroll->add( $object{browser} );
		$viewport = Gtk2::Viewport->new();
		$viewport->add( $scroll );
	}

	# Pack everything into the vbox
	$vbox->pack_start( $object{panel}, 0, 0, 0 ) if exists $object{panel};
	$vbox->pack_start( $object{alphabet}, 0, 0, 0 ) if exists $object{alphabet};
	$vbox->pack_start( $viewport, 1, 1, 0 ) if $viewport;
	$vbox->pack_start( $object{buttons}->box, 0, 0, 0 ) if exists $object{buttons};

	# Add it all to the window
	$self->get_window->add( $vbox );
	$self->get_window->show_all;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

