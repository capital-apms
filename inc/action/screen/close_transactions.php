<?php
/**
* Component for closing transactions
* @package   apms
* @subpackage   close_transactions
* @author    Andrew McMillan <andrew@morphoss.com>
* @copyright Morphoss Ltd  http://www.morphoss.com/
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/

// Can only close transactions for tenant/creditor
param_to_global( 'id', '#^[CT]-\d+(-\d+(\.\d+)?)?$#i' );
if ( isset($id) ) {
  list( $et, $ec, $ac ) = explode('-',$id,3);
  $et = strtoupper(substr($et,0,1));
  $ec = intval($ec);
  // $ac = floatval($ac); // ignored, in this case.
}

$c->scripts[] = 'js/jquery.js';
$page_elements[] = <<<EOSCRIPT
<script type="text/javascript">
function recalculate_total() {
  total = Number(0);
  $(":input").each(function(index){
    if ( !this.checked ) return;
    checkBoxCell = this.parentNode;
    amountCell = checkBoxCell.previousSibling.previousSibling;
//	alert("AmountCell is "+amountCell+", Amount is "+amountCell.innerHTML+", Total is "+total );
	if ( amountCell ) {
    	total = total + Number(amountCell.innerHTML);
  	}
  });
  total = Number(total).toFixed(2);
//  alert("Total is now " + total );
  $("#current_total").val(total);
}

$(document).ready(function(){
  $(":input").each(function(index){
    if ( this.name == "close[]" ) {
      cell = this.parentNode;
      cell.innerHTML = '<input readonly="readonly" name="selected_total" id="current_total" class="money" size="9" value="0.00">';
    }
  });
  recalculate_total();
  $(":input").on("change",recalculate_total);
});
</script>

EOSCRIPT;

/**
* The browser
*/
$browser = new Browser("Close Transactions");
if ( isset($et) && isset($ec) ) {
  $qry = new PgQuery( "SELECT get_entity_type_name(?) AS entitytypename, get_entity_name(?,?) AS entityname;", $et, $et, $ec);
  if ( $qry->Exec("Browse::Transaction") && $row = $qry->Fetch() ) {
    $browser->Title("Close $row->entitytypename Transactions for $row->entityname ($et$ec)");
  }
}

$browser->SetWhere( "closedstate IN ( 'O', 'P' ) " );
if ( isset($et) )
  $browser->AndWhere( "entitytype = '$et'" );
else
  $browser->AddColumn( 'entitytype', 'ET', 'center' );

if ( isset($ec) )
  $browser->AndWhere( "entitycode = $ec" );
else
  $browser->AddColumn( 'entitycode', 'Code', 'right' );

$browser->AddHidden( 'txn', "batchcode || '.' || documentcode || '.' || transactioncode" );
$browser->AddHidden( 'document', "batchcode || '.' || documentcode" );
$browser->AddHidden( 'accountname', "chartofaccount.name" );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'reference', 'Reference', 'left', '', "COALESCE(accttran.reference, document.reference)" );
$browser->AddColumn( 'description', 'Description', 'left', '', "COALESCE(accttran.description, document.description)" );
$browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$browser->AddColumn( 'closedstate', 'Close', 'center', '<input type="checkbox" name="close[##txn##]" value="C" onclick="Toggle(\'close[##txn##]\');">' );
$browser->AddTotal( 'amount' );
$browser->SetJoins( "accttran JOIN batch USING (batchcode) JOIN document USING (batchcode, documentcode) JOIN month USING (monthcode) JOIN chartofaccount USING(accountcode)" );

include_once("menus_entityaccount.php");

$browser->AddOrder( 'date', 'ASC' );
$rowurl = '/view.php?t=document&id=%s';
$browser->RowFormat( "<tr onclick=\"Toggle('close[%s]');\" title=\"Mark transactions to be closed as a group\" class=\"r%d\">\n", "</tr>\n", 'txn', '#even' );
$browser->SetDiv( '<div class="browser"><form method="POST">', '<input class="submit" type="submit" name="submit" value="Close Group"></div>' );
$browser->DoQuery();

$c->page_title = $browser->Title();
$page_elements[] = $browser;
