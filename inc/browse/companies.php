<?php
$related_menu->AddOption("Create Company","/edit.php?t=company","Create a new company", false, 7001);

$browser = new Browser("Companies");
$browser->AddColumn( 'parentcode', 'Parent', 'right' );
$browser->AddColumn( 'companycode', 'Company', 'right' );
$browser->AddColumn( 'name', 'Company Name', 'left', '', 'legalname' );
$browser->SetJoins( "company" );
$browser->SetWhere( 'active' );
$browser->SetOrdering( 'name' );
$rowurl = '/view.php?t=company&id=%d';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'companycode', '#even' );
$browser->DoQuery();

$c->page_title = "Browse Companies";

$page_elements[] = $browser;
