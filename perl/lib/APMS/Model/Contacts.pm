package APMS::Model::Contacts;

use strict;

=item contact_types

Returns the contact types from the DB

  my @contacttypes = $self->model->contact_types();

=cut

sub contact_types {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT contacttype, description, systemcode FROM contacttype;
	) );

	$sth->execute();
	my $contacttypes = $sth->fetchall_arrayref({});
	$sth->finish;

	return $contacttypes;
}

=item system_contact_types

Returns the contact types from the DB

  my @contacttypes = $self->model->system_contact_types();

=cut

sub system_contact_types {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT contacttype, description, systemcode FROM contacttype WHERE systemcode = TRUE;
	) );

	$sth->execute();
	my $contacttypes = $sth->fetchall_arrayref({});
	$sth->finish;

	return $contacttypes;
}

=item postal_types

Returns the address types from the DB

  my @addresstypes = $self->model->postal_types();

=cut

sub postal_types {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT postaltype, description FROM postaltype;
	) );

	$sth->execute();
	my $postaltypes = $sth->fetchall_arrayref({});
	$sth->finish;

	return $postaltypes;
}

sub phone_types {
	my $self = shift;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT phonetype, description FROM phonetype;
	) );

	$sth->execute;
	my $phonetypes = $sth->fetchall_arrayref({});
	$sth->finish;

	# Include an integer index here as it proves to be usefull as the selection
	# is controlled by code, as well as code being executed by selection.
	my $count = 0;
	foreach my $phonetype ( @{ $phonetypes } ) {
		$phonetype->{index} = $count;
		$count++;
	}

	return $phonetypes;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

