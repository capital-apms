package APMS::View;

use strict;

use base qw/ APMS::View::Window /;

=item APMS::View::Viewer

Some basic functions for manipulating viewer window widgets

=cut

sub connect_signal {
	my ( $self, $name, $signal, $function ) = @_;

	if ( exists $self->{"$name"} ) {
		$self->{"$name"}->signal_connect( $signal, $function );
	}
}

sub disable_widgets {
	my ( $self, @names ) = @_;

	foreach my $name ( @names ) {
		if ( exists $self->{"$name"} ) {
			$self->{"$name"}->set_sensitive( 0 );
		}
	}
}

sub enable_widgets {
	my ( $self, @names ) = @_;

	foreach my $name ( @names ) {
		if ( exists $self->{"$name"} ) {
			$self->{"$name"}->set_sensitive( 1 );
		}
	}
}

sub get_value {
	my ( $self, $name, %opts ) = @_;

	if ( exists $self->{"$name"} ) {
		my $widget = $self->{"$name"};
		my $type = ref( $widget );
		if ( $type =~ /ComboBox/ ) {
			my $selected = $widget->get_active_iter();
			print "Get the value from the combo\n";
		}
		elsif ( $type =~ /Entry/ ) {
			return $widget->get_text();
		}
	}
}

sub set_value {
	my ( $self, $name, $value ) = @_;

	if ( exists $self->{$name} ) {
		$self->{$name}->set_text( $value );
	}
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

