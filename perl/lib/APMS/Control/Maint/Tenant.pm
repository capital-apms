package APMS::Control::Maint::Tenant;

=head1 APMS::Control::Maint::Tenant

Controller for the tenant maintenance screen

=cut

use strict;
use APMS::Class qw/ new_object /;
use APMS::Constants qw/ APMS_VIEW APMS_MAINT APMS_ADD /;
use APMS::View::Maint::Tenant;

use base qw/ APMS::Control::Window /;

sub run {
	my ( $proto, $apms, $thisnode, %args ) = @_;
	my $self = new_object( $proto );

	$self->{apms} = $apms;

	# What mode is this screen called in
	my $mode;

	if ( $args{mode} =~ /^view$/i ) {
		$mode = APMS_VIEW;
	}
	elsif ( $args{mode} =~ /^maintain$/i ) {
		$mode = APMS_MAINT;
	}
	elsif ( $args{mode} =~ /^add$/i ) {
		$mode = APMS_ADD;
	}

	# Fetch the tenant record, or get a new tenant code if in ADD mode
	if ( $mode == APMS_ADD ) {
		$self->{tenantrecord}->{tenantcode} = $apms->model->available_tenantcode();
		$self->{tenantrecord}->{active} = 1;
	}
	else {
		# Fetch the selected record id from the previous window
		return unless exists $args{recordid};

		# Its a function ref since %args was attached to the button before the
		# user pressed the button that brought them here.
		my $tenantcode = $args{recordid}->();

		unless ( $tenantcode ) {
			$apms->errormessage( "No tenant selected" );
			return;
		}

		# Used the (hopefully) passed recid to find the working record
		$self->{tenantrecord} = $apms->model->tenant_record( $tenantcode );
	}

	# Get required extra data for the tenant window select dropdowns
	$self->{paymentstyles} = $apms->model->tenant_paymentstyles();
	$self->{debtclasses}   = $apms->model->tenant_debt_classifications();
	$self->{variances}     = $apms->model->tenant_variance_classifications();
	$self->{contacttypes}  = $apms->model->tenant_contacts();
	$self->{addresstypes}  = $apms->model->postal_types();
	$self->{phonetypes}    = $apms->model->phone_types();

	# Create the tenant window
	$self->{tenantwindow} = APMS::View::Maint::Tenant->new(
		tenantrecord  => $self->{tenantrecord},
		paymentstyles => $self->{paymentstyles},
		debtclasses   => $self->{debtclasses},
		variances     => $self->{variances},
		contacttypes  => $self->{contacttypes},
		addresstypes  => $self->{addresstypes},
		phonetypes    => $self->{phonetypes}
	);

	# Connect the inputs to the callbacks
	if ( $mode == APMS_ADD ) {
		# Check what the user just entered for a tenant ID
		$self->{tenantwindow}->connect_signal( 'tenantid', 'focus-out-event', sub { $self->check_tenant_id( @_ ) } );
	}
	else {
		# No callback and not editable
		$self->{tenantwindow}->disable_widgets( 'tenantid' );
	}
	$self->{tenantwindow}->connect_signal( 'entitytype',  'changed', sub { $self->check_entity_type( @_ ) } );
	$self->{tenantwindow}->connect_signal( 'entitycode',  'changed', sub { $self->check_entity_code( @_ ) } );
	$self->{tenantwindow}->connect_signal( 'contacttype', 'changed', sub { $self->contact_type_changed( @_ ) } );
	$self->{tenantwindow}->connect_signal( 'addresstype', 'changed', sub { $self->address_type_changed( @_ ) } );
	$self->{tenantwindow}->connect_signal( 'clearbutton', 'clicked', sub { $self->clear_current_address( @_ ) } );
	$self->{tenantwindow}->connect_signal( 'save',        'clicked', sub { $self->save_record( @_ ) } );
	$self->{tenantwindow}->connect_signal( 'phonetype1',  'changed', sub { $self->phone_type_changed( 1, @_ ) } );
	$self->{tenantwindow}->connect_signal( 'phonetype2',  'changed', sub { $self->phone_type_changed( 2, @_ ) } );
	$self->{tenantwindow}->connect_signal( 'phonetype3',  'changed', sub { $self->phone_type_changed( 3, @_ ) } );
	$self->{tenantwindow}->connect_signal( 'phonetype4',  'changed', sub { $self->phone_type_changed( 4, @_ ) } );

	# Attach the destroy event
	$self->{tenantwindow}->attach_destroy( sub { $self->destroy_window( @_ ) } );

	# Put the address record into the correct state by simulating a callback
	if ( $mode != APMS_ADD ) {
		$self->contact_type_changed( $self->{tenantwindow}->{contacttype} );
	}

	return $self->{tenantwindow};
}

=item check_tenant_id

Callback on the tenantid entry box that the user just entered a tenant ID not
currently used.

=cut

sub check_tenant_id {
	my ( $self, $widget ) = @_;

	my $val = $widget->get_text();
	$val = $self->{apms}->model->available_tenantcode( $val );
	$widget->set_text( $val );

	return 0;
}

=item check_entity_type

Callback on changes to the entity type

=cut

sub check_entity_type {
	print "check entity type\n";
}

sub check_entity_code {
	print "check entity code\n";
}

=item contact_type_changed

Callback for when the contact type is changed. If a personcode is found for this
contact type then all the address records for that person are pre-fetched (via the
model method person_contact_records().

=cut

sub contact_type_changed {
	my ( $self, $widget ) = @_;

	# Get the field name in the tenant record from the contact selection
	my $selection = $widget->get_active();
	my $fieldname = $self->{contacttypes}->[$selection]->{field};
	my $personcode = $self->{tenantrecord}->{"$fieldname"};

	if ( $personcode ) {
		# Pre-fetch all address and phone records for this person
		$self->{personrecord} = $self->{apms}->model->person_contact_records( $personcode );
		$self->{tenantwindow}->fill_person_details(
			persontitle => $self->{personrecord}->{persontitle},
			firstname   => $self->{personrecord}->{firstname},
			lastname    => $self->{personrecord}->{lastname},
			company     => $self->{personrecord}->{company}
		);

		# Simulate an address type change to initially populate the address details
		$self->address_type_changed( $self->{tenantwindow}->{addresstype} );

		# Simulate the phone boxes getting changed
		$self->phone_type_changed( 1, $self->{tenantwindow}->{phonetype1} );
		$self->phone_type_changed( 2, $self->{tenantwindow}->{phonetype2} );
		$self->phone_type_changed( 3, $self->{tenantwindow}->{phonetype3} );
		$self->phone_type_changed( 4, $self->{tenantwindow}->{phonetype4} );
	}
	else {
		# Clear the person details and disable the address fields
		$self->{tenantwindow}->blank_person_details();
	}
}

=item address_type_changed

Callback for when the address type is changed. If the changed-to address type exists
in the pre-fetched person record, then fill it in.

=cut

sub address_type_changed {
	my ( $self, $widget ) = @_;

	# No need to bother unless a person record was found
	return unless exists $self->{personrecord};

	# Get the selected postal type
	my $selection = $widget->get_active();
	my $addresstype = $self->{addresstypes}->[$selection]->{postaltype};

	if ( $addresstype ) {
		# Does the current person have postal records for this type?
		if ( exists $self->{personrecord}->{address}->{"$addresstype"} ) {
			$self->{tenantwindow}->fill_address_details(
				address => $self->{personrecord}->{address}->{"$addresstype"}->{address},
				city    => $self->{personrecord}->{address}->{"$addresstype"}->{city},
				state   => $self->{personrecord}->{address}->{"$addresstype"}->{state},
				country => $self->{personrecord}->{address}->{"$addresstype"}->{country},
				zip     => $self->{personrecord}->{address}->{"$addresstype"}->{zip}
			);
		}
		else {
			# Clear the address details
			$self->{tenantwindow}->blank_address_details();
		}
	}
}

=item phone_type_changed

Callback for when the phone type dropdown is changed. Note: this is a callback
for four separate widgets, so when its called it is done so with the id number
as the first parameter. Also when this controller calls the view method to
update the phone number entry boxes, it also must pass the id.

=cut

sub phone_type_changed {
	my ( $self, $id, $widget ) = @_;

	my $selection = $widget->get_active();
	my $phonetype = $self->{phonetypes}->[$selection]->{phonetype};

	if ( $phonetype ) {
		if ( exists $self->{personrecord}->{phone}->{"$phonetype"} ) {
			$self->{tenantwindow}->fill_phone_detail(
				id     => $id,
				number => $self->{personrecord}->{phone}->{"$phonetype"}->{number}
			);
		}
		else {
			$self->{tenantwindow}->blank_phone_detail( id => $id );
		}
	}
}

=item clear_current_address

Callback for pushing the clear button

=cut

sub clear_current_address {
	print "clearing address\n";
}

=item save_record

Callback for pushing save

=cut

sub save_record {
	print "saving record\n";
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

