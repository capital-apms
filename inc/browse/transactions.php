<?php
/**
* Component for browsing transactions
* @package   apms
* @subpackage   browse-transactions
* @author    Andrew McMillan <andrew@catalyst.net.nz>
* @copyright Catalyst IT Ltd
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2
*/
include_once("classWidgets.php");

param_to_global( 'id', '#^[ACTLPJ]-\d+-\d+(\.\d+)?$#i' );
if ( isset($id) ) {
  list( $et, $ec, $ac ) = explode('-',$id,3);
  $et = strtoupper(substr($et,0,1));
  $ec = intval($ec);
  $ac = floatval($ac);
}
param_to_global('month', '#[0-9:/-]+#');
param_to_global('year', 'int');

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget('transactions');
$widget->ReadWrite();
if ( isset($year) && !isset($_POST['year'] ) ) {
  $widget->Assign('year', $year);
}
$widget->AddField( 'year', 'int1', "SELECT 0, '--- All Years ---' UNION SELECT financialyearcode, description FROM financialyear ORDER BY 1" );
$widget->Defaults( array('year' => $year ) );
$widget->Layout( '<table> <tr> <th>For year:</th> <td>##year.select##</td> <td>##Show.submit##</td> </tr></table>' );

$page_elements[] = $widget;



/**
* The browser
*/
$browser = new Browser("Transactions");
if ( isset($et) && isset($ec) ) {
  $qry = new PgQuery( "SELECT get_entity_type_name(?) AS entitytypename, get_entity_name(?,?) AS entityname;", $et, $et, $ec);
  if ( $qry->Exec("Browse::Transaction") && $row = $qry->Fetch() ) {
    $c->title = $browser->Title("$row->entitytypename Transactions for $row->entityname ($et$ec)");
  }
}
if ( isset($ac) ) {
  $qry = new PgQuery( "SELECT accountgroupcode, name FROM chartofaccount WHERE accountcode = ?;", $ac );
  if ( $qry->Exec("Browse::Transaction") && $row = $qry->Fetch() ) {
    $browser->SetSubTitle( sprintf("%s - %s (%s)", $ac, $row->name, $row->accountgroupcode) );
  }
}
if ( isset($et) )
  $browser->AndWhere( "entitytype = '$et'" );
else
  $browser->AddColumn( 'entitytype', 'ET', 'center' );

if ( isset($ec) )
  $browser->AndWhere( "entitycode = $ec" );
else
  $browser->AddColumn( 'entitycode', 'Code', 'right' );

if ( isset($ac) )
  $browser->AndWhere( "accountcode = $ac" );
else
  $browser->AddColumn( 'accountcode', 'Account', 'right', '<label title="##accountname##">%7.2lf</label>' );

$browser->AddHidden( 'document', "batchcode || '.' || documentcode" );
$browser->AddHidden( 'accountname', "chartofaccount.name" );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'reference', 'Reference', 'left', '<td class="left" style="width:8em;">%s</td>', "COALESCE(accttran.reference, document.reference)" );
$browser->AddColumn( 'description', 'Description', 'left', '<td class="left" style="width:30em;">%s</td>', "COALESCE(accttran.description, document.description)" );
$browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$browser->AddTotal( 'amount' );
$browser->SetJoins( "accttran JOIN batch USING (batchcode) JOIN document USING (batchcode, documentcode) JOIN month USING (monthcode) JOIN chartofaccount USING(accountcode)" );

if ( $widget->Record->{'year'} > 0 ) $browser->AndWhere( "financialyearcode = ".qpg($widget->Record->{'year'}) );
if ( isset($month) ) $browser->AndWhere( sprintf("(month.startdate <= %s AND month.enddate >= %s) ", qpg($month), qpg($month)) );

include_once("menus_entityaccount.php");

$browser->AddOrder( 'date', 'ASC' );
$rowurl = '/view.php?t=document&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'document', '#even' );
$browser->DoQuery();

$c->page_title = $browser->Title();
$page_elements[] = $browser;
