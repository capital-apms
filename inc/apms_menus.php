<?php
require_once("MenuSet.php");

$main_menu = new MenuSet('menu', 'menu', 'menu_active');

$browse_menu = new MenuSet('submenu', 'submenu', 'submenu_active');
$browse_menu->AddOption("Debtors","/browse.php?t=debtors","Browse all tenants", false, 4100);
$browse_menu->AddOption("Creditors","/browse.php?t=creditors","Browse all creditors", false, 4200);
$browse_menu->AddOption("Companies","/browse.php?t=companies","Browse all companies", false, 4300);
$browse_menu->AddOption("Properties","/browse.php?t=properties","Browse all properties", false, 4400);
$browse_menu->AddOption("Account Groups","/browse.php?t=accountgroups","Browse all account groups", false, 5000);
$browse_menu->AddOption("Chart of Accounts","/browse.php?t=chartofaccounts","Browse and modify chart of accounts records");

$accounts_menu = new MenuSet('submenu', 'submenu', 'submenu_active');
$accounts_menu->AddOption("Browse Invoices","/browse.php?t=invoices","Browse all debtor invoices");
$accounts_menu->AddOption("Browse Vouchers","/browse.php?t=vouchers","Browse all creditor vouchers");
$accounts_menu->AddOption("Browse Cheques","/browse.php?t=cheques","Browse all cheque payments");
$accounts_menu->AddOption("New Batch","/edit.php?t=newbatch","Create a new batch");
$accounts_menu->AddOption("Unposted Batches","/browse.php?t=newbatch","Browse all unposted batches");
$accounts_menu->AddOption("Posted Batches","/browse.php?t=batch","Browse all posted batches");
$accounts_menu->AddOption("Approve Invoices","/action.php?t=approve_invoices","Approve unapproved invoices");
$accounts_menu->AddOption("Approve Vouchers","/action.php?t=approve_vouchers","Approve unapproved vouchers");
$accounts_menu->AddOption("Payment Run","/action.php?t=payment_run","Pay approved vouchers");
$accounts_menu->AddOption("Import Bank File","/action.php?t=bank-import","Import transaction file from bank");
$accounts_menu->AddOption("Period GST","/action.php?t=period_gst","Create a batch of transactions for GST for a period");

$reports_menu = new MenuSet('submenu', 'submenu', 'submenu_active');
$reports_menu->AddOption("Browse Users","/browse.php?t=users","Browse all users");

$admin_menu = new MenuSet('submenu', 'submenu', 'submenu_active');
$admin_menu->AddOption("Browse Users","/browse.php?t=users","Browse all users");
$admin_menu->AddOption("Office Settings","/browse.php?t=office_settings","Browse and modify office settings");
$admin_menu->AddOption("Bank Accounts","/browse.php?t=bankaccounts","Browse and modify bank account details");

$home_menu = new MenuSet('submenu', 'submenu', 'submenu_active');
$home_menu->AddOption("My User Details","/view.php?t=user&id=$session->user_no","View your user details", false, 1100);
$home_menu->AddOption("Edit My Details","/edit.php?t=user&id=$session->user_no","Edit your user details", false, 1101);
$home_menu->AddOption("Logout","/?logout=1","Logout of APMS", false, 9999);

$related_menu = new MenuSet('submenu', 'submenu', 'submenu_active');

$main_menu->AddSubMenu($home_menu, "Home", "/", "Go back to the home page", false, 1000);
$main_menu->AddSubMenu($browse_menu, "Browse", "/", "Browse Entities", false, 4000);
$main_menu->AddSubMenu($accounts_menu, "Accounts", "/", "Accounting Functions Menu", false, 5000);
$main_menu->AddSubMenu($reports_menu, "Reports", "/", "Reports Menu", false, 6000);
$main_menu->AddSubMenu($admin_menu, "Admin", "/", "Administrative Menu", false, 7000);
$main_menu->AddSubMenu($related_menu, "Related", $_SERVER['REQUEST_URI'], "Related Items", false, 8000);

