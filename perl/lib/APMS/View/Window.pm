package APMS::View::Window;

=head1 APMS::View::Window

Defines a base class for windows for the views. These are methods that are generic
to views, so should contain no application logic, only view logic.

=cut

use strict;

=item new_window

Simply ensures that the window is always named correctly inside the view object.

=cut

sub new_window {
	my ( $self, $main ) = @_;
	$self->{window} = Gtk2::Window->new( 'toplevel' );
}

=item get_window

View classes call this method on their own object in order to perform window
operations, eg:

  $self->get_window->set_size( X, Y );

=cut

sub get_window {
	my $self = shift;
	return $self->{window};
}

=item append_child

Create a unique ID for each child window based on the class and a random code. If
the ID already exists then generate a new ID. In this way, when a window is
destroyed, its children can be destroyed without having to use the Gtk2 concept
of parent and child windows (which does not fit the APMS paradigm).

=cut

sub append_child {
	my ( $self, $view ) = @_;

	# Create a string unique to this window
	my $class = ref( $view );
	my $ref;

	# Keep generating ID's until a unique one is found
	do {
		$ref = $self->generate_id( $class );
		print "Generated $ref\n";
	} until ( ! exists $self->{children}->{$ref} );

	# Add as a child of the parent window
	$self->{children}->{$ref} = $view;

	# Tell the child what its ref is
	$view->{id} = $ref;

	# Tell the child who the parent is
	$view->{parent} = $self;

	return 1;
}

sub generate_id {
	my ( $self, $class ) = @_;
	
	# Create a string to identify the window
	$class = lc( $class );
	$class =~ s/::/_/g;
	my $idx = int( rand( 10000 ) );
	$class .= "-$idx";

	return $class;
}

sub has_child {
	my ( $self, $class ) = @_;

	$class = lc( $class );
	$class =~ s/::/_/g;

	# Return 1 if this window has already been spawned
	foreach my $key ( keys %{ $self->{children} } ) {
		return 1 if $key =~ /$class\-\d+/;
	}

	return 0;
}

=item tell_parent_im_dead

Tell the parent window that the child has been destroyed to remove it from the list
of child windows the parent maintains.

=cut

sub tell_parent_im_dead {
	my $self = shift;

	my $myid = $self->{id};
	unless ( exists $self->{parent}->{children}->{$myid} ) {
		print "My parent didn\'t know about me..\n";
	}
	else {
		# Delete the reference to the window
		undef $self->{parent}->{children}->{$myid};
		delete $self->{parent}->{children}->{$myid};
	}
}

=item dump_relations

Debug function to dump a parent-child tree to stdout.

=cut

sub dump_relations {
	my ( $self, $indent ) = @_;

	if ( $self->{id} ) {
		print $indent . $self->{id} . "\n";
	}
	else {
		print $indent . "(no id)\n";
	}

	foreach my $child ( $self->children ) {
		$child->dump_relations( $indent . "  " );
	}
}

=item children

Accessor method for returning a list of the child windows

=cut

sub children {
	my $self = shift;

	my @childs;
	foreach my $key ( keys %{ $self->{children} } ) {
		push @childs, $self->{children}->{$key};
	}

	my $count = scalar @childs;
	return @childs;
}

=item attach_destroy

This method attaches a controller callback to a windows delete_event signal. This
allows the controllers to do the management of window destroying, as they may need
access to the model when being destroyed.

=cut

sub attach_destroy {
	my ( $self, $callback ) = @_;

	return unless ref( $callback ) =~ /CODE/;

	# Put a reference to the callback somewhere it can be called without having to
	# click the little X in the corner.
	$self->{delete_event} = sub { $callback->( $self ) };

	# Attach that callback to the window
	$self->{window}->signal_connect( delete_event => $self->{delete_event} );
}

=item destroy_callback

Return the callback attached to the delete_event signal of the window. This allows
the controller to call the delete event without the user pressing the close window
X on the window.

=cut

sub destroy_callback {
	my $self = shift;
	return $self->{delete_event};
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

