<?php
include_once("classViewer.php");
include_once("classEditor.php");

param_to_global('id', 'int', 'batchcode');

// Viewer component for posted batches
$viewer = new Viewer("Posted Batch", 'batch');
$viewer->AddField( 'updatedat', "updatedat::timestamp(0)" );

$viewer->SetWhere( 'batchcode='.$id );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Batch:</th>
  <td class="left">##batchcode.value##</td>
 </tr>
 <tr>
  <th class="right">Documents:</th>
  <td class="left">##documentcount.value##</td>
 </tr>
 <tr>
  <th class="right">Updated:</th>
  <td class="left">##updatedat.value## by ##updatedby.value## </td>
 </tr>
 <tr>
  <th class="right">Description:</th>
  <td class="left">##description.value##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Posted Batch: $id - ".$viewer->Record->{'description'});
$documentcount = $viewer->Record->{'documentcount'};

$page_elements[] = $viewer;

// Now list income accounts
require_once('classBrowser.php');

$entity_totals = array();
// Define a function to decide whether this amount should be added to the total
function check_row_is_ledger_transaction( $r, $v ) {
  global $entity_totals;
  if ( isset($entity_totals[$r->entitytype]) )
    $entity_totals[$r->entitytype] += $v;
  else
    $entity_totals[$r->entitytype] = $v;

  if ( $r->entitytype == 'L' ) return $v;
  return 0;
}

/**
* After all the rendering is done, this will be called...
*/
function post_render_function() {
  global $entity_totals;
  foreach( $entity_totals AS $et => $total ) {
    printf( "<p> Total for %s = %.2lf</p>\n", $et, $total );
  }
}


$template = new Browser("Document");
$template->AddColumn( 'transactioncode', '#', 'right' );
$template->AddHidden( 'account', "entitytype || '-' || TO_CHAR(entitycode,'FM00009') || '-' || TO_CHAR(accountcode,'FM0009.00')" );
// $template->AddHidden( 'color', "CASE WHEN entitytype = 'L' THEN '#ffe0f0' WHEN entitytype = 'T' THEN '#fff0b0' " );
$template->AddColumn( 'entitytype', 'ET', 'center' );
$template->AddColumn( 'entitycode', 'Code', 'right' );
$template->AddColumn( 'accountcode', 'Account', 'right', '', "TO_CHAR(accountcode,'FM0009.00')" );
$template->AddColumn( 'date', 'Date', 'center', '', "TO_CHAR(date,'DD/MM/YYYY')" );
$template->AddColumn( 'reference', 'Reference', 'left', '<td class="left" style="width:8em;">%s</td>', "COALESCE(accttran.reference, document.reference)" );
$template->AddColumn( 'description', 'Description', 'left', '<td class="left" style="width:30em;">%s</td>', "COALESCE(accttran.description, document.description)" );
$template->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$template->AddTotal( 'amount', 'check_row_is_ledger_transaction' );
$template->SetJoins( "document LEFT JOIN accttran USING ( batchcode, documentcode ) " );
$template->SetWhere( "document.batchcode = $id" );
$rowurl = '/view.php?t=account&id=%s';
$template->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'account', '#even' );

$qry = new PgQuery( "SELECT * FROM document WHERE batchcode = ? order by batchcode, documentcode;", $id );
if ( $qry->Exec("Browse::Documents") ) {
  while( $doc = $qry->Fetch() ) {
    // Now, based on the template lets add that for each document...
    $browser = clone($template);
    $browser->Title("Document $doc->documentcode: $doc->description");
    $browser->AndWhere("document.documentcode = $doc->documentcode" );
    $browser->SetOrdering( 'transactioncode', 'ASC', $doc->documentcode );
    $page_elements[] = $browser;
  }
}

