<?php
/**
* Links
* @package   apms
* @subpackage   classLinks
* @author    Andrew McMillan <andrew@catalyst.net.nz>
* @copyright Catalyst IT Ltd
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2
*/
class Link
{
  var $Title;

  function Link( $title = "link" ) {
    $this->Title = $title;
  }

  function Render() {
  $html = '<div id="links">';
  $html .= '<p class="link">'.$this->Title.'</p>';
  $html .= '</div>';
  return $html;
  }

}
?>