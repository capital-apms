<?php
/**
* Component for browsing and modifying office settings
* @package   apms
* @subpackage   office_settings
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/
include_once("classWidgets.php");

param_to_global('officecode','#^[A-Z0-9]{2,4}$#i');  if ( isset($officecode) ) $officecode = strtoupper($officecode);
param_to_global('action','#^(edit|delete)$#i');
param_to_global('setname','#^[A-Z0-9_+-]+$#i');

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'officecode', 'char1', "SELECT officecode, officecode || ' - ' || name FROM office ORDER BY 1;" );
$widget->ReadWrite();
$widget->Layout( '<table> <tr> <th>Office:</th> <td>##officecode.select##</td> <td>##Show.submit##</td> </tr> </table>' );
$officecode = (isset($widget->Record->{'officecode'}) ? $widget->Record->{'officecode'} : null);
$page_elements[] = $widget;


$editrow = new Editor("OfficeSetting", 'officesettings');

if ( $editrow->IsSubmit() ) {
  $editrow->SetWhere( "officecode=".qpg($officecode)." AND setname=" . qpg($_POST['orig_setname']) );
  $editrow->Write();
  unset($action);
}
if ( isset($action) && $action == 'delete' ) {
  $qry = new PgQuery( "DELETE FROM officesettings WHERE officecode=? AND setname=?", $officecode, $setname);
  if ( $qry->Exec() ) {
    $c->messages[] = translate("Office Setting deleted: " . $setname );
  }
}

function edit_row( $row_data ) {
  global $editrow, $officecode;

  $rowsubmittype = ($row_data->setname == '' ? 'Add' : 'Save');
  $form_target = preg_replace( '#&action=edit#', '', $_SERVER['REQUEST_URI'] );

  $template = <<<EOTEMPLATE
##form##
  <td class="left"><input type="hidden" name="officecode" value="$officecode"><input type="hidden" name="orig_setname" value="##setname.enc##">##setname.input.25##</td>
  <td class="left">##setvalue.textarea.40x3##</td>
  <td class="center">##$rowsubmittype.submit##</td>
</form>

EOTEMPLATE;

  $editrow->SetTemplate( $template );
  if ( $row_data->setname == '' )
    $editrow->Initialise( (array) $row_data );
  else
    $editrow->SetRecord( $row_data );

  $editrow->Title("");
  return $editrow->Render();
}


/**
* Now build the browser
*/
$browser = new Browser("Office Settings");
$browser->AddHidden( 'officecode' );
$browser->AddColumn( 'setname', 'Setting' );
$browser->AddColumn( 'setvalue', 'Value' );
$edit_link = "<a href=\"/browse.php?t=$component&officecode=##officecode##&setname=##setname##&action=edit\" class=\"submit\">Edit</a>";
$del_link  = "<a href=\"/browse.php?t=$component&officecode=##officecode##&setname=##setname##&action=delete\" class=\"submit\">Del</a>";
$browser->AddColumn( 'action', 'Action', 'center', '', "'$edit_link &nbsp; $del_link'" );
$browser->SetJoins( "officesettings" );
$browser->AddOrder( "setname", "A" );
$browser->SetWhere( "officecode=".qpg($officecode) );
$browser->RowFormat( "<tr class=\"r%d\">\n", "</tr>\n", '#even' );

if ( isset($action) && $action == 'edit' ) {
  $browser->MatchedRow('setname', $setname, 'edit_row');
}
else {
  $browser->MatchedRow('setname', '', 'edit_row');
  $extra_row = (object) array( 'officecode' => $officecode, 'setname' => '' );
  $browser->AddRow($extra_row);
}

$browser->DoQuery();
$c->page_title = "Office Settings";

$page_elements[] = $browser;

