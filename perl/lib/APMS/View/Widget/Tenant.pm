package APMS::View::Widget::Tenant;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Widget::Button;

sub tenant_select {
	my ( $proto ) = @_;
	my $self = new_object( $proto );

	$self->{tenantbox} = Gtk2::HBox->new( 0, 5 );

	$self->{tenantcode} = Gtk2::Entry->new();
	$self->{tenantcode}->set_size_request( 50, 25 );
	$self->{tenantbox}->pack_start( $self->{tenantcode}, 0, 1, 0 );

	$self->{choosetenant} = APMS::View::Widget::Button->jump_to_button();
	$self->{tenantbox}->pack_start( $self->{choosetenant}, 0, 1, 0 );

	$self->{tenantname} = Gtk2::Label->new( '(select tenant)' );
	$self->{tenantbox}->pack_start( $self->{tenantname}, 0, 1, 0 );

	return $self;
}

sub box {
	my $self = shift;
	return $self->{tenantbox};
}

sub set_tenant_details {
	my ( $self, $code, $name ) = @_;

	$self->{tenantcode}->set_text( $code );
	$self->{tenantname}->set_text( $name );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

