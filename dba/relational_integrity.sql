-- Apply relational integrity constraints to the database
--

-- First remove any existing ones...
ALTER TABLE invoiceline  DROP CONSTRAINT "invoiceline_invoiceno_fkey";
ALTER TABLE voucherline  DROP CONSTRAINT "voucherline_voucherseq_fkey";
ALTER TABLE newdocument  DROP CONSTRAINT "newdocument_newbatch_fkey";
ALTER TABLE newaccttrans DROP CONSTRAINT "newaccttrans_newdocument_fkey";
ALTER TABLE document     DROP CONSTRAINT "document_batch_fkey";
ALTER TABLE accttran     DROP CONSTRAINT "accttran_document_fkey";

ALTER TABLE postaldetail DROP CONSTRAINT "postaldetail_person_fkey";
ALTER TABLE phonedetail  DROP CONSTRAINT "phonedetail_person_fkey";
ALTER TABLE contact      DROP CONSTRAINT "contact_person_fkey";
ALTER TABLE creditor     DROP CONSTRAINT "creditor_person_fkey";
ALTER TABLE tenant       DROP CONSTRAINT "tenant_person_fkey";


-- Now add new ones.
ALTER TABLE invoiceline  ADD CONSTRAINT "invoiceline_invoiceno_fkey" FOREIGN KEY (invoiceno) REFERENCES invoice(invoiceno)    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;
ALTER TABLE voucherline  ADD CONSTRAINT "voucherline_voucherseq_fkey" FOREIGN KEY (voucherseq) REFERENCES voucher(voucherseq) ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;
ALTER TABLE newdocument  ADD CONSTRAINT "newdocument_newbatch_fkey" FOREIGN KEY (batchcode) REFERENCES newbatch(batchcode)    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;

ALTER TABLE newaccttrans ADD CONSTRAINT "newaccttrans_newdocument_fkey" FOREIGN KEY (batchcode,documentcode) REFERENCES newdocument(batchcode,documentcode)     ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;

ALTER TABLE document     ADD CONSTRAINT "document_batch_fkey" FOREIGN KEY (batchcode) REFERENCES batch(batchcode)    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;

ALTER TABLE accttran     ADD CONSTRAINT "accttran_document_fkey" FOREIGN KEY (batchcode,documentcode) REFERENCES document(batchcode,documentcode)    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;

ALTER TABLE postaldetail ADD CONSTRAINT "postaldetail_person_fkey" FOREIGN KEY (personcode) REFERENCES person(personcode)     ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;
ALTER TABLE phonedetail  ADD CONSTRAINT "phonedetail_person_fkey"  FOREIGN KEY (personcode) REFERENCES person(personcode)     ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;
ALTER TABLE contact      ADD CONSTRAINT "contact_person_fkey"      FOREIGN KEY (personcode) REFERENCES person(personcode)     ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE;
ALTER TABLE creditor     ADD CONSTRAINT "creditor_person_fkey"     FOREIGN KEY (paymentcontact) REFERENCES person(personcode) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE tenant       ADD CONSTRAINT "tenant_person_fkey"       FOREIGN KEY (billingcontact) REFERENCES person(personcode) ON UPDATE CASCADE ON DELETE RESTRICT;
