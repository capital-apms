<?php

$c->scripts[] = "js/jquery.js";

param_to_global('creditorcode', '#[0-9]+#');
param_to_global('orig_voucherseq', '#[0-9]+#');
if ( isset($orig_voucherseq) ) $id = $orig_voucherseq;
if ( $id == 0 ) {
  param_to_global('id', '#^(VCHR[ ,])?\d+$#i', 'voucherseq');
  if ( isset($id) && strtoupper(substr($id,0,4)) == 'VCHR' ) {
    $id = intval(substr($_REQUEST['id'],4));
  }
  else {
    param_to_global('copy', '#[0-9]+#');
    if ( isset($copy) ) {
      $q = new AwlQuery("SELECT nextval('voucher_voucherseq_seq')");
      if ( $q->Exec() && $row = $q->Fetch(true) ) {
        $id = $row[0];
        $q->Begin();
        $q->QDo("INSERT INTO voucher
         (voucherseq, voucherstatus, creditorcode, date, invoicereference, ourorderno, capexcode, approvercode, goodsvalue, taxvalue, description, bankaccountcode, datedue, entitytype, entitycode, accountcode, secondapprover, ordercode, projectcode, invoiceperiod, paymentstyle )
          SELECT $id, 'U', creditorcode, date, invoicereference, ourorderno, capexcode, approvercode, goodsvalue, taxvalue, description, bankaccountcode, datedue, entitytype, entitycode, accountcode, secondapprover, ordercode, projectcode, invoiceperiod, paymentstyle
           FROM voucher WHERE voucherseq = ?", $copy);      
        $q->QDo("INSERT INTO voucherline 
        (voucherseq, lineseq, entitytype, entitycode, accountcode, description, taxamount, amount )
         SELECT $id, lineseq, entitytype, entitycode, accountcode, description, taxamount, amount
          FROM voucherline WHERE voucherseq = ?", $copy);
        $q->Commit();
        $orig_voucherseq = $id;
      }
    }
  }
}

// Editor component for voucher
$editor = new Editor("Voucher", 'voucher');
$editor->AddField( 'orig_voucherseq', 'voucher.voucherseq' );
$editor->AddField( 'etname', 'get_entity_type_name(entitytype)' );
$editor->AddField( 'loweretname', 'lower(get_entity_type_name(entitytype))' );
$editor->AddField( 'entityname', 'get_entity_name(entitytype,entitycode)' );
$editor->AddField( 'creditorname', "get_entity_name('C',creditorcode)" );
$editor->AddField( 'voucher_total', '(goodsvalue + taxvalue)' );
$editor->SetLookup( 'paymentstyle', 'SELECT entitytype, description FROM entitytype');
$editor->SetLookup( 'entitytype', 'SELECT entitytype, description FROM entitytype');
$editor->SetLookup( 'approvercode', "SELECT '', '---' UNION SELECT approvercode, approvercode || ' - ' || firstname || ' ' || lastname FROM approver LEFT OUTER JOIN person USING (personcode) WHERE active ORDER BY 1;");
$editor->SetLookup( 'secondapprover', "SELECT '', '---' UNION SELECT approvercode, approvercode || ' - ' || firstname || ' ' || lastname FROM approver LEFT OUTER JOIN person USING (personcode) WHERE active ORDER BY 1;");
$editor->AddAttribute( 'goodsvalue', 'onBlur', "return calculate_tax_total(document.forms[0].goodsvalue, document.forms[0].taxvalue, document.forms[0].voucher_total, true);" );
$editor->AddAttribute( 'goodsvalue', 'onKeydown', "return EnterToNextField(event,document.forms[0].taxvalue);" );
$editor->AddAttribute( 'taxvalue', 'onBlur', "return calculate_total(document.forms[0].taxvalue, document.forms[0].goodsvalue, document.forms[0].voucher_total, true);" );
$editor->AddAttribute( 'taxvalue', 'onKeydown', "return EnterToNextField(event,document.forms[0].voucher_total);" );
$editor->AddAttribute( 'voucher_total', 'onBlur', "return calculate_tax_split(document.forms[0].voucher_total, document.forms[0].goodsvalue, document.forms[0].taxvalue, true );" );
$editor->AddAttribute( 'voucher_total', 'onKeydown', "return EnterToNextField(event,document.forms[0].description);" );

$editor->AddAttribute( 'entitycode', 'onBlur', "return validate_positive_integer(document.forms[0].entitycode);" );

$editor->AddAttribute( 'description', 'title', "A short description of the goods or services supplied." );
$editor->AddAttribute( 'entitycode', 'title', "The entity accruing the expenses for these goods or services." );

$editor->SetWhere( "voucherstatus IN ( 'U', 'H' ) AND voucherseq=".(isset($id)?$id:'0'));

$editor->AddAttribute( 'creditorcode', 'id', 'creditorcode' );

// Editor component for voucher line
$editrow = new Editor("VoucherLine", 'voucherline');
$editrow->AddField( 'line_voucherseq', 'voucherline.voucherseq' );
$editrow->AddField( 'etname', 'get_entity_type_name(entitytype)' );
$editrow->AddField( 'my_lineseq', 'lineseq' );
$editrow->AddField( 'accountcode', 'accountcode', "SELECT accountcode, to_char(accountcode,'0009.99 - ') || c.name FROM chartofaccount c JOIN accountgroup g USING (accountgroupcode) WHERE accountgroupcode IN ('ENTERT', 'DISBUR', 'CONSUM', 'CLRNG', 'COMMS', 'DIRCTR', 'DIRTAX', 'FEES', 'FIXASS', 'MARKET', 'OTHEXP', 'SALARY', 'TRAVEL', 'CLRNG' ) ORDER BY accountcode" );
$editrow->AddAttribute( 'accountcode', 'style', "width:13em;" );
$editrow->SetSubmitName( 'saverow' );

if ( $editor->IsSubmit() ) {
  if ( !isset($_POST['voucherstatus']) || strstr( 'UH', $_POST['voucherstatus'] ) === false ) $_POST['voucherstatus'] = 'U';
  $editor->WhereNewRecord( " voucherseq=currval('voucher_voucherseq_seq') " );
  $editor->Write();
}
else {
  $editor->GetRecord();
}
if ( $editrow->IsSubmit() ) {
  $_POST['voucherseq'] = $id;
  $is_update = ($_POST['saverow'] == 'Save');
  if ( ! $is_update ) {
    $sql = "SELECT lineseq FROM voucherline WHERE voucherseq = ? ORDER BY lineseq DESC LIMIT 1";
    $qry = new PgQuery($sql, $id);
    if ( $qry->Exec("edit/voucher") &&  $qry->rows == 1 ) {
      $row = $qry->Fetch();
      $lineseq = $row->lineseq + 1;
    }
    else {
      $lineseq = 1;
    }
    $_POST['lineseq'] = $lineseq;
  }
  else {
    $lineseq = $_POST['lineseq'];
  }
  if ( $_POST['inctax'] != 0 ) {
    $_POST['amount'] = round( $_POST['inctax'] / (1 + $office_settings->{'GST-Rate'}), 2);
  }
  $editrow->SetWhere( "voucherseq=$id AND lineseq=$lineseq");
  $editrow->WhereNewRecord( "voucherseq=$id AND lineseq=$lineseq" );
  $editrow->Write( $is_update );
  unset($_GET['editseq']);
}

$id = (isset($editor->Record->{'orig_voucherseq'}) ? $editor->Record->{'orig_voucherseq'} : '');
$submittype = 'Apply Changes';
if ( ! $editor->Available() ) {
  $submittype = 'Create';
  $editor->Initialise( array('entitytype' => 'L', 'voucherdate' => 'today', 'voucherstatus' => 'U', 'description' => '') );
  if ( isset($creditorcode) ) {
    $qry = new PgQuery('SELECT * FROM creditor WHERE creditorcode = ?', $creditorcode );
    if ( $qry->Exec('edit/voucher') && $qry->rows == 1 ) {
      $creditor = $qry->Fetch();
      $editor->Initialise( array('creditorcode' => $creditorcode, 'entitytype' => $creditor->vchrentitytype, 'entitycode' => $creditor->vchrentitycode, 'accountcode' => $creditor->vchraccountcode ) );
    }
  }
}


$template = <<<EOTEMPLATE
<script type="text/javascript">
function do_lookup(type,code,callback) {
  $.getJSON("lookup.php",{request:type,code:code},callback);
}

function show_creditor_name() {
  do_lookup( "creditor", $("#creditorcode").val(), function(result){
    $("#creditorname").html("<a href=\"view.php?t=creditor&id="+$("#creditorcode").val()+"\">"+result.name+"</a>");    
  });
}

$(document).ready(function(){
  show_creditor_name();

  $("#creditorcode").change(function(e){
    show_creditor_name();
  });

  $("#creditorcode").click(function(){
  	$("#creditorcode").select();
  });

  $("input").on("focusin",function(e){e.target.select();});
  $("input").on("keypress",nextFieldOnEnter);
});
</script>
<form method="POST" enctype="multipart/form-data" id="voucher">
<table>
 <tr>
  <th class="right">Voucher:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left" width="10%">##orig_voucherseq.value####orig_voucherseq.hidden##</td>
     <th class="right">Coding:</th>
     <td class="left">##entitytype.select##</td>
     <td class="left">##entitycode.input.4##</td>
     <td class="left">##accountcode.input.5##</td>
     <th class="right">APMS Order:</th>
     <td class="left">##ordercode.input.4##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Creditor:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left" style="width:25em">##creditorcode.input.5## <span id="creditorname" style="width:40em">##creditorname.value##</span></td>
     <th class="right">Invoice Date:</th>
     <td class="left">##date.date##</td>
     <th class="right">Due:</th>
     <td class="left">##datedue.date##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Approvers:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left">##approvercode.select##</td>
     <td class="left">##secondapprover.select##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Invoice No:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left" style="width:6em;">##invoicereference.input.12##</td>
     <th class="right" style="width:6em;">Order ref:</th>
     <td class="left">##ourorderno.input.12##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Value:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left" style="width:6em;">##goodsvalue.money.12##</td>
     <th class="right" style="width:6em;">+ Tax:</th>
     <td class="left">##taxvalue.money.12##</td>
     <th class="right">Total:</th>
     <td class="left">##voucher_total.money.12##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Description:</th>
  <td class="left">##description.input.50##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left" colspan="2">##submit##</td>
 </tr>
</table>
</form>

EOTEMPLATE;

$editor->SetTemplate( $template );
$c->page_title = $editor->Title("Voucher $id / ".$editor->Record->{'description'});
$page_elements[] = $editor;

$related_menu->AddOption("View Voucher","/view.php?t=voucher&id=$id","View this voucher");
$related_menu->AddOption("Print Voucher","/print.php?t=voucher&id=$id","Print this voucher");

$et = 'C';
$ec = $editor->Value('creditorcode');
include_once("menus_entityaccount.php");


if ( $submittype != 'Create' ) {

  function edit_row( $row_data ) {
    global $editrow, $id;

    $rowsubmittype = ($row_data->lineseq == -1 ? 'Add' : 'Save');
    $form_url = preg_replace( '#&(edit|del)seq=\d+#', '', $_SERVER['REQUEST_URI'] );

    $template = <<<EOTEMPLATE
<form method="POST" enctype="multipart/form-data" id="voucherline" action="$form_url">
  <td class="left">##lineseq.value##<input type="hidden" name="id" value="$id">##lineseq.hidden##</td>
  <td class="left" style="white-space:nowrap">##entitytype.input.1####entitycode.input.3####accountcode.select##</td>
  <td class="left">##description.input.40##</td>
  <td class="left">##amount.money.9##</td>
  <td class="left">##taxamount.money.9##</td>
  <td class="left">##inctax.money.9##</td>
  <td class="left">##$rowsubmittype.submit##</td>
</form>

EOTEMPLATE;

    $editrow->SetTemplate( $template );
    $editrow->SetRecord( $row_data );
    $editrow->Title("");
    return $editrow->Render();
  }


  // And the voucher line details
  require_once('classBrowser.php');
  $browser = new Browser("Voucher Detail");
  $browser->AddHidden( 'etname', 'lower(get_entity_type_name(entitytype))' );
  $browser->AddColumn( 'lineseq', '#', 'right' );
  $browser->AddHidden( 'entitytype' );
  $browser->AddHidden( 'entitycode' );
  $browser->AddHidden( 'accountcode' );
  $browser->AddHidden( 'account', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')" );
  $browser->AddColumn( 'accountname', 'Account', 'left', '', 'chartofaccount.name');
  $browser->AddColumn( 'description', 'Description', 'left' );
  $browser->AddColumn( 'amount', 'Ex Tax', 'right', '%0.2lf' );
  $browser->AddColumn( 'taxamount', 'GST', 'right', '%0.2lf' );
  $browser->AddTotal( 'amount' );
  $browser->AddColumn( 'inctax', 'Inc Tax', 'right', '%0.2lf', '0' );
  $edit_link = "<a href=\"/edit.php?t=voucher&id=$id&editseq=##lineseq##\" class=\"submit\">Edit</a>";
  $del_link  = "<a href=\"/edit.php?t=voucher&id=$id&delseq=##lineseq##\" class=\"submit\">Del</a>";
  $browser->AddColumn( 'action', 'Action', 'center', '', "'$edit_link&nbsp;$del_link'" );
  $browser->AddOrder( 'lineseq', 'ASC' );
  $browser->SetJoins( "voucherline LEFT OUTER JOIN chartofaccount USING ( accountcode )" );

  $browser->AndWhere( "voucherseq=$id" );
  $rowurl = '/view.php?t=account&id=%s';
  $browser->RowFormat( "<tr class=\"r%d\">\n", "</tr>\n", 'account', 'account', 'accountname', '#even' );
  $page_elements[] = $browser;

  if ( isset($_GET['delseq']) ) {
    $qry = new PgQuery("DELETE FROM voucherline WHERE voucherseq=? AND lineseq = ?", $id, intval($_GET['delseq']) );
    $qry->Exec('voucher');
  }
  if ( isset($_GET['editseq']) ) {
    $browser->MatchedRow('lineseq', intval($_GET['editseq']), 'edit_row');
  }
  else {
    $remaining = $editor->Record->{'goodsvalue'};
    $extra_row = array( 'lineseq' => -1, 'amount' => $remaining, 'inctax' => 0, 'entitytype' => $editor->Record->{'entitytype'},
                        'entitycode' => $editor->Record->{'entitycode'}, 'accountcode' => $editor->Record->{'accountcode'},
                        'description' => $editor->Record->{'description'}  );
    $qry = new PgQuery("SELECT entitytype, entitycode, accountcode, description FROM voucherline WHERE voucherseq=$id ORDER BY lineseq DESC LIMIT 1");
    if ( $qry->Exec('voucher') && $qry->rows == 1 ) {
      $row = $qry->Fetch();
      $extra_row['entitytype']  = $row->entitytype;
      $extra_row['entitycode']  = $row->entitycode;
      $extra_row['accountcode'] = $row->accountcode;
      $extra_row['description'] = $row->description;
      $qry = new PgQuery("SELECT SUM(amount) FROM voucherline WHERE voucherseq=$id");
      if ( $qry->Exec('voucher') && $qry->rows == 1 ) {
        $row = $qry->Fetch();
        $extra_row['amount']    = $remaining - $row->sum;
      }
    }
    $browser->MatchedRow('lineseq', -1, 'edit_row');
    $extra_row = (object) $extra_row;
    $browser->AddRow($extra_row);
  }

}
