<?php

require_once('classWidgets.php');

param_to_global( 'id', '#^[ACTLPJ]-\d+-\d+(\.\d+)?$#i' );
if ( isset($id) ) {
  list( $et, $ec, $ac ) = explode('-',$id,3);
  $et = strtoupper(substr($et,0,1));
  $ec = intval($ec);
  $ac = floatval($ac);
}
param_to_global('accountgroup', '#^[a-z0-9_-]+$#i'); if (isset($accountgroup) ) $accountgroup = strtoupper($accountgroup);

$show_balances = ( (isset($et) && isset($ec)) || isset($ac) );

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget('accounts');
$widget->AddField( 'updates', 'char1', "SELECT ' ', '--Show All--' UNION SELECT 'Z', '--No Manual Posting--' UNION SELECT entitytype, description FROM entitytype ORDER BY 1" );
$widget->ReadWrite();
$widget->Defaults( array('updates' => ' ' ) );
$widget->Layout( '<table> <tr> <th>Update to:</th> <td>##updates.select##</td> <td>##Show.submit##</td> </tr></table>' );

$allow_updates = ' ';
if ( isset($widget->Record->{'updates'}) && preg_match( '#([ACTLPJZ]+)#i', $widget->Record->{'updates'}, $matches) ) $allow_updates = strtoupper($matches[1]);

$page_elements[] = $widget;


$browser = new Browser("Accounts");
$c->page_title = "Browse Accounts";

if ( !isset($ac) ) {
  if ( !isset($accountgroup) ) $browser->AddColumn( 'accountgroupcode', 'Group', 'left' );
  $browser->AddColumn( 'accountcode', 'Account', 'right', '', "TO_CHAR(accountcode,'FM0009.00')" );
}

if ( ! $show_balances ) {
  $browser->AddColumn( 'shortname', 'ShortName' );
}

if ( $show_balances ) {
  $browser->AddHidden( 'account', "entitytype || '-' || TO_CHAR(entitycode,'FM00009') || '-' || TO_CHAR(accountcode,'FM0009.00')" );
  if ( !isset($ec) ) {
    $browser->AddColumn( 'entitycode', "Entity", 'left', '', "entitytype || entitycode" );
    $browser->AddColumn( 'entityname', "Entity Name", 'left', '', "get_entity_name(entitytype,entitycode)" );
  }
  else {
    $browser->AddColumn( 'name', 'Name', 'left', '<td class="left" style="width:30em;">%s</td>', 'chartofaccount.name' );
  }
  $browser->SetJoins( "accountsummary LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode )" );
  $browser->AddColumn( 'balance', 'Balance', 'right', '%0.2lf' );
  $browser->AddTotal( 'balance' );

  $rowurl = '/view.php?t=account&id=%s';
  $browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'account', '#even' );
}
else {
  $browser->AddColumn( 'name', 'Name', 'left', '<td class="left" style="width:30em;" title="##alternativecode##">%s</td>', 'chartofaccount.name' );
  $browser->AddHidden( 'alternativecode' );
  $browser->AddColumn( 'updateto', 'Updates' );
  $browser->AddColumn( 'highvolume', 'Volume', 'left', '', "CASE WHEN highvolume THEN 'Yes' ELSE 'No' END" );
  $browser->SetJoins( "chartofaccount LEFT JOIN accountgroup USING ( accountgroupcode )" );
  $rowurl = '/browse.php?t=accounts&ac=%s';
  $browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'accountcode', '#even' );
  $browser->SetWhere( "TRUE" );
}

$browser->SetOrdering( 'accountcode' );

if ( isset($et) )   $browser->AndWhere( "entitytype = '$et'" );
if ( isset($ec) )   $browser->AndWhere( "entitycode = $ec" );
if ( isset($ac) )   $browser->AndWhere( "accountcode = $ac" );
if ( isset($accountgroup) )   $browser->AndWhere( "accountgroupcode = ".qpg($accountgroup) );

if ( $allow_updates != ' ' ) {
  if ( $allow_updates != 'Z' )
    $browser->AndWhere( "updateto ~ '[$allow_updates]'" );
  else
    $browser->AndWhere( "updateto IS NULL OR updateto = ''" );
}

include_once("menus_entityaccount.php");

$rowurl = '/view.php?t=document&id=%s';
$browser->DoQuery();
$page_elements[] = $browser;
