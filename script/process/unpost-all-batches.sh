#!/bin/sh
#
# Unpost all APMS batches
#

for B in `psql -qAt -c "SELECT batchcode FROM batch;" apms`; do
  ./batch-unpost.pl --batch $B | psql -qAt apms;
  psql -qAt -c "vacuum batch; vacuum document; vacuum accttran; vacuum accountbalance; vacuum accountsummary;" apms
done
