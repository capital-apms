<?php
// This process builds a batch (which we will then post) which
// transfers the amounts in all income and expenditure accounts
// into the retained earnings account.
require_once('classViewer.php');

param_to_global( 'companycode', 'int' );
param_to_global( 'monthcode', 'int' );

get_office_accounts( 'YEAREND' );

$year_end_account = $office_accounts['YEAREND']->accountcode;

require_once( "transaction_helpers.php" );

$sql = <<<EOQ
SELECT
  chartofaccount.accountcode, chartofaccount.name AS accountname,
  (SELECT legalname FROM company WHERE companycode = $companycode) AS legalname,
  (SELECT shortname FROM company WHERE companycode = $companycode) AS shortname,
  (SELECT enddate FROM month WHERE monthcode = $monthcode) AS year_end_date
 FROM chartofaccount
WHERE accountcode = $year_end_account
;
EOQ;
$qry = new PgQuery( $sql );
if ( $qry->Exec() && $row = $qry->Fetch() ) {
  $year_end_account = $row->accountcode;
  $fmt_account = sprintf( "%7.2lf", $year_end_account );
  $account_name = $row->accountname;
  $company_name = $row->legalname;
  $shortname = $row->legalname;
  $year_end_date = $row->year_end_date;
  $batch_description = "Year End Transfers, $shortname (L$companycode) to $year_end_date";
}
else {
  $c->messages[] = "ERROR: Cannot get control parameters for run.";
  return true;
}

$batchcode = create_newbatch( 'AUTO', 0, $batch_description );

// The first part of the results is a viewer listing the run parameters
$runparms = new Viewer("Year End Processing");

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Year End Company:</th>
  <td class="right">$companycode</td>
  <td class="left">$company_name</td>
 </tr>
 <tr>
  <th class="right">Year End Month:</th>
  <td class="right">$monthcode</td>
  <td class="left">$year_end_date</td>
 </tr>
 <tr>
  <th class="right">Year End Account:</th>
  <td class="right">$fmt_account</td>
  <td class="left">$account_name</td>
 </tr>
 <tr>
  <th class="right">New Batch Code:</th>
  <td class="right">$batchcode</td>
  <td class="left">$batch_description</td>
 </tr>
</table>

EOTEMPLATE;

$runparms->SetTemplate( $template );
$c->page_title = $runparms->Title("Year End Processing");

$page_elements[] = $runparms;

$p_and_l_sql = <<<EOQ
SELECT entitytype, entitycode, accountbalance.accountcode,
       sequencecode, accountgroupcode, sum(balance::numeric) AS balance
  FROM accountbalance JOIN chartofaccount USING ( accountcode )
                      JOIN accountgroup USING ( accountgroupcode )
 WHERE entitytype = 'L' AND entitycode = ? AND monthcode <= ?
       AND upper(grouptype) = 'P'
 GROUP BY entitytype, entitycode, sequencecode, accountgroupcode, accountbalance.accountcode
 HAVING sum(balance::numeric) != 0::numeric
 ORDER BY entitytype, entitycode, sequencecode, accountgroupcode, accountbalance.accountcode
EOQ;


$qry = new PgQuery($p_and_l_sql, $companycode, $monthcode );
if ( !$qry->Exec("year_end") ) {
  $c->messages[] = "ERROR: Unable to read P&L accounts for L$companycode to $year_end_date";
  return 0;
}

create_newdocument( "Y/E Transfer to Retained Earnings", 'YEAR END', 'YEND' );

$total = 0;
while( $row = $qry->Fetch() ) {
  create_newtransaction( 'L', $companycode, $row->accountcode, $year_end_date, 0 - $row->balance, null, null, 0, $monthcode );
  $total += $row->balance;
}
create_newtransaction( 'L', $companycode, $year_end_account, $year_end_date, $total, "Y/E Accumulated P&L", null, 0, $monthcode );

$sql .= "UPDATE newbatch SET documentcount = (SELECT count(1) FROM newdocument d WHERE d.batchcode = ?), total = (SELECT sum(amount) FROM newaccttrans t WHERE t.batchcode = ?)  WHERE batchcode = ?;";
$qry = new PgQuery( $sql . 'COMMIT;', $batchcode, $batchcode, $batchcode, $batchcode );
if ( !$qry->Exec("year_end") ) {
  $c->messages[] = "ERROR: Unable to create new Y/E batch for L$companycode to $year_end_date";
  return 0;
}

$c->messages[] = "New year end batch created for review and updating.";

include_once("menus_entityaccount.php");

