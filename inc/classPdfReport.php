<?php
/**
* Class for producing a formatted PDF report
*
* @package   apms
* @subpackage   classPdfReport
* @author    Andrew McMillan <andrew@catalyst.net.nz>
* @copyright Catalyst IT Ltd
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2
*/

require_once("classViewer.php");
require_once("class.ezpdf.php");

/**
* A class for the fields in the editor
* @package   apms
*/
class PdfReport extends Viewer
{
  var $p;
  var $master;
  var $font;
  var $points;
  var $margins;
  var $mmy;
  var $numbered;
  var $stream_options;

  function PdfReport( $page_size = "a4", $orientation = "portrait" ) {
    $this->p = new Cezpdf( $page_size, $orientation );
    $this->master = false;
    $this->stream_options = array();

    $this->SetFont("Helvetica", 12);

    $this->p->fontFamilies['ZapfHumanist601BT-Roman.afm']=array(
        'b'=>'ZapfHumanist601BT-Bold.afm'
       ,'i'=>'ZapfHumanist601BT-Italic.afm'
      ,'bi'=>'ZapfHumanist601BT-BoldItalic.afm'
      ,'ib'=>'ZapfHumanist601BT-BoldItalic.afm'
    );

    $this->margins = array();
    $this->SetMargins(6);
    $this->numbered = false;
  }

  function Mm2Pt($mm) {
    return ($mm * 2.83464567);
  }

  function Pt2Mm($pt) {
    return ($pt / 2.83464567);
  }

  /**
  * Start writing to the master layer
  */
  function MasterLayerOn() {
    if ( $this->master === false ) {
      // Create a master layer that we may use
      $this->master = $this->p->openObject();
      $GLOBALS['session']->Dbg("PdfReport", "Starting new master layer");
    }
    else {
      $this->p->reopenObject($this->master);
      $GLOBALS['session']->Dbg("PdfReport", "Switching writes to existing master layer");
    }
  }

  /**
  * Stop writing to the master layer
  */
  function MasterLayerOff() {
    $this->p->closeObject();
    $this->p->addObject($this->master,'all');
    $GLOBALS['session']->Dbg("PdfReport", "Switching off writes to master layer");
  }

  /**
  * Clear the master layer
  */
  function MasterLayerClear() {
    $this->p->stopObject($this->master);
    $this->master = false;
  }

  function SetFont( $font, $points ) {
    $this->font = $font;
    $this->points = $points;
    $f = '../fonts/'.$font.'.afm';
    $this->p->selectFont( $f);
  }

  /**
  * Set the margins in millimetres, and in the same sort of
  * way they are set in HTML.
  */
  function SetMargins( $top, $left = -1, $bottom = -1, $right = -1 ) {
    $this->margins['top'] = $top;
    $left = ($left >= 0 ? $left : $top);
    $this->margins['left'] = $left;
    $this->margins['bottom'] = ($bottom >= 0 ? $bottom : $top);
    $this->margins['right'] = ($right >= 0 ? $right : $left);
//    $this->SetY($top);
    $this->p->ezSetMargins( $this->Mm2Pt($this->margins['top']), $this->Mm2Pt($this->margins['bottom']), $this->Mm2Pt($this->margins['left']), $this->Mm2Pt($this->margins['right']) );

    if ( $top >= $this->margins['bottom'] )
      $this->SetY( $this->Pt2Mm($this->p->ez['pageHeight']) - $this->margins['top'] );

  }

  /**
  * Set the Y position in millimetres from the bottom of the page
  */
  function SetY( $y ) {
    $this->mmy = $y;
    $this->p->ezSetY($this->Mm2Pt($y));
  }

  function WriteParagraphs( $text, $options = false ) {
    $this->mmy = $this->Pt2Mm($this->p->ezText( $text, $this->points, $options ));
  }

  function WriteParagraphsAt( $x, $y, $width, $height, $text, $options = false ) {
    $this->mmy = $this->Pt2Mm($this->p->ezText( $text, $this->points, $options ));
  }

  function WriteAt( $x, $y, $text, $font = false, $points = false, $angle = 0, $adjust = 0 ) {
    if ( $font != false ) {
      $this->SetFont( $font, ($points ? $points : $this->points) );
    }
    else if ( $points != false ) {
      $this->points = $points;
    }
    $this->p->addText( $this->Mm2Pt($x), $this->Mm2Pt($y), $this->points, $text, $angle, $adjust );
    $this->p->addText( $this->Mm2Pt($x), $this->Mm2Pt($y), $this->points, $text, $angle, $adjust );
    $GLOBALS['session']->Dbg("PdfReport", "Writing text '%s' at (%d,%d)", $text, $this->Mm2Pt($x), $this->Mm2Pt($y) );
  }

  function WriteAtJust( $just, $x, $y, $text, $font = false, $points = false, $angle = 0, $adjust = 0 ) {
    if ( $font != false ) {
      $this->SetFont( $font, ($points ? $points : $this->points) );
    }
    else if ( $points != false ) {
      $this->points = $points;
    }
    if ( $just == 'center' || $just == 'centre' || $just == 'right' ) {
      $length = $this->Pt2Mm($this->p->getTextWidth($this->points,$text));
      $x -= ($just == 'right' ? $length : $length / 2);
    }
    $this->WriteAt( $x, $y, $text, $font, $points, $angle, $adjust );
  }

  function PlaceImage( $x, $y, $img_path, $width, $height=false ) {
    $height = (( isset($height) && $height )  ? $this->Mm2Pt($height) : false );
    if ( preg_match( '/\.jpe?g$/i', $img_path ) ) {
      $GLOBALS['session']->Dbg("PdfReport", "Adding JPEG '$img_path' image");
      $this->p->addJpegFromFile($img_path,$this->Mm2Pt($x),$this->Mm2Pt($y),$this->Mm2Pt($width),$height);
    }
    elseif ( preg_match( '/\.png$/i', $img_path ) ) {
      $GLOBALS['session']->Dbg("PdfReport", "Adding PNG '$img_path' image");
      $this->p->addPngFromFile($img_path,$this->Mm2Pt($x),$this->Mm2Pt($y),$this->Mm2Pt($width),$height);
    }
    else {
      $GLOBALS['session']->Dbg("PdfReport", "Don't know type of '$img_path' image");
    }
  }

  function SetLineStyle( $width, $cap = '', $join = '', $dash = false, $phase = 0 ) {
    $this->p->setLineStyle( $this->Mm2Pt($width), $cap, $join, $dash, $phase );
  }

  function Line( $x1, $y1, $x2, $y2 ) {
    $this->y = $this->p->line( $this->Mm2Pt($x1), $this->Mm2Pt($y1), $this->Mm2Pt($x2), $this->Mm2Pt($y2) );
  }

  function PageNumbering( $x, $y, $justification = 'right', $pattern = 'Page {PAGENUM}', $num = 1 ) {
    $pos = ($justification == 'right' ? 'left' : 'right');
    $this->p->ezStartPageNumbers( $this->Mm2Pt($x), $this->Mm2Pt($y), $pos, $pattern, $num );
    $this->numbered = true;
  }

  function Box( $x1, $y1, $width, $height ) {
    $this->Line( $x1,           $y1,            $x1,           $y1 + $height );
    $this->Line( $x1,           $y1 + $height,  $x1 + $width,  $y1 + $height );
    $this->Line( $x1 + $width,  $y1 + $height,  $x1 + $width,  $y1 );
    $this->Line( $x1 + $width,  $y1,            $x1,           $y1 );
  }

  function SetFileName( $file_name ) {
    $this->stream_options['Content-Disposition'] = $file_name;
  }

  function Render( $title_tag = null ) {
    if ( $this->numbered ) {
      $this->p->NewPage();
      $this->p->ezStopPageNumbers(1,1);
      $GLOBALS['session']->Dbg("PdfReport", "Stopping page numbers now...");
    }
    $this->p->ezStream($this->stream_options);
  }

}

