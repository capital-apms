<?php
/**
* This process converts an unposted batch into a posted batch, producing
* transactions for intercompany, and consequential updates to the parent
* ledgers as required.
*
* @package   apms
* @subpackage   BatchUpdate
* @author    Andrew McMillan <andrew@catalyst.net.nz>
* @copyright Catalyst IT Ltd
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2
*/

param_to_global('batchcode', 'int', 'int1');
param_to_global('forecemonth', 'int', 'int1');

$ic = array();  // Intercompany matrix

get_office_accounts( 'GST-IN', 'GST-OUT', 'DEBTORS', 'CREDITORS', 'IC-BASE', 'IC-SUSPENSE' );

$default_db_account =  $office_accounts['DEBTORS']->accountcode;
$default_cr_entity = $office_accounts['CREDITORS']->entitycode;
$default_cr_account = $office_accounts['CREDITORS']->accountcode;
$gst_in_entity = $office_accounts['GST-IN']->entitycode;
$gst_in_account = $office_accounts['GST-IN']->accountcode;
$ic_base_ledger = $office_accounts['IC-BASE']->entitycode;
$ic_base_account = $office_accounts['IC-BASE']->accountcode;
$ic_base_offset = floor( $ic_base_account - ( floor( $ic_base_account / 1000 ) * 1000) );
$ic_suspense_account = $office_accounts['IC-SUSPENSE']->accountcode;

require_once( "transaction_helpers.php" );

class Batch {

  var $batchcode;
  var $documentcode;
  var $txncode;

  var $docrec;
  var $docsql;

  var $ic;
  var $icdoc;

  var $month_cache;

  var $successful;

  /**
  * Create a batch
  * @param string $batchcode The batch number for the batch.
  */
  function Batch( $batchcode ) {
    global $session, $c;

    $this->ic = array();
    $this->batchcode = $batchcode;
    $this->documentcode = 0;
    $this->txncode = 0;
    $this->month_cache = array();
    $this->successful = true;

    $qry = new PgQuery( "SELECT 1 FROM batch WHERE batchcode = ?", $batchcode );
    if ( $qry->Exec("batch-update") && $qry->rows > 0 ) {
      dbg_error_log( "WARN", "Batch $batchcode already exists as updated batch." );
      $qry = new PgQuery('ROLLBACK'); $qry->Exec("batch-update");

      $this->SeedIntercompany();

      return;
    }

    $sql = <<<EOSQL
INSERT INTO batch (batchcode, operatorcode, description, updatedby )
  SELECT batchcode, ?, description, ? FROM newbatch WHERE newbatch.batchcode = ?;
EOSQL;

    $qry = new PgQuery( $sql, $session->user_no, $session->fullname, $batchcode );
    if ( ! $qry->Exec("batch-update") ) {
      $this->successful = false;
      $c->messages[] = "Database problem creating batch.  Update aborted.";
    }

  }


  /**
  * Increment the documentcode and return the new doc no
  */
  function Nextdoc() {
    $this->documentcode++;
    $this->txncode = 0;
  }


  /**
  * Access the batch status
  */
  function Successful() {
    return $this->successful;
  }


  /**
  * Seed the intercompany array with any already posted txns
  */
  function SeedIntercompany( ) {
    $sql = <<<EOQ
SELECT documentcode, entitycode, monthcode, amount FROM accttran
 WHERE accttran.batchcode = ? AND entitytype = 'L';
EOQ;

    dbg_error_log( "batch-update", "Seeding intercompany from already posted transactions" );
    $qry = new PgQuery($sql, $this->batchcode);
    if ( $qry->Exec('batch-update') && $qry->rows > 0 ) {
      while( $row = $qry->Fetch() ) {
        $this->AddIntercompany( $row->entitycode, $row->monthcode, $row->amount );
        if ( $row->documentcode > $this->documentcode ) $this->documentcode = $row->documentcode;
      }
    }
  }


  /**
  * Add the amount to the intercompany array for this code / month
  */
  function AddIntercompany( $ec, $month, $amount ) {
    if ( isset($this->ic[$month][$ec]) ) {
      if ( -$amount == $this->ic[$month][$ec] ) {
        unset( $this->ic[$month][$ec] );
      }
      else {
        $this->ic[$month][$ec] += $amount;
      }
    }
    else {
      if ( !isset($this->ic[$month]) ) $this->ic[$month] = array();
      $this->ic[$month][$ec] = $amount;
    }
    if ( isset($this->ic[$month]) && isset($this->ic[$month][$ec]) && round($this->ic[$month][$ec],5) == 0 ) unset( $this->ic[$month][$ec] );
  }


  /**
  * Go through the intercompany matrix balancing all that stuff.
  */
  function DoIntercompany() {
    global $ic_base_ledger, $ic_suspense_account, $c;

    $this->icdoc = array();
    $this->Nextdoc();

    foreach ( $this->ic AS $monthcode => $eamounts ) {

      dbg_error_log( "batch-update", "Handling intercompany for month %d", $monthcode );

      $let_me_out = 500;  # We will escape from the loop if we ever go round this many times...
      $entities = array_keys($eamounts);

      while ( $let_me_out-- > 0 && count($entities) > 0 ) {
        $entitycode = $entities[0];
        if( $entitycode == $ic_base_ledger ) {
          if ( !isset($entities[1]) ) break;
          $entitycode = $entities[1];
        }
        $amount = $this->ic[$monthcode][$entitycode];
        $reversal   = $this->FindMatchingIntercompany( $entities, $monthcode, $entitycode );

        $accountcode = $ic_base_account + $reversal - $ic_base_offset;
        $reversal   = $this->AddIntercompanyTransaction( $monthcode, $entitycode, $accountcode, -$amount );

        $accountcode = $ic_base_account + $entitycode - $ic_base_offset;
        $reversal   = $this->AddIntercompanyTransaction( $monthcode, $reversal, $accountcode, $amount );

        $entities = array_keys($eamounts);
      }

      if ( count($entities) != 0 ) {
        foreach ( $entities AS $k => $entitycode ) {
          dbg_error_log( "batch-update", "Posting unbalanced difference to suspense for L%d, month %d", $entitycode, $monthcode );
          $amount = $this->ic[$monthcode][$entitycode];
          $this->AddIntercompanyTransaction( $monthcode, $entitycode,
                                      $ic_suspense_account, -$amount, "Imbalance", "Automatically re-balance batch/month" );
        }
      }
    }

    $post_sql = "BEGIN;\n";

    /**
    * Don't do anything unless we actually have some transactions to do
    */
    if ( $this->txncode > 0 ) {
      $post_sql .= <<<EOQ
INSERT INTO document ( batchcode, documentcode, description, reference, transactioncount, documenttype )
    VALUES( $this->batchcode, $this->documentcode, 'InterCompany for batch $this->batchcode', 'InterCompany', $this->txncode, 'ICOA' );

EOQ;

      $post_sql .= implode( "\n", $this->icdoc );
    }
    else {
      $this->documentcode--;
    }

    $post_sql .= <<<EOQ
DELETE FROM newbatch WHERE batchcode=$this->batchcode;
UPDATE batch
   SET documentcount=$this->documentcode, updatedat=current_timestamp::timestamp without time zone
 WHERE batchcode=$this->batchcode;
COMMIT;

EOQ;

    $qry = new PgQuery( $post_sql );
    if ( !$qry->Exec("batch-update") ) {
      $this->successful = false;
      $c->messages[] = "Database problem completing batch update.  Update aborted.";
    }
  }



  /**
  * Find a matching company to exchange the intercompany with
  */
  function FindMatchingIntercompany( $entities, $monthcode, $ec ) {

    /**
    * At this point we don't do anything fancy - all intercompany goes via
    * the base ledger...
    */
    return $ic_base_ledger;
  }



  /**
  * Add a single intercompany transaction to our array
  */
  function AddIntercompanyTransaction( $mth, $ec, $ac, $amt, $ref=null, $desc=null ) {

    $this->txncode++;
    $ref  = qpg($ref);
    $desc = qpg($desc);

    $this->icdoc[] = <<<EOQ
  INSERT INTO accttran (batchcode, documentcode, transactioncode, date, monthcode, entitytype, entitycode, accountcode, amount, reference, description )
      VALUES( $this->batchcode, $this->documentcode, $this->txncode, (SELECT enddate FROM month WHERE monthcode = $mth), $mth, 'L', $ec, $ac, $amt::numeric(20,2), $ref, $desc );
EOQ;
    dbg_error_log( "batch-update", "Added intercompany transaction %d-%d-%d L-%05d-%06.1lf month %d for %.2lf",
                                                 $this->batchcode, $this->documentcode, $this->txncode, $ec, $ac, $mth, $amt );
    $this->AddIntercompany( $ec, $mth, $amt );
  }


  /**
  * Start a new document in this batch
  */
  function StartDocument( $docrec ) {
    $this->docrec = clone($docrec);
    $this->docsql = array();
    $this->Nextdoc();
  }


  /**
  * Add this transaction and any consequences onto the list to be posted
  */
  function GetPostingMonth( $month, $date ) {
    if ( isset($month) && $month > 0 ) return $month ;

    // If we have one transaction for a particular date, we usually have many...
    if ( isset($this->month_cache[$date]) ) return $this->month_cache[$date];

    // We look for the ideal month, then the earliest later month, and finally the latest earlier month
    $get_month = <<<EOQ
  SELECT monthcode, -99999999 AS sorter FROM month WHERE monthstatus = 'Open' AND '$date'::date BETWEEN startdate AND enddate
    UNION
  SELECT monthcode, monthcode AS sorter FROM month WHERE monthstatus = 'Open' AND startdate >= '$date'::date
    UNION
  SELECT monthcode, (99999999 - monthcode) AS sorter FROM month WHERE monthstatus = 'Open' AND enddate < '$date'::date
    ORDER BY 2 LIMIT 1
EOQ;
    $qry = new PgQuery( $get_month );
    if ( ! $qry->Exec('batch-update') || $qry->rows == 0 ) {
      dbg_error_log( "ERROR", "No open months.  Giving up.");
      die( "No open months.  Giving up.");
    }
    $month = $qry->Fetch();

    dbg_error_log( "batch-update", "Posting month for $date is %d", $month->monthcode );
    $this->month_cache[$date] = $month->monthcode;
    return $month->monthcode;
  }


  /**
  * Add a transaction to the current document
  */
  function AddTransaction( $tx, $depth = 0 ) {
    global $default_cr_entity;

    if ( round($tx->amount,5) == 0 ) return;   # Nothing to do :-)

    $this->txncode++;

    if ( $tx->consequenceof == 0 ) $tx->consequenceof = null;
    $tx->entitytype = strtoupper($tx->entitytype);

    $tx->monthcode = (isset($tx->monthcode) && $tx->monthcode > 0 ? $tx->monthcode : $this->GetPostingMonth($tx->monthcode,$tx->date) );
    $ref  = qpg($tx->reference);
    $desc = qpg($tx->description);
    $consequenceof = qpg($tx->consequenceof);

    $sql = <<<EOQ
  INSERT INTO accttran (batchcode, documentcode, transactioncode, date, monthcode, entitytype, entitycode, accountcode, amount, reference, description, consequenceof )
      VALUES( $this->batchcode, $this->documentcode, $this->txncode, '$tx->date', $tx->monthcode, '$tx->entitytype', $tx->entitycode, $tx->accountcode, $tx->amount::numeric(20,2), $ref, $desc, $consequenceof );
EOQ;
    $this->docsql[] = $sql;
    dbg_error_log( "batch-update", "Added transaction %d-%d-%d: $tx->entitytype-%05d-%06.1lf on %s (%d): \$%0.2lf, %s: %s",
                            $tx->batchcode-$tx->documentcode-$this->txncode,
                            $tx->entitycode, $tx->accountcode, $tx->date, $tx->monthcode, $tx->amount, $tx->reference, $tx->description );

    switch( $tx->entitytype ) {
      case "L":
        $this->AddIntercompany( $tx->entitycode, $tx->monthcode, $tx->amount );
        return;

      case 'T':
        $sql = <<<EOQ
SELECT 'tenant' AS resulttype, entitytype, entitycode, $tx->accountcode AS entityaccount
       FROM tenant WHERE tenantcode = $tx->entitycode
EOQ;
        break;

      case 'C':
        $sql = <<<EOQ
SELECT 'creditor' AS resulttype, 'L' AS entitytype, $default_cr_entity AS entitycode, $tx->accountcode AS entityaccount
EOQ;
        break;

      case 'P':
        $sql = <<<EOQ
SELECT 'property' AS resulttype, 'L' AS entitytype, companycode AS entitycode, $tx->accountcode AS entityaccount
       FROM property WHERE propertycode = $tx->entitycode
EOQ;
        break;

      case 'A':  /** @todo Are assets 'A' or 'F'? We should declare constants for these... */
      case 'F':
        $sql = <<<EOQ
SELECT 'fixedasset' AS resulttype, entitytype, entitycode, $tx->accountcode AS entityaccount
       FROM fixedasset WHERE assetcode = $tx->entitycode
EOQ;
        break;

      case 'J':
        // This is the most complicated case, because we could be directed by the ProjectBudget
        // record (if it exists) or the Project record (if it doesn't).
        $sql = <<<EOQ
SELECT 'budget' AS resulttype, entitytype, entitycode, entityaccount
       FROM projectbudget WHERE projectcode = $tx->entitycode AND accountcode = $tx->accountcode
UNION
SELECT 'project' AS resulttype, entitytype, entitycode, entityaccount
       FROM project WHERE projectcode = $tx->entitycode
ORDER BY 1
EOQ;
        break;

      default:
        dbg_error_log( "ERROR", "Batch contains transaction with unknown entitytype '%s'", $tx->entitytype );
        die("Batch contains transaction with unknown entitytype '".$tx->entitytype."'.  It'll all end in tears, I know it!");
    }

    $qry = new PgQuery( $sql );

    dbg_error_log( "batch-update", "Finding consequences of %s-%05d-%06.1lf", $tx->entitytype, $tx->entitycode, $tx->accountcode );
    if ( !$qry->Exec("batch-update") ) {
      dbg_error_log( "ERROR", "Database error updating batch" );
      return;
    }

    $cons = $qry->Fetch();
    if ( !isset($tx->consequenceof) || $tx->consequenceof == 0 ) $tx->consequenceof = $this->txncode;
    $tx->entitytype = $cons->entitytype;
    $tx->entitycode = $cons->entitycode;
    $tx->accountcode = $cons->entityaccount;

    $this->AddTransaction($tx, $depth + 1 );
  }


  /**
  * When we've built up all of the transactions for a document, commit it to the database
  */
  function CommitDocument() {
    global $c;
    if ( $this->txncode < 1 ) return;

    $doc = $this->docrec;

    dbg_error_log( "batch-update", "Committing document $this->batchcode-$this->documentcode with $this->txncode transactions" );

    $post_sql = <<<EOQ
BEGIN;
  INSERT INTO document ( batchcode, documentcode, description, reference, transactioncount, documenttype )
      SELECT $this->batchcode, $this->documentcode, description, reference, $this->txncode, documenttype
               FROM newdocument WHERE batchcode = $doc->batchcode AND documentcode = $doc->documentcode;

EOQ;

    $post_sql .= implode( "\n", $this->docsql );

    $post_sql .= <<<EOQ

  DELETE FROM newdocument WHERE batchcode=$doc->batchcode AND documentcode=$doc->documentcode;
COMMIT;

EOQ;

    $qry = new PgQuery( $post_sql );
    if ( !$qry->Exec("batch-update") ) {
      $this->successful = false;
      $c->messages[] = "Database problem updating transactions.  Update aborted.";
    }
  }

}





$batch = new Batch($batchcode);

$last_document = null;
$to_post = null;

$txnsql = <<<EOQ
SELECT newdocument.reference AS documentreference,
       newdocument.description AS documentdescription,
       newaccttrans.*
  FROM newdocument LEFT JOIN newaccttrans USING (batchcode,documentcode)
 WHERE newdocument.batchcode = ?
 ORDER BY newdocument.batchcode, newdocument.documentcode, transactioncode;
EOQ;
$txns = new PgQuery( $txnsql, $batchcode );
$txns->Exec("batch-update");


/**
* Main loop.  We keep this as simple as possible.
*/
while( $batch->Successful() && $unposted = $txns->Fetch() ) {
  if ( !isset($unposted->transactioncode) || $unposted->transactioncode == "" ) continue;
  if ( !isset($last_document) || $last_document->documentcode != $unposted->documentcode ) {
    $batch->CommitDocument();
    $last_document = $unposted;
    $batch->StartDocument($unposted);
  }
  $batch->AddTransaction( $unposted );
}
if ( $batch->Successful() ) $batch->CommitDocument();

if ( $batch->Successful() ) $batch->DoIntercompany();

if ( $batch->Successful() ) {
  include_once("view/batch.php");
}
/*
else {
  include_once("view/newbatch.php");
}
*/