package APMS::View::Panel;

=head1 APMS::View::Panel

Manage the panel widgets for APMS. Panel widgits are everything above
the browse widget in a drill window - sort-by widget, alphabet widgets
etc.

=cut

use strict;

sub radio_choose {
	my ( $self, @opts ) = @_;

	my @buttons;

	# The parent HBox of all the frames
	my $hbox = Gtk2::HBox->new( 0, 5 );

	foreach my $group ( @opts ) {
		# Create a frame to put the buttons into
		my $frame = Gtk2::Frame->new( $group->{name} );

		# Create an HBox to pack the radios in before added to the frame
		my $framehbox = Gtk2::HBox->new( 0, 0 );

		my $first = 1;
		foreach my $buttondef ( @{ $group->{options} } ) {
			# If this is the first button in this group, then dont supply a group
			if ( $first ) {
				push @buttons, Gtk2::RadioButton->new( undef, $buttondef );
			}
			else {
				my $buttongroup = $buttons[ -1 ]->get_group;
				push @buttons, Gtk2::RadioButton->new( $buttongroup, $buttondef );
			}

			# Add the latest radio to the frame
			$framehbox->pack_start( $buttons[ -1 ], 0, 0, 0 );
			$first = 0;
		}

		# Add the hbox into the frame
		$frame->add( $framehbox );

		# Add the frame into the global hbox
		$hbox->pack_start( $frame, 0, 0, 0 );
	}

	return $hbox;
}

=item alphabet_soup

Create an alphabet sort 

=cut

sub alphabet_soup {
	my $self = shift;

	# A simple horizontal box of letters
	my $hbox = Gtk2::HBox->new( 1, 0 );

	# Create an alphabet of buttons
	for my $letter ( 'A'..'Z' ) {
		my $button = Gtk2::Button->new( $letter );
		$hbox->pack_start( $button, 1, 1, 0 );
	}

	return $hbox;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

