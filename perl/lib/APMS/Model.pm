package APMS::Model;

use strict;
use APMS::Class qw/ new_object /;
use DBI;

use base qw/
	APMS::Model::Accounts
	APMS::Model::BankAccount
	APMS::Model::ButtonList
	APMS::Model::Companies
	APMS::Model::Tenants
	APMS::Model::Invoices
	APMS::Model::Contacts
	APMS::Model::Entity
	APMS::Model::Office
	APMS::Model::Persons
	APMS::Model::OfficeSetting
	APMS::Model::ProcedureMapper
/;

=item connect

Connect to the APMS database and return the database handle

=cut

sub connect {
	my ( $proto, $dbconfig ) = @_;
	my $self = new_object( $proto );

	# Check that we have the appropriate driver installed
	my $installeddriver = 0;
	my $driverusing = $dbconfig->{DBIDriver};
	my $driverargs  = $dbconfig->{DBIConnection};
	my $username    = $dbconfig->{DBIUserName};
	my $password    = $dbconfig->{DBIUserPass};

	my @driver_names = DBI->available_drivers;

	foreach my $driver ( @driver_names ) {
		$installeddriver = 1
			if $driver eq $driverusing;
	}

	# Check the driver is installed
	unless ( $installeddriver ) {
		print "The DBI driver for $driverusing is not installed\n";
		return 0;
	}

	# Connect to the db
	unless ( $self->{dbh} = DBI->connect(
		"dbi:$driverusing:$driverargs",
		$username, $password,
#		{ PrintError => 0 }
	 ) ) {
		if ( $DBI::errstr =~ m|could not connect|i ) {
			gtk_error( "Could not connect to the database", $DBI::errstr );
		}
		return 0;
	}

	return $self;
}

=item disconnect

Disconnect from the database

=cut

sub disconnect {
	my $self = shift;
	$self->{dbh}->disconnect;
}

sub gtk_error {
	my ( $message, $dbierror ) = @_;

	my $window = Gtk2::Window->new( 'toplevel' );
	my ( $amount, $response );
	my $dialog = Gtk2::MessageDialog->new(
		$window,
		'destroy-with-parent',
		'error',
		'ok',
		"%s :\n\n%s", $message, $dbierror
	);

	$response = $dialog->run;
	$dialog->destroy;

	exit 1;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

