-- APMS Utility Functions

-- Returns the name of the entity type
CREATE or REPLACE FUNCTION get_entity_type_name( TEXT ) RETURNS TEXT AS '
  SELECT CASE
            WHEN $1 = ''T'' THEN ''Debtor''
            WHEN $1 = ''C'' THEN ''Creditor''
            WHEN $1 = ''L'' THEN ''Company''
            WHEN $1 = ''P'' THEN ''Property''
            WHEN $1 = ''J'' THEN ''Project''
            WHEN $1 = ''A'' THEN ''Asset''
            ELSE ''UNKNOWN''
         END;
' LANGUAGE SQL IMMUTABLE STRICT;

-- Returns the internal name of the entity type
CREATE or REPLACE FUNCTION get_entity_type_internal_name( TEXT ) RETURNS TEXT AS '
  SELECT CASE
            WHEN $1 = ''T'' THEN ''tenant''
            WHEN $1 = ''C'' THEN ''creditor''
            WHEN $1 = ''L'' THEN ''company''
            WHEN $1 = ''P'' THEN ''property''
            WHEN $1 = ''J'' THEN ''project''
            WHEN $1 = ''A'' THEN ''asset''
            ELSE ''UNKNOWN''
         END;
' LANGUAGE SQL IMMUTABLE STRICT;

-- Returns a link for the document reference, if possible
CREATE or REPLACE FUNCTION get_docref_link( TEXT, TEXT ) RETURNS TEXT AS '
  SELECT CASE
            WHEN $2 = ''VCHR'' THEN ''<a href="/view.php?t=voucher&id=''||$1||''" title="View the creditor voucher that created this document">''||$1||''</a>''
            WHEN $2 = ''CHEQ'' THEN ''<a href="/view.php?t=cheque&id=''||$1||''" title="View the cheque that created this document">''||$1||''</a>''
            WHEN $2 = ''INVC'' THEN ''<a href="/view.php?t=invoice&id=''||$1||''" title="View the debtor invoice that created this document">''||$1||''</a>''
            ELSE $1
         END;
' LANGUAGE SQL IMMUTABLE STRICT;


-- Returns the name of the entity, depending on the type
CREATE or REPLACE FUNCTION get_entity_name( TEXT, INT4 ) RETURNS TEXT AS '
  SELECT CASE
            WHEN $1 = ''T'' THEN (SELECT name FROM tenant WHERE tenantcode=$2)
            WHEN $1 = ''C'' THEN (SELECT name FROM creditor WHERE creditorcode=$2)
            WHEN $1 = ''L'' THEN (SELECT legalname FROM company WHERE companycode=$2)
            WHEN $1 = ''P'' THEN (SELECT name FROM property WHERE propertycode=$2)
            WHEN $1 = ''J'' THEN (SELECT name FROM project WHERE projectcode=$2)
            ELSE $1 || $2::text
         END;
' LANGUAGE SQL STABLE;

-- Returns the name of the entity, depending on the type
CREATE or REPLACE FUNCTION get_account_name( NUMERIC ) RETURNS TEXT AS $$
  SELECT name FROM chartofaccount WHERE accountcode = $1
$$ LANGUAGE SQL IMMUTABLE STRICT;


CREATE or REPLACE FUNCTION previous_balance( TEXT, INT, NUMERIC, INT, INT ) RETURNS NUMERIC AS '
DECLARE
  et ALIAS FOR $1;
  ec ALIAS FOR $2;
  ac ALIAS FOR $3;
  mcode ALIAS FOR $4;
  years ALIAS FOR $5;
  result FLOAT8;
BEGIN
  SELECT balance INTO result
          FROM accountbalance ab
                 JOIN month oldm USING ( monthcode )
                 JOIN month newm ON newm.startdate = (oldm.startdate + (years::text||'' years'')::interval)
         WHERE entitytype = et AND entitycode = ec AND accountcode = ac
           AND newm.monthcode = mcode;
  IF NOT FOUND THEN
    RETURN 0::numeric;
  END IF;
  RETURN result;
END;
' LANGUAGE plpgsql STABLE;



CREATE or REPLACE FUNCTION previous_summary( TEXT, INT, NUMERIC, INT, INT ) RETURNS NUMERIC AS $$
DECLARE
  et ALIAS FOR $1;
  ec ALIAS FOR $2;
  ac ALIAS FOR $3;
  mcode ALIAS FOR $4;
  years ALIAS FOR $5;
  result NUMERIC;
  year_end NUMERIC;
  mth INT4;
  first_mth INT4;
  is_bs BOOLEAN;
BEGIN
  SELECT oldm.monthcode INTO mth FROM month oldm JOIN month newm ON newm.startdate = (oldm.startdate + (years::text||' years')::interval)
                        WHERE newm.monthcode = mcode;
  IF NOT FOUND THEN
    RAISE NOTICE 'No month record found (%).', mcode;
    RETURN NULL;
  END IF;

  SELECT grouptype = 'B' INTO is_bs FROM chartofaccount LEFT JOIN accountgroup ag USING(accountgroupcode)
         WHERE accountcode = ac;
  IF NOT FOUND THEN
    RAISE NOTICE 'No chartofaccount/accountgroup record found for account "%".', ac;
    RETURN NULL;
  END IF;

  IF is_bs THEN
    first_mth := 0;
  ELSE
    SELECT monthcode INTO first_mth FROM month
                    WHERE month.financialyearcode = (SELECT oldm.financialyearcode FROM month oldm WHERE oldm.monthcode = mth)
                    ORDER BY monthcode LIMIT 1;
  END IF;

  SELECT sum(balance) INTO result FROM accountbalance ab
         WHERE entitytype = et AND entitycode = ec AND accountcode = ac AND monthcode >= first_mth AND monthcode <= mth;
  IF NOT FOUND OR result IS NULL THEN
    result := 0::numeric;
  END IF;

  IF is_bs THEN
    RETURN result;
  END IF;

  SELECT sum(amount) INTO year_end FROM accttran
               LEFT JOIN document USING (batchcode,documentcode)
         WHERE entitytype = et AND entitycode = ec AND accountcode = ac AND monthcode = mth
           AND documenttype = 'YEND';
  IF FOUND AND year_end IS NOT NULL THEN
    result := result - year_end;
  END IF;

  RETURN result;
END;
$$ LANGUAGE plpgsql STABLE;



CREATE or REPLACE FUNCTION year_end_balance( TEXT, INT, INT ) RETURNS SETOF accountbalance AS $$
DECLARE
  et ALIAS FOR $1;
  ec ALIAS FOR $2;
  fy ALIAS FOR $3;
  abrec RECORD;
  fy_last_month INT;
  yend_reversals NUMERIC;
  result accountbalance%ROWTYPE;
BEGIN

  SELECT max(monthcode) INTO fy_last_month FROM month WHERE financialyearcode = fy;

  result.entitytype := et;
  result.entitycode := ec;
  result.monthcode  := fy_last_month;
  result.notecode   := NULL;

  FOR abrec IN
      SELECT accountcode, sum(balance) AS balance, sum(budget) AS budget, sum(revisedbudget) AS revisedbudget, accountgroup.grouptype
          FROM month LEFT JOIN accountbalance USING (monthcode)
               RIGHT JOIN chartofaccount USING(accountcode)
               LEFT JOIN accountgroup USING(accountgroupcode)
          WHERE (month.financialyearcode = fy OR (grouptype != 'P' AND month.financialyearcode < fy) )
            AND accountbalance.entitytype = et AND accountbalance.entitycode = ec
          GROUP BY accountgroup.grouptype, accountcode
  LOOP
    SELECT sum(amount) INTO yend_reversals
         FROM accttran JOIN document USING (batchcode,documentcode)
         WHERE accttran.entitytype = et AND accttran.entitycode = ec AND accttran.accountcode = abrec.accountcode
           AND documenttype = 'YEND' AND accttran.monthcode = fy_last_month
         GROUP BY accountcode;
    result.balance       := coalesce(abrec.balance,0) - coalesce(yend_reversals,0);
    result.budget        := coalesce(abrec.budget,0);
    result.revisedbudget := coalesce(abrec.revisedbudget,0);
    IF result.balance != 0 OR result.budget != 0 OR result.revisedbudget != 0 THEN
      result.accountcode   := abrec.accountcode;
      RETURN NEXT result;
    END IF;
  END LOOP;

  -- That caught most of them, but for the ones that got zeroed out, and which have no budget
  -- or revised budget, the accountbalance records get removed by a trigger, so we need a special
  -- pass through looking at anything which had YEND transactions, and which now doesn't exist!
  FOR abrec IN
    SELECT accttran.accountcode, sum(amount) AS balance, 0::numeric AS budget, 0::numeric AS revisedbudget, ag.grouptype
         FROM accttran LEFT JOIN document USING (batchcode,documentcode)
               LEFT JOIN chartofaccount USING(accountcode)
               LEFT JOIN accountgroup ag USING(accountgroupcode)
         WHERE entitytype = et AND entitycode = ec
           AND documenttype = 'YEND' AND monthcode = fy_last_month AND grouptype = 'P'
           AND NOT EXISTS( SELECT 1 FROM month LEFT JOIN accountbalance USING (monthcode)
                                              RIGHT JOIN chartofaccount USING(accountcode)
                                               LEFT JOIN accountgroup USING(accountgroupcode)
                            WHERE month.financialyearcode = fy AND accountgroup.grouptype = 'P' AND accountbalance.entitytype = accttran.entitytype
                              AND accountbalance.entitycode = accttran.entitycode AND accountbalance.accountcode = accttran.accountcode )
         GROUP BY ag.grouptype, accountcode
  LOOP
    result.balance       := 0 - coalesce(abrec.balance,0);
    IF result.balance != 0 THEN
      result.budget        := 0;
      result.revisedbudget := 0;
      result.accountcode   := abrec.accountcode;
      RETURN NEXT result;
    END IF;
  END LOOP;

  RETURN;
END;
$$ LANGUAGE plpgsql STABLE;


CREATE or REPLACE FUNCTION debtor_statistics( IN tcode INT, IN mfrom INT, IN mto INT,
  OUT tenant_name TEXT,
  OUT total_paid NUMERIC,
  OUT average_month NUMERIC,
  OUT times INT,
  OUT average_days NUMERIC,
  OUT balance NUMERIC,
  OUT balance_days INT )
 RETURNS RECORD AS '
DECLARE
  debtors_control NUMERIC;
  start_balance NUMERIC;
  total_days INT;
  groups RECORD;
  group_count NUMERIC;
  group_unpaid_days INT;
  group_amount NUMERIC;
  group_earliest DATE;
  group_last DATE;
  period_start DATE;
  period_end DATE;
BEGIN
  SELECT accountcode INTO debtors_control FROM office JOIN officecontrolaccount USING (officecode) where officecontrolaccount.name = ''DEBTORS'' AND office.thisoffice;

  SELECT name INTO tenant_name FROM tenant WHERE tenantcode=tcode;

  SELECT sum(amount), count(amount) INTO total_paid, times FROM accttran
         WHERE entitytype = ''T'' AND entitycode = tcode
           AND accountcode = debtors_control
           AND monthcode >= mfrom AND monthcode <= mto
           AND amount > 0;

  SELECT total_paid / count(*), min(startdate), max(enddate) INTO average_month, period_start, period_end
          FROM month WHERE monthcode >= mfrom AND monthcode <= mto;

  SELECT sum(balance) INTO start_balance FROM accountbalance
         WHERE entitytype = ''T'' AND entitycode = tcode
           AND accountcode = debtors_control
           AND monthcode < mfrom;

  total_days := 0;
  group_count := 0;
  balance_days := 0;

  FOR groups IN SELECT DISTINCT closinggroup FROM accttran
          WHERE entitytype = ''T'' AND entitycode = tcode
            AND accountcode = debtors_control
            AND monthcode >= mfrom AND monthcode <= mto
  LOOP
    group_count := group_count + 1;

    SELECT date INTO group_earliest FROM accttran
          WHERE entitytype = ''T'' AND entitycode = tcode
            AND accountcode = debtors_control
            AND closinggroup = groups.closinggroup
          ORDER BY date LIMIT 1;

    SELECT date INTO group_last FROM accttran
          WHERE entitytype = ''T'' AND entitycode = tcode
            AND accountcode = debtors_control
            AND closinggroup = groups.closinggroup
          ORDER BY date DESC LIMIT 1;

    total_days := total_days + (group_last - group_earliest);
    IF group_last > period_end THEN
      group_unpaid_days := period_end - group_earliest;
      IF group_unpaid_days > balance_days THEN
        balance_days := group_unpaid_days;
      END IF;
    END IF;

  END LOOP;

  average_days := CASE WHEN group_count = 0 THEN 0 ELSE total_days::numeric / group_count END;

END;
' LANGUAGE plpgsql STABLE;

-- Retrieve all phone numbers and nicely format them into a single string
-- FIXME: This assumes local STD dialling is preceded by a '0' and other unwarranted things
CREATE or REPLACE FUNCTION get_person_phones( INT, TEXT, TEXT ) RETURNS TEXT AS '
DECLARE
  in_personcode ALIAS FOR $1;
  loc_ccode ALIAS FOR $2;
  loc_cstd ALIAS FOR $3;
  phonelist TEXT;
  phone RECORD;
BEGIN
  phonelist := '''';
  FOR phone IN SELECT phonetype, ccountrycode, cstdcode, number FROM phonedetail
          WHERE personcode = in_personcode
  LOOP
    phonelist := phonelist
              || (CASE WHEN phonelist = '''' THEN '''' ELSE '', '' END)
              || phone.phonetype || '': ''
              || (CASE
                    WHEN loc_ccode = phone.ccountrycode THEN
                      (CASE WHEN loc_cstd = phone.cstdcode THEN '''' ELSE ''0'' || phone.cstdcode || '' '' END)
                    ELSE
                      ''+'' || phone.ccountrycode || ''('' || phone.cstdcode || '')''
                 END)
              || phone.number;
  END LOOP;
  RETURN phonelist;
END;
' LANGUAGE plpgsql STRICT;


CREATE or REPLACE FUNCTION get_office_setting( TEXT ) RETURNS TEXT AS $$
  SELECT setvalue FROM office JOIN officesettings USING ( officecode ) WHERE setname = $1 AND thisoffice;
$$ LANGUAGE sql STRICT IMMUTABLE;


CREATE or REPLACE FUNCTION date_series( DATE, INT4, INT4 ) RETURNS SETOF DATE AS $$
DECLARE
  base_date ALIAS FOR $1;
  repeats   ALIAS FOR $2;
  period    ALIAS FOR $3;
  this_date DATE;
  counter INT;
  gap  INTERVAL;
BEGIN
  this_date := base_date;
  counter   := 0;
  gap       := (period::text || ' days')::interval;

  WHILE counter < repeats LOOP
    RETURN NEXT this_date;
    counter := counter + 1;
    this_date := this_date + gap;
  END LOOP;
  RETURN;
END;
$$ LANGUAGE plpgsql STRICT IMMUTABLE;


DROP TYPE day_balance_type CASCADE;
CREATE TYPE day_balance_type AS (
  day DATE,
  balance NUMERIC
);


CREATE or REPLACE FUNCTION daily_balance( CHAR, INT4, NUMERIC, DATE, INT4 ) RETURNS SETOF day_balance_type AS $$
DECLARE
  et ALIAS FOR $1;
  ec ALIAS FOR $2;
  ac ALIAS FOR $3;
  base_date ALIAS FOR $4;
  days      ALIAS FOR $5;
  result   day_balance_type%ROWTYPE;
  delta    RECORD;
  starting NUMERIC;
  mth      INT4;
BEGIN
  SELECT monthcode INTO mth FROM month WHERE month.startdate <= base_date AND month.enddate >= base_date;
  IF NOT FOUND THEN
    RAISE NOTICE 'No month record covering start of period (%).', base_date;
    RETURN;
  END IF;

  SELECT sum(balance) INTO starting FROM accountbalance WHERE entitytype = et AND entitycode = ec AND accountcode = ac AND monthcode < mth;
  IF NOT FOUND OR starting IS NULL THEN
    starting := 0;
  END IF;
  result.day := base_date - '1 day'::interval;
  result.balance := starting;

  SELECT sum(amount) INTO starting FROM accttran WHERE entitytype = et AND entitycode = ec AND accountcode = ac AND monthcode >= mth AND date < base_date;
  IF FOUND THEN
    result.balance := result.balance + COALESCE(starting,0);
  END IF;

  FOR delta IN
    SELECT date_series, sum(amount) FROM date_series( base_date, days, 1)
                              LEFT JOIN accttran ON (accttran.date = date_series AND entitytype = et AND entitycode = ec AND accountcode = ac AND monthcode >= mth)
                              GROUP BY date_series ORDER BY date_series
  LOOP
    result.day := delta.date_series;
    result.balance := result.balance + COALESCE(delta.sum, 0);
    RETURN NEXT result;
  END LOOP;

  RETURN;

END;
$$ LANGUAGE plpgsql STRICT IMMUTABLE;
