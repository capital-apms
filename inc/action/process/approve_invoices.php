<?php
/**
* Component for approving specified invoices
* @package   apms
* @subpackage   approve_invoices
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/

// In this case we only want to process when $_POST['submit'] is set.
if ( $widget->IsSubmit() ) return;

param_to_global('approve', '#^\d+#' );

get_office_accounts( 'GST-OUT', 'DEBTORS' );

$gst_ec = $office_accounts['GST-OUT']->entitycode;
$gst_inv_ac = $office_accounts['GST-OUT']->accountcode;
$sundry_debtors = $office_accounts['DEBTORS']->accountcode;

require_once( "transaction_helpers.php" );

$in_list = implode( ',', array_keys($approve) );
$sql = <<<EOQ
SELECT invoiceno, i.entitytype AS inv_et, i.entitycode AS inv_ec, invoicedate, todetail, taxapplies, taxamount, total AS lesstax,
       il.entitytype AS et, il.entitycode AS ec, accountcode AS ac, accounttext AS linedescription, yourshare AS lineamount
  FROM invoice i JOIN invoiceline il USING (invoiceno)
 WHERE invoiceno IN ( $in_list ) AND invoicestatus = 'U'
 ORDER BY invoiceno, lineseq
EOQ;

$qry = new PgQuery( $sql );
if ( $qry->Exec("approve_invoices") && $qry->rows > 0 ) {
  $batchcode = create_newbatch( 'AUTO', 0, "Debtor invoices" );
  $documentcode = null;
  $invoiceno = null;
  while( $il = $qry->Fetch() ) {
    if ( $invoiceno != $il->invoiceno ) {
      $invoiceno = $il->invoiceno;
      create_newdocument( $il->todetail, sprintf('INV %05d', $invoiceno), 'INVC' );
      $transactioncode = 1;
      create_newtransaction( $il->inv_et, $il->inv_ec, $sundry_debtors, $il->invoicedate, ($il->lesstax + $il->taxamount) );
      create_newtransaction( 'L', $gst_ec, $gst_inv_ac, $il->invoicedate, (0 - $il->taxamount), 'GST, '.$il->todetail );
    }
    create_newtransaction( $il->et, $il->ec, $il->ac, $il->invoicedate, (0 - $il->lineamount),
                            ($il->linedescription == $il->todetail ? null : $il->linedescription) );
  }
  $sql = "UPDATE invoice SET invoicestatus = 'A' WHERE invoiceno IN ( $in_list );";
  $sql .= "UPDATE newbatch SET documentcount = (SELECT count(1) FROM newdocument d WHERE d.batchcode = ?), total = (SELECT sum(amount) FROM newaccttrans t WHERE t.batchcode = ?)  WHERE batchcode = ?;";
  $qry = new PgQuery( $sql . 'COMMIT;', $batchcode, $batchcode, $batchcode, $batchcode );
  if ( !$qry->Exec("approve_invoices") ) {
    $c->messages[] = "Database transaction failure - please correct problem and retry.";
  }
}
else {
  $c->messages[] = "No invoices processed.";
}