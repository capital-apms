<?php
/**
* Component for creating a batch of transactions reflecting GST for the period specified
* @package   apms
* @subpackage   period_gst
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/

param_to_global('companycode',  'int' );
param_to_global('period_start', 'int' );
param_to_global('period_end',   'int' );

get_office_accounts( 'GST-BAD', 'GST-IN', 'GST-OUT', 'GST-SUSPENSE' );

$gst_bad_ac = $office_accounts['GST-BAD']->accountcode;
$gst_in_ac  = $office_accounts['GST-IN']->accountcode;
$gst_out_ac = $office_accounts['GST-OUT']->accountcode;

$gst_washup = $office_accounts['GST-SUSPENSE']->accountcode;

require_once( "transaction_helpers.php" );

$qry = new AwlQuery("SELECT min(startdate), max(enddate) FROM month WHERE monthcode >= ? AND monthcode <= ?", $period_start, $period_end);
if ( $qry->Exec("period_gst") && $qry->rows() > 0 ) {
  $period = $qry->Fetch();
}
else {
  $c->messages[] = "Unable to find period start/end";
  return true;
}

$sql = <<<EOQ
SELECT accountcode, sum(balance) AS sum
  FROM accountbalance
 WHERE monthcode >= ? AND monthcode <= ?
   AND entitytype = 'L' AND entitycode = ?
   AND accountcode IN ( ?, ?, ? )
 GROUP BY accountcode
EOQ;

$qry = new AwlQuery( $sql, intval($period_start), intval($period_end), intval($companycode), $gst_bad_ac, $gst_in_ac, $gst_out_ac );
if ( $qry->Exec("period_gst") && $qry->rows() > 0 ) {

  $batch = new NewBatch( 'AUTO', 0, "Period GST from $period->min to $period->max");
  $batch->newDocument( "Period GST from $period->min to $period->max", 'Period GST', 'JRNL' );
  $journal_total = 0.0;

  while( $account_balance = $qry->Fetch() ) {
    if ( $account_balance->sum != 0.0 ) {
      $activity = '';
      switch( $account_balance->accountcode ) {
        case $gst_out_ac:  $activity = 'collected '; break;
        case $gst_in_ac:   $activity = 'paid out '; break;
        case $gst_bad_ac:  $activity = 'on bad debt written off '; break;
      }
      $description = sprintf( 'GST %sfrom %s to %s', $activity, $period->min, $period->max);
      $batch->newTransaction( 'L', $companycode, $account_balance->accountcode, $period->max, - $account_balance->sum, $description );
      $journal_total += $account_balance->sum;
    }
  }
  $description = sprintf( 'GST %sdue for the period %s to %s', ($journal_total > 0 ? 'refund ' : ''), $period->min, $period->max);
  $batch->newTransaction( 'L', $companycode, $gst_washup, $period->max, $journal_total, $description );
  $batch->Commit();
  $c->messages[] = sprintf( "GST batch %s created.", $batch->getCode());
}
else {
  $c->messages[] = "No GST batch to create.";
}
