package APMS::Class;

use strict;
use base qw/ Exporter /;
our @EXPORT = qw/ new_object /;

=item APMS::Class

Object creation helper

=cut

sub new_object {
	my $proto = shift;
	my $class = ref( $proto ) || $proto;

	my $self = {};
	bless $self, $class;

	return $self;
}

1;

