package APMS::Model::Accounts;

use strict;

=item account_map

Returns a hash of the chart of accounts, with the key set to the account
code, and the value set to the name.

=cut

sub account_names {
	my ( $self ) = @_;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT accountcode, name FROM chartofaccount;
	) );

	$sth->execute();
	my $accounts = {};
	foreach my $acct ( @{ $sth->fetchall_arrayref({}) } ) {
		my $code = $acct->{accountcode};
		$accounts->{$code} = $acct->{name};
	}
	$sth->finish;

	return $accounts;
}

sub all_accounts {
	my $self = shift;

	my $sth = $self->{dbh}->prepare( qq(
		SELECT
			accountcode,
			accountgroupcode,
			expenserecoverytype,
			highvolume,
			name,
			shortname,
			updateto
		FROM chartofaccount;
	) );

	$sth->execute();
	my $accounts = $sth->fetchall_arrayref({});
	$sth->finish();

	return $accounts;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

