#!/usr/bin/perl -w
#
# Script to convert a progress data dump file (.d) into
# SQL suitable to load into a PostgreSQL database that
# was built with the df-2-postgresql.pl script.
#

use strict;
use Getopt::Long qw(:config permute);  # allow mixed args.
use DBI;
use Encode;
use encoding "latin-1";

my $dfile ="";
my $tname ="";
my $dbname ="apms";
my $dbuser ="";
my $debug = 0;
my $helpmeplease = 0;

GetOptions ('debug!'    => \$debug,
            'table=s'   => \$tname,
            'file=s'    => \$dfile,
            'dbname=s'  => \$dbname,
            'user=s'    => \$dbuser,
            'U=s'       => \$dbuser,
            'help'      => \$helpmeplease  );

show_usage() if ( $helpmeplease );

my %xlate_names = (
      'limit' => 'limitation',
      'order' => 'purchaseorder'
    );

my %xlate_booleans = (
      'db' => 'FALSE',
      'no' => 'FALSE',
      'n' => 'FALSE',
      'yes' => 'TRUE',
      'y' => 'TRUE'
    );

############################################################
# Open database connection and set up our queries
############################################################
my $dbh = DBI->connect("dbi:Pg:dbname=$dbname", $dbuser, "", { AutoCommit => 0 } ) or die "Can't connect to database $dbname";

# Get the Progress metadata from the last 12 lines...
open(DFILE, "<", $dfile) or die "cannot read from '$dfile' ";
seek( DFILE, -20, 2);
# Skip to the end of everything line (solo '.')
while( <DFILE> ) { /^\.$/ && last; }
# Seek position for metadata is last line of file...
my $metapos = <DFILE>;
chomp $metapos;
seek( DFILE, $metapos, 0);

my $mrecords = 0;
my $mtname   = "";
my $mdtformat = "dmy";
my $mdtanchor = "1950";

while( <DFILE> ) {
  if ( /^filename=(.+)\s*$/ ) {
    $mtname = fix_name($1);
  }
  elsif ( /^records=([0-9]+)\s*$/ ) {
    $mrecords = $1 * 1;
  }
  elsif ( /^dateformat=([dmy]+)-([0-9]+)\s*$/ ) {
    $mdtformat = $1;
    $mdtanchor = $2;
  }
}

$tname = $mtname if ( $tname eq "" );

my $field_types = $dbh->selectall_arrayref( <<EOQ  ) or die $dbh->errstr;
SELECT attname
     , tp.typname
  FROM pg_class t
  JOIN pg_attribute f ON (t.oid = f.attrelid)
  JOIN pg_type tp ON (tp.oid = f.atttypid)
  JOIN _progress_field_data pfd ON ( tname = relname AND fname = f.attname AND attribute = 'ORDER' )
 WHERE relname = '$tname'
   AND attnum >= 0
 ORDER BY pfd.value::int;
EOQ

my $num_fields = $#$field_types + 1;
my $field_list = '';

for ( my $i=0; $i < $num_fields; $i++ ) {
  $field_list .= ", " if ( $i > 0 );
  $field_list .= $$field_types[$i][0];
}

print STDERR "Found $num_fields fields in table $tname\n" if ( $debug );

print "COPY $tname ( $field_list ) FROM stdin;\n";

# And back to the beginning, now we have some metadata
seek( DFILE, 0, 0);
my $lno = 0;
my $record_count = 0;
while( <DFILE> ) {
  $lno++;
  /^\.$/ && last;  # Data ends with a solo '.'

  s/\r//g;  # Remove any carriage returns

  my $fields = split_fields( $_ );
  my $fcount = $#$fields + 1;

  if ( $fcount != $num_fields ) {
    print STDERR "WARNING: field count mismatch on input line $lno\n";
  }

  for ( my $i = 0; $i < $fcount ; $i++ ) {
    print "\t" if ( $i > 0 );
    print $$fields[$i];
  }
  print "\n";
  if ( $record_count++ > 2000 ) {
    $record_count = 0;
    print "\\.\n";
    print "COPY $tname ( $field_list ) FROM stdin;\n";
  }
}
print "\\.\n";

############################################################
############################################################
# Called to fix the case and content of a name
############################################################
sub fix_name {
  my $name = shift ;
  $name =~ tr/A-Z-/a-z_/;
  $name = $xlate_names{$name} if ( defined($xlate_names{$name}) );
  return $name;
}


############################################################
# Called to split a line into an array of fields
############################################################
sub split_fields {
  my $line = shift ;

  my $fcount = 0;
  my @flds = ();
  my $field = "";
  while( defined($line) && $line ne "" ) {
    $line =~ s/^ // if ( $fcount > 0 );
    if ( $line =~ /^"/ ) {
      ($field, $line) = get_quoted_field( $line );
    }
    else {
      ($field, $line) = split / /, $line, 2;
      $line = ' '.$line if ( defined($line));
    }

    $field =~ s/\\/\\\\/g;
    $field =~ s/'/\\047/g;
    $field =~ s/�/é/g;
    $field =~ s/\t/\\011/g;
    $field =~ s/\n/\\n/gm;
    $field =~ s/\r//gm;
    $field =~ s/\000/\\000/g;
    $field =~ s/\000/\\000/g;

    $field = encode_utf8($field);

    my $ftype = $field_types->[$fcount][1];
    # print STDERR "DBG: F<<$field>>, type<<$ftype>>\n" if ( $debug );
    if ( $ftype =~ /(int|double)/ ) {
      # Do nothing;
      $field = 0 if ( $field eq "" );
    }
    elsif ( $ftype =~ /text/ ) {
      # $field = "'$field'" unless( $field eq "?" );
    }
    elsif ( $ftype =~ /bool/ ) {
      if ( $field eq "no" ) {
        $field = "FALSE";
      }
      else {
        $field = "TRUE";
      }
    }
    $field = '\\N' if ( $field eq "?" );
    $flds[$fcount++] = $field;
  }
  return \@flds;
}


############################################################
# Called to get the field up to the next un-escaped quote,
# and possibly across multiple lines.
############################################################
sub get_quoted_field {
  my $line = shift ;

  my $field = "";

  $line =~ s/^"//;

  # Replace the delimiting quote with a carriage return - we know
  # that we have removed all of the pre-existing ones.

  # I think that this could be flawed in some situations that do
  # not occur at present in the Catalyst APMS data.  See regextest
  # script and data if you want to improve it.

  $line =~ s/((?<="")|(?<!"))"(?=( |$))/\r/;

  $line =~ /^(.*)\r(.*)$/ && do {
    $field = $1;
    $line = $2;

    print STDERR "DBG: $lno F<<$field>>, L<<$line>>\n" if ( $debug );
    return ( $field, $line );
  };

  # Somehow we have lost our trailling newline at this point, so we refund it here...
  $line .= "\n";

  while( <DFILE> ) {
    $field .= $line;
    s/\r//g;  # Remove any carriage returns

    # Over-complex regex again.  See notes above.
    s/((?<="")|(?<!"))"(?=( |$))/\r/;

    $line  = $_;
    $lno++;

    $line =~ /^(.*)\r(.*)$/ && do {
      $field .= $1;
      $line = $2;

      print STDERR "DBG: $lno F<<$field>>, L<<$line>>\n" if ( $debug );
      return ( $field, $line );
    };
  }
  $field .= $line;
  $line = "";

  print STDERR "WARN: $lno unexpected end of file.\n" if ( $debug );
  return ( $field, $line );
}

############################################################
# Tell the nice user how we do things.  Short and sweet.
############################################################
sub show_usage {
    print <<OPTHELP;

data-2-postgresql.pl [options] -f <filename.d>

Options are:
    --debug       Turn on debugging
    --table       Specify a tablename to use
    --file        Specify a data filename (required)
    --dbname      The database to dig into
    --user        Connect to the database as this user.

The program will take the source data file and output a COPY
command for PostgreSQL that should be able to be used to load
the data into a PostgreSQL database using psql.

The database connection is used to ensure that the fields are
formatted appropriately for the type of content they should
be receiving.

OPTHELP
    exit 0;
}

