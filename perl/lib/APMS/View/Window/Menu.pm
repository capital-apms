package APMS::View::Window::Menu;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Buttons;

# Base class providing destroy methods etc
use base qw/
	APMS::View::Window
	APMS::View::ButtonsWindow
/;

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	# Create the window
	$self->new_window();

	# Pack buttons into a box
	$self->{buttons} = APMS::View::Buttons->button_pack( 'V', $data{buttons} );

	# Pack the box into the window
	$self->get_window->add( $self->{buttons}->box );
	$self->get_window->show_all;

	return $self;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

