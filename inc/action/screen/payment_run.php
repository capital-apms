<?php
/**
* Component for browsing vouchers for payment
* @package   apms
* @subpackage   approve_vouchers
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/

param_to_global('status','#^[UA]#i');  if ( isset($status) ) $status = strtoupper($status); else $status = 'U';

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'status', 'char1', "SELECT voucherstatus, description FROM voucherstatus ORDER BY 1;" );
$widget->AddField( 'creditor', 'int1', "SELECT 0, ' --- All Creditors --- ' UNION SELECT creditorcode, name FROM creditor WHERE EXISTS(SELECT 1 FROM voucher WHERE voucher.creditorcode = creditor.creditorcode AND voucherstatus = 'A') ORDER BY 2;" );
$widget->ReadWrite();
$widget->Defaults( array('status' => 'A', 'creditorcode' => 0 ) );
$widget->Layout( '<table><tr><th>Creditor:</th><td>##creditor.select##</td><th>Status:</th><td>##status.select##</td><td>##Show.submit##</td></tr></table>' );
$page_elements[] = $widget;


/**
* The main browse, which is a form, in this case.
*/
$browser = new Browser("Payment Run");
$c->page_title = $browser->Title();

if ( isset($et) && isset($ec) ) {
  $qry = new PgQuery( "SELECT get_entity_type_name(?) AS entitytypename, get_entity_name(?,?) AS entityname;", $et, $et, $ec);
  if ( $qry->Exec("action-$component") && $row = $qry->Fetch() ) {
    $browser->Title("$row->entitytypename Vouchers for $row->entityname ($et$ec)");
  }
}

$browser->AddColumn( 'voucherseq', '#', 'center' );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'creditor_name', 'Creditor', 'center', '', 'creditor.name' );
$browser->AddColumn( 'voucherstatus', 'Approve', 'center', '<input type="checkbox" name="approve[##voucherseq##]" value="t">' );
$browser->AddColumn( 'invoicereference', 'Invoice#', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'ourorderno', 'Order', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'description', 'Description' );
$browser->AddColumn( 'goodsvalue', 'Goods', 'right', '%0.2lf' );
$browser->AddColumn( 'taxvalue', 'Tax', 'right', '%0.2lf' );
$browser->AddColumn( 'total', 'Total', 'right', '%0.2lf', '(goodsvalue + taxvalue)' );
$browser->SetJoins( "voucher JOIN creditor USING (creditorcode) " );

$browser->SetWhere( "voucher.voucherstatus=" . qpg($widget->Value('status')) );
if ( $widget->Value('creditor') > 0 )
  $browser->AndWhere( "voucher.creditorcode=".qpg($widget->Value('creditor')) );
$browser->AddOrder( 'voucherseq', 'ASC', 1 );
$rowurl = '/view.php?t=voucher&id=%s';
$browser->RowFormat( "<tr onclick=\"Toggle('approve[%s]');\" title=\"Click to toggle voucher payment\" class=\"r%d\">\n", "</tr>\n", 'voucherseq', '#even' );
$browser->SetDiv( '<div class="browser"><form method="POST">', '</div>' );
$browser_html = $browser->Render();


$widget = new Widget();
$widget->AddField( 'bankaccountcode', 'char1', "SELECT bankaccountcode, bankaccountcode || ' - ' || accountname || ', ' || bankname || ', ' || bankbranchname FROM bankaccount WHERE active AND chequeaccount ORDER BY 1" );
$widget->AddField( 'nextchequeno', 'int1' );
$widget->ReadWrite();

$next_cheque = 1;
$qry = new PgQuery( 'SELECT chequeno + 1 AS next_cheque FROM cheque WHERE bankaccountcode = ? ORDER BY bankaccountcode DESC, chequeno DESC LIMIT 1', $widget->Value('bankaccountcode') );
if ( $qry->Exec('payment_run') && $qry->rows == 1 ) {
  $row = $qry->Fetch();
  $next_cheque = $row->next_cheque;
}

// Initialise rather than Default, to force the values.
$widget->Initialise( array('nextchequeno' => $next_cheque, 'chequedate' => date('Y-m-d')) );

$layout = <<<EOLAYOUT
$browser_html
<table width="100%">
 <tr>
  <th align="right" width="15%">Bank:</th>
  <td>##bankaccountcode.select##</td>
 </tr>
 <tr>
  <th align="right">Cheque No:</th>
  <td>##nextchequeno.input.6##</td>
 </tr>
 <tr>
  <th align="right">Cheque Date:</th>
  <td>##chequedate.date##</td>
 </tr>
 <tr>
  <td> </td>
  <td>##Approve.submit##</td>
 </tr>
</table>
EOLAYOUT;
$widget->Layout( $layout );

$page_elements[] = $widget;
