<?php

// Viewer component for tenant records
$viewer = new Viewer("Debtor", 'debtor_maintenance');
$viewer->AddField( 'entityname', 'get_entity_name(debtor_et,debtor_ec)' );
$viewer->AddField( 'balance', "(SELECT TO_CHAR( COALESCE(balance,0.0),'999G999G990D00PR') FROM accountsummary WHERE entitytype='T' AND entitycode=debtorcode)" );
$viewer->AddField( 'billto', "billperson_first || ' ' || billperson_last" );
$viewer->AddField( 'debtclassification_name', "COALESCE( debtclassification.description, 'Unknown')" );
$viewer->AddField( 'varianceclassification_name', "COALESCE( varianceclassification.description, 'Unknown')" );
$viewer->SetJoins( 'debtor_maintenance LEFT JOIN debtclassification USING ( debtclassification ) LEFT JOIN varianceclassification USING ( varianceclassification ) ' );
$viewer->SetWhere( 'debtorcode=' . intval($id) );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Debtor:</th>
  <td class="center">##debtorcode.value##</td>
  <td class="left">##debtor_name.value##</td>
 </tr>
 <tr>
  <th colspan="1" rowspan="3">Billing Details</th>
  <th class="right">Address:</th>
  <td class="left">##bill_address.textarea.40x2##</td>
 </tr>
 <tr>
  <th class="right">City:</th>
  <td class="left">##bill_city.input.16##, ##bill_state.input.16##</td>
 </tr>
 <tr>
  <th class="right">Country:</th>
  <td class="left">##bill_country.input.20## ##bill_zip.input.11##</td>
 </tr>
 <tr>
  <th colspan="1" rowspan="3">Contact</th>
  <th class="right">Person:</th>
  <td class="left">##billperson_title.options## ##billperson_first.input.15## ##billperson_last.input.20##</td>
 </tr>
 <tr>
  <th class="right">Job Title:</th>
  <td class="left">##billperson_job.input.41##</td>
 </tr>
 <tr>
  <th class="right">Phone</th>
  <td class="left">+##busphone_isd.input.5## ( ##busphone_std.input.6## ) ##busphone_no.input.33##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">Active:</th>
  <td class="left">##debtor_active.yesno##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">Debt class:</th>
  <td class="left">##debtclassification_name.options##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">Variance class:</th>
  <td class="left">##varianceclassification_name.options##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">Account Ledger:</th>
  <td class="left">##debtor_et.options## ##debtor_ec.input.5## - ##debtor_entity_name.enc##</td>
 </tr>
 <tr>
  <th class="right">Balance:</th>
  <td class="left" colspan="2">##balance.value##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Debtor: ".$viewer->Record->{'debtor_name'});
$page_elements[] = $viewer;

include_once("menus_entityaccount.php");

// And let's show a transaction browser below the tenant details...
require_once('classBrowser.php');
$browser = new Browser("Outstanding Transactions");
$browser->AddHidden( 'document', "batchcode || '.' || documentcode" );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'reference', 'Reference', 'left', '', "COALESCE(accttran.reference, document.reference)" );
$browser->AddColumn( 'description', 'Description', 'left', '', "COALESCE(accttran.description, document.description)" );
$browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$browser->AddTotal( 'amount' );
$browser->SetJoins( "accttran JOIN document USING (batchcode, documentcode)" );
$browser->SetWhere( "entitytype='T' AND entitycode=".intval($id)." AND closedstate != 'F'" );
$browser->AddOrder( 'date', 'ASC' );
$rowurl = '/view.php?t=document&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'document', '#even' );
$browser->DoQuery();
$page_elements[] = $browser;

// Now for an Invoice browser
$browser = new Browser("Invoices in Last 12 Months");
$browser->AddColumn( 'invoiceno', 'Invoice#', 'center' );
$browser->AddColumn( 'invoicedate', 'Date', 'center' );
$browser->AddColumn( 'todetail', 'Brief', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'blurb', 'Invoice Details' );
$browser->AddColumn( 'total', 'Amount', 'right', '%0.2lf' );
$browser->AddTotal( 'total' );
$browser->SetJoins( "invoice" );
// $browser->SetJoins( "invoice JOIN invoiceline USING (invoiceno)" );
$browser->SetWhere( "invoice.entitytype='T' AND invoice.entitycode=".intval($id)." " );
$browser->AndWhere( "invoicedate >= (current_timestamp - '1 year'::interval)" );
$browser->AddOrder( 'invoiceno', 'ASC', 1 );
$rowurl = '/view.php?t=invoice&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'invoiceno', '#even' );
$browser->DoQuery();
$page_elements[] = $browser;

