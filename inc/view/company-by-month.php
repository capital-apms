<?php

// Viewer component for company records
$viewer = new Viewer("Company", 'company');
$viewer->SetWhere( 'companycode='.$id );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Company:</th>
  <td class="center">##companycode.value##</td>
  <td class="left">##legalname.value##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Company: ".$viewer->Record->{'legalname'});
$page_elements[] = $viewer;

if ( !isset($et) ) {
  $et = 'L';
  $ec = $id;
}
include_once("menus_entityaccount.php");

// Now list income accounts
require_once('classBrowser.php');
include_once("classWidgets.php");

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'year', 'int1', "SELECT 0, '--- All Years ---' UNION SELECT financialyearcode, description FROM financialyear ORDER BY 1" );
$widget->ReadWrite();
$widget->Defaults( array('year' => $year ) );
$widget->Layout( '<table> <tr> <th>For year:</th> <td>##year.select##</td> <td>##Show.submit##</td> </tr></table>' );

$page_elements[] = $widget;


$months = array();
$qry = new PgQuery( "SELECT * FROM month WHERE financialyearcode = ? ORDER BY startdate", $widget->Record->{'year'} );
if ( $qry->Exec("company-by-month") ) {
  while( $row = $qry->Fetch() ) {
    $months[] = $row;
  }
}

$template = new Browser("Accounts");
$template->AddHidden( 'account', "summary.entitytype || '-' || TO_CHAR(summary.entitycode,'FM00009') || '-' || TO_CHAR(summary.accountcode,'FM0009.00')" );
$template->AddHidden( 'entitytype', 'summary.entitytype' );
$template->AddHidden( 'entitycode', 'summary.entitycode' );
$template->AddHidden( 'accountgroupcode' );
$template->AddColumn( 'accountcode', 'Account', 'right', '', "TO_CHAR(summary.accountcode,'FM0009.00')" );
$template->AddColumn( 'name', 'Name', 'left', '<td class="left" style="width:30em;">%s</td>', 'chartofaccount.name' );
$month_joins = "";
foreach( $months AS $k => $v ) {
  $template->AddColumn( "month_$k", sprintf("<small>%s</small>", substr($v->startdate,0,7)), 'right', '%0.2lf', "(CASE WHEN COALESCE(creditgroup,FALSE) THEN -1 ELSE 1 END * month_$k.balance)" );
  $month_joins .= "LEFT OUTER JOIN accountbalance month_$k ON (summary.entitytype=month_$k.entitytype AND summary.entitycode=month_$k.entitycode AND summary.accountcode=month_$k.accountcode AND month_$k.monthcode=$v->monthcode) ";
  $template->AddTotal( "month_$k" );
}
$template->AddColumn( 'summarybalance', 'Balance', 'right', '%0.2lf', '(CASE WHEN COALESCE(creditgroup,FALSE) THEN -1 ELSE 1 END * summary.balance)' );
$template->AddTotal( 'summarybalance' );
$template->SetWhere( "summary.entitytype='L' AND summary.entitycode=$id AND summary.balance != 0.0" );
$rowurl = '/view.php?t=account&id=%s';
$template->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d\">\n", "</tr>\n", 'account', '#even' );

/**
* Income and expenditure accounts just look at the current financial year
*/
$template->SetJoins( "yearsummary summary LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode ) $month_joins" );

$browser = clone($template);
$browser->Title("Income Accounts");
$browser->AndWhere("grouptype = 'P' AND creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 1 );
$browser->AndWhere( "financialyearcode = ".qpg($widget->Record->{'year'}) );
$page_elements[] = $browser;

$browser = clone($template);
$browser->Title("Expense Accounts");
$browser->AndWhere("grouptype = 'P' AND NOT creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 2 );
$browser->AndWhere( "financialyearcode = ".qpg($widget->Record->{'year'}) );
$page_elements[] = $browser;

/**
* Asset and liability accounts look at the full history
*/
$template->SetJoins( "accountsummary summary LEFT JOIN chartofaccount USING ( accountcode ) LEFT JOIN accountgroup USING ( accountgroupcode ) $month_joins" );

$browser = clone($template);
$browser->Title("Asset Accounts");
$browser->AndWhere("grouptype != 'P' AND NOT creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 3 );
$page_elements[] = $browser;

$browser = clone($template);
$browser->Title("Liability Accounts");
$browser->AndWhere("grouptype != 'P' AND creditgroup" );
$browser->SetOrdering( 'accountcode', 'ASC', 4 );
$page_elements[] = $browser;

$browser = clone($template);
$browser->Title("Bogus Accounts Incorrectly Set Up");
$browser->AndWhere("grouptype IS NULL" );
$browser->SetOrdering( 'accountcode', 'ASC', 5 );
$page_elements[] = $browser;

