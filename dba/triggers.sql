-- APMS Trigger Functions


-----------------------------------------------------------------
-- When we insert, update or delete an accttran record, we need
-- a corresponding update to the accountbalance record.
-----------------------------------------------------------------
DROP TRIGGER accttran_iud ON accttran CASCADE;
CREATE or REPLACE FUNCTION update_accttran() RETURNS trigger AS '
  DECLARE
    junk INT4;
  BEGIN

    IF TG_OP != ''DELETE'' THEN
      IF TG_OP = ''UPDATE'' THEN
        IF NEW.entitytype = OLD.entitytype AND NEW.entitycode = OLD.entitycode
             AND NEW.accountcode = OLD.accountcode AND NEW.monthcode = OLD.monthcode AND NEW.amount = OLD.amount
        THEN
          -- Nothing for us to do
          RETURN NEW;
        END IF;

        IF NEW.entitytype = OLD.entitytype AND NEW.entitycode = OLD.entitycode
             AND NEW.accountcode = OLD.accountcode AND NEW.monthcode = OLD.monthcode
        THEN
          -- We also try and delete it, if that is reasonable...
          DELETE FROM accountbalance
                      WHERE entitytype = OLD.entitytype
                        AND entitycode = OLD.entitycode
                        AND accountcode = OLD.accountcode
                        AND monthcode = OLD.monthcode
                        AND balance = OLD.amount - NEW.amount
                        AND budget = 0::numeric
                        AND revisedbudget = 0::numeric
                        AND notecode IS NULL ;
          IF NOT FOUND THEN
            SELECT 1 INTO junk FROM accountbalance
                          WHERE entitytype = OLD.entitytype
                            AND entitycode = OLD.entitycode
                            AND accountcode = OLD.accountcode
                            AND monthcode = OLD.monthcode;
            IF FOUND THEN
              UPDATE accountbalance SET balance = balance - OLD.amount + NEW.amount
                        WHERE entitytype = OLD.entitytype
                          AND entitycode = OLD.entitycode
                          AND accountcode = OLD.accountcode
                          AND monthcode = OLD.monthcode;
            ELSE
              INSERT INTO accountbalance (entitytype, entitycode, accountcode, monthcode, balance, budget, revisedbudget)
                      VALUES(OLD.entitytype, OLD.entitycode, OLD.accountcode, OLD.monthcode, NEW.amount, 0.0, 0.0);
            END IF;
          END IF;
          RETURN NEW;
        END IF;
      END IF;
      -- Check that necessary fields are not null..
      IF NEW.amount IS NULL THEN
          RAISE EXCEPTION ''accttran amount cannot be null'';
      END IF;
      IF NEW.monthcode IS NULL THEN
          RAISE EXCEPTION ''accttran monthcode cannot be null'';
      END IF;
      NEW.entitytype := upper(NEW.entitytype);
      IF NEW.entitytype IS NULL OR NEW.entitytype NOT IN (''L'', ''P'', ''T'', ''C'', ''J'') THEN
          RAISE EXCEPTION ''accttran entitytype must be one of ''''L'''', ''''P'''', ''''T'''', ''''C'''' or ''''J'''' '';
      END IF;
      IF NEW.entitycode IS NULL THEN
          RAISE EXCEPTION ''accttran entitycode cannot be null '';
      END IF;
      IF NEW.accountcode IS NULL THEN
          RAISE EXCEPTION ''accttran accountcode cannot be null '';
      END IF;
      IF NEW.date IS NULL THEN
          RAISE EXCEPTION ''accttran date cannot be null '';
      END IF;
    END IF;

    IF TG_OP = ''DELETE'' OR TG_OP = ''UPDATE'' THEN
      -- We also try and delete it, if that is reasonable...
      DELETE FROM accountbalance
                  WHERE entitytype = OLD.entitytype
                    AND entitycode = OLD.entitycode
                    AND accountcode = OLD.accountcode
                    AND monthcode = OLD.monthcode
                    AND balance = OLD.amount
                    AND budget = 0::numeric
                    AND revisedbudget = 0::numeric
                    AND notecode IS NULL ;
      IF NOT FOUND THEN
        SELECT 1 INTO junk FROM accountbalance
                      WHERE entitytype = OLD.entitytype
                        AND entitycode = OLD.entitycode
                        AND accountcode = OLD.accountcode
                        AND monthcode = OLD.monthcode;
        IF FOUND THEN
          UPDATE accountbalance SET balance = balance - OLD.amount
                    WHERE entitytype = OLD.entitytype
                      AND entitycode = OLD.entitycode
                      AND accountcode = OLD.accountcode
                      AND monthcode = OLD.monthcode;
        ELSE
          INSERT INTO accountbalance (entitytype, entitycode, accountcode, monthcode, balance, budget, revisedbudget)
                      VALUES(OLD.entitytype, OLD.entitycode, OLD.accountcode, OLD.monthcode, 0 - OLD.amount, 0.0, 0.0);
        END IF;
      END IF;
      IF TG_OP = ''DELETE'' THEN
        -- We are done!
        RETURN OLD;
      END IF;
    END IF;

    SELECT 1 INTO junk FROM accountbalance
                  WHERE entitytype = NEW.entitytype
                    AND entitycode = NEW.entitycode
                    AND accountcode = NEW.accountcode
                    AND monthcode = NEW.monthcode;
    IF FOUND THEN
      -- We also try and delete it, if that is reasonable...
      DELETE FROM accountbalance
                  WHERE entitytype = NEW.entitytype
                    AND entitycode = NEW.entitycode
                    AND accountcode = NEW.accountcode
                    AND monthcode = NEW.monthcode
                    AND balance = 0::numeric - NEW.amount
                    AND budget = 0::numeric
                    AND revisedbudget = 0::numeric
                    AND notecode IS NULL ;
      IF NOT FOUND THEN
        UPDATE accountbalance SET balance = balance + NEW.amount
                    WHERE entitytype = NEW.entitytype
                      AND entitycode = NEW.entitycode
                      AND accountcode = NEW.accountcode
                      AND monthcode = NEW.monthcode;
      END IF;
    ELSE
      INSERT INTO accountbalance (entitytype, entitycode, accountcode, monthcode, balance, budget, revisedbudget)
                      VALUES(NEW.entitytype, NEW.entitycode, NEW.accountcode, NEW.monthcode, NEW.amount, 0.0, 0.0);
    END IF;
    RETURN NEW;
  END;
' LANGUAGE plpgsql;

CREATE TRIGGER accttran_iud BEFORE INSERT OR UPDATE OR DELETE ON accttran
    FOR EACH ROW EXECUTE PROCEDURE update_accttran();


-----------------------------------------------------------------
-- When we insert, update or delete an accountbalance record, we
-- need a corresponding update to the accountsummary record.
-- Since this procedure is called from within transaction update
-- through the trigger on accttran, special effort has gone into
-- trying to minimise the actual number of database reads/writes.
-- Sometimes that might make it look arcane, like having some
-- pre-emptive DELETEs...
-----------------------------------------------------------------
DROP TRIGGER accountbalance_iud ON accountbalance CASCADE;
CREATE or REPLACE FUNCTION update_accountbalance() RETURNS trigger AS '
  DECLARE
    junk INT4;
  BEGIN

    IF TG_OP != ''DELETE'' THEN
      IF TG_OP = ''UPDATE'' THEN
        IF NEW.entitytype = OLD.entitytype AND NEW.entitycode = OLD.entitycode
             AND NEW.accountcode = OLD.accountcode AND NEW.balance = OLD.balance
             AND NEW.budget = OLD.budget AND NEW.revisedbudget = OLD.revisedbudget
        THEN
          -- Nothing for us to do
          RETURN NEW;
        END IF;

        IF NEW.entitytype = OLD.entitytype AND NEW.entitycode = OLD.entitycode
             AND NEW.accountcode = OLD.accountcode
        THEN
          -- We try and delete first, if that is reasonable...
          DELETE FROM accountsummary
                      WHERE entitytype = OLD.entitytype
                        AND entitycode = OLD.entitycode
                        AND accountcode = OLD.accountcode
                        AND balance = OLD.balance - NEW.balance
                        AND budget = OLD.budget - NEW.budget
                        AND revisedbudget = OLD.revisedbudget - NEW.revisedbudget
                        AND notecode IS NULL ;
          IF NOT FOUND THEN
            -- Only the balance/budget/revised has changed, so a single update/insert is sufficient
            SELECT 1 INTO junk FROM accountsummary
                        WHERE entitytype = OLD.entitytype
                          AND entitycode = OLD.entitycode
                          AND accountcode = OLD.accountcode;
            IF FOUND THEN
              UPDATE accountsummary SET balance = balance - OLD.balance + NEW.balance,
                                  budget = budget - OLD.budget + NEW.budget,
                                  revisedbudget = revisedbudget - OLD.revisedbudget + NEW.revisedbudget
                    WHERE entitytype = OLD.entitytype
                      AND entitycode = OLD.entitycode
                      AND accountcode = OLD.accountcode;
            ELSE
              INSERT INTO accountsummary (entitytype, entitycode, accountcode, balance, budget, revisedbudget)
                    VALUES( OLD.entitytype, OLD.entitycode, OLD.accountcode, NEW.balance - OLD.balance,
                            NEW.budget - OLD.budget, NEW.revisedbudget - OLD.revisedbudget );
            END IF;
          END IF;
          RETURN NEW;
        END IF;
      END IF;

      -- Check that necessary fields are not null..
      IF NEW.balance IS NULL THEN
          RAISE EXCEPTION ''accountbalance balance cannot be null'';
      END IF;
      IF NEW.monthcode IS NULL THEN
          RAISE EXCEPTION ''accountbalance monthcode cannot be null'';
      END IF;
      IF NEW.entitytype IS NULL OR NEW.entitytype NOT IN (''L'', ''P'', ''T'', ''C'', ''J'') THEN
          RAISE EXCEPTION ''accountbalance entitytype must be one of ''''L'''', ''''P'''', ''''T'''', ''''C'''' or ''''J'''' '';
      END IF;
      IF NEW.entitycode IS NULL THEN
          RAISE EXCEPTION ''accountbalance entitycode cannot be null '';
      END IF;
      IF NEW.accountcode IS NULL THEN
          RAISE EXCEPTION ''accountbalance accountcode cannot be null '';
      END IF;
    END IF;

    IF TG_OP = ''DELETE'' OR TG_OP = ''UPDATE'' THEN
      -- We try and delete first, if that is reasonable...
      DELETE FROM accountsummary
                  WHERE entitytype = OLD.entitytype
                    AND entitycode = OLD.entitycode
                    AND accountcode = OLD.accountcode
                    AND balance = OLD.balance
                    AND budget = OLD.budget
                    AND revisedbudget = OLD.revisedbudget
                    AND notecode IS NULL ;
      IF NOT FOUND THEN
        -- OK, we have to update or insert it...
        SELECT 1 INTO junk FROM accountsummary
                    WHERE entitytype = OLD.entitytype
                      AND entitycode = OLD.entitycode
                      AND accountcode = OLD.accountcode;
        IF FOUND THEN
          UPDATE accountsummary SET balance = balance - OLD.balance,
                                  budget = budget - OLD.budget,
                                  revisedbudget = revisedbudget - OLD.revisedbudget
                    WHERE entitytype = OLD.entitytype
                      AND entitycode = OLD.entitycode
                      AND accountcode = OLD.accountcode;
        ELSE
          INSERT INTO accountsummary (entitytype, entitycode, accountcode, balance, budget, revisedbudget)
                VALUES( OLD.entitytype, OLD.entitycode, OLD.accountcode,
                          0 - OLD.balance,  0 - OLD.budget,  0 - OLD.revisedbudget );
        END IF;
      END IF;
      IF TG_OP = ''DELETE'' THEN
        -- We are done!
        RETURN OLD;
      END IF;
    END IF;

    SELECT 1 INTO junk FROM accountsummary
                  WHERE entitytype = NEW.entitytype
                    AND entitycode = NEW.entitycode
                    AND accountcode = NEW.accountcode;
    IF FOUND THEN
      -- We try and delete first, if that is reasonable...
      DELETE FROM accountsummary
                  WHERE entitytype = NEW.entitytype
                    AND entitycode = NEW.entitycode
                    AND accountcode = NEW.accountcode
                    AND balance = 0::numeric - NEW.balance
                    AND budget = 0::numeric - NEW.budget
                    AND revisedbudget = 0::numeric - NEW.revisedbudget
                    AND notecode IS NULL ;
      IF NOT FOUND THEN
        UPDATE accountsummary SET balance = balance + NEW.balance,
                                  budget = budget + NEW.budget,
                                  revisedbudget = revisedbudget + NEW.revisedbudget
                    WHERE entitytype = NEW.entitytype
                      AND entitycode = NEW.entitycode
                      AND accountcode = NEW.accountcode;
      END IF;
    ELSE
      INSERT INTO accountsummary (entitytype, entitycode, accountcode, balance, budget, revisedbudget)
             VALUES(NEW.entitytype, NEW.entitycode, NEW.accountcode, NEW.balance, NEW.budget, NEW.revisedbudget);
    END IF;
    RETURN NEW;
  END;
' LANGUAGE plpgsql;

CREATE TRIGGER accountbalance_iud BEFORE INSERT OR UPDATE OR DELETE ON accountbalance
    FOR EACH ROW EXECUTE PROCEDURE update_accountbalance();
