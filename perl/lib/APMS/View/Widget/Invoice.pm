package APMS::View::Widget::Invoice;

=item APMS::View::Widget::Invoice

A widget for displaying invoice lines from an invoice

=cut

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Browser;

sub invoice_lines {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	$self->{browser} = APMS::View::Browser->browse_window(
		fields => [
			{
				name   => 'T',
				column => 'lentitytype',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
			}, {
				name   => 'Code',
				column => 'lentitycode',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
			}, {
				name   => 'Account',
				column => 'accountcode',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
			}, {
				name   => 'Description',
				column => 'accounttext',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
				expand => 1,
			}, {
				name   => 'Amount',
				column => 'amount',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
			}, {
				name   => 'Percent',
				column => 'percent',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText',
			}, {
				name   => 'Share',
				column => 'yourshare',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => $data{invoicelines},
	);

	# Make chart of account names available
	$self->{accountnames} = $data{accountnames};

	# Attach callback for selection change
	my $selection = $self->{browser}->get_selection();
	$selection->signal_connect( 'changed' => sub { $self->row_selected( @_ ) } );
	#$selection->set_select_function( sub { $self->row_selected( @_ ) } );

	# Scrolled window for browse widget
	my $scroll = Gtk2::ScrolledWindow->new();
	$scroll->set_policy( 'never', 'always' );
	$scroll->add( $self->{browser} );

	# Nice border
	my $viewport = Gtk2::Viewport->new();
	$viewport->add( $scroll );

	# A box down the bottom showing the name of the entered account code
	my $accountbox = Gtk2::HBox->new( 0, 0 );
	my $label = Gtk2::Label->new( 'Account Name:' );
	$accountbox->pack_start( $label, 0, 0, 0 );
	$self->{accountname} = Gtk2::Label->new( '' );
	$accountbox->pack_start( $self->{accountname}, 0, 0, 0 );

	# Pack it all in
	$self->{box} = Gtk2::VBox->new( 0, 0 );
	$self->{box}->pack_start( $viewport, 1, 1, 0 );
	$self->{box}->pack_start( $accountbox, 0, 0, 3 );

	return $self;
}

sub box {
	my $self = shift;
	return $self->{box};
}

sub set_accountname {
	my ( $self, $text ) = @_;
	$self->{accountname}->set_text( $text );
}

sub row_selected {
	my ( $self, $selection ) = @_;

	my ( $model, $iter ) = $selection->get_selected();
	my $accountcode = $model->get_value( $iter, 2 );
	if ( exists $self->{accountnames}->{$accountcode} ) {
		$self->{accountname}->set_text( $self->{accountnames}->{$accountcode} );
	}

	return 0;
}

1;

