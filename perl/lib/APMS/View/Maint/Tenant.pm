package APMS::View::Maint::Tenant;

use strict;
use APMS::Class qw/ new_object /;
use APMS::View::Widget::Packing;
use APMS::View::Widget::Button;
use APMS::View::Widget::Entity;
use APMS::View::Widget::Combo::Payment;
use APMS::View::Widget::Combo::Contact;

use base qw/ APMS::View /;

=item new

Construct the tenant edit window

  my $tenantwindow = APMS::View::Viewer::Maint::Tenant->new(
    tenantrecord  => <hashref>,
    paymentstyles => <arrayref>,
    debtclasses   => <arrayref>,
    variances     => <arrayref>,
    contacttypes  => <arrayref>,
    addresstypes  => <arrayref>,
    phonetypes    => <arrayref>
  );

=cut

sub new {
	my ( $proto, %data ) = @_;
	my $self = new_object( $proto );

	unless ( exists $data{tenantrecord} ) {
		die "Must pass either a blank tenant record, or an existing tenant record to this";
	}
	my $tenantrecord = $data{tenantrecord};

	# NOTE: The intent of placing widgets inside $self is to make them
	# available for manipulation by logic elsewhere. If its not in $self
	# then its just a formatting widget, not something that will have
	# callbacks attached

	# Working VBox or HBox and a working label
	my ( $wbox, $label );

	# The window
	$self->new_window();
	$self->get_window->realize();

	# The entire window holds a vbox for all the sub-components
	my $rootbox = Gtk2::VBox->new( 0, 5 );
	$rootbox->set_border_width( '5' );
	$self->get_window->add( $rootbox );

	######################################################
	# The first row: the tenant number box
	$wbox = Gtk2::HBox->new( 0, 5 );
	$rootbox->pack_start( $wbox, 0, 1, 0 );

	# Tenant: text
	$label = Gtk2::Label->new( 'Tenant:' );
	$wbox->pack_start( $label, 0, 0, 0 );

	$self->{tenantid} = Gtk2::Entry->new();
	$self->{tenantid}->set_max_length( '0' );
	$self->{tenantid}->set_size_request( 100, 25 );
	$wbox->pack_start( $self->{tenantid}, 0, 1, 0 );

	# Initial fill of the tenant record
	$self->{tenantid}->set_text( $data{tenantrecord}->{tenantcode} );

	$self->{active} = Gtk2::CheckButton->new_with_label( 'Active' );
	$self->{active}->set_active( $tenantrecord->{active} );
	$wbox->pack_end( $self->{active}, 0, 0, 0 );


	######################################################
	# Second row: Legal Name, Trading, Entity etc
	my $detailstable = Gtk2::Table->new( 5, 2, 0 );
	$detailstable->set_row_spacings( '5' );
	$detailstable->set_col_spacings( '5' );
	$detailstable->set_border_width( '5' );
	$rootbox->pack_start( $detailstable, 0, 0, 0 );

	# Table labels
	$label = APMS::View::Widget::Packing->new_right_label( 'Legal Name:' );
	$detailstable->attach( $label, 0, 1, 0, 1, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Trading As:' );
	$detailstable->attach( $label, 0, 1, 1, 2, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Entity:' );
	$detailstable->attach( $label, 0, 1, 2, 3, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Pays By:' );
	$detailstable->attach( $label, 0, 1, 3, 4, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Quality:' );
	$detailstable->attach( $label, 0, 1, 4, 5, ['fill'], ['fill', 'expand'], 0, 0 );

	# Entry elements
	$self->{legalname} = Gtk2::Entry->new();
	$self->{legalname}->set_text( $tenantrecord->{legalname} );
	$detailstable->attach( $self->{legalname}, 1, 2, 0, 1, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	$self->{tradingas} = Gtk2::Entry->new();
	$self->{tradingas}->set_text( $tenantrecord->{name} );
	$detailstable->attach( $self->{tradingas}, 1, 2, 1, 2, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	# Entity entry
	$self->{entity} = APMS::View::Widget::Entity->entity_select();
	$self->{entity}->set_entity_details(
		$tenantrecord->{entitytype},
		$tenantrecord->{entitycode},
		$tenantrecord->{entityname}
	);
	$detailstable->attach( $self->{entity}->box, 1, 2, 2, 3, ['fill'], ['fill', 'expand'], 0, 0 );

	# Pays by and debt class
	my $paydebtbox = Gtk2::HBox->new( 0, 5 );
	my $paymentstyle = APMS::View::Widget::Combo::Payment->paymentstyle_combo( $data{paymentstyles} );
	$paymentstyle->set_active( 0 );
	$paymentstyle->set_size_request( 225, 35 );
	$paydebtbox->pack_start( $paymentstyle, 0, 1, 0 );

	my $debtclass = APMS::View::Widget::Combo::Payment->debtclass_combo( $data{debtclasses} );
	$debtclass->set_active( 0 );
	$debtclass->set_size_request( 225, 35 );
	$paydebtbox->pack_end( $debtclass, 0, 1, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Debt Class:' );
	$paydebtbox->pack_end( $label, 0, 1, 0 );

	$detailstable->attach( $paydebtbox, 1, 2, 3, 4, ['fill'], ['fill', 'expand'], 0, 0 );

	# Quality and variance
	my $qualitybox = Gtk2::HBox->new( 0, 5 );
	$self->{quality} = Gtk2::Entry->new();
	$self->{quality}->set_size_request( 50, 25 );
	$qualitybox->pack_start( $self->{quality}, 0, 1, 0 );

	my $variance = APMS::View::Widget::Combo::Payment->variance_combo( $data{variances} );
	$variance->set_active( 0 );
	$variance->set_size_request( 225, 35 );
	$qualitybox->pack_end( $variance, 0, 1, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Variance:' );
	$qualitybox->pack_end( $label, 0, 1, 0 );

	$detailstable->attach( $qualitybox, 1, 2, 4, 5, ['fill'], ['fill', 'expand'], 0, 0 );

	# 3mths+ etc
	my $contractbox = Gtk2::HBox->new( 0, 5 );
	$rootbox->pack_start( $contractbox, 0, 1, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( '3mths +:' );
	$contractbox->pack_start( $label, 0, 0, 0 );

	$self->{mnth3} = Gtk2::Entry->new();
	$self->{mnth3}->set_size_request( 100, 25 );
	$contractbox->pack_start( $self->{mnth3}, 0, 0, 0 );
	
	$label = APMS::View::Widget::Packing->new_right_label( '2mths:' );
	$contractbox->pack_start( $label, 0, 0, 0 );

	$self->{mnth2} = Gtk2::Entry->new();
	$self->{mnth2}->set_size_request( 100, 25 );
	$contractbox->pack_start( $self->{mnth2}, 0, 0, 0 );
	
	$label = APMS::View::Widget::Packing->new_right_label( '1mths:' );
	$contractbox->pack_start( $label, 0, 0, 0 );

	$self->{mnth1} = Gtk2::Entry->new();
	$self->{mnth1}->set_size_request( 100, 25 );
	$contractbox->pack_start( $self->{mnth1}, 0, 0, 0 );
	
	$label = APMS::View::Widget::Packing->new_right_label( 'Current:' );
	$contractbox->pack_start( $label, 0, 0, 0 );

	$self->{current} = Gtk2::Entry->new();
	$self->{current}->set_size_request( 100, 25 );
	$contractbox->pack_start( $self->{current}, 0, 0, 0 );
	
	# Horozontal rule
	my $hrule = Gtk2::HSeparator->new();
	$rootbox->pack_start( $hrule, 0, 1, 0 );

	######################################################
	# Third row: Contact, Person, Company, Address
	my $addresstable = Gtk2::Table->new( 4, 2, 0 );
	$addresstable->set_row_spacings( '5' );
	$addresstable->set_col_spacings( '5' );
	$addresstable->set_border_width( '5' );
	$rootbox->pack_start( $addresstable, 0, 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Contact:' );
	$addresstable->attach( $label, 0, 1, 0, 1, ['fill'], ['fill', 'expand'], 0, 0 );

	# Things in the first column
	$label = APMS::View::Widget::Packing->new_right_label( 'Person:' );
	$addresstable->attach( $label, 0, 1, 1, 2, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Company:' );
	$addresstable->attach( $label, 0, 1, 2, 3, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Address:' );
	$addresstable->attach( $label, 0, 1, 3, 4, ['fill'], ['fill', 'expand'], 0, 0 );

	# Things in the second comumn
	$self->{contacttype} = APMS::View::Widget::Combo::Contact->tenant_contact_combo( $data{contacttypes} );
	$self->{contacttype}->set_active( 0 );
	$self->{contacttype}->set_size_request( 250, 35 );
	my $contactalign = Gtk2::HBox->new( 0, 0 );
	$contactalign->pack_start( $self->{contacttype}, 0, 0, 0 );
	$addresstable->attach( $contactalign, 1, 2, 0, 1, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	my $personbox = Gtk2::HBox->new( 0, 5 );
	$self->{chooseperson} = APMS::View::Widget::Button->jump_to_button();
	$personbox->pack_start( $self->{chooseperson}, 0, 1, 0 );
	$self->{person} = Gtk2::Entry->new();
	$personbox->pack_start( $self->{person}, 1, 1, 0 );
	$addresstable->attach( $personbox, 1, 2, 1, 2, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	$self->{company} = Gtk2::Entry->new();
	$addresstable->attach( $self->{company}, 1, 2, 2, 3, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	$self->{addresstype} = APMS::View::Widget::Combo::Contact->address_type_combo( $data{addresstypes} );
	$self->{addresstype}->set_active( 0 );
	$self->{addresstype}->set_size_request( 250, 35 );
	my $addresstypealign = Gtk2::HBox->new( 0, 0 );
	$addresstypealign->pack_start( $self->{addresstype}, 0, 0, 0 );

	$self->{clearbutton} = APMS::View::Widget::Button->clear_button();
	$self->{clearbutton}->set_size_request( 90, 30 );
	$addresstypealign->pack_start( $self->{clearbutton}, 0, 0, 0 );

	$addresstable->attach( $addresstypealign, 1, 2, 3, 4, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	######################################################
	# Forth row: Address text and City State etc
	$wbox = Gtk2::HBox->new( 0, 5 );
	$rootbox->pack_start( $wbox, 0, 1, 0 );

	# Address text
	$self->{addresstext} = Gtk2::TextBuffer->new();
	$self->{addressedit} = Gtk2::TextView->new_with_buffer( $self->{addresstext} );

	my $scrolledwindow = Gtk2::ScrolledWindow->new( undef, undef );
	$scrolledwindow->set_shadow_type( 'in' );
	$scrolledwindow->add( $self->{addressedit} );
	$wbox->pack_start( $scrolledwindow, 1, 1, 0 );

	my $ziptable = Gtk2::Table->new( 4, 2, 0 );
	$ziptable->set_row_spacings( '5' );
	$ziptable->set_col_spacings( '5' );
	$ziptable->set_border_width( '5' );
	$wbox->pack_end( $ziptable, 0, 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'City:' );
	$ziptable->attach( $label, 0, 1, 0, 1, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'State:' );
	$ziptable->attach( $label, 0, 1, 1, 2, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Country:' );
	$ziptable->attach( $label, 0, 1, 2, 3, ['fill'], ['fill', 'expand'], 0, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Zip:' );
	$ziptable->attach( $label, 0, 1, 3, 4, ['fill'], ['fill', 'expand'], 0, 0 );

	$self->{city} = Gtk2::Entry->new();
	$ziptable->attach( $self->{city}, 1, 2, 0, 1, ['fill'], ['fill', 'expand'], 0, 0 );

	$self->{state} = Gtk2::Entry->new();
	$ziptable->attach( $self->{state}, 1, 2, 1, 2, ['fill'], ['fill', 'expand'], 0, 0 );

	$self->{country} = Gtk2::Entry->new();
	$ziptable->attach( $self->{country}, 1, 2, 2, 3, ['fill'], ['fill', 'expand'], 0, 0 );

	$self->{zip} = Gtk2::Entry->new();
	$ziptable->attach( $self->{zip}, 1, 2, 3, 4, ['fill'], ['fill', 'expand'], 0, 0 );

	######################################################
	# Fifth row: Phone numbers and email addresses
	my $phonetable = Gtk2::Table->new( 3, 4, 0 );
	$phonetable->set_row_spacings( '5' );
	$phonetable->set_col_spacings( '5' );
	$phonetable->set_border_width( '5' );
	$rootbox->pack_start( $phonetable, 0, 1, 0 );

	$label = APMS::View::Widget::Packing->new_right_label( 'Telephones:' );
	$phonetable->attach( $label, 0, 1, 0, 1, ['fill'], ['fill', 'expand'], 0, 0 );

	# Phone types
	$self->{phonetype1} = APMS::View::Widget::Combo::Contact->phone_type_combo( $data{phonetypes} );
	$self->{phonetype1}->set_active( 0 );
	$self->{phonetype1}->set_size_request( 200, 35 );
	$phonetable->attach( $self->{phonetype1}, 0, 1, 1, 2, [], ['fill', 'expand'], 0, 0 );
	$self->{phonetype2} = APMS::View::Widget::Combo::Contact->phone_type_combo( $data{phonetypes} );
	$self->{phonetype2}->set_active( 1 );
	$self->{phonetype2}->set_size_request( 200, 35 );
	$phonetable->attach( $self->{phonetype2}, 2, 3, 1, 2, [], ['fill', 'expand'], 0, 0 );
	$self->{phonetype3} = APMS::View::Widget::Combo::Contact->phone_type_combo( $data{phonetypes} );
	$self->{phonetype3}->set_active( 2 );
	$self->{phonetype3}->set_size_request( 200, 35 );
	$phonetable->attach( $self->{phonetype3}, 0, 1, 2, 3, [], ['fill', 'expand'], 0, 0 );
	$self->{phonetype4} = APMS::View::Widget::Combo::Contact->phone_type_combo( $data{phonetypes} );
	$self->{phonetype4}->set_active( 3 );
	$self->{phonetype4}->set_size_request( 200, 35 );
	$phonetable->attach( $self->{phonetype4}, 2, 3, 2, 3, [], ['fill', 'expand'], 0, 0 );

	# Phone entries
	$self->{phonenumber1} = Gtk2::Entry->new();
	$phonetable->attach( $self->{phonenumber1}, 1, 2, 1, 2, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );
	$self->{phonenumber2} = Gtk2::Entry->new();
	$phonetable->attach( $self->{phonenumber2}, 3, 4, 1, 2, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );
	$self->{phonenumber3} = Gtk2::Entry->new();
	$phonetable->attach( $self->{phonenumber3}, 1, 2, 2, 3, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );
	$self->{phonenumber4} = Gtk2::Entry->new();
	$phonetable->attach( $self->{phonenumber4}, 3, 4, 2, 3, ['fill', 'expand'], ['fill', 'expand'], 0, 0 );

	# Save and cancel buttons
	my $buttonbox = Gtk2::HBox->new( 0, 5 );
	$self->{save} = APMS::View::Widget::Button->save_button();
	$buttonbox->pack_end( $self->{save}, 0, 0, 0 );
	$self->{cancel} = APMS::View::Widget::Button->cancel_button();
	$buttonbox->pack_end( $self->{cancel}, 0, 0, 0 );
	$rootbox->pack_end( $buttonbox, 0, 0, 0 );

	# Show everything
	$self->get_window->show_all();

	return $self;
}

sub fill_person_details {
	my ( $self, %field ) = @_;

	my $nametext;
	$nametext .= $field{persontitle} if $field{persontitle};
	$nametext .= $field{firstname} . " " . $field{lastname};

	$self->{person}->set_text( $nametext );
	$self->{company}->set_text( $field{company} );

	# Since a person was found, enable related fields
	$self->enable_widgets( 'addressedit', 'city', 'state', 'country', 'zip' );
	$self->enable_widgets( 'addresstype' );
	$self->enable_widgets( 'phonenumber1', 'phonenumber2', 'phonenumber3', 'phonenumber4' );
	$self->enable_widgets( 'phonetype1', 'phonetype2', 'phonetype3', 'phonetype4' );
}

sub blank_person_details {
	my ( $self ) = @_;

	# Blank the person details
	$self->{person}->set_text( "" );
	$self->{company}->set_text( "" );

	# Blank the address details and disable appropriate fields
	#$self->{addresstype}->set_active( 0 );
	$self->blank_address_details();
	$self->disable_widgets( 'addressedit', 'city', 'state', 'country', 'zip' );
	$self->disable_widgets( 'addresstype' );

	# Blank the phone numbers and disable the phone fields
	$self->blank_phone_detail( id => 1 );
	$self->blank_phone_detail( id => 2 );
	$self->blank_phone_detail( id => 3 );
	$self->blank_phone_detail( id => 4 );
	$self->disable_widgets( 'phonenumber1', 'phonenumber2', 'phonenumber3', 'phonenumber4' );
	$self->disable_widgets( 'phonetype1', 'phonetype2', 'phonetype3', 'phonetype4' );
}

sub fill_address_details {
	my ( $self, %field ) = @_;

	$self->{addresstext}->set_text( $field{address} );
	$self->{city}->set_text( $field{city} );
	$self->{state}->set_text( $field{state} );
	$self->{country}->set_text( $field{country} );
	$self->{zip}->set_text( $field{zip} );
}

sub blank_address_details {
	my ( $self ) = @_;

	$self->{addresstext}->set_text( "" );
	$self->{city}->set_text( "" );
	$self->{state}->set_text( "" );
	$self->{country}->set_text( "" );
	$self->{zip}->set_text( "" );
}

sub fill_phone_detail {
	my ( $self, %data ) = @_;

	my $numberident = 'phonenumber' . $data{id};
	$self->{"$numberident"}->set_text( $data{number} );
}

sub blank_phone_detail {
	my ( $self, %data ) = @_;

	my $numberident = 'phonenumber' . $data{id};
	$self->{"$numberident"}->set_text( "" );
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

