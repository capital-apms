<?php
/** @todo Should have active/inactive and find name pattern widgets on here. */

$browser = new Browser("Users");
$browser->AddColumn( 'user_no', 'No.', 'center' );
$browser->AddColumn( 'username', 'Username' );
$browser->AddColumn( 'fullname', 'Full Name' );
$browser->SetJoins( "usr" );
$browser->SetWhere( 'active' );
$browser->AddOrder( 'username', 'ASC', 1 );

$rowurl = '/view.php?t=user&id=%d';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Display User Detail\" class=\"r%d\">\n", "</tr>\n", 'user_no', '#even' );
$browser->DoQuery();
$page_elements[] = $browser;

$c->page_title = "Browse Users";
