CREATE OR REPLACE VIEW yearsummary AS
SELECT entitytype, entitycode, accountcode
, sum(balance) AS balance
, financialyearcode
FROM accountbalance LEFT JOIN month USING (monthcode)
GROUP BY entitytype, entitycode, accountcode, financialyearcode;

GRANT SELECT ON yearsummary TO general;

CREATE OR REPLACE VIEW year_balance_sheet AS
SELECT entitytype, entitycode, accountcode
, sum(balance) AS balance
, financialyear.financialyearcode
FROM accountbalance LEFT JOIN month USING (monthcode) LEFT JOIN financialyear ON month.financialyearcode <= financialyear.financialyearcode
GROUP BY entitytype, entitycode, accountcode, financialyear.financialyearcode;

GRANT SELECT ON year_balance_sheet TO general;

-- DROP VIEW menu_links CASCADE;
CREATE OR REPLACE VIEW menu_links AS
     SELECT DISTINCT ON ( linkcode ) linkcode, sequencecode, nodecode AS menucode FROM linknode JOIN usrgroupmenuitem ON (menuname = description) ;
GRANT SELECT ON menu_links TO general;

-- DROP VIEW menu_links;
CREATE OR REPLACE VIEW button_links AS
SELECT menucode, buttonlabel, linktype, source, target, programlink.linkcode, viewer,
       sortpanel, filterpanel, programlink.description, function, sequencecode
  FROM menu_links JOIN programlink USING (linkcode) ;
GRANT SELECT ON button_links TO general;

