<?php

// In this case we only want to process when $_POST['submit'] is set.
if ( $widget->IsSubmit() ) return;

get_office_accounts( 'CREDITORS', 'DEBTORS' );

$sundry_debtors = $office_accounts['DEBTORS']->accountcode;
$sundry_creditors = $office_accounts['CREDITORS']->accountcode;

require_once( "transaction_helpers.php" );

param_to_global('bankaccountcode', '/^[a-z0-9 ,_-]+$/i');
param_to_global('description');
if ( !isset($bankaccountcode) ) $bankaccountcode = 'CHEQ';
$qry = new AwlQuery( "SELECT * FROM bankaccount WHERE bankaccountcode = ?", $bankaccountcode );
if ( ! $qry->Exec('bank-import',__LINE__,__FILE__) || $qry->rows() == 0 || ! $bank = $qry->Fetch() ) {
  $c->messages[] = "Could not read bank account '$bankaccountcode'";
  return;
}

$transactions = $_POST['txn'];
if ( count($transactions) > 0 ) {
  $batchcode = create_newbatch( 'AUTO', 0, $description );

  $documentcode=0;
  for( $dcode=1; $dcode <= count($transactions); $dcode++ ) {
    $doc = $transactions[$dcode];
    $new_doc = true;
    $transactioncode = 1;
    for( $tcode=1; $tcode <= count($doc); $tcode++ ) {
      $tx = $doc[$tcode];
      if ( $tx['entitytype'] == 'X' ) continue;

      if ( $new_doc ) {
        create_newdocument( $tx['description'], $tx['reference'], 'BANK' );
        $new_doc = false;
        $amount = 0;
        foreach( $doc AS $v ) {
          $amount += $v['amount'];
        }
        create_newtransaction( 'L', $bank->companycode, $bank->accountcode, $tx['date'], $amount );
      }

      if ( $tx['entitytype'] == 'T' ) $ac = $sundry_debtors;
      if ( $tx['entitytype'] == 'C' ) $ac = $sundry_creditors;
      else $ac = $tx['accountcode'];
      create_newtransaction( $tx['entitytype'], $tx['entitycode'], $ac, $tx['date'], -$tx['amount'] );
    }

  }
  $sql = "UPDATE newbatch
   SET documentcount = (SELECT count(1) FROM newdocument d WHERE d.batchcode = newbatch.batchcode),
       total = (SELECT sum(amount) FROM newaccttrans t WHERE t.batchcode = newbatch.batchcode)
   WHERE batchcode = :batchcode";
  $qry = new AwlQuery( $sql, array( ':batchcode' => $batchcode) );
  if ( !$qry->Exec("bank-import",__LINE__,__FILE__) ) {
    $c->messages[] = "Database transaction failure - please correct problem and retry.";
  }
  $qry->Commit();
}