<?php

param_to_global( 'creditorcode', 'int' );

/**
* Add some widgets onto the top of the browser
*/
include_once("classWidgets.php");
$widget = new Widget("browse-$component");
$widget->AddField( 'year', 'int1', "SELECT 0, '--- All Years ---' UNION SELECT financialyearcode, description FROM financialyear ORDER BY 1;" );
$widget->AddField( 'status', 'char1', "SELECT '[^PC]', '--- Yet to be Paid ---', -10 UNION SELECT '.', '--- Any Status ---', -1 UNION SELECT voucherstatus, description, sequencecode FROM voucherstatus ORDER BY 3;" );
$widget->ReadWrite();
$widget->Defaults( array( 'year' => $year, 'status' => '[^PC]')  );
$widget->Layout( '<table> <tr>
<th>Status:</th> <td>##status.select##</td>
<th>For year:</th> <td>##year.select##</td>
<td>##Show.submit##</td> </tr> </table>' );
$page_elements[] = $widget;

$related_menu->AddOption("New Voucher","/edit.php?t=voucher","Create a new voucher", false, 3100);

$browser = new Browser("Vouchers");
$browser->AddColumn( 'voucherseq', 'Vchr#', 'right' );
$browser->AddHidden( 'vchrst', "'vchrst'||lower(voucherstatus)" );
$browser->AddColumn( 'voucherstatus', 'S', 'center' );
$browser->AddColumn( 'date', 'Date', 'center' );
$browser->AddColumn( 'invoicereference', 'Invoice#', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'ourorderno', 'Order', 'left', '<span style="white-space:nowrap;">%s</span>' );
$browser->AddColumn( 'description', 'Description' );
$browser->AddColumn( 'goodsvalue', 'Goods', 'right', '%0.2lf' );
$browser->AddColumn( 'taxvalue', 'Tax', 'right', '%0.2lf' );
$browser->AddColumn( 'total', 'Total', 'right', '%0.2lf', '(goodsvalue + taxvalue)' );
$browser->AddTotal( 'goodsvalue' );
$browser->AddTotal( 'taxvalue' );
$browser->AddTotal( 'total' );
$browser->SetJoins( "voucher " );
if ( isset($creditorcode) ) {
  $qry = new PgQuery( "SELECT name FROM creditor WHERE creditorcode = ?;", $creditorcode );
  if ( $qry->Exec("Browse::Vouchers") && $row = $qry->Fetch() ) {
    $browser->Title("Vouchers for $row->name (C$creditorcode)");
  }
  $browser->AndWhere( "voucher.creditorcode=$creditorcode" );
}
if ( $widget->Record->{'status'} != '' ) {
  $browser->AndWhere( "voucherstatus ~* " .qpg($widget->Record->{'status'}) );
}
if ( $widget->Record->{'year'} > 0 ) {
  $browser->SetJoins("voucher JOIN month ON month.startdate <= voucher.date AND month.enddate >= voucher.date ");
  $browser->AndWhere( "financialyearcode = " . qpg($widget->Record->{'year'}) );
}
$browser->AddOrder( 'voucherseq', 'DESC', 1 );
$rowurl = '/view.php?t=voucher&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" class=\"r%d%s\">\n", "</tr>\n",
                         'voucherseq', '#even', 'vchrst' );
$browser->DoQuery();
$c->page_title = $browser->Title();
$page_elements[] = $browser;

include_once("menus_entityaccount.php");
