<?php

param_to_global( 'id', '#^\d+$#', 'oldcreditorcode', 'creditorcode' );

$editor = new Editor("Creditor", "creditor_maintenance");
$editor->SetLookup( 'vchrentitytype', 'SELECT entitytype, description FROM entitytype ORDER BY entitytype' );
$editor->SetLookup( 'paymentstyle', 'SELECT paymentstyle, description FROM paymentstyle ORDER BY paymentstyle' );
$editor->AddField( 'accountname', '(SELECT name FROM chartofaccount coa WHERE coa.accountcode = vchraccountcode)' );
$editor->AddAttribute( 'paymentstyle', 'style', 'width:9em' );

if ( isset($id) ) $editor->SetWhere( "creditorcode=$id" );

if ( isset($_POST[$editor->SubmitName]) ) {
  if ( isset($_POST['vchrentitycode']) && intval($_POST['vchrentitycode']) == 0 ) $_POST['vchrentitycode'] = null;
  if ( isset($_POST['vchraccountcode']) && intval($_POST['vchraccountcode']) == 0 ) $_POST['vchraccountcode'] = null;
  $editor->WhereNewRecord( "creditorcode=currval('creditor_creditorcode_seq')" );
  $editor->Write();
}
else {
  $editor->GetRecord();
  if ( ! $editor->Available() ) {
    $editor->Initialise( array('vchrentitytype' => 'L', 'chequespermonth' => 5, 'creditor_active' => 't',
                               'payment_style' => 'DC', 'creditorcode' => '', 'creditor_name' => '', ) );
  }
}
$id = $editor->Value('creditorcode');



$editor->SetOptionList( 'pymtperson_title', array('Mr' => 'Mr', 'Ms' => 'Ms', 'Mrs' => 'Mrs' ), (isset($editor->Record->{'billperson_title'})?$editor->Record->{'billperson_title'}:'') );
// $editor->SetOptionList( 'companycode', "SELECT companycode, name FROM company WHERE active ORDER BY companycode" );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Creditor:</th>
  <td class="center">##creditorcode.enc##<input type="hidden" name="oldcreditorcode" value="##creditorcode.enc##"></td>
  <td class="left">##creditor_name.input.50##</td>
 </tr>
 <tr>
  <th class="right">Active:</th>
  <td class="center">##creditor_active.checkbox##</td>
  <td>
   <table class="form_inner">
    <tr>
     <th class="right">Payment style:</th>
     <td class="left">##paymentstyle.select##</td>
     <th class="right">Cheques/mth:</th>
     <td class="left">##chequespermonth.input.2##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right" colspan="2">Payee Name:</th>
  <td class="left">##payeename.input.50##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">Bank A/C Details:</th>
  <td class="left">##bankdetails.input.50##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">DC Statement Text:</th>
  <td class="left">##dcstatementtext.input.36##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">DC Remittance Email:</th>
  <td class="left">##dcremittanceemail.input.50##</td>
 </tr>
 <tr>
  <th class="right" colspan="2">Voucher Coding:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left">##vchrentitytype.select##</td>
     <td class="left">##vchrentitycode.input.4##</td>
     <td class="left">##vchraccountcode.input.7## ##accountname.value##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th colspan="1" rowspan="3">Payment Details</th>
  <th class="right">Address</th>
  <td class="left">##pymt_address.textarea.40x2##</td>
 </tr>
 <tr>
  <th class="right">City:</th>
  <td class="left">##pymt_city.input.16## State:##pymt_state.input.16##</td>
 </tr>
 <tr>
  <th class="right">Country:</th>
  <td class="left">##pymt_country.input.20## Zip:##pymt_zip.input.11##</td>
 </tr>
 <tr>
  <th colspan="1" rowspan="3">Contact</th>
  <th class="right">Person:</th>
  <td class="left">##pymtperson_title.select## ##pymtperson_first.input.15## ##pymtperson_last.input.20##</td>
 </tr>
 <tr>
  <th class="right">Job Title:</th>
  <td class="left">##pymtperson_job.input.41##</td>
 </tr>
 <tr>
  <th class="right">Phone</th>
  <td class="left">+##busphone_isd.input.5## ( ##busphone_std.input.6## ) ##busphone_no.input.33##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left" colspan="2">##submit##</td>
 </tr>
</table>

EOTEMPLATE;

$editor->Layout( $template );

$c->page_title = $editor->Title("Creditor: ".$editor->Value('creditor_name'));
$page_elements[] = $editor;

include_once("menus_entityaccount.php");
