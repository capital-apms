<?php

$browser = new Browser("Posted Batches");
$browser->AddColumn( 'batchcode', 'Batch', 'center' );
// $browser->AddColumn( 'batchtype', 'Type' );
$browser->AddColumn( 'documentcount', 'Documents' );
$browser->AddColumn( 'description', 'Description' );
$browser->AddColumn( 'total', 'Total', 'right', '%0.2lf', 'COALESCE(total,0.0)' );
$browser->AddColumn( 'updatedby', 'Updated By' );

$browser->SetJoins( "batch b LEFT JOIN usr u ON u.user_no = b.operatorcode" );

$browser->AddOrder( 'batchcode', 'DESC', 1 );

$rowurl = '/view.php?t=batch&id=%d';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Display Batch Detail\" class=\"r%d\">\n", "</tr>\n", 'batchcode', '#even' );
$browser->SetLimit(300);
$browser->DoQuery();

$c->page_title = "Browse Posted Batches";

$page_elements[] = $browser;