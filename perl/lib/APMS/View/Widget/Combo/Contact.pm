package APMS::View::Widget::Combo::Contact;

use strict;
use APMS::View::Widget::Combo;

sub tenant_contact_combo {
	my ( $self, $data ) = @_;

	my $combobox = APMS::View::Widget::Combo->combo_select(
		fields => [
			{
				column => 'name',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => $data
	);

	return $combobox;
}

sub contact_type_combo {
	my ( $self, $data ) = @_;

	my $combobox = APMS::View::Widget::Combo->combo_select(
		fields => [
			{
				column => 'description',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => $data
	);

	return $combobox;
}

sub address_type_combo {
	my ( $self, $data ) = @_;

	my $combobox = APMS::View::Widget::Combo->combo_select(
		fields => [
			{
				column => 'description',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => $data
	);

	return $combobox;
}

sub phone_type_combo {
	my ( $self, $data ) = @_;

	my $combobox = APMS::View::Widget::Combo->combo_select(
		fields => [
			{
				column => 'description',
				type   => 'Glib::String',
				widget => 'Gtk2::CellRendererText'
			}
		],
		data => $data
	);

	return $combobox;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

