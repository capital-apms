<?php

if ( isset($batch) ) return true;

$widget->AddField( 'companycode', 'int1', "SELECT companycode, companycode::text || ' - ' || legalname FROM company ORDER BY companycode;" );
$widget->AddField( 'period_start', 'int2', "SELECT monthcode, startdate FROM month ORDER BY startdate;" );
$widget->AddField( 'period_end', 'int3', "SELECT monthcode, enddate FROM month ORDER BY startdate;" );
$widget->ReadWrite();
$c->page_title = $widget->Title("Create Period GST Batch");
$widget->Layout( '<table>
 <tr>
  <th class="right">Ledger:</th>
  <td class="left">##companycode.select##</td>
 </tr>
 <tr>
  <th class="right">Period:</th>
  <td class="left">##period_start.select## to ##period_end.select##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left">##Create Batch.submit##</td>
 </tr>
</table>' );

$page_elements[] = $widget;
