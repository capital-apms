<?php

// Editor component for company records
$editor = new Editor("User", 'usr');
$editor->AddField( 'date_format_type', null, "SELECT 'E', 'European' UNION SELECT 'U', 'US Format'" );
$editor->SetBaseTable( "usr" );
param_to_global('id', 'int', 'olduser_no', 'user_no' );
$editor->SetWhere( 'user_no='.$id );

$pwstars = '@@@@@@@@@@';
if ( $editor->IsSubmit() ) {
  $editor->WhereNewRecord( "user_no=(SELECT CURRVAL('usr_user_no_seq'))" );
  unset($_POST['password']);
  if ( $_POST['newpass1'] != '' && $_POST['newpass1'] != $pwstars ) {
    if ( $_POST['newpass1'] == $_POST['newpass2'] ) {
      $_POST['password'] = $_POST['newpass1'];
    }
    else {
      $c->messages[] = "Password not updated. The supplied passwords do not match.";
    }
  }
  $editor->Write();
}
else {
  $editor->GetRecord();
}
$c->page_title = $editor->Title("User: ".$editor->Record->{'fullname'});

$id = $editor->Record->{'user_no'};

$template = <<<EOTEMPLATE
##form##
<table>
 <tr> <th class="right">User ID:</th>           <td class="left">##user_no.value##</td> </tr>
 <tr> <th class="right">Username:</th>          <td class="left">##xxxxusername.input.10##</td> </tr>
 <tr> <th class="right">Change Password:</th>   <td class="left">##newpass1.password.$pwstars##</td> </tr>
 <tr> <th class="right">Confirm Password:</th>  <td class="left">##newpass2.password.$pwstars##</td> </tr>
 <tr> <th class="right">Full name:</th>         <td class="left">##fullname.input.50##</td> </tr>
 <tr> <th class="right">EMail:</th>             <td class="left">##email.input.50##</td> </tr>
 <tr> <th class="right">Date Format Type:</th>  <td class="left">##date_format_type.select##</td> </tr>
 <tr> <th class="right"></th>                   <td class="left" colspan="2">##submit##</td> </tr>
</table>
</form>
EOTEMPLATE;

$editor->SetTemplate( $template );

$page_elements[] = $editor;
