<?php

$c->scripts[] = "js/jquery.js";

param_to_global('orig_invoiceno', '#[0-9]+#');
//param_to_global('entitycode', 'int', 'tenantcode', 'creditorcode');
if ( !isset($orig_invoiceno) ) {
  param_to_global('copy', '#[0-9]+#');
  if ( isset($copy) ) {
    $q = new AwlQuery("SELECT nextval('invoice_invoiceno_seq')");
    if ( $q->Exec() && $row = $q->Fetch(true) ) {
      $id = $row[0];
      $q->Begin();
      $q->QDo("INSERT INTO invoice
       (invoiceno, invoicestatus, invoicedate, entitytype, entitycode, topay, todetail, taxapplies, termscode, taxamount, total, blurb, invoicetype, attnto )
        SELECT $id, 'U', current_date, entitytype, entitycode, topay, todetail, taxapplies, termscode, taxamount, total, blurb, invoicetype, attnto
         FROM invoice WHERE invoiceno = ?", $copy);      
      $q->QDo("INSERT INTO invoiceline 
      (invoiceno, lineseq, entitytype, entitycode, accountcode, accounttext, quantity, amount, percent, yourshare )
       SELECT $id, lineseq, entitytype, entitycode, accountcode, accounttext, quantity, amount, percent, yourshare
        FROM invoiceline WHERE invoiceno = ?", $copy);
      $q->Commit();
      $orig_invoiceno = $id;
    }
  }
}

// Editor component for invoice
$editor = new Editor("Invoice", 'invoice');
$editor->AddField( 'orig_invoiceno', 'invoice.invoiceno' );
$editor->AddField( 'etname', 'get_entity_type_name(entitytype)' );
$editor->AddField( 'loweretname', 'lower(get_entity_type_name(entitytype))' );
$editor->AddField( 'entityname', 'get_entity_name(entitytype,entitycode)' );
$editor->AddField( 'termscode', null, 'SELECT termscode, description FROM invoiceterms ORDER BY termscode' );
$editor->AddField( 'invoice_amount', '(total + taxamount)' );
$editor->AddAttribute( 'entitytype', 'id', 'entitytype');
$editor->AddAttribute( 'entitycode', 'id', 'entitycode');
$editor->AddAttribute( 'taxapplies', 'onChange', "return calculate_tax_total(document.forms[0].total, document.forms[0].taxamount, document.forms[0].invoice_amount, document.forms[0].taxapplies[1].checked);" );
$editor->AddAttribute( 'total', 'onChange', "return calculate_tax_total(document.forms[0].total, document.forms[0].taxamount, document.forms[0].invoice_amount, document.forms[0].taxapplies[1].checked);" );
$editor->AddAttribute( 'total', 'onKeydown', "return EnterToNextField(event,document.forms[0].taxamount);" );
$editor->AddAttribute( 'taxamount', 'onChange', "return calculate_total(document.forms[0].taxamount, document.forms[0].total, document.forms[0].invoice_amount);" );
$editor->AddAttribute( 'taxamount', 'onKeydown', "return EnterToNextField(event,document.forms[0].invoice_amount);" );
$editor->AddAttribute( 'invoice_amount', 'onChange', "return calculate_tax_split(document.forms[0].invoice_amount, document.forms[0].total, document.forms[0].taxamount, document.forms[0].taxapplies[1].checked );" );
$editor->AddAttribute( 'invoice_amount', 'onKeydown', "return EnterToNextField(event,document.forms[0].submit);" );
$editor->AddAttribute( 'entitycode', 'onBlur', "return validate_positive_integer(self);" );
$editor->AddAttribute( 'entitycode', 'title', "The debtor to be charged for this invoice." );
$editor->SetWhere( "invoiceno=".(isset($id)?$id:'0'));
$page_elements[] = $editor;

// Editor component for invoice line
$editrow = new Editor("InvoiceLine", 'invoiceline');
$editrow->AddField( 'line_invoiceno', 'invoiceline.invoiceno' );
$editrow->AddField( 'etname', 'get_entity_type_name(entitytype)' );
$editrow->AddField( 'my_lineseq', 'lineseq' );
$editrow->AddField( 'accountcode', 'accountcode', "SELECT accountcode, c.name FROM chartofaccount c JOIN accountgroup g USING (accountgroupcode) WHERE creditgroup AND grouptype = 'P'" );
$editrow->AddAttribute( 'accountcode', 'style', "width:10em;" );
$editrow->AddAttribute( 'amount', 'onBlur', "return calculate_percent_share(document.forms[1].amount, document.forms[1].percent, document.forms[1].yourshare);" );
$editrow->AddAttribute( 'percent', 'onBlur', "return calculate_percent_share(document.forms[1].amount, document.forms[1].percent, document.forms[1].yourshare);" );
$editrow->SetSubmitName( 'saverow' );

if ( $editor->IsSubmit() ) {
  $editor->WhereNewRecord( " invoiceno=currval('invoice_invoiceno_seq') " );
  $editor->Write();
}
else {
  $editor->GetRecord();
}
if ( $editrow->IsSubmit() ) {
  $_POST['invoiceno'] = $id;
  $is_update = ($_POST['saverow'] == 'Save');
  if ( ! $is_update ) {
    $sql = "SELECT lineseq FROM invoiceline WHERE invoiceno = ? ORDER BY lineseq DESC LIMIT 1";
    $qry = new PgQuery($sql, $id);
    if ( $qry->Exec("edit/invoice") &&  $qry->rows == 1 ) {
      $row = $qry->Fetch();
      $lineseq = $row->lineseq + 1;
    }
    else {
      $lineseq = 1;
    }
    $_POST['lineseq'] = $lineseq;
  }
  else {
    $_POST['lineseq'] = intval($_GET['editseq']);
    $lineseq = $_POST['lineseq'];
  }
  $editrow->SetWhere( "invoiceno=$id AND lineseq=$lineseq");
  $editrow->WhereNewRecord( "invoiceno=$id AND lineseq=$lineseq" );
  $editrow->Write( $is_update );
  unset($_GET['editseq']);
}

$id = (isset($editor->Record->{'orig_invoiceno'}) ? $editor->Record->{'orig_invoiceno'} : null );
$submittype = 'Update';
if ( ! $editor->Available() ) {
  $submittype = 'Create';
  $editor->Initialise( array(
        'invoicedate' => 'today',
        'termscode'  => 'M',
        'taxapplies'  => 't',
        'todetail' => '',
  		'invoicestatus' => 'U',
  		'entitytype' => $et,
        'entitycode' => $id
  	 ));
  if ( isset($et) ) $editor->Record->{'entitytype'} = $et;
  if ( isset($ec) ) $editor->Record->{'entitycode'} = $ec;

}
$editor->SetOptionList( 'entitytype', array('T' => 'Debtor', 'C' => 'Creditor' ), $editor->Record->{'entitytype'} );

$template = <<<EOTEMPLATE
<script type="text/javascript">
function do_lookup(type,et,ec,callback) {
  code = et + ec;
  $.getJSON("lookup.php",{request:type,code:code},callback);
}

function show_entity_name() {
  do_lookup( "entity", $("#entitytype").val(), $("#entitycode").val(), function(result){
    linktype = ( $("#entitytype").val() == "T" ? "debtor" : "creditor");
    $("#entityname").html("<a href=\"view.php?t="+linktype+"&id="+$("#entitycode").val()+"\">"+result.name+"</a>");    
  });
}

$(document).ready(function(){
  show_entity_name();

  $("#entitycode").change(function(e){
    show_entity_name();
  });

  $("#entitycode").click(function(){
  	$("#entitycode").select();
  });
});
</script>
<form method="POST" enctype="multipart/form-data" id="entry">
<table>
 <tr>
  <th class="right">Invoice#:</th>
  <td class="left">##orig_invoiceno.value## ##orig_invoiceno.hidden##</td>
 </tr>
 <tr>
  <th class="right">Date:</th>
  <td>
   <table class="form_inner">
    <tr>
     <td class="left">##invoicedate.input.10##</td>
     <th class="right">Terms:</th>
     <td class="left">##termscode.select##</td>
     <th class="right">GST applies:</th>
     <td class="left">##taxapplies.checkbox##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right">Invoice To:</th>
  <td class="left" style="width:30em">##entitytype.select## ##entitycode.input.5## <span id="entityname">##entityname.value##</span></td>
 </tr>
 <tr>
  <th class="right">Attn:</th>
  <td class="left">##attnto.input.50##</td>
 </tr>
 <tr>
  <th class="right">Re:</th>
  <td class="left">##todetail.input.50##</td>
 </tr>
 <tr>
  <th class="right">Description:</th>
  <td class="left">##blurb.textarea.49x4##</td>
 </tr>
 <tr>
  <th class="right">Amount:</th>
  <td>
   <table width="100%">
    <tr>
     <td class="left">##total.money.12##</td>
     <th class="right">+ Tax:</th>
     <td class="left">##taxamount.money.12##</td>
     <th class="right">Total:</th>
     <td class="left">##invoice_amount.money.12##</td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left">##$submittype.submit##</td>
 </tr>
</table>
</form>

EOTEMPLATE;

$editor->SetTemplate( $template );
$c->page_title = $editor->Title("Invoice $id / ".$editor->Record->{'todetail'});

$et = $editor->Record->{'entitytype'};
$ec = $editor->Record->{'entitycode'};
include_once("menus_entityaccount.php");

$related_menu->AddOption("Print Invoice","/print.php?t=invoice&id=$id","Print this invoice", false, 200);
if ( $editor->Record->{'invoicestatus'} == 'U' ) {
  $related_menu->AddOption("Edit Invoice","/edit.php?t=invoice&id=$id","Edit this invoice", false, 190);
}

if ( $submittype == 'Update' ) {

  function edit_row( $row_data ) {
    global $editrow, $id;

    $form_url = preg_replace( '#&delseq=\d+#', '', $_SERVER['REQUEST_URI'] );
    $rowsubmittype = 'Save';
    if ( $row_data->lineseq == -1 ) {
      $form_url = preg_replace( '#&editseq=\d+#', '', $form_url );
      $rowsubmittype = 'Add';
    }

    $template = <<<EOTEMPLATE
<form method="POST" enctype="multipart/form-data" id="rowentry" action="$form_url">
  <td class="left">##lineseq.value##<input type="hidden" name="id" value="$id"></td>
  <td class="left" style="white-space:nowrap">##entitytype.input.1####entitycode.input.3####accountcode.select##</td>
  <td class="left">##accounttext.input.30##</td>
  <td class="left">##amount.money.8##</td>
  <td class="left">##percent.input.4##</td>
  <td class="left">##yourshare.money.8##</td>
  <td class="left">##$rowsubmittype.submit##</td>
</form>

EOTEMPLATE;

    $editrow->SetTemplate( $template );
    $editrow->SetRecord( $row_data );
    $editrow->Title("");
    return $editrow->Render();
  }

  // And the invoice line details
  require_once('classBrowser.php');
  $browser = new Browser("Invoice Detail");
  $browser->AddHidden( 'etname', 'lower(get_entity_type_name(entitytype))' );
  $browser->AddColumn( 'lineseq', '#', 'right' );
  $browser->AddHidden( 'entitytype' );
  $browser->AddHidden( 'entitycode' );
  $browser->AddHidden( 'accountcode' );
  $browser->AddHidden( 'account', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')" );
  $browser->AddColumn( 'accountname', 'Account', 'left', '', 'chartofaccount.name');
  $browser->AddColumn( 'accounttext', 'Description', 'left' );
  $browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
  $browser->AddColumn( 'percent', 'Percent', 'right', '%0.3lf' );
  $browser->AddColumn( 'yourshare', 'Share', 'right', '%0.2lf' );
  $edit_link = "<a href=\"/edit.php?t=invoice&id=$id&editseq=##lineseq##\" class=\"submit\">Edit</a>";
  $del_link  = "<a href=\"/edit.php?t=invoice&id=$id&delseq=##lineseq##\" class=\"submit\">Del</a>";
  $browser->AddColumn( 'action', 'Action', 'center', '', "'$edit_link&nbsp;$del_link'" );
  $browser->AddOrder( 'lineseq', 'ASC' );
  $browser->SetJoins( "invoiceline LEFT OUTER JOIN chartofaccount USING ( accountcode )" );

  $browser->AndWhere( "invoiceno=$id" );
  $rowurl = '/view.php?t=account&id=%s';
  $browser->RowFormat( "<tr class=\"r%d\">\n", "</tr>\n", 'account', 'account', 'accountname', '#even' );
  $page_elements[] = $browser;

  if ( isset($_GET['delseq']) ) {
    $qry = new PgQuery("DELETE FROM invoiceline WHERE invoiceno=? AND lineseq = ?", $id, intval($_GET['delseq']) );
    $qry->Exec('invoice');
  }
  if ( isset($_GET['editseq']) ) {
    $browser->MatchedRow('lineseq', intval($_GET['editseq']), 'edit_row');
  }
  else {
    $remaining = $editor->Record->{'total'};
    $extra_row = array( 'lineseq' => -1, 'percent' => 100, 'amount' => $remaining, 'yourshare' => $remaining, 'accounttext' => $editor->Value('todetail') );
    $qry = new PgQuery("SELECT entitytype, entitycode, accountcode, percent FROM invoiceline WHERE invoiceno=$id ORDER BY lineseq DESC LIMIT 1");
    if ( $qry->Exec('invoice') && $qry->rows == 1 ) {
      $row = $qry->Fetch();
      $extra_row['entitytype']  = $row->entitytype;
      $extra_row['entitycode']  = $row->entitycode;
      $extra_row['accountcode'] = $row->accountcode;
      $extra_row['accounttext'] = $row->accounttext;
      $extra_row['percent']     = $row->percent;
      $qry = new PgQuery("SELECT SUM(amount) FROM invoiceline WHERE invoiceno=$id");
      if ( $qry->Exec('invoice') && $qry->rows == 1 ) {
        $row = $qry->Fetch();
        $extra_row['amount']    = $remaining - $row->sum;
      }
      $extra_row['yourshare']   = round( ($extra_row['amount'] * ($extra_row['percent'] / 100)), 2);
    }
    $browser->MatchedRow('lineseq', -1, 'edit_row');
    $extra_row = (object) $extra_row;
    $browser->AddRow($extra_row);
  }

}
