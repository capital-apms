<?php
/**
* Functions to provide assistance when creating new transactions
* @package   apms
* @subpackage   transaction_helpers
* @author    Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
* 
* @todo: Rewrite this as a class!
*/

require_once("AwlQuery.php");

/**
* Create a new batch
* @param string $batchtype The type of batch - 'AUTO' or 'NORM'
* @param int $documentcount The number of documents in the batch
* @param float $total The total of the transactions in the batch
* @param string $description The description of the batch
*
* @return int The batchcode for the created batch
*/
function create_newbatch( $batchtype, $total, $description ) {
  global $session;
  $qry = new AwlQuery("SELECT nextval('newbatch_batchcode_seq')");
  dbg_error_log("ERROR","Getting new batch code.");
  if ( $qry->Exec("txn_helper: $description",__LINE__,__FILE__) && $qry->rows() == 1 && $row = $qry->Fetch() ) {
    $batchcode = $row->nextval;
  }

  $qry->Begin();
  $qry->QDo('INSERT INTO newbatch ( batchcode, personcode, batchtype, total, description ) '.
    			'VALUES(:batchcode, :session_user, :type, :total, :description)',
                array( ':batchcode'=>$batchcode, ':session_user'=>$session->user_no,
                       ':type'=>$batchtype, ':total'=>$total, ':description'=>$description) );

  return $batchcode;
}


/**
 * Update the details of the newbatch and commit it.
 * @param string $description
 */
function commit_newbatch( $description) {
  if ( $transactioncode > 1 ) {
    create_newtransaction( 'L', $companycode, $gst_washup, $period->max, $journal_total, $description );
    $sql = "UPDATE newbatch SET documentcount = (SELECT count(1) FROM newdocument d WHERE d.batchcode = newbatch.batchcode), total = (SELECT sum(amount) FROM newaccttrans t WHERE t.batchcode = newbatch.batchcode)  WHERE batchcode = :batchcode";
    $qry = new AwlQuery( $sql, array( ':batchcode' => $batchcode ) );
    if ( $qry->Exec("period_gst") && $qry->Commit() ) {
      $c->messages[] = "Batch $batchcode created: ".$description;
    }
    else {
      $c->messages[] = "Database transaction failure - please correct problem and retry.";
      $qry->Rollback();
    }
  }
  else {
    $qry = new AwlQuery();
    $qry->Rollback();
  }
  
}


/**
* Create a new document in this batch
* @param string $description The description of the document
* @param string $reference The reference for the document
* @param string $documenttype The type of document 'INVC' or 'VCHR' or 'CHEQ' or such
*/
function create_newdocument( $description, $reference, $documenttype ) {
  global $batchcode, $documentcode;

  $documentcode =  (isset($documentcode) ? $documentcode + 1 : 1);
  $transactioncode = 1;

  $sql = 'INSERT INTO newdocument ( batchcode, documentcode, description, reference, documenttype ) '.
  				'VALUES( :batchcode, :documentcode, :description, :reference, :documenttype )';
  $qry = new AwlQuery( $sql, array(':batchcode'=>$batchcode, ':documentcode'=> $documentcode, ':description'=>$description,
                                      ':reference' => $reference, ':documenttype' => $documenttype) );
  $qry->Exec("txn_helper: $reference",__LINE__,__FILE__);
}


/**
* Create a new transaction in this batch
* @param string $et The entitytype for the transaction to apply against
* @param string $ec The entitycode for the transaction to apply against
* @param string $ac The accountcode for the transaction to apply against
* @param date   $date The date of the transaction
* @param money  $amount The value of the transaction
* @param string $description The description for this transaction (default null means use document description)
* @param string $reference The reference for this transaction (default null means use document reference)
* @param int $consequenceof This transaction is a consequence of some other transaction (default null)
* @param int $monthcode The month to post the transaction to (default to null, which means the month will be set on batch update)
*/
function create_newtransaction( $et, $ec, $ac, $date, $amount, $description=null, $reference=null, $consequenceof=0, $monthcode=null ) {
  global $batchcode, $documentcode, $transactioncode;

  if ( !isset($transactioncode) ) $transactioncode = 1;

  $sql = 'INSERT INTO newaccttrans ( batchcode, documentcode, transactioncode, entitytype, entitycode, accountcode, date, description, amount, reference, consequenceof, monthcode) '.
  			'VALUES( :batchcode, :documentcode, :transactioncode, :et, :ec, :ac, :date, :description, :amount, :reference, :consequenceof, :monthcode )';
  $params = array(
      ':batchcode' => $batchcode,
      ':documentcode' => $documentcode, 
      ':transactioncode' => $transactioncode++, 
      ':et' => $et, 
      ':ec' => $ec, 
      ':ac' => $ac, 
      ':date' => $date, 
      ':description' => $description, 
      ':amount' => $amount, 
      ':reference' => $reference, 
      ':consequenceof' => $consequenceof, 
      ':monthcode' => $monthcode
  );
  $qry = new AwlQuery( $sql, $params );
  $qry->Exec("txn_helper: Batch $batchcode, Doc $documentcode", __LINE__,__FILE__);
}


class NewBatch {
  private $batchtype;
  private $total;
  private $description;
  private $batchcode;
  private $documentcode;
  private $transactioncode;

  private $doc_description;
  private $doc_reference;
  private $doc_type;
  
  /**
  * Prepare a new batch.  No transactions will be written until newTransaction() is called for the first time.
  * @param string $batchtype The type of batch - 'AUTO' or 'NORM'
  * @param int $documentcount The number of documents in the batch
  * @param float $total The total of the transactions in the batch
  * @param string $description The description of the batch
  *
  * @return int The batchcode for the created batch
  */
  function __construct( $batchtype, $total, $description ) {
    $this->batchtype = $batchtype;
    $this->total = $total;
    $this->description = $description;
    
    $this->batchcode = null;
    $this->documentcode = null;
    $this->transactioncode = null;

    $this->doc_description = null;
    $this->doc_reference = null;
    $this->doc_type = null; 
  }


  /**
   * Actually start the batch.
   */
  private function beginBatch() {
    if ( empty($this->batchtype) ) throw new Exception("Document must be initialised with a type before first transaction");
    global $session;
    $qry = new AwlQuery("SELECT nextval('newbatch_batchcode_seq')");
    dbg_error_log("ERROR","Getting new batch code.");
    if ( $qry->Exec("txn_helper: $description",__LINE__,__FILE__) && $qry->rows() == 1 && $row = $qry->Fetch() ) {
      $this->batchcode = $row->nextval;
    }
  
    $qry->Begin();
    $qry->QDo('INSERT INTO newbatch ( batchcode, personcode, batchtype, total, description ) '.
      			'VALUES(:batchcode, :session_user, :type, :total, :description)',
                  array( ':batchcode'=>$this->batchcode, ':session_user'=>$session->user_no,
                         ':type'=>$this->batchtype, ':total'=>$total, ':description'=>$this->description) );
  }

  /**
  * Update the details of the newbatch and commit it.
  * @param string $description
  */
  public function Commit() {
    if ( !empty($this->documentcode) ) {
      $sql = "UPDATE newbatch SET documentcount = (SELECT count(1) FROM newdocument d WHERE d.batchcode = newbatch.batchcode), total = (SELECT sum(amount) FROM newaccttrans t WHERE t.batchcode = newbatch.batchcode)  WHERE batchcode = :batchcode";
      $qry = new AwlQuery( $sql, array( ':batchcode' => $this->batchcode ) );
      if ( $qry->Exec("period_gst") && $qry->Commit() ) {
        $c->messages[] = "Batch ".$this->batchcode." created: ".$this->description;
      }
      else {
        $c->messages[] = "Database transaction failure - please correct problem and retry.";
        $qry->Rollback();
      }
    }
    else {
      $qry = new AwlQuery();
      $qry->Rollback();
    }
  
  }
  
  /**
  * Prepare to add a new document into this batch.  Nothing will be written to the batch until the
  * first newTransaction() call after this.
  * 
  * @param string $description The description of the document
  * @param string $reference The reference for the document
  * @param string $documenttype The type of document 'INVC' or 'VCHR' or 'CHEQ' or such
  */
  public function newDocument( $description, $reference, $documenttype ) {
    $this->documentcode = null;
    $this->doc_description = $description;
    $this->doc_reference = $reference;
    $this->doc_type = $documenttype; 
  }


  /**
   * Does the actual creation of the newdocument record.
   */
  private function beginDocument() {
    if ( empty($this->batchcode) ) $this->beginBatch();
    $this->documentcode = ( isset($this->documentcode) ? $this->documentcode + 1 : 1);    

    $this->transactioncode = 1;
    
    $sql = 'INSERT INTO newdocument ( batchcode, documentcode, description, reference, documenttype ) '.
    				'VALUES( :batchcode, :documentcode, :description, :reference, :documenttype )';
    $qry = new AwlQuery( $sql, array(':batchcode'=>$this->batchcode, ':documentcode'=> $this->documentcode,
                   ':description'=>$this->doc_description, ':reference' => $this->doc_reference, ':documenttype' => $this->doc_type) );
    $qry->Exec("txn_helper: $reference",__LINE__,__FILE__);
  }

  
  /**
  * Create a new transaction in this batch
  * @param string $et The entitytype for the transaction to apply against
  * @param string $ec The entitycode for the transaction to apply against
  * @param string $ac The accountcode for the transaction to apply against
  * @param date   $date The date of the transaction
  * @param money  $amount The value of the transaction
  * @param string $description The description for this transaction (default null means use document description)
  * @param string $reference The reference for this transaction (default null means use document reference)
  * @param int $consequenceof This transaction is a consequence of some other transaction (default null)
  * @param int $monthcode The month to post the transaction to (default to null, which means the month will be set on batch update)
  */
  public function newTransaction( $et, $ec, $ac, $date, $amount, $description=null, $reference=null, $consequenceof=0, $monthcode=null ) {
    if ( empty($this->doc_type) ) throw new Exception("Document must be initialised with a type before first transaction");
    if ( empty($this->documentcode) ) $this->beginDocument();
    
    if ( !isset($this->transactioncode) ) $this->transactioncode = 1;
  
    $sql = 'INSERT INTO newaccttrans ( batchcode, documentcode, transactioncode, entitytype, entitycode, accountcode, date, description, amount, reference, consequenceof, monthcode) '.
    			'VALUES( :batchcode, :documentcode, :transactioncode, :et, :ec, :ac, :date, :description, :amount, :reference, :consequenceof, :monthcode )';
    $params = array(
        ':batchcode' => $this->batchcode,
        ':documentcode' => $this->documentcode, 
        ':transactioncode' => $this->transactioncode++, 
        ':et' => $et, 
        ':ec' => $ec, 
        ':ac' => $ac, 
        ':date' => $date, 
        ':description' => $description, 
        ':amount' => $amount, 
        ':reference' => $reference, 
        ':consequenceof' => $consequenceof, 
        ':monthcode' => $monthcode
    );
    $qry = new AwlQuery( $sql, $params );
    $qry->Exec("txn_helper: Batch $this->batchcode, Doc $this->documentcode", __LINE__,__FILE__);
  }

  
  public function getCode() {
    return $this->batchcode;
  }
}