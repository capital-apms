<?php
include_once("classViewer.php");
include_once("classEditor.php");

param_to_global('id', 'int', 'batchcode');
// Viewer component for company records
$viewer = new Viewer("Unposted Batch");
$viewer->AddField( 'batchcode' );
$viewer->AddField( 'batchtype' );
$viewer->AddField( 'description' );
$viewer->AddField( 'documentcount' );
$viewer->SetJoins( 'newbatch' );

$viewer->SetWhere( 'batchcode='.$id );

$template = <<<EOTEMPLATE
<table>
 <tr>
  <th class="right">Batch:</th>
  <td class="center">##batchcode.value##</td>
  <th class="right">Type:</th>
  <td class="center">##batchtype.value##</td>
 </tr>
 <tr>
  <th class="right">Description:</th>
  <td class="left" colspan="3">##description.value##</td>
 </tr>
</table>

EOTEMPLATE;

$viewer->SetTemplate( $template );
$viewer->GetRecord();
$c->page_title = $viewer->Title("Unposted Batch: $id - ".$viewer->Record->{'description'});
$page_elements[] = $viewer;

$related_menu->AddOption("View Batch","/view.php?t=newbatch&id=$id","View this unposted batch of transactions.");
$related_menu->AddOption("Edit Batch","/edit.php?t=newbatch&id=$id","Edit this unposted batch of transactions.");
$related_menu->AddOption("Create Batch","/edit.php?t=newbatch","Create a new batch of transactions.");
if ( $viewer->Record->{'batchtype'} == 'ACCR' ) {
  $related_menu->AddOption("Copy Batch","/action.php?t=batch-copy&batchcode=$id","Copy this batch of accruals transactions");
}
else {
  $related_menu->AddOption("Update Batch","/action.php?t=batch-update&batchcode=$id","Update this batch of transactions");
}


// Now list income accounts
require_once('classBrowser.php');

$documentcount = $viewer->Record->{'documentcount'};
// $browsers[] = array();

$template = new Browser("Document");
$template->AddColumn( 'transactioncode', 'TxNo', 'right' );
// $template->AddColumn( 'account', 'Account', 'left', "entitytype || '-' || TO_CHAR(entitycode,'FM00009') || '-' || TO_CHAR(accountcode,'FM0009.00')" );
$template->AddColumn( 'entitytype', 'ET', 'center' );
$template->AddColumn( 'entitycode', 'Code', 'right' );
$template->AddColumn( 'accountcode', 'Account', 'right', '', "TO_CHAR(accountcode,'FM0009.00')" );
$template->AddColumn( 'date', 'Date', 'center', '', "TO_CHAR(date,'DD/MM/YYYY')" );
$template->AddColumn( 'reference', 'Reference', 'left', '<td class="left" style="width:8em;">%s</td>', "COALESCE(newaccttrans.reference, newdocument.reference)" );
$template->AddColumn( 'description', 'Description', 'left', '<td class="left" style="width:30em;">%s</td>', "COALESCE(newaccttrans.description, newdocument.description)" );
$template->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
$template->AddTotal( 'amount' );
$template->SetJoins( "newdocument LEFT JOIN newaccttrans USING ( batchcode, documentcode ) " );
$template->SetWhere( "newdocument.batchcode = $id" );
$template->AddOrder( 'transactioncode', 'ASC' );
// $rowurl = '/view.php?t=account&id=%s';
$template->RowFormat( "<tr class=\"r%d\">\n", "</tr>\n", '#even' );

// Now, based on the template lets add that for each document...
for ( $doc = 1; $doc <= $documentcount; $doc++ ) {
  $browser = clone($template);
  $browser->Title("Document $doc <a href=\"/edit.php?t=newdocument&id=${id}.${doc}\">Edit</a>");
  $browser->SetOrdering('transactioncode', 'ASC', $doc );
  $browser->AndWhere("newdocument.documentcode = $doc" );
  $page_elements[] = $browser;
}

