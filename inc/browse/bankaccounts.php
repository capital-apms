<?php
include_once("classWidgets.php");

/**
* Add some widgets onto the top of the browser
*/
$widget = new Widget();
$widget->AddField( 'active', 'log1' );
$widget->ReadWrite();
$widget->Defaults( array('active' => 't' ) );
$widget->Layout( '<table> <tr> <th>Status:</th> <td>##active.checkbox##</td> <td>##Show.submit##</td> </tr></table>' );
$page_elements[] = $widget;

$show_active = ($widget->Record->{'active'} == 't');

$browser = new Browser("Browse Bank Accounts");

$browser->AddColumn( 'bankaccountcode', 'Bank' );
$browser->AddColumn( 'accountname', 'Account Name' );
$browser->AddColumn( 'bankbranchname', 'Branch Name' );
$browser->AddColumn( 'bankname', 'Bank Name' );
$browser->AddColumn( 'bankaccount', 'Bank Account' );
$browser->AddColumn( 'companycode', 'Company', 'center' );
$browser->AddColumn( 'accountcode', 'Account' );
$browser->SetJoins( "bankaccount" );
if ( $show_active == 't' ) {
  $browser->AndWhere( "active" );
}
$browser->SetOrdering( 'bankaccountcode', 'DESC', 1 );
$rowurl = '/view.php?t=bankaccount&id=%s';
$browser->RowFormat( "<tr onclick=\"window.location='$rowurl';\" title=\"Click to Display Bank Account Detail\" class=\"r%d\">\n", "</tr>\n", 'bankaccountcode', '#even' );

$page_elements[] = $browser;

$related_menu->AddOption("New Bank Account","/edit.php?t=bankaccount","Create a new bank account", false, 3100);
