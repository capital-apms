<?php

// Editor component for unposted batches
$editor = new Editor("Unposted Document", 'newdocument');
$editor->AddField( 'orig_batchcode', 'batchcode' );
$editor->AddField( 'orig_documentcode', 'documentcode' );

param_to_global( 'id', '/\d+\.\d+/' );
param_to_global( 'batch', 'int', 'orig_batchcode', 'batchcode' );
param_to_global( 'doc', 'int', 'orig_documentcode', 'documentcode' );
if ( isset($id ) && (!isset($batch) || !isset($doc) || $batch < 1 || $doc < 1) ) {
  list ($batch,$doc) = explode('.',$id,2);
}
$editor->SetWhere( "batchcode=$batch AND documentcode=$doc" );

// Editor component for invoice line
$editrow = new Editor("Transaction", 'newaccttrans');
$editrow->AddField( 'txn_batchcode', 'newaccttrans.batchcode' );
$editrow->AddField( 'txn_documentcode', 'newaccttrans.documentcode' );
$editrow->AddField( 'etname', 'get_entity_type_name(entitytype)' );
$editrow->AddField( 'my_transactioncode', 'transactioncode' );
$editrow->SetSubmitName( 'saverow' );
if ( $editor->IsSubmit() ) {
  $_POST['batchcode'] = $batch;
  $_POST['documentcode'] = $doc;
  $editor->WhereNewRecord( "batchcode=$batch AND documentcode=$doc" );
  $editor->Write();

  $sql = "UPDATE newbatch SET documentcount = (SELECT count(1) FROM newdocument WHERE newdocument.batchcode = newbatch.batchcode) WHERE batchcode = ?";
  $qry = new PgQuery($sql, $batch);
  $qry->Exec('edit/newdocument');
}
else if ( isset($batch) && isset($doc) ) {
  $editor->GetRecord();
}

if ( $editrow->IsSubmit() ) {
  $transactioncode = null;
  error_log("submitting");
  $_POST['batchcode'] = $batch;
  $_POST['documentcode'] = $doc;
  if ( ! $editrow->IsUpdate() ) {
    error_log("Not updating");
    $qry = new PgQuery("SELECT transactioncode FROM newaccttrans WHERE batchcode = ? AND documentcode = ? ORDER BY transactioncode DESC LIMIT 1", $batch, $doc );
    if ( $qry->Exec("newbatch") ) {
      if ( $qry->rows == 1 ) {
        $row = $qry->Fetch();
        $transactioncode = $row->transactioncode + 1;
      }
      else {
        $transactioncode = 1;
      }
      $_POST['transactioncode'] = $transactioncode;
    }
    else {
      $c->messages[] = "Database Error!";
    }
  }
  else {
    $transactioncode = $_POST['transactioncode'];
  }
  if ( isset($transactioncode) ) {
    $editrow->SetWhere( "batchcode=$batch AND documentcode=$doc AND transactioncode=$transactioncode");
    $editrow->WhereNewRecord( "batchcode=$batch AND documentcode=$doc AND transactioncode=$transactioncode" );

    if ( $_POST['description'] == '' || $_POST['description'] == $editor->Value('description') ) {
      $_POST['description'] = null;
    }
    if ( $_POST['reference'] == '' || $_POST['reference'] == $editor->Value('reference') ) {
      $_POST['reference'] = null;
    }
    $editrow->Write();

    unset($_GET['editseq']);
  }
}

$submittype = 'Update';
if ( ! $editor->Available() ) {
  $submittype = 'Create';
  $editor->Initialise( array('documenttype' => 'JRNL', 'reference' => '', 'description' => '' ) );
}

$editor->SetLookup( 'documenttype', "SELECT documenttype, documenttype || ' - ' || description FROM documenttype ORDER BY 1;");

$template = <<<EOTEMPLATE
<form method="POST" enctype="multipart/form-data" id="entry">
<table>
 <tr>
  <th class="right">Unposted Document:</th>
  <td class="center">##orig_batchcode.value####orig_batchcode.hidden## - ##orig_documentcode.value####orig_documentcode.hidden##</td>
  <th class="right">Type:</th>
  <td class="center">##documenttype.select##</td>
 </tr>
 <tr>
  <th class="right">Reference:</th>
  <td class="left" colspan="3">##reference.input.12##</td>
 </tr>
 <tr>
  <th class="right">Description:</th>
  <td class="left" colspan="3">##description.input.50##</td>
 </tr>
 <tr>
  <th class="right"></th>
  <td class="left" colspan="3">##submit##</td>
 </tr>
</table>
</form>

EOTEMPLATE;

$editor->SetTemplate( $template );
$c->page_title = $editor->Title("Unposted Document: $batch - $doc - ".$editor->Record->{'description'});
$documentcount = $editor->Record->{'documentcount'};
$page_elements[] = $editor;

$related_menu->AddOption("View Batch","/view.php?t=newbatch&id=$batch","View this unposted batch of transactions.");
$related_menu->AddOption("Edit Batch","/edit.php?t=newbatch&id=$batch","Edit this unposted batch of transactions.");
$related_menu->AddOption("Create Batch","/edit.php?t=newbatch","Create a new batch of transactions.");
if ( $editor->Record->{'batchtype'} == 'ACCR' ) {
  $related_menu->AddOption("Copy Batch","/action.php?t=batch-copy&batchcode=$batch","Copy this batch of accruals transactions");
}
else {
  $related_menu->AddOption("Update Batch","/action.php?t=batch-update&batchcode=$batch","Update this batch of transactions");
}


// Now list income accounts
require_once('classBrowser.php');

if ( $submittype == 'Update' ) {

  function edit_row( $row_data ) {
    global $editrow, $batch, $doc;

    $rowsubmittype = ($row_data->transactioncode == -1 ? 'Add' : 'Save');
    $form_url = preg_replace( '#&(edit|del)seq=\d+#', '', $_SERVER['REQUEST_URI'] );

    $template = <<<EOTEMPLATE
<form method="POST" enctype="multipart/form-data" id="rowentry" action="$form_url">
  <td class="left">##transactioncode.value####transactioncode.hidden##<input type="hidden" name="id" value="${batch}.${doc}"></td>
  <td class="left" style="white-space:nowrap">##entitytype.input.1## ##entitycode.input.5## ##accountcode.input.7##</td>
  <td class="left">##date.date##</td>
  <td class="left">##reference.input.8##</td>
  <td class="left">##description.input.25##</td>
  <td class="left">##amount.money.8##</td>
  <td class="left">##$rowsubmittype.submit##</td>
</form>

EOTEMPLATE;

    $editrow->SetTemplate( $template );
    if ( $row_data->transactioncode == -1 )
      $editrow->Initialise( (array) $row_data );
    else
      $editrow->SetRecord( $row_data );

    $editrow->Title("");
    return $editrow->Render();
  }

  // And the invoice line details
  require_once('classBrowser.php');
  $browser = new Browser("Transaction Detail");
  $browser->AddHidden( 'etname', 'lower(get_entity_type_name(entitytype))' );
  $browser->AddColumn( 'transactioncode', '#', 'right' );
  $browser->AddHidden( 'entitytype' );
  $browser->AddHidden( 'entitycode' );
  $browser->AddHidden( 'entityname', 'get_entity_name(entitytype,entitycode)' );
  $browser->AddHidden( 'accountcode' );
//  $browser->AddHidden( 'account', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')" );
  $browser->AddHidden( 'accountname', 'chartofaccount.name');
  $browser->AddColumn( 'account', 'Account', 'left', '', "entitytype||'-'||TO_CHAR(entitycode,'FM00009')||'-'||TO_CHAR(accountcode,'FM0009.00')");
  $browser->AddColumn( 'date', 'Date', 'center' );
  $browser->AddColumn( 'reference', 'Reference', 'left' );
  $browser->AddColumn( 'description', 'Description', 'left' );
  $browser->AddColumn( 'amount', 'Amount', 'right', '%0.2lf' );
  $edit_link = "<a href=\"/edit.php?t=newdocument&id=${batch}.${doc}&editseq=##transactioncode##\" class=\"submit\">Edit</a>";
  $del_link  = "<a href=\"/edit.php?t=newdocument&id=${batch}.${doc}&delseq=##transactioncode##\" class=\"submit\">Del</a>";
  $browser->AddColumn( 'action', 'Action', 'center', '', "'$edit_link&nbsp;$del_link'" );
  $browser->AddOrder( 'transactioncode', 'ASC' );
  $browser->SetJoins( "newaccttrans LEFT OUTER JOIN chartofaccount USING ( accountcode )" );

  $browser->AndWhere( "batchcode=$batch AND documentcode=$doc" );
  $rowurl = '/view.php?t=account&id=%s';
  $browser->RowFormat( "<tr class=\"r%d\" title=\"%s: %s - %s\">\n", "</tr>\n", '#even', 'entitytype', 'entityname', 'accountname' );
  $page_elements[] = $browser;

  if ( isset($_GET['delseq']) ) {
    $qry = new PgQuery("DELETE FROM newaccttrans WHERE batchcode=? AND documentcode=? AND transactioncode = ?", $batch, $doc, intval($_GET['delseq']) );
    $qry->Exec('newbatch');
  }
  if ( isset($_GET['editseq']) ) {
    $browser->MatchedRow('transactioncode', intval($_GET['editseq']), 'edit_row');
  }
  else {
    $remaining = $editor->Record->{'total'};
    $extra_row = array( 'transactioncode' => -1, 'percent' => 100, 'amount' => $remaining, 'yourshare' => $remaining );
    $qry = new PgQuery("SELECT entitytype, entitycode, accountcode, date, reference, description FROM newaccttrans WHERE batchcode=$batch AND documentcode=$doc ORDER BY transactioncode DESC LIMIT 1");
    if ( $qry->Exec('newbatch') && $qry->rows == 1 ) {
      $row = $qry->Fetch();
      $extra_row['entitytype']  = $row->entitytype;
      $extra_row['entitycode']  = $row->entitycode;
      $extra_row['accountcode'] = $row->accountcode;
      $extra_row['date']        = $row->date;
      $extra_row['reference']   = $row->reference;
      $extra_row['description'] = $row->description;
    }
    else {
      $extra_row['entitytype']  = 'L';
      $extra_row['entitycode']  = 1;
      $extra_row['accountcode'] = '';
    }
    $browser->MatchedRow('transactioncode', -1, 'edit_row');
    $extra_row = (object) $extra_row;
    $browser->AddRow($extra_row);
  }

}
