package APMS::View::Widget::Combo;

use Glib qw/ TRUE FALSE /;
use strict;

=head1 APMS::View::Widget::Combo

Generic combo widget

=cut

# Provide a map between data types, and Gtk2::ComboBox attributes.
my %attrmap = (
	'Glib::Boolean' => 'active',
	'Glib::Uint'    => 'text',
	'Glib::String'  => 'text',
);

=item combo_select

=cut

sub combo_select {
	my ( $self, %def ) = @_;

	# Columns for the list store
	my @columns;
	map { push @columns, $_->{column} } @{ $def{fields} };

	my @columntypes;
	map { push @columntypes, $_->{type} } @{ $def{fields} };

	my $store = Gtk2::ListStore->new( @columntypes );

	# Shove the data into the model
	foreach my $row ( @{ $def{data} } ) {
		my @data;
		my $count = 0;
		foreach my $column ( @columns ) {
			push @data, ( $count, $row->{$column} );
			$count++;
		}

		# Append row to store, and fill the row with data
		my $iter = $store->append;
		$store->set( $iter, @data );
	}

	# Create a new combobox
	my $combobox = Gtk2::ComboBox->new( $store );

	# Add the field widgets to the view
	my $count = 0;
	foreach my $field ( @{ $def{fields} } ) {

		# Create the field widget
		my $class = $field->{widget};
		my $renderer = $class->new;

		$combobox->pack_start( $renderer, TRUE );
		$combobox->add_attribute(
			$renderer,
			$attrmap{"$field->{type}"} => $count
		);

		$count++;
	}

	# Return the completed object
	return $combobox;
}

1;

=head2 Licence

Copyright (C) 2007 by Chris Eade

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Library General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

=cut

